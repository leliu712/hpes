import sys, re, os

infilename = "postNHCdata.txt"
infile = open(infilename)
outfilename = "post2NHCdata.txt"
outfile = open(outfilename, "w")

pretime = 0
prelon = 0
prelat = 0
outlines = ""
for line in infile:
 items = re.split(" ", line)
 time = float(items[0])
 lon = float(items[1])
 lat = float(items[2])
 
 if(time > 0 and time <= 120):
  newtime = (time + pretime) * 0.5
  newlon  = (lon + prelon) * 0.5
  newlat  = (lat + prelat) * 0.5
  newline = str(newtime) + " " + str(newlon) + " " + str(newlat) + "\n"
  outlines = outlines + newline
 
 newline = str(time) + " " + str(lon) + " " + str(lat) + "\n"
 outlines = outlines + newline
 
 pretime = time
 prelon = lon
 prelat = lat

outfile.write(outlines)
