#ifndef __KD_TREE__H
#define __KD_TREE__H
#include "KDTree.h"

// format raw input data into Input type
//template<typename T>
//Input* KDTree::formatInput(T rawdata, int size, int dim)
////Input* KDTree::formatInput(std::vector<Vector2d> rawdata, int size, int dim)
//{
// Point** points = new Point*[size];
// for(int i = 0; i < size; i++)
// {
//  points[i] = new Point;
//  points[i]->coord = new float[dim];
//  points[i]->k = dim;
//  points[i]->index = i;
//  for(int j = 0; j < dim; j++)
//   points[i]->coord[j] = rawdata[i][j];
// }
// inputdata = new Input;
// inputdata->size = size;
// inputdata->points = points;
// //return testData(2, 9);
// return inputdata;
//}

// contruct a KDTree
Node* KDTree::buildKDTree(Input* input)
{
 root = NULL;
 for(int i = 0; i < input->size; i++)
 {
  insertKDTree(&root, input->points[i]);
 }
 return root;
}

void KDTree::insertKDTreeUtil(Node** root, Node* newNode, unsigned depth)
{
 if(!*root)
 {
  *root = newNode;
  return;
 }

 unsigned axisofComparison = depth % (newNode->point).k;
 newNode->depth = depth+1;

 if((newNode->point).coord[axisofComparison] < 
   ((*root)->point).coord[axisofComparison])
  insertKDTreeUtil(&((*root)->left), newNode, depth+1);
 else
  insertKDTreeUtil(&((*root)->right), newNode, depth+1);

}

void KDTree::insertKDTree(Node* *root, Point* point)
{
 unsigned zeroDepth = 0;
 Node* newNode = createNode(point, zeroDepth);
 insertKDTreeUtil(root, newNode, zeroDepth);
}

Point* KDTree::createPoint(unsigned k)
{
 Point* point = new Point;
 point->k = k;
 point->coord = new float[k];
 point->index = -1;
 return point;
}

Node* KDTree::createNode(Point* point, int depth)
{
  Node* toReturn = new Node;
  toReturn->point.k = point->k;
  toReturn->point.coord = new float[point->k];
  toReturn->point.index = point->index;
  for(int i = 0; i < point->k; i++)
   toReturn->point.coord[i] = point->coord[i];
  toReturn->left = toReturn->right = NULL;
  toReturn->depth = depth;
  return toReturn;
}

Input* KDTree::createInput(unsigned k, unsigned size)
{
 Input* input = new Input;
 input->size = size;
 input->points = new Point*[size];
 return input;
}

Input* KDTree::testData(unsigned k, unsigned size)
{
 Input* input = createInput(k, size);
 std::cout << "test datta: " << std::endl;
 for(unsigned i = 0; i < size; i++)
 {
  input->points[i] = createPoint(k);
  for(unsigned j = 0; j < k; j++)
   input->points[i]->coord[j] = rand() % 20;
  printPoints(input->points[i]->coord, k);
 }
 cout << "-----" << endl;
 cout << "finish creating testing data" << endl;
 return input;
}

void KDTree::printPoints(float* points, unsigned size)
{
 for(int i = 0; i < size; i++)
  cout << points[i] << " ";
 cout << endl;
}

// inorder tree traversal
void KDTree::inorderKDTree(Node* root)
{
 if(root)
 {
  inorderKDTree(root->left);
  //std::cout << root->depth << std::endl;
  //printPoints((root->point).coord, (root->point).k);
  inorderKDTree(root->right);
 }
}

int KDTree::checkDuplication(Point p0, Point p1)
{
 if(p0.k != p1.k)
  return 0;

 for(int i = 0; i < p0.k; i++)
 {
  if(p0.coord[i] != p1.coord[i])
   return 0;
 }
 
 return 1;
}

int KDTree::searchKDTreeUtil(Node* root, Point point, unsigned depth)
{
 if(!root)
  return 0;

 if(checkDuplication(root->point, point))
  return 1;

 unsigned axisofComparison = depth % point.k;

 if((point.coord[axisofComparison]) <
  (root->point).coord[axisofComparison])
  return searchKDTreeUtil(root->left, point, depth + 1);
 
 return searchKDTreeUtil(root->right, point, depth + 1);
}

int KDTree::searchKDTree(Node* root, Point point)
{
 unsigned zeroDepth = 0;
 return searchKDTreeUtil(root, point, zeroDepth);
}

float KDTree::computeSquaredDistance(Point* point, Node* node)
{
 int k = point->k;
 Point tmp = node->point;
 float dis(0);
 for(int i = 0; i < k; i++)
 {
  dis += (point->coord[i] - node->point.coord[i]) * (point->coord[i] - node->point.coord[i]);
 }
 return dis;
}

float KDTree::computeDistanceFromAxis(Point* point, Node* node)
{
 int depth = node->depth;
 int axis = depth % node->point.k;
 return point->coord[axis] - node->point.coord[axis];
}

void KDTree::knearestUtil(Point* query, Node* currentNode, int K, 
  bounded_priority_queue<QueueTuple>& bpq)
{
 if(!currentNode)
  return;

 float dis = computeSquaredDistance(query, currentNode);
 float disAxis = computeDistanceFromAxis(query, currentNode);
 if(bpq.size() < K || dis <= bpq.queue_.top().first)
 {
  Point* point = createPoint(currentNode->point.k);
  point->index = currentNode->point.index;
  for(int i = 0; i < point->k; i++)
   point->coord[i] = currentNode->point.coord[i];
  bpq.push(QueueTuple(dis, point));
 }

 knearestUtil(query, disAxis <= 0 ? currentNode->left : currentNode->right, K, bpq);
 if((disAxis * disAxis) > bpq.queue_.top().first)
  return;
 knearestUtil(query, disAxis <= 0 ? currentNode->right : currentNode->left, K, bpq);
}

static int testcounter = 0;
bounded_priority_queue<QueueTuple>
KDTree::knearest(Point* query, int K){
 bounded_priority_queue<QueueTuple> bpq(K);

 if(!root || K < 1)
  throw std::runtime_error("root is NULL or K < 1");

 knearestUtil(query, root, K, bpq);

 return bpq;
}


#endif















