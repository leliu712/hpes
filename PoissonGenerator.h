/**
 * Poisson Disk Points Generator
 */
// Fast Poisson Disk Sampling in Arbitrary Dimensions
// http://people.cs.ubc.ca/~rbridson/docs/bridson-siggraph07-poissondisk.pdf
#ifndef __POISSONGENERATOR_H__
#define __POISSONGENERATOR_H__
#include <vector>
#include <random>
#include <stdint.h>
#include <time.h>
#include <iostream>
#include <omp.h>

#include "CDFSample.h"


class DefaultPRNG
{
 public:
  DefaultPRNG()
   : m_Gen( std::random_device()() )
     , m_Dis( 0.0f, 1.0f )
 {
  // prepare PRNG
  m_Gen.seed( time( nullptr ) );
 }

  explicit DefaultPRNG( uint32_t seed )
   : m_Gen( seed )
     , m_Dis( 0.0f, 1.0f )
 {
 }

  float RandomFloat()
  {
   return static_cast<float>( m_Dis( m_Gen ) );
  }

  int RandomInt( int Max )
  {
   std::uniform_int_distribution<> DisInt( 0, Max );
   return DisInt( m_Gen );
  }

 private:
  std::mt19937 m_Gen;
  std::uniform_real_distribution<float> m_Dis;
};

struct sPoint
{
 sPoint()
  : x( 0 )
    , y( 0 )
    , m_Valid( false )
 {}
 sPoint( float X, float Y )
  : x( X )
    , y( Y )
    , m_Valid( true )
 {}
 float x;
 float y;
 bool m_Valid;
 //
 bool IsInRectangle() const
 {
  return x >= 0 && y >= 0 && x <= 1 && y <= 1;
 }
 //
 bool IsInCircle() const
 {
  float fx = x - 0.5f;
  float fy = y - 0.5f;
  return ( fx*fx + fy*fy ) <= 0.25f;
 }
};

struct sGridPoint
{
 sGridPoint(): x(0), y(0), IsEmpty(false) {}
 sGridPoint( int X, int Y ) : x( X ), y( Y ), IsEmpty(true) {}
 int x;
 int y;
 bool IsEmpty;
};

static float GetDistance( const sPoint& P1, const sPoint& P2 )
{
 return std::sqrt( ( P1.x - P2.x ) * ( P1.x - P2.x ) + ( P1.y - P2.y ) * ( P1.y - P2.y ) );
}

static sGridPoint ImageToGrid( const sPoint& P, float CellSize )
{
 return sGridPoint( ( int )( P.x / CellSize ), ( int )( P.y / CellSize ) );
}

struct sGrid
{
 sGrid( int W, int H, float CellSize )
  : m_W( W )
    , m_H( H )
    , m_CellSize( CellSize )
 {
  m_Grid.resize( m_H );
  for ( auto i = m_Grid.begin(); i != m_Grid.end(); i++ ) { i->resize( m_W ); }

  m_VisitedGridCells.resize(m_H);
  for ( auto i = m_VisitedGridCells.begin(); i != m_VisitedGridCells.end(); i++ ) { i->resize( m_W ); }

 }
 void Insert( const sPoint& P )
 {
  sGridPoint G = ImageToGrid( P, m_CellSize );
  m_Grid[ G.x ][ G.y ] = P;
  m_VisitedGridCells[G.x][G.y] = 1; // set this cell as non-empty
 }
 bool IsInNeighbourhood( sPoint Point, float MinDist, float CellSize )
 {
  sGridPoint G = ImageToGrid( Point, CellSize );

  // number of adjucent cells to look for neighbour points
  const int D = 5;

  // scan the neighbourhood of the point in the grid
  for ( int i = G.x - D; i < G.x + D; i++ )
  {
   for ( int j = G.y - D; j < G.y + D; j++ )
   {
    if ( i >= 0 && i < m_W && j >= 0 && j < m_H )
    {
     sPoint P = m_Grid[ i ][ j ];

     if ( P.m_Valid && GetDistance( P, Point ) < MinDist ) { return true; }
    }
   }
  }
  return false;
 }

 // Finds the next empty grid cell point 
 sGridPoint NextEmptyGridCell(){
  sGridPoint emptyCell;
  for(int i = 0; i < m_VisitedGridCells.size(); i++){
   for(int j = 0; j < m_VisitedGridCells[i].size(); j++){
    if(m_VisitedGridCells[i][j] == 0){
     m_VisitedGridCells[i][j] = 1;
     emptyCell = sGridPoint(i, j);
     return emptyCell;  // find an empty cell
    }
   }
  }
  return emptyCell; // all cells are non-empty
 }
 // Finds the center of the next grid cell point
 sPoint NextEmptyGridCellCenter(){
  sGridPoint emptycell = NextEmptyGridCell();
  if(!emptycell.IsEmpty){
   sPoint not_valid_point;
   return not_valid_point;
  }
  float x = m_CellSize * (emptycell.x + 0.5f);
  float y = m_CellSize * (emptycell.y + 0.5f);
  return sPoint(x, y);
 }

 void DrawGrid(){
  glPointSize(3);
  glColor4f(1, 0, 0, 1);
  glBegin(GL_POINTS);
  for(int i = 0; i < m_W; i++){
   for(int j = 0; j < m_H; j++){
    float x = m_CellSize * (i + 0.5);
    float y = m_CellSize * (j + 0.5);
    glVertex3f(x * 400, y * 400, 0);
    cout << x << " " << y << endl;
   }
  }
  glEnd();
 }

 private:
 int m_W;
 int m_H;
 float m_CellSize;

 std::vector<std::vector<sPoint>> m_Grid;
 std::vector<std::vector<int>> m_VisitedGridCells;
};

#define PRNG DefaultPRNG

static sPoint PopRandom( std::vector<sPoint>& Points, PRNG& Generator )
{
 const int Idx = Generator.RandomInt( Points.size()-1 );
 const sPoint P = Points[ Idx ];
 Points.erase( Points.begin() + Idx );
 return P;
}

static sPoint GenerateRandomPointAround( const sPoint& P, float MinDist, PRNG& Generator )
{
 // start with non-uniform distribution
 float R1 = Generator.RandomFloat();
 float R2 = Generator.RandomFloat();

 // radius should be between MinDist and 2 * MinDist
 float Radius = MinDist * ( R1 + 1.0f );

 // random angle
 float Angle = 2 * 3.141592653589f * R2;

 // the new point is generated around the point (x, y)
 float X = P.x + Radius * cos( Angle );
 float Y = P.y + Radius * sin( Angle );

 return sPoint( X, Y );
}


struct ProjectMap{                                                                                                   
 std::vector<Vector2d> ODGrid;
 std::vector<Vector2d> UDGrid;
 int nrows, ncols;
 CDFSample cdfsample;
 Vector2d InvMap(const Vector2d input){
  return cdfsample.invmap(input, UDGrid, ODGrid);
 }
 // Returns true if input is inside of the UDGrid, otherwise returns false
 bool InvMap(const Vector2d& input, Vector2d& output){
  int inside(0);
  output = cdfsample.invmap(input, UDGrid, ODGrid, inside);
  if(inside == 0){
   return true;
  }
  else{
   return false;
  }
 }
 // Forward map from OD to UD space
 Vector2d ForwardMap(const Vector2d& input, 
   const std::vector<std::vector<float>>& U, const std::vector<std::vector<float>>& V){
  return cdfsample.forwardmap(input, UDGrid, ODGrid, nrows, ncols, U, V);
 }

 Vector2d CenterOfUDGrid(){
  return UDGrid.front() + .5 * (UDGrid.back() - UDGrid.front());
 }
 Vector2d CenterOfODGrid(){
  return cdfsample.invmap(CenterOfUDGrid(), UDGrid, ODGrid);
 }
 int Idx(const int row, const int col){
  return ncols * row + col;
 }
 // Computes the area of UD grid
 float UDGridArea(){
  float totalArea(0);
  for(int i = 0; i < nrows - 1; i++){
   for(int j = 0; j < ncols - 1; j++){
    int idx[4];
    idx[0] = Idx(i, j);
    idx[1] = Idx(i, j+1);
    idx[2] = Idx(i+1, j+1);
    idx[3] = Idx(i+1, j);
    Vector2d v[4] = {UDGrid[idx[0]], UDGrid[idx[1]], UDGrid[idx[2]], UDGrid[idx[3]]};
    Vector2d e01 = v[1] - v[0];
    Vector2d e02 = v[2] - v[0];
    Vector2d e03 = v[3] - v[0];
    float area012 = (e01 % e02).norm() * 0.5;
    float area023 = (e02 % e03).norm() * 0.5;
    float area = area012 + area023;
    totalArea += area;
    //DrawUDGridCell(v, 4);
   }
  }
  return totalArea;
 }
 // Debug function
 void DrawODGrid(){
  for(int i = 0; i < nrows - 1; i++){
   for(int j = 0; j < ncols - 1; j++){
    int idx[4];
    idx[0] = Idx(i, j);
    idx[1] = Idx(i, j+1);
    idx[2] = Idx(i+1, j+1);
    idx[3] = Idx(i+1, j);
    Vector2d v[4] = {ODGrid[idx[0]], ODGrid[idx[1]], ODGrid[idx[2]], ODGrid[idx[3]]};
    DrawODGridCell(v, 4);
   }
  }
 }
 void DrawODGridCell(const Vector2d v[], const int size){
  glColor4f(1, 0, 0, 1);
  glBegin(GL_LINE_LOOP);
  for(int i = 0; i < size; i++){
   glVertex3f(v[i].x, v[i].y , 0);
  }
  glEnd();
 }
 void DrawUDGrid(){
  for(int i = 0; i < nrows - 1; i++){
   for(int j = 0; j < ncols - 1; j++){
    int idx[4];
    idx[0] = Idx(i, j);
    idx[1] = Idx(i, j+1);
    idx[2] = Idx(i+1, j+1);
    idx[3] = Idx(i+1, j);
    Vector2d v[4] = {UDGrid[idx[0]], UDGrid[idx[1]], UDGrid[idx[2]], UDGrid[idx[3]]};
    DrawUDGridCell(v, 4);
   }
  }
 }
 void DrawUDGridCell(const Vector2d v[], const int size){
  glColor4f(1, 0, 0, 1);
  glBegin(GL_LINE_LOOP);
  for(int i = 0; i < size; i++){
   glVertex3f((v[i].x + 0.5) * 400, (v[i].y + 0.5) * 400, 0);
  }
  glEnd();
 }
 void DrawUDGridPoints(){
  glColor4f(1, 0, 0, 1);
  glBegin(GL_POINTS);
  for(auto v : UDGrid){
   glVertex3f((v.x + 0.5) * 800, (v.y + 0.5) * 800, 0);
  }
  glEnd();
 }
};

/**
  Return a vector of generated points

  MaxNewPoints - refer to bridson-siggraph07-poissondisk.pdf for details (the value 'k')
  Circle  - 'true' to fill a circle, 'false' to fill a rectangle
  MinDist - minimal distance estimator, use negative value for default
 **/
class PoissonPointsGenerator{
 public:
  PoissonPointsGenerator(const int _NumPoints, const unsigned _TimeSeed): 
   NumPoints(_NumPoints),
   TimeSeed(_TimeSeed){
    MinDist = -1.0f;
    CellSize = 0.0f;
    SetMinDis();
    MaxNewPoints = 30;
    NewPointsCount = 0;
    SamplePoints.clear();
    ProcessList.clear();
    CurReferencePoint = sPoint(0, 0);
    cout << TimeSeed << endl;
    Generator = DefaultPRNG(TimeSeed);

   }
  ~PoissonPointsGenerator(){}

  void SetMinDis(const float _minDist){
   MinDist = _minDist;
   // update Grid
   SetGrid();
  }

  void SetMinDis(){
   MinDist = sqrt( float(NumPoints) ) / float(NumPoints) * 0.2;
   //MinDist = 1.0 / sqrt(float(NumPoints)) * 0.5 * sqrt(2.0);
   SetGrid();
  }

  void SetGrid(){
   //CellSize = MinDist / sqrt(2.0f);
   CellSize = MinDist / sqrt(NumPoints);
   GridW = (int)ceil(1.0f / CellSize);
   GridH = (int)ceil(1.0f / CellSize);
   Grid = new sGrid(GridW, GridH, CellSize);
   //Grid->DrawGrid();
  }

  // Checks whether a point in side of the UD space
  bool IsInside(ProjectMap& map, sPoint input){
   // Needs to translate points to [-0.5, 0.5]
   Vector2d v(input.x - 0.5, input.y - 0.5);
   Vector2d vProjection(0, 0);
   bool inside = map.InvMap(v, vProjection);
   return inside;
  }

  void UpdateContainer(const sPoint& point){
   ProcessList.push_back(point);
   SamplePoints.push_back(point);
   Grid->Insert(point);
  }

  // The first point has been pre-determined
  void PushFirstPoint(const float x, const float y){
   sPoint FirstPoint(x + 0.5, y + 0.5);
   UpdateContainer(FirstPoint);
  }

  // Randomly generated the first point
  void GenerateFirstPoint(ProjectMap& map){
   sPoint FirstPoint(0.5, 0.5);
   do{
    FirstPoint = sPoint(Generator.RandomFloat(), Generator.RandomFloat());	     
   } while(!IsInside(map, FirstPoint));
   UpdateContainer(FirstPoint);
  }

  // Generates poisson points
  std::vector<sPoint> GeneratePoissonPoints(bool Circle = true){
   MinDist = sqrt(float(NumPoints)) / float(NumPoints);
   MinDist = 1.0 / sqrt(float(NumPoints)) * 0.5 * sqrt(2.0);
   SamplePoints.clear();
   ProcessList.clear();

   // create the grid
   CellSize = MinDist / sqrt( 2.0f );

   GridW = ( int )ceil( 1.0f / CellSize );
   GridH = ( int )ceil( 1.0f / CellSize );

   sGrid newGrid( GridW, GridH, CellSize );

   sPoint FirstPoint;
   do {
    FirstPoint = sPoint( Generator.RandomFloat(), Generator.RandomFloat() );	     
   } while (!(Circle ? FirstPoint.IsInCircle() : FirstPoint.IsInRectangle()));

   // update containers
   ProcessList.push_back( FirstPoint );
   SamplePoints.push_back( FirstPoint );
   newGrid.Insert( FirstPoint );

   // generate new points for each point in the queue
   while ( !ProcessList.empty() && SamplePoints.size() < NumPoints )
   {
    cout << "here" << endl;
    sPoint Point = PopRandom( ProcessList, Generator );

    for ( int i = 0; i < MaxNewPoints; i++ )
    {
     sPoint NewPoint = GenerateRandomPointAround( Point, MinDist, Generator );

     bool Fits = Circle ? NewPoint.IsInCircle() : NewPoint.IsInRectangle();

     if ( Fits && !newGrid.IsInNeighbourhood( NewPoint, MinDist, CellSize ) )
     {
      ProcessList.push_back( NewPoint );
      SamplePoints.push_back( NewPoint );
      newGrid.Insert( NewPoint );
      continue;
     }
    }
   }

   return SamplePoints;
  }

  // Generates a new point
  sPoint GeneratePoint(ProjectMap& map){
   sPoint toReturn(0.5, 0.5);
   // Adds the first point
   if(SamplePoints.empty()){
    // The first point, which is the point at the second time step, 
    // is selected randomly
    // Uses the inv map to check whether the new point is in the UDSpace
    toReturn = sPoint(Generator.RandomFloat(), Generator.RandomFloat());	     
    while(!IsInside(map, toReturn)){
     toReturn = sPoint(Generator.RandomFloat(), Generator.RandomFloat());	     
    }
    UpdateContainer(toReturn);
    cout << "\tFrist point " << toReturn.x << " " << toReturn.y << endl;
   } else {
    cout << "\tOther points: " << endl;
    bool Found = false;
    while(Found == false){
     if(ProcessList.empty() && NewPointsCount == 0){
      // If there is no points in ProcessList, we find the next empty grid cell and push the center of this cell into the ProcessList
      cout << "Finding the next empty cell ..." << endl;
      bool AllVisited = false;
      sPoint nextEmptyCellCenter;
      do{
       nextEmptyCellCenter= Grid->NextEmptyGridCellCenter();
       if(!nextEmptyCellCenter.m_Valid){
        cout << "\t\tBreak! All grid cells have been visited and no valid points are found" << endl;
        AllVisited = true;
       }
      } while(!IsInside(map, nextEmptyCellCenter) && !AllVisited);
      
      if(AllVisited){
       break;
      } else {
       UpdateContainer(nextEmptyCellCenter);
      }
     }
     // Randomly picks a point from ProcessList if needed
     if(!ProcessList.empty() && NewPointsCount == 0){
      CurReferencePoint = PopRandom(ProcessList, Generator);
     }

     // Generates a new point around reference point point
     if(NewPointsCount < MaxNewPoints){
      sPoint NewPoint = GenerateRandomPointAround(CurReferencePoint, MinDist, Generator);
      // A valid point should be inside the UD space, and satisfies the poisson disk property
      if(IsInside(map, NewPoint) && !Grid->IsInNeighbourhood(NewPoint, MinDist, CellSize)){
       //if(!Grid->IsInNeighbourhood(NewPoint, MinDist, CellSize)){
       toReturn = sPoint(NewPoint.x, NewPoint.y);
       Found = true;
       UpdateContainer(toReturn);
      }
      cout << "\t\t" << NewPointsCount << " " << Found << 
       " [" << toReturn.x << " " << toReturn.y << "] " <<
       " [" << NewPoint.x << " " << NewPoint.y << "] " << 
       " [" << CurReferencePoint.x << " " << CurReferencePoint.y << "]" << endl; 
      // Increases counter
      NewPointsCount ++;
      // Updates NewPointsCount if needed
      if(NewPointsCount == MaxNewPoints){
       NewPointsCount = 0;
      }
      }
     }
    }

    // Needs to translate point to [-0.5, 0.5]
    toReturn.x -= 0.5;
    toReturn.y -= 0.5;
    return toReturn;
   }

   private:
   std::vector<sPoint> SamplePoints;
   std::vector<sPoint> ProcessList;
   size_t NumPoints;
   PRNG Generator;
   int MaxNewPoints;
   int NewPointsCount;
   float MinDist;
   float CellSize;
   int GridW;
   int GridH;
   sGrid* Grid;
   sPoint CurReferencePoint;  // current reference point for generating new point
   unsigned TimeSeed;
  };
#endif
