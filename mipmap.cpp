#include "mipmap.h"

std::vector<float> Mipmap::nextLevel(std::vector<Vector2d> &trainningSet, 
  std::vector<double> &trainningVal, int scaleFactor,
  RBF rbf,
  std::vector<Vector2d> &nextlevelloc){
 std::vector<float> toReturn;
 // build grid up on the map
 int nextW = width / scaleFactor;
 int nextH = height / scaleFactor;
 cout << "nextW: " << nextW << " nextH: " << nextH << endl;

 toReturn.resize(nextW * nextH, 0.0);
 nextlevelloc.resize(nextW * nextH);
 for(int i = 0; i < nextW; i++){
  for(int j = 0; j < nextH; j++){
   // compute indices in prev level
   std::vector<Vector2d> pixelgroup;
   for(int ii = i * scaleFactor; ii < (i+1) * scaleFactor; ii++)
    for(int jj = j * scaleFactor; jj < (j+1) * scaleFactor; jj++)
     pixelgroup.push_back(Vector2d(ii, jj));
   float f = 0;
   Vector2d c = pixelgroup[0] + (pixelgroup.back() - pixelgroup[0]) / 2.0;
   f = rbf.eval(c);
   int idx = j * nextW + i;
   toReturn[idx] = f;
   nextlevelloc[idx] = c;

   if(f > 10e-6){
    trainningSet.push_back(c);
    trainningVal.push_back(f);
   }
  }
 }

 cout << "next level size: " << width * (1.0 / scaleFactor) << "X" 
  << height * (1.0 / scaleFactor) << " ? " << trainningSet.size() << endl;

 return toReturn;
}


