/*
 * VelocityPath Class
 * for extracting vector field from the Monte-Carlo paths ensemble
 */
#ifndef __VELOCITY_PATH__
#define __VELOCITY_PATH__

#include <stdio.h>
#include <vector>
#include "path.h"


class VelocityPoint{
 public:
  VelocityPoint(Vector2d _pos, Vector2d _vel):
   pos(_pos),
   vel(_vel)
 {}
  VelocityPoint(const VelocityPoint& _p)
  {
   pos = _p.Pos();
   vel = _p.Vel();
  }
  ~VelocityPoint(){};

  const Vector2d& Pos() const {return pos;}
  Vector2d& Pos() {return pos;}

  const Vector2d& Vel() const {return vel;}
  Vector2d& Vel() {return vel;}
 private:
  Vector2d pos; // in lat and long
  Vector2d vel; // velocity
};

class Bin{
 public:
  Bin(){};
  Bin(const std::vector<VelocityPoint> _samples)
  {
   for(int i = 0; i < _samples.size(); i++)
    samples.push_back(_samples[i]);
  }
  Bin(const Bin& _bin)
  {
   for(int i = 0; i < _bin.Samples().size(); i++)
    samples.push_back((_bin.Samples())[i]);
   llc = _bin.LLC();
   urc = _bin.URC();
   v = _bin.V();
   sd = _bin.SD();
  }

  ~Bin(){}

  void findVector()
  {
   int size = samples.size();
   if(size == 0)
    v.set(0, 0);
   else
   {
    for(int i = 0; i < samples.size(); i++)
    {
     Vector2d vel = samples[i].Vel();
     v = v + vel;
    }
    //v = v / v.norm();
    v = v / samples.size();
   }
  }


  const std::vector<VelocityPoint>& Samples() const {return samples;}
  std::vector<VelocityPoint>& Samples() {return samples;}

  const Vector2d& LLC() const {return llc;}
  Vector2d& LLC() {return llc;}

  const Vector2d& URC() const {return urc;}
  Vector2d& URC() {return urc;}

  const Vector2d& V() const {return v;}
  Vector2d& V() {return v;}

  const float& SD() const {return sd;}
  float& SD() {return sd;}

 private:
  std::vector<VelocityPoint> samples;
  Vector2d llc;
  Vector2d urc;
  Vector2d v;
  float sd;
};

class VelocityPath{
 public:
  VelocityPath(){};

  VelocityPath(const VelocityPath& _p)
  {
   samples = _p.Samples();
  }

  VelocityPath(path* oldpath)
  {
   for(int i = 0; i < oldpath->posList.size(); i++)
   {
    Vector2d vel, p0, p1;
    if(i == oldpath->posList.size() - 1)
    {
     p0 = *(oldpath->posList[i-1]);
     p1 = *(oldpath->posList[i]);
    }
    else
    {
     p0 = *(oldpath->posList[i]);
     p1 = *(oldpath->posList[i+1]);
    }
    p0 = translateToGeoCoor(p0);
    p1 = translateToGeoCoor(p1);
    vel = p1 - p0;
    //vel = vel / vel.norm(); // maintaining the time uncertainty, so do not normlize here
    Vector2d pos = *(oldpath->posList[i]);
    pos = translateToGeoCoor(pos);
    VelocityPoint toPush(pos, vel);
    samples.push_back(toPush);
   }
  }

  const std::vector<VelocityPoint>& Samples() const {return samples;}
  std::vector<VelocityPoint>& Samples() {return samples;}

  int Size() {return samples.size();}

  ~VelocityPath(){};

 private:
  std::vector<VelocityPoint> samples;

};
#endif
