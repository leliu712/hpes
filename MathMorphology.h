#ifndef __MATH_MORPHOLOGY__
#define __MATH_MORPHOLOGY__
/*
 * mathematical morphology
 */

#include <iostream>
#include <vector>
#include <Matrix.h>

using namespace std;

class MathMorphology{
 public:
  MathMorphology(float* _set, Matrix _element, int _rows, int _cols, int _size);
  ~MathMorphology();

  void setImage(float* image, const int size){
   set = new float[size];
   for(int i = 0; i < size; i++)
    set[i] = image[i];
  }

  const Matrix& Element() const{ return element; }
        Matrix& Element()      { return element; }

  // operator for sets
  void dilation();
  void erosion();
  void closing();
  void opening();

  // operator for images
  void dilationImage();
  void erosionImage();
  void openingImage();
  void closingImage();


  float* getSet(){
   return set;
  }

  void clearSet(){
   for(int i = 0; i < rows * cols; i++)
    set[i] = 0.0;
  }
  int getIndex(const int row, const int col){
   return row * cols + col;
  }
  void drawBinaryImage();

 private:
  float *set;
  int size;
  Matrix element;   //structring element
  int rows, cols;
  int erows, ecols; //structreing element dims

};


#endif
