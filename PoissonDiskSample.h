/*
 * Class to do poisson disk sampling
 */
#ifndef __POISSON_DISK_SAMPLE__
#define __POISSON_DISK_SAMPLE__

#ifdef __APPLE__
# include <GLUT/glut.h>
#else
# include <GL/glut.h>
# include <GL/glu.h>
#endif

#include <iostream>
#include <fstream>
#include <ctime>
#include <limits.h>
#include "KDTree.h"

#define Neighbors std::vector<QueueTuple>

//#define DENSITY_FLAG

// Minimal distance for PDS.
// This could be associated with the size of an icon.
static float Dmin = 5;

extern string InputPrefix;
extern string OutputPrefix;
extern string DatafileName;

template<typename T, typename S>
class PoissonDiskSample
{
 public:
  PoissonDiskSample(){ subsetSize = 100; /*default subset size*/}
  ~PoissonDiskSample(){}

  void setSubsetSize(const int s)
  {
   subsetSize = s;
  }

  void weightedSampleElim2(T input, T original, 
    std::vector<S> scalefactors, std::vector<int> sampleingrid,
    S area) 
  {
   // Build a kd tree of the the input data
   KDTree kdtree;
   int dim = 2; // input data element dim
   S alpha = 8; // user defined parameter
   kdtree.buildKDTree(kdtree.formatInput<T>(input, input.size(), dim));

   // Compute rmax and rmin
   int M = input.size(); int N = 100;
   if(N > M)
    cerr << "size of subset should be <= size of input set." << endl;
   float globaldensity = M / 1.0;
   S rmax = Rmax(area, N);
   S rmin = Rmin(rmax, 1.5, 0.65 * 0.25, N, M);
   cout << "old:" << rmax << " rmin " << rmin << " alpha " << alpha << endl;
   std::vector<Vector2d> testsamples;
   readRaxRmin(rmin, rmax, alpha);
   cout << "space area: " << area << endl;
   cout << "method 2: rmax " << rmax << " rmin " << rmin << " alpha " << alpha << endl;

   //for(int i = 0; i < densities.size(); i++)
   // cout << i << " " << densities[i] << endl;
   //exit(0);

   // Initial weights vector
   std::vector<S> weights;
   weights.resize(M, 0.0);

   std::vector<Vector2d> boundaries;
   std::vector<Vector2d> badpoints;
   // Assign weights to each sample
   for(int i = 0; i < input.size(); i++)
   {
    std::vector<Neighbors> neighbors;
    neighbors.resize(3);
    Point* query = kdtree.createPoint(dim);
    for(int j = 0; j < dim; j++)
     query->coord[j] = input[i][j];
    float scalefactor = scalefactors[sampleingrid[i]];
    rmax = Rmax(area, N, densities[i]);
    //cout << i << " " << rmin << " " << rmax << " "<< densities[i] << endl ;
    kdtree.rangeQuery<float>(query, 2 * rmin, 2 * rmax, neighbors[0], neighbors[1], neighbors[2]);

    //if(neighbors[1].size() == 0)
    //{
    // cout << i << ": " << endl; 
    // cout << scalefactor << endl;
    // cout << sampleingrid[i] << endl;
    // badpoints.push_back(input[i]);
    //}
    // dis < 2 * rmin
    for(int j = 0; j < neighbors[0].size(); j++)
    {
     float wij(0);
     int idx = neighbors[0][j].second->index;
     float dis = sqrt(neighbors[0][j].first);
     float dij(0);
     dij = 2 * rmin;
     wij = pow(1 -  dij / (2 * rmax), alpha);
     weights[i] += wij;
    }

    // 2 * rmin < dis < 2 * rmax 
    for(int j = 0; j < neighbors[1].size(); j++)
    {
     float dis = sqrt(neighbors[1][j].first);
     float dij = dis;
     float wij = pow(1 - dij / (2 * rmax), alpha);
     weights[i] += wij;
    }
   }
   write2file(badpoints, "badpoints.txt");

   // Build a piority queue for samples using weights
   bounded_priority_queue<QueueTuple> bpq(M);
   for(int i = 0; i < M; i++)
   {
    Point* point = KDTree::createPoint(dim);
    for(int j = 0; j < dim; j++)
     point->coord[j] = input[i][j];
    point->index = i;
    bpq.push(QueueTuple(weights[i], point));
   }
   //printQueue(bpq, "fullqueue.txt", original.size());
   //exit(0);

   // while number of samples > desired,
   // pop samples with bigger weights
   std::vector<int> zeros;
   zeros.resize(M, 0);
   while(bpq.size() > N)
   {
    // find a sample will be eliminated
    int idx = bpq.queue_.top().second->index;
    zeros[idx] = 1;

    // find its neighbors in the kd tree
    std::vector<Neighbors> neighbors;
    neighbors.resize(3);
    Point* query = kdtree.createPoint(dim);
    for(int i = 0; i < dim; i++)
    {
     query->coord[i] = input[idx][i];
     //query->coord[i] = bpq.queue_.top().second->coord[i];
    }
    float scalefactor = scalefactors[sampleingrid[idx]];
    rmax = Rmax(area, N, densities[idx]);
    kdtree.rangeQuery<float>(query, 2 * rmin, 2 * rmax, neighbors[0], neighbors[1], neighbors[2]);

    // remove its weight from its neighbors
    for(int i = 0; i < neighbors[0].size(); i++)
    {
     float dis = sqrt(neighbors[0][i].first);
     float dij(0);
     dij = 2 * rmin;
     float wij = pow(1 - dij / (2 * rmax), alpha);
     int idx_neighbor = neighbors[0][i].second->index; 
     weights[idx_neighbor] -= wij;
    }

    for(int i = 0; i < neighbors[1].size(); i++)
    {
     int idx_neighbor = neighbors[1][i].second->index; 
     float dis = sqrt(neighbors[1][i].first);
     float wij = pow(1 - dis / (2 * rmax), alpha);
     weights[idx_neighbor] -= wij;
    }

    // re-constrcut the priority queue
    bpq.clear();
    for(int i = 0; i < M; i++)
    {
     if(zeros[i] != 1)
     {
      Point* point = KDTree::createPoint(dim);
      for(int j = 0; j < dim; j++)
       point->coord[j] = input[i][j];
      point->index = i;
      bpq.push(QueueTuple(weights[i], point));
     }
    } 

    if(checkValidSamples(zeros, original.size(), N))
     break;
   }
   //write2file(printQueue(bpq), "PDSFiles/PDSfile.txt");
   std::vector<Vector2d> toReturn;
   char* fname = new char[100];
   sprintf(fname, "PDSFiles/PDS-subset-CDFnew.txt");
   toReturn = printQueue(bpq, fname, original.size());
   std::cout << "poisson disk sampling!" << std::endl;
   exit(0);
  }


  // input contains extral samples that will not be selected in the result
  // original contains the original data samples
  // input = original + extral
  std::vector<Vector2d> weightedSampleElim(T input, T original, float& mindis, const int frameid)
  {
   // Build a kd tree of the the input data
   KDTree kdtree;
   int dim = 2; // input data element dim
   S alpha = 8; // user defined parameter
   area = 1.0;  // the total area of cdf space is 1.0
   kdtree.buildKDTree(kdtree.formatInput<T>(input, input.size(), dim));

   // Compute rmax and rmin
   int M = input.size(); int N = subsetSize;
   if(N > M)
    cerr << "size of subset should be <= size of input set." << endl;
   float globaldensity = M / 1.0;
   float constant = 1;
   S rmax = constant * Rmax(area, N);
   mindis = rmax;
   S rmin = Rmin(rmax / constant, 1.5, 0.65 * 0.25, N, M);
   std::vector<Vector2d> testsamples;
   cout << "method 1: rmax " << rmax << " " << "rmin " << rmin << endl;

   // Initial weights vector
   std::vector<S> weights;
   weights.resize(M, 0.0);

   std::vector<Vector2d> boundaries;
   // Assign weights to each sample
   for(int i = 0; i < input.size(); i++)
   {
    std::vector<Neighbors> neighbors;
    neighbors.resize(3);
    Point* query = kdtree.createPoint(dim);
    for(int j = 0; j < dim; j++)
     query->coord[j] = input[i][j];
    kdtree.rangeQuery<float>(query, 2 * rmin, 2 * rmax, neighbors[0], neighbors[1], neighbors[2]);

    // dis < 2 * rmin
    for(int j = 0; j < neighbors[0].size(); j++)
    {
     float wij(0);
     int idx = neighbors[0][j].second->index;
     float dij(0);
     dij = 2 * rmin;
     wij = pow(1 -  dij / (2 * rmax), alpha);
     weights[i] += wij;
     float dis = sqrt(neighbors[0][j].first);
    }

    // 2 * rmin < dis < 2 * rmax 
    for(int j = 0; j < neighbors[1].size(); j++)
    {
     float dis = sqrt(neighbors[1][j].first);
     float dij = dis;
     float wij = pow(1 - dij / (2 * rmax), alpha);
     weights[i] += wij;
    }

    //if(i == 645 || i == 917 || i == 494 || i == 658 || i == 181 || i == 775 || i == 840 ||
    //  i == 60 || i == 675){
    // weights[i] = 0;
    //}
    //if(i == 850 || i == 397 || i == 470 || i == 925 || i == 931 || i == 929 ||
    //  i == 565 || i == 65 || i == 773 || i == 255 || i == 907 || i == 81 || 
    //  i == 646 || i == 195 || i == 692 || i == 926 || i == 74 || i == 44 || i == 566 ||
    //  i == 784 || i == 934 || i == 509 || i == 284){
    // weights[i] = 100;
    //}

    //// test adjusting weights
    //float ix, iy;
    //if(GetLineIntersection(-0.1, -0.1, 0.1, 0.1, 0.5, -0.5, input[i][0], input[i][1], &ix, &iy)){
    // weights[i] += 0.1;
    //}

    // adjust weight based on global density
    //if(findDistance(input[i], Vector2d(-0.5, 0), Vector2d(0, 0.5)) < 2 * rmax || 
    //   findDistance(input[i], Vector2d(0, 0.5), Vector2d(0.5, 0))  < 2 * rmax ||
    //   findDistance(input[i], Vector2d(0.5, 0), Vector2d(0, -0.5)) < 2 * rmax ||
    //   findDistance(input[i], Vector2d(0, -0.5), Vector2d(-0.5, 0))< 2 * rmax)
    //{
    // weights[i] *= 4;
    // boundaries.push_back(input[i]);
    //}

    //int numofneighbors = neighbors[0].size() + neighbors[1].size();
    //weights[i] += adjustWeights(input[i], weights[i], numofneighbors, 
    //  rmax, rmin, alpha, testsamples);
   }
   //write2file(boundaries, "boundaries.txt");
   //write2file(testsamples, "testsamples.txt");

   // Build a piority queue for samples using weights
   bounded_priority_queue<QueueTuple> bpq(M);
   for(int i = 0; i < M; i++)
   {
    Point* point = KDTree::createPoint(dim);
    for(int j = 0; j < dim; j++)
     point->coord[j] = input[i][j];
    point->index = i;
    bpq.push(QueueTuple(weights[i], point));
   }
   //printQueue(bpq, "fullqueue.txt");
   //exit(0);

   // while number of samples > desired,
   // pop samples with bigger weights
   std::vector<int> zeros;
   zeros.resize(M, 0);
   while(bpq.size() > N)
   {
    // find a sample will be eliminated
    int idx = bpq.queue_.top().second->index;
    zeros[idx] = 1;

    // find its neighbors in the kd tree
    std::vector<Neighbors> neighbors;
    neighbors.resize(3);
    Point* query = kdtree.createPoint(dim);
    for(int i = 0; i < dim; i++)
    {
     query->coord[i] = input[idx][i];
     //query->coord[i] = bpq.queue_.top().second->coord[i];
    }
    kdtree.rangeQuery<float>(query, 2 * rmin, 2 * rmax, neighbors[0], neighbors[1], neighbors[2]);

    // remove its weight from its neighbors
    for(int i = 0; i < neighbors[0].size(); i++)
    {
     float dis = sqrt(neighbors[0][i].first);
     float dij(0);
     dij = 2 * rmin;
     float wij = pow(1 - dij / (2 * rmax), alpha);
     int idx_neighbor = neighbors[0][i].second->index; 
     weights[idx_neighbor] -= wij;
    }

    for(int i = 0; i < neighbors[1].size(); i++)
    {
     int idx_neighbor = neighbors[1][i].second->index; 
     float dis = sqrt(neighbors[1][i].first);
     float wij = pow(1 - dis / (2 * rmax), alpha);
     weights[idx_neighbor] -= wij;
    }

    // re-constrcut the priority queue
    bpq.clear();
    for(int i = 0; i < M; i++)
    {
     // do not consider an eliminated point
     if(zeros[i] != 1)
     {
      Point* point = KDTree::createPoint(dim);
      for(int j = 0; j < dim; j++)
       point->coord[j] = input[i][j];
      point->index = i;
      bpq.push(QueueTuple(weights[i], point));
     }
    } 

    if(checkValidSamples(zeros, original.size(), N))
     break;
   }
   //write2file(printQueue(bpq), "PDSFiles/PDSfile.txt");
   std::vector<Vector2d> toReturn;
   //sprintf(buff, "NewData/NHC-PDS-idx-%d.txt", frameid);
   char* buff = new char[200];
   string str = OutputPrefix + "InternalData/PathSamplingData/NHC-PDS-idx-%d.txt";
   const char* cstr = str.c_str();
   sprintf(buff, cstr, frameid);
   toReturn = printQueue(bpq, buff, original.size());
   //toReturn.clear();
   //str = OutputPrefix + "InternalData/sub-idx-%d.txt";
   //cstr = str.c_str();
   ////sprintf(buff, "demoDisplayData/sub-idx-%d.txt", frameid);
   //sprintf(buff, cstr, frameid);
   //toReturn = printQueue(bpq, buff, original.size());
   return toReturn;
  }

  // rmax = sqrt(A / (2 * sqrt(3) * N))
  // N -- the desired size of the subset
  static S Rmax(S A, int N) 
  {
   return sqrt(A / (2 * 1.732 * N));
  }

  S Rmax(S A, int N, double density) 
  {
   return sqrt(A / (2 * 1.732 * density * 10));
  }

  // rmin = rmax * (1 - pow(N/M, gamma)) * beta
  // M -- the total number of samples
  static S Rmin(S rmax, S gamma, S beta, int N, int M) 
  { 
   return rmax * (1 - std::pow(N * (1.0 / M), gamma)) * beta;
  }

  // given a density value of a sample and lambda,
  // return rmin at this sample
  S Rmin(S sigma, S lambda)
  {
   return 1.0 / sqrt(PI * lambda * sigma);
  }

  // given rmin of a sample,
  // return rmax at this sample
  S Rmax(S rmin, S gamma, S beta, int N, int M)
  {
   return rmin / ((1 - pow(N * (1.0 / M), gamma)) * beta);
  }


  // Print queue for testing
  T printQueue(bounded_priority_queue<QueueTuple> bpq, const char* filename,
    const int M)
  {
   ofstream myfile(filename, fstream::out);
   T toReturn;
   glPointSize(5);
   glBegin(GL_POINTS);
   glColor4f(1, 0, 0, 1);
   while(!bpq.queue_.empty())
   {
    //printf("%3d\t%1.9f\t%4f\t%4f\n",
    //  bpq.queue_.top().second->index,
    //  bpq.queue_.top().first,
    //  bpq.queue_.top().second->coord[0],
    //  bpq.queue_.top().second->coord[1]);
    if(bpq.queue_.top().second->index < M) // output original points
    {
     myfile << bpq.queue_.top().second->index+1 << " " << 
      bpq.queue_.top().second->coord[0] << " " << 
      bpq.queue_.top().second->coord[1] << endl;
     //myfile << bpq.queue_.top().second->index+1 << endl; 
     Vector2d toPush(bpq.queue_.top().second->coord[0],
       bpq.queue_.top().second->coord[1]);
     //Vector2d toPush(bpq.queue_.top().second->index, 0);
     toReturn.push_back(toPush);
     //glVertex3f(bpq.queue_.top().second->coord[0] * 1000, 
     //           bpq.queue_.top().second->coord[1] * 800, 0);
    }
    bpq.queue_.pop();
   }
   glEnd();
   myfile.close();
   cout << "finish writing samples to file: " << filename << endl;
   return toReturn;
  }

  //static void write2file(V data, char* filename)
  //{
  // ofstream myfile(filename, fstream::out);
  // int size = data.size();
  // for(int i = 0; i < size; i++)
  //  myfile << data[i][2] << " " << data[i][0] << " " << data[i][1] << std::endl;
  //  //myfile << data[i][0] << std::endl;
  // myfile.close();
  // std::cout << "finish writing poisson disk samples file." << std::endl;
  //}

  void setInputDensities(std::vector<double> _densities)
  {
   densities = _densities;
  }

  void setBoundingArea(S _area)
  {
   area = _area;
  }

  float adjustWeights(
    const Vector2d query,
    const S weight, 
    const int numofneighbors, 
    const S rmax,
    const S rmin,
    const S alpha,
    std::vector<Vector2d>& testsamples)
  {
   // pick a target global density 
   // this should be the max density of all samples
   float globaldensity = 150;
   int extrasamples = 0;
   float extraweights(0);

   unsigned int counter = time(NULL);
   while(numofneighbors + extrasamples < globaldensity)
   {
    extrasamples ++;
    // generate more samples
    srand(counter);
    counter = (counter*21)%UINT_MAX;
    float u = (rand()%100+1) * (1.0 / 100);
    srand(counter);
    counter = (counter*21)%UINT_MAX;
    float v = (rand()%100+1) * (1.0 / 100);
    if(v < u) std::swap(u, v);
    Vector2d newsample(v * rmax * cos(2 * PI * u / v), 
      v * rmax * sin(2 * PI * u / v));
    newsample = query + newsample;

    // compute extra weight for query sample
    float dis = (newsample - query).norm();
    float dij(0);
    dij = dis < 2 * rmin ? 2 * rmin : dis;
    float wij = pow(1 - dij / (2 * rmax), alpha);
    extraweights += wij;
   }
   return extraweights;
  }

  static float findDistance(const Vector2d query, const Vector2d v0, const Vector2d v1)
  {
   Vector2d e0 = v1 - v0;
   Vector2d e1 = query - v0;
   Vector h = e1 - e1 * e0 / e0.norm() * (e0 / e0.norm());
   float dis = h.norm();
   return dis;
  }

  bool checkBPQ(bounded_priority_queue<QueueTuple> tmp, 
    const int size, const int N)
  {
   int total(0);
   while(!tmp.queue_.empty())
   {
    int idx = tmp.queue_.top().second->index;
    if(idx < size)
     total++;
   }
   if(total > N)
    return false;
   else
    return true;
  }

  bool checkValidSamples(
    const std::vector<int> zeros,
    const int size, const int N)
  {
   int total(0);
   // count the number of zeros, meaning the number of points have been selected in the original samples
   total = std::count(zeros.begin(), zeros.begin()+size-1, 0);
   // return true if N samples have been selected from the original samples
   if(total > N){
    return false;
   } else{
    return true;
   }
  }

  static void write2file(std::vector<Vector2d> data, const char* filename)
  {
   ofstream myfile(filename, fstream::out);
   int size = data.size();
   for(int i = 0; i < size; i++)
    myfile << data[i].x << " " << data[i].y << endl;
   myfile.close();
   cout << "finish writing " << size << " entries to "  << filename << endl; 
  }

  void readRaxRmin(S& rmin, S& rmax, S& alpha)
  {
   ifstream myfile("rminrmax.txt");
   string line;
   getline(myfile, line);
   rmin = stof(line);
   getline(myfile, line);
   rmax = stof(line);
   getline(myfile, line);
   alpha = stof(line);
   myfile.close();
  }

  // Returns true if the lines intersect, otherwise 0. In addition, if the lines 
  // intersect the intersection point may be stored in the floats i_x and i_y.
  static bool GetLineIntersection(float p0_x, float p0_y, float p1_x, float p1_y, 
    float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
  {
   float s1_x, s1_y, s2_x, s2_y;
   s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
   s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

   float s, t;
   s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
   t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

   if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
   {
    // Collision detected
    if (i_x != NULL)
     *i_x = p0_x + (t * s1_x);
    if (i_y != NULL)
     *i_y = p0_y + (t * s1_y);
    return true;
   }

   return false; // No collision
  }

 private:
  std::vector<double> densities;
  S area;
  int subsetSize;


};

#endif

