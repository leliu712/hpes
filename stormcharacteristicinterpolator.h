#ifndef STORMCHARACTERISTICINTERPOLATOR_H
#define STORMCHARACTERISTICINTERPOLATOR_H

#include "Utility.h"
#include "Vector.h"
#include "Matrix.h"
#include "RBF.h"

#include<vector>
#include <cassert>
#include <fstream>

#include <QString>


class StormCharacteristicInterpolator
{
public:
    struct PredictionNode{
        Vector2d _loc;
        double _size;
        double _intensity;
        PredictionNode() {
            _loc = Vector2d(0, 0);
            _size = 0;
            _intensity = 0;
        }
        PredictionNode (const Vector2d& loc, const double size, const double intensity) : _loc(loc), _size(size), _intensity(intensity){}
        PredictionNode (const PredictionNode& n){
            _loc = n._loc;
            _size = n._size;
            _intensity = n._intensity;
        }
    };

public:
    StormCharacteristicInterpolator(std::vector<PredictionNode>& predictions, const int timepoint = 0);
    StormCharacteristicInterpolator(const std::vector<Vector2d>& locations, QString* ofilename = NULL, std::vector<Vector2d>* externalpoints = NULL, std::vector<Vector2d>* bbox = NULL, const int timepoint = 0);

    void ParsePredictions();
    void CorrectPredictions(const std::vector<int> validlist);
    void BuildRBFFromSubset(const int timepoint, const std::string& type);

    void Process(QString* ofilename);
    void OLSSelection(const int timepoint, const QString& type);
    void ComputeSimplicialDepth();
    // bbox representd by [minx, miny], [maxx, maxy]
    void ComputeBBox();
    void ComputeBBoxFromPointSet(const std::vector<Vector2d>& points);
    static void ComputeBBoxFromPointSet(const std::vector<Vector2d> &points, std::vector<Vector2d>* bbox);
    std::vector<int> ComputeKDNNDensity();
    static void ComputeKDNNDensity(const std::vector<Vector2d>& points,
                              const std::vector<Vector2d>& bbox,
                              std::vector<Vector2d>* validpoints,
                              std::vector<double>* densities);

    void ComputeKNNDensityRBF();
    void ComputeRBFRadiiUsingKNNDensity();
    static void ComputeRBFRadiiUsingKNNDensity(const std::vector<Vector2d>& bbox, std::vector<Vector2d>& points, std::vector<double>& densities, std::vector<double>* rbfradii);
    RBF ComputeRBF(const std::vector<Vector2d>& samples, const std::vector<double>& values, const std::vector<double>& radii);
    void ComputeSDRBF(const int timepoint);
    void ComputeSizeRBF(const int timepoint);
    void ComputeIntensityRBF(const int timepoint);

    const std::vector<Vector2d> Locations() const { return _locations;}
    std::vector<Vector2d>& Locations() { return _locations; }
    const std::vector<double> StormSizes() const { return _stormsizes; }
    std::vector<double>& StormSizes() { return _stormsizes; }
    const std::vector<double> StormIntensities() const { return _stormintensities; }
    std::vector<double>& StormIntensities() { return _stormintensities; }
    const std::vector<double> SDValues() const { return _sdvalues; }
    std::vector<double>& SDValues() { return _sdvalues; }

    const std::vector<Vector2d> ValidKDTLocations() const { return _validKNNlocations;}
    std::vector<Vector2d>& ValidKDTLocations() { return _validKNNlocations; }

    const std::vector<double> KDTDensity() const { return _knndensities; }
    std::vector<double>& KDTDensity() { return _knndensities; }

    const RBF KDTDensityRBF() const { return _kdtdensityrbf;}
    RBF& KDTDensityRBF() { return _kdtdensityrbf; }

    const RBF SDRBF() const { return _sdrbf; }
    RBF& SDRBF() { return _sdrbf; }

    const RBF SubRBF(const std::string& type) const {
        if(type == "sd"){
            return _subsdrbf;
        } else if(type == "size"){
            return _subsizerbf;
        } else if(type == "intensity"){
            return _subintensityrbf;
        } else {
            cerr << type << " is not a valid storm characteristic." << endl;
            exit(0);
        }
    }

    RBF& SubRBF(const std::string& type){
        if(type == "sd"){
            return _subsdrbf;
        } else if(type == "size"){
            return _subsizerbf;
        } else if(type == "intensity"){
            return _subintensityrbf;
        } else {
            cerr << type << " is not a valid storm characteristic." << endl;
            exit(0);
        }
    }

    float SubRBFEval(const Vector2d& p, const std::string& type, const bool withradii = true){
        if(withradii){
          if(type == "sd"){
              return _subsdrbf.eval(p, _sdradii);
          } else if(type == "size"){
              return _subsizerbf.eval(p, _sizeradii);
          } else if(type == "intensity"){
              return _subintensityrbf.eval(p, _intensityradii);
          } else {
              cerr << type << " is not a valid storm characteristic." << endl;
              exit(0);
          }
        } else {
          if(type == "sd"){
              return _subsdrbf.eval(p);
          } else if(type == "size"){
              return _subsizerbf.eval(p);
          } else if(type == "intensity"){
              return _subintensityrbf.eval(p);
          } else {
              cerr << type << " is not a valid storm characteristic." << endl;
              exit(0);
          }
        }
    }

    // Is i adjacent category of j
    static bool IsAjacent(const int i, const int j);
    static int StormCategory(const double windspeed, const bool needCorrect = false);
    static void StormCategoryColorMapping(const int category, float *r, float *g, float *b);
    static void StormCategoryColorMapping2(const int category, float *r, float *g, float *b);

    const std::vector<double> RBFRadii() const { return _rbfradii; }
    std::vector<double>& RBFRadii() { return _rbfradii; }

    const std::vector<double> StormCharacteristicRadii(const std::string type) const {
        if(type == "sd"){
            return _sdradii;
        } else if (type == "size"){
            return _sizeradii;
        } else if (type == "intensity"){
            return _intensityradii;
        } else {
            cerr << "unknown storm characteristic " << type << endl;
            exit(0);
        }
    }


    std::vector<double>& StormCharacteristicRadii(const std::string type) {
        if(type == "sd"){
            return _sdradii;
        } else if (type == "size"){
            return _sizeradii;
        } else if (type == "intensity"){
            return _intensityradii;
        } else {
            cerr << "unknown storm characteristic " << type << endl;
            exit(0);
        }
    }

    const std::vector<Vector2d> BBox() const { return _bbox; }
    std::vector<Vector2d>& BBox() { return _bbox; }


    void WriteReproducedField2Image(const int timepoint, const std::string& type, RBF rbf);
    void WriteScalarField2Image(RBF rbf, const std::vector<double> radii, const bool withradii = false, const QString& filename="scalarfield.png", const int imagewidth = 720);

    // drawing functions
    static void DrawLocationWithScalarValues(const std::vector<Vector2d>& locations, const std::vector<double>& values);
    void DrawScalarField(RBF rbf, const bool withradii = false);
    void DrawReproducedScalarValues(RBF rbf);

    void PrintPredictions();


private:
    std::vector<PredictionNode> _predictions;
    std::vector<Vector2d> _locations;
    std::vector<double> _stormintensities;
    std::vector<double> _stormsizes; // maybe 4 different value
    std::vector<Vector2d> _validKNNlocations; // merge overlapped points in KDTree
    std::vector<double> _knndensities;
    std::vector<double> _sdvalues; // simplicial depth values
    std::vector<Vector2d> _bbox;

    // RBF radii for this data set
    std::vector<double> _rbfradii;
    std::vector<double> _sizeradii;
    std::vector<double> _intensityradii;
    std::vector<double> _sdradii;

    // RBF interpolation systems
    RBF _kdtdensityrbf;
    RBF _sdrbf;
    RBF _sizerbf;
    RBF _intensityrbf;
    RBF _subsdrbf;
    RBF _subsizerbf;
    RBF _subintensityrbf;

};

#endif // STORMCHARACTERISTICINTERPOLATOR_H
