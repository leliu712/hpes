#ifndef KDTREEDENSITYESTIMATOR_H
#define KDTREEDENSITYESTIMATOR_H

#include "KDTree.h"

#include<vector>
#include<limits>

template<typename T>
 std::vector<double> KDTreeDensity(T data, int size, int dim,
   std::vector<int>& validlist, float threshold, const int K = 10)
{
 KDTree kdtree;
 kdtree.buildKDTree(kdtree.formatInput<T>(data, data.size(), dim));
 std::vector<double> density;
 density.resize(data.size(), 0);
 validlist.clear();
 validlist.resize(data.size(), 1);
 for(int i = 0; i < size; i++)
 {
  if(validlist[i] != 0)
  {
   Point* p = kdtree.createPoint(dim);
   for(int j = 0; j < dim; j++)
    p->coord[j] = data[i][j];

   // 11 is the K value for KNN
   // 11 = 10 + 1, the last element in the queue is the testing point
   // maybe 1/100 of the size of the training set
   bounded_priority_queue<QueueTuple> bpq = kdtree.knearest(p, K+1);
   //cout << "bpq size: " << bpq.size() << endl;
   //bounded_priority_queue<QueueTuple> tmp(bpq);

   float maxR(0);
   // merge points too close
   while(!bpq.queue_.empty())
   {
    // r is already sqr distance
    float r = bpq.queue_.top().first;
    maxR = max(maxR, r);
    if(r < threshold * threshold)
    {
     // if a point is too close to the query
     // set its valid flag to 0
     int idx = 0;
     Point* p_ = bpq.queue_.top().second;
     idx = p_->index;
     if(idx != i)
      validlist[idx] = 0;
    }
    bpq.queue_.pop();
   }

   float area = PI * maxR;
   // if area is very small, density should be very large
   float d = area < 10e-6 ? numeric_limits<float>::max() : K / area;
   //cout << d << " " << K << " " << area << " " << maxR << endl;
   density[i] = d;

   if(dim > 2)
   {
    area = maxR;
    d = K / area;
    density[i] = d;
   }

  }
 }

 //normalize density to [0, 1];
 float max_den = *(max_element(density.begin(), density.end()));
 for(int i = 0; i < density.size(); i++)
 {
  density[i] /= max_den;
 }

 return density;
}


#endif // KDTREEDENSITYESTIMATOR_H
