/*
 * KD-Tree class
 */
#ifndef __KD_TREE__
#define __KD_TREE__

#include <algorithm>
#include <iostream>
#include <vector>
#include "Vector.h"
#include "BPQ.h"

using namespace std;

struct Point
{
 unsigned k; // coord dim
 float* coord;
 int index; // index in the input data list
};

struct Node
{
 Point point;
 int depth;
 Node *left, *right;
};

struct Input
{
 int size;
 Point* *points;
};

// data structure stored in the BPQ 
#define QueueTuple std::pair<float, Point*>

class KDTree{
 public:
  KDTree(){};
  ~KDTree(){};

  // format raw input data into Input type
  template<typename T>
   Input* formatInput(T rawdata, int size, int dim)
   {
    Point** points = new Point*[size];
    for(int i = 0; i < size; i++)
    {
     points[i] = new Point;
     points[i]->coord = new float[dim];
     points[i]->k = dim;
     points[i]->index = i;
     for(int j = 0; j < dim; j++)
      points[i]->coord[j] = rawdata[i][j];
    }
    inputdata = new Input;
    inputdata->size = size;
    inputdata->points = points;
    //return testData(2, 9);
    return inputdata;
   }

  //Input* formatInput(std::vector<Vector2d> rawdata, int size, int dim);
  Node* buildKDTree(Input* input);

  void insertKDTree(Node* *root, Point* point);
  void insertKDTreeUtil(Node** root, Node* newNode, unsigned depth);

  int searchKDTree(Node* root, Point point);
  int searchKDTreeUtil(Node* root, Point point, unsigned depth);

  static Point* createPoint(unsigned k);
  Node* createNode(Point* point, int depth);
  Input* createInput(unsigned k, unsigned size);
  Input* testData(unsigned k, unsigned size);

  void inorderKDTree(Node* root);
  void printPoints(float* points, unsigned size);

  int checkDuplication(Point p0, Point p1);
  float computeSquaredDistance(Point* point, Node* node);
  float computeDistanceFromAxis(Point* point, Node* node);

  // given a query point and the k value, compute its kNN
  bounded_priority_queue<QueueTuple> knearest(Point* query, int K);
  void knearestUtil(Point* query, Node* currentNode, int K, 
    bounded_priority_queue<QueueTuple>& bpq);

  /* Functions for poisson dis sampling */

  // Given rmin and rmax and the query point, 
  // find neighbors1 that dis <= rmin,
  // find neighbors2 that rmin < dis <= rmax,
  // find neighbors3 that dis > rmax.
  template<typename T>
   void rangeQuery(
     Point* query,
     const T rmin, const T rmax, 
     std::vector<QueueTuple>& neighbors1,
     std::vector<QueueTuple>& neighbors2,
     std::vector<QueueTuple>& neighbors3)
   {
    rangeQueryUtil(query, root, rmin, rmax, neighbors1, neighbors2, neighbors3);
   }

  template<typename T>
   void rangeQueryUtil(Point* query, Node* currentNode, 
     const T rmin, const T rmax,
     std::vector<QueueTuple>& neighbors1,
     std::vector<QueueTuple>& neighbors2,
     std::vector<QueueTuple>& neighbors3)
   {
    if(!currentNode)
     return;

    // This distance is sqr distances
    float dis = computeSquaredDistance(query, currentNode);
    float disAxis = computeDistanceFromAxis(query, currentNode);

    Point* point = createPoint(currentNode->point.k);
    point->index = currentNode->point.index;
    for(int i = 0; i < point->k; i++) 
     point->coord[i] = currentNode->point.coord[i];

    if(dis <= rmax * rmax)
    {
     if(dis == 0){} //query point itself
     else if(dis <= rmin * rmin)
      neighbors1.push_back(QueueTuple(dis, point));
     else if(dis > rmin * rmin)
      neighbors2.push_back(QueueTuple(dis, point));
    }
    else
     neighbors3.push_back(QueueTuple(dis, point));

    rangeQueryUtil(query, disAxis <= 0 ? currentNode->left : currentNode->right,
      rmin, rmax, neighbors1, neighbors2, neighbors3);
    rangeQueryUtil(query, disAxis <= 0 ? currentNode->right : currentNode->left,
      rmin, rmax, neighbors1, neighbors2, neighbors3);
   }

  // considering scale factors
  template<typename T>
   void rangeQuery(
     Point* query,
     const T rmin, const T rmax, 
     std::vector<QueueTuple>& neighbors1,
     std::vector<QueueTuple>& neighbors2,
     std::vector<QueueTuple>& neighbors3,
     T scalefactor)
   {
    rangeQueryUtil(query, root, rmin, rmax, neighbors1, neighbors2, neighbors3, scalefactor);
   }

  template<typename T>
   void rangeQueryUtil(Point* query, Node* currentNode, 
     const T rmin, const T rmax,
     std::vector<QueueTuple>& neighbors1,
     std::vector<QueueTuple>& neighbors2,
     std::vector<QueueTuple>& neighbors3,
     T scalefactor)
   {
    if(!currentNode)
     return;

    //scalefactor = 1;
    // This distance is sqr distances
    float dis = computeSquaredDistance(query, currentNode) * (scalefactor * scalefactor);
    float disAxis = computeDistanceFromAxis(query, currentNode);

    Point* point = createPoint(currentNode->point.k);
    point->index = currentNode->point.index;
    for(int i = 0; i < point->k; i++) 
     point->coord[i] = currentNode->point.coord[i];

    if(dis <= rmax * rmax)
    {
     if(dis == 0){} //query point itself
     else if(dis <= rmin * rmin)
      neighbors1.push_back(QueueTuple(dis, point));
     else if(dis > rmin * rmin)
      neighbors2.push_back(QueueTuple(dis, point));
    }
    else
     neighbors3.push_back(QueueTuple(dis, point));

    rangeQueryUtil(query, disAxis <= 0 ? currentNode->left : currentNode->right,
      rmin, rmax, neighbors1, neighbors2, neighbors3, scalefactor);
    rangeQueryUtil(query, disAxis <= 0 ? currentNode->right : currentNode->left,
      rmin, rmax, neighbors1, neighbors2, neighbors3, scalefactor);
   }


 private:
  Node* root;
  Input* inputdata;

};
#endif
