/*
 * PathsGenerator Class
 * This class generats a collection of paths by extracted velocity fields (at different time instances)
 * from NHC's hurricane predicted paths ensemble.
 */
#ifndef __PATHS_GENERATOR__
#define __PATHS_GENERATOR__

#include "VectorField.h"

class PathsGenerator{
 public:
  PathsGenerator(){}
  PathsGenerator(const std::vector<VectorField> _v):
   vectorfields(_v){}
  PathsGenerator(const PathsGenerator& _p)
  {
    vectorfields = _p.VF();
    refSamples = _p.RefSamples();
    paths = _p.Paths();
  }
  ~PathsGenerator(){}

  void Generate(const int refFrame)
  {
   if(refSamples.size() == 0){ cerr << "No reference samples." << endl; exit(1);}
   paths.clear();
   for(int i = 0; i < refSamples.size(); i++)
   {
    std::vector<Vector2d> path;
    path.resize(6);
    paths.push_back(path);
   }

   for(int i = 0; i < refSamples.size(); i++)
   {
    Vector2d p = refSamples[i];
    paths[i][refFrame] = p;

    int binidx = vectorfields[refFrame].findBinIdx(p, refFrame);
    Vector2d v = (vectorfields[refFrame].Bins())[binidx].V();
    //v = v / v.norm();
    float sd = (vectorfields[refFrame].Bins())[binidx].SD();
    ColorGradient colorgradient;
    float red, green, blue;
    colorgradient.getColorAtValue(sd, red, green, blue);
    glColor4f(red, green, blue, 1);
    //drawVector(p, v);
    //vectorfields[refFrame].drawBin(binidx);
    //Vector2d q = findNext(p, v, sd, refFrame + 1);
    //drawVector(p, q - p);
    vectorfields[refFrame].drawBins();
   }
  }

  Vector2d findNext(const Vector2d p, const Vector2d v, const float sd, const int timeframe)
  {
   float dt = 0.1;
   bool find = false;
   Vector2d q = p;
   Vector2d toReturn(0, 0);
   while(find == false)
   {
    q = q + v * dt;
    dt += 0.1;
    int binidx = vectorfields[timeframe].findBinIdx(q, timeframe);
    if(binidx == -1) // out of range
     return p;
    float nextsd = (vectorfields[timeframe].Bins())[binidx].SD();
    if(abs(nextsd - sd) <= 0.1 * sd)
    {
     find = true;
     toReturn = q;
    }
   }
   return toReturn;
  }

  void drawVector(const Vector2d p, const Vector2d v)
  {
   Vector2d q = p + v;
   Vector2d c = translateToScreen(p.x, p.y);
   q = translateToScreen(q.x, q.y);
   glPointSize(3);
   glBegin(GL_POINTS);
   glVertex3f(c.x, c.y, 0);
   glEnd();
   glBegin(GL_LINES);
   glVertex3f(c.x, c.y, 0);
   glVertex3f(q.x, q.y, 0);
   glEnd();
  }

  const std::vector<VectorField>& VF() const { return vectorfields;}
  std::vector<VectorField>& VF() {return vectorfields;}

  const std::vector<Vector2d>& RefSamples() const {return refSamples;}
  std::vector<Vector2d>& RefSamples() {return refSamples;}

  const std::vector< std::vector<Vector2d> >& Paths() const {return paths;}
  std::vector< std::vector<Vector2d> >& Paths() {return paths;}

 private:
  std::vector<VectorField> vectorfields;
  std::vector<Vector2d> refSamples;
  std::vector< std::vector<Vector2d> > paths; 
};

#endif
