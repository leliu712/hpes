/* Jonathan Cox
   CPSC 805
   */
#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#  include <GL/glu.h>
#endif

#include "posPoint.h"
#include "hurVis.h"
#include "geoFunct.h"
#include "advisory.h"
#include "path.h"
#include "P6Image.h"
#include "Matrix.h"
#include "simulation.h"
#include "gridStruct.h"
#include "window.h"
#include "ui.h"
#include "PathResampling.h"

#include <QApplication>
#include <QGLFormat>
#include <QPixmap>
#include <QImage>
#include <QPainter>
#include <QDesktopWidget>
#include <QGLContext>

#include <Eigen/Core>

gridStruct * dataGrid;
path * testPath;
path * testPath2;
path * curPath;

simulation * sim;
ui * userInt;

PathResampling* pathresampling;

// Distance of projected path
double projPathDist = 0.0;

// Map texture
GLuint textures[8];
P6Image * imageMap;

// Display mode variables
int displayDataPoints = -1;
int displayDataPaths = -1;
int displayProjectedPath = 1;
int displayGenPaths = 1;
int heatMap = -1;
int interactive = -1;
int hitButton = -1;
int showCurPath = -1;
int chooseForward = -1;
int stall = 1;
int preOnly = -1;
int colorClosest = -1;
int averageOnAll = 1;

int totalPaths = 0;
double omega = 0.0;
double userSig = 298.0;   //  Starts at error cone radius at 69 hours
int curAdvisory = 0;

double speedRatio = 0.00;
double bearRatio = 0.00;

int drawMethod = 0;
int totalDrawMethods = 2;

// Poor use of global variables....
double curDistance;

// NHC data locations
string InputPrefix;
string OutputPrefix;
string DatafileName;
int VisSwitch = 0;
float OilRigLoc = 0.0;

// Paths vis mode
int PathVisMode = 0;
int RepSubsetSize = 0;
int SampledTimePoint = 48;

//-----------------------Main--------------------------------------------------

int main( int argc, char *argv[] ){

 // Make sure we have a file to open
 if(argc != 13){
  cout << "usage: main {0|1} {0|1} timeframe rbf-kernel-constant input-prefix output-prefix filename VisSwitch OilRigLoc// cone, tracks\n";
  return 1;
 }

 int im = atoi(argv[1]);
 drawMethod = atoi(argv[2])%2;
 // Build simulation
 //sim = new simulation(1, -100.0, -75.0, 17.0, 31.0, (180.0/60.0), 1.0, 0.953, 15.8, -25.0, 33, -100.0, im);
 sim = new simulation(1, -102.5, -75.0, 20.0, 40.0, (180.0/60.0), 1.0, 0.953, 20.0, -27.5, 40, -102.5, im);
 //sim = new simulation(1, -95.37, -85.66, 26.26, 35, (180.0/60.0), 1.0, 0.953, 8.74, -9.71, 35, -95.37, im);
 //sim = new simulation(1, -95.37, -85.66, 25.93, 35, (180.0/60.0), 1.0, 0.953, 9.07, -9.71, 35, -95.37, im);
 sim->TimeFrame = atoi(argv[3]); 
 sim->setRBFKernelFactor(atof(argv[4]));
 InputPrefix = string(argv[5]);
 OutputPrefix = string(argv[6] + string(argv[7]) + "/");
 DatafileName = string(argv[7]);
 VisSwitch = atoi(argv[8]);
 OilRigLoc = atof(argv[9]);
 SampledTimePoint = atoi(argv[10]);
 PathVisMode = atoi(argv[11]);
 RepSubsetSize = atoi(argv[12]);
 //sim->setNHCdatafile(string(argv[5]), string(argv[6])+string(argv[7])+"/", string(argv[7]));
 sim->setNHCdatafile(InputPrefix, OutputPrefix, DatafileName);
 //if(sim->getDispNum() == 1){
 //   sim->setW(1024);
 //   sim->setH(790);
 //}
 //else{
 //   sim->setW(614);
 //   sim->setH(474);
 //}
 if(sim->getDispNum() == 1){
  sim->setW(1024);
  sim->setH(868);
 }
 else{
  sim->setW(614);
  sim->setH(521);
 }
 sim->buildHeatMapGrid();
 sim->setPixPerKM(sim->getH()/haversine(sim->getMaxLon(), sim->getMaxLon(), sim->getMaxLat(), sim->getMaxLat()-sim->getTotLat()));
 cout << "pixPerKM: " << sim->getPixPerKM() << "\n";

 buildAdvisory();
 buildAdvisory();
 sim->buildExp();
 //exit(0);

 // Build ui
 userInt = new ui();

 dataGrid = new gridStruct(17, 26, 60, 17, 33, -75, -100, string("resHistCur.txt"));
 //dataGrid = new gridStruct(20, 27.5, 60, 20, 40, -75, -102.5, string("resHistCur.txt"));

 //dataGrid->printGrid();
 //exit(0);

 //sim->buildStatPaths();
 
 // Median path resampling
 // The parameters do not matter here
 pathresampling = new PathResampling(SampledTimePoint);

 // QT Stuff
 QApplication app(argc, argv);

 if(!QGLFormat::hasOpenGL()){
  qWarning("This system has no OpenGL support.  Exiting.");
  return -1;
 }

 QGLFormat glf;
 glf.setDoubleBuffer(true);
 glf.setRgba(true);
 glf.setDepth(true);
 QGLFormat::setDefaultFormat(glf);

 //QGLContext * context = new QGLContext(glf);
 //context->create();
 //context->makeCurrent();

 // Main window
 window * win = new window();
 win->resize((int)(sim->getW()*(max(1, sim->getDispNum()/2))), 
   (int)(sim->getH()*(max(1, sim->getDispNum()/3))));

 win->setWindowTitle(QApplication::translate("hurricane", "Hurricane Visualization"));

 //QPixmap originalPixmap = QPixmap();
 //originalPixmap = QPixmap::grabWidget(win);
 ////originalPixmap = QPixmap::grabWindow(QApplication::desktop()->winId());
 //originalPixmap.save("qttest.png", "png");

 win->show();

 /*
    QImage img(win->size(), QImage::Format_RGB888);
    QPainter painter(&img);
    win->render(&painter);
    img.save("qttest.jpeg");
    */


 int result = app.exec();

 delete win;
 return result;
}

