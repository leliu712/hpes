/* Jonathan Cox
   Clemson University
   Hurricane Visualization Functions
*/

#include "Vector.h"
#include "posPoint.h"
#include "geoFunct.h"
#include "velocityFunct.h"

#include <string>
#include <vector>

#ifndef HURVIS_H
#define HURVIS_H


Vector4d nextPoint(Vector4d curPoint, double steps);
double findDeg(double x, double y);
Vector2d closePointOnPath(Vector4d * projPath1, Vector4d * projPath2, Vector4d curPoint);
void buildAdvisory();
int randCategory(int mean, int std);
double randHurSize(double mean, double std);
Vector4d categoryColor(int category);
Vector2d correctHurrianceLocation(const Vector2d& p, const std::string hurricaneid, const Vector2d& ref);


#endif
