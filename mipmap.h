/*
 * mipmap
 */
#ifndef __MIP_MAP__
#define __MIP_MAP__

#ifdef __APPLE__
# include<GLUT/glut.h>
#else
# include <GL/glut.h>
# include <GL/glu.h>
#endif

#include <vector>
#include "Vector.h"
#include "RBF.h"
using namespace std;

class Mipmap{
 public:
  Mipmap(const Mipmap& m){
   set(m);
  }
  Mipmap(int w, int h):
   width(w),
   height(h){
    mipmap.resize(w*h, 0.0);
   }
  Mipmap(){};
  ~Mipmap(){
  }

  std::vector<Vector2d*> buildGrid(int width, int height, int rows, int cols){
   std::vector<Vector2d*> grid;
   float dw = width / float(cols);
   float dh = height / float(rows);
   float w, h, pw, ph;
   for(int i = 1; i <= rows; i++){
    h = dh * i;
    ph = dh * (i - 1);
    for(int j = 1; j <= cols; j++){
     w = dw * j;
     pw = dw * (j - 1);
     Vector2d* toPush = new Vector2d[4];
     Vector2d llc(pw, ph);
     Vector2d lrc(w, ph);
     Vector2d urc(w, h);
     Vector2d rlc(pw, h);
     toPush[0] = llc;
     toPush[1] = lrc;
     toPush[2] = urc;
     toPush[3] = rlc;
     grid.push_back(toPush);
     //Vector2d c = llc + (urc - llc)  / 2.0;
     //glColor4f(1, 0, 0, 1);
     //glPointSize(5.0);
     //glBegin(GL_POINTS);
     //glVertex3f(c.x, c.y, 0.0);
     //glEnd();
    }
   }
   return grid;
  }

  std::vector<float> nextLevel(std::vector<Vector2d> &trainningSet, 
    std::vector<double> &trainningVal, int scaleFactor,
    RBF rbf,
    std::vector<Vector2d> &nextlevelloc);


  void set(Mipmap m){
   width = m.getWidth();
   height = m.getHeight();
   mipmap = m.getMipmap();
  }


  void initial(int w, int h){
   width = w;
   height = h;
   mipmap.resize(w * h, 0.0);
  }

  int Idx(const int i, const int j){
   return j * width + i;
  }

  void setPixelValue(const int i, const int j, const float val){
   int idx = Idx(i, j);
   mipmap[idx] = val;
  }

  float getPixelValue(const int i, const int j){
   int idx = Idx(i, j);
   return mipmap[idx];
  }

  void setMipmap(std::vector<float> m){
   mipmap = m;
  }

  std::vector<float> getMipmap(){
   return mipmap;
  }

  void printmap(){
   for(int i = 0; i < width; i++){
    for(int j = 0; j < height; j++){
     int idx = Idx(i, j);
     cout << mipmap[idx] << endl;
    }
   }
  }

  int getWidth(){return width;};
  int getHeight(){return height;};

 private:
  int width, height;
  std::vector<float> mipmap;
};
#endif
