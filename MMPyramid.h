/*
 * mipmap pyramid
 */
#ifndef __MM__PYRAMID__
#define __MM__PYRAMID__
#include <vector>
#include "mipmap.h"

class MMPyramid{
 public:
  MMPyramid(int n, Mipmap bl):
   nlevel(n){
    std::cout << "mipmap pyramid level: " << n << std::endl;
    mipmaps.push_back(bl);
   }
  ~MMPyramid(){
  };

 void setBottomLevel(Mipmap mipmap){
  mipmaps[0] = mipmap;
 }
 
 void generatePyramid(){
 }

 private:
  int nlevel;
  std::vector<Mipmap> mipmaps;
};




#endif
