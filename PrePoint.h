/*
 * predicted point class
 * for displaying
 */

#include "Vector.h"

class PrePoint
{
 public:
  PrePoint(){}
  PrePoint(Vector2d _loc, float _opc, int _cat):
   loc(_loc),
   opc(_opc)
  {}
  PrePoint(const PrePoint& _p)
  {
   loc = _p.Loc();
   opc = _p.Opc();
   cat = _p.Cat();
   UDpos = _p.UDPos();
   idx = _p.Idx();
   sd = _p.SD();
   radAvg = _p.RadAvg();
  }

  const Vector2d& Loc() const {return loc;}
  Vector2d& Loc() {return loc;}

  const float& Opc() const {return opc;}
  float& Opc() {return opc;}

  const int& Cat() const {return cat;}
  int& Cat() {return cat;}

  const Vector2d& UDPos() const {return UDpos;}
  Vector2d& UDPos() {return UDpos;}

  const int& Idx() const {return idx;}
  int& Idx() {return idx;}
  
  const float& SD() const {return sd;}
  float& SD() {return sd;}

  const float& RadAvg() const {return radAvg;}
  float& RadAvg() {return radAvg;}

 private:
  Vector2d loc;
  float opc;
  int cat;
  Vector2d UDpos;
  int idx;
  float sd;
  float radAvg;
};


