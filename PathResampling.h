/*
 * Resampling a set representative paths from UD spaces.
 */
#ifndef __PATH_RESAMPLING_H__
#define __PATH_RESAMPLING_H__

#include <QRectF>
#include <QPolygonF>

#include "path.h"
#include "CDFSample.h"
#include "PoissonGenerator.h"
#include "stormcharacteristicinterpolator.h"

#include "dlib/optimization/max_cost_assignment.h"

#include <queue>
#include <chrono>
typedef std::chrono::high_resolution_clock myclock;

typedef std::vector<Vector2d> CenterLine;

// Each partition is represented by a set of sub`regions,
// each subregion is represented by four 2D points
typedef std::vector<Vector2d> SubRegion;
typedef std::vector<SubRegion> Partition;

typedef std::vector<std::vector<std::vector<Vector2d>>> PointsGroup;

struct UDSpace{
 std::vector<std::vector<float>> U;
 std::vector<std::vector<float>> V;
 int nrows, ncols;
 // Computes the area of UD space
 float Area(){
  float area(0);
  for(int i = 0; i < ncols - 1; i++){
   for(int j = 0; j < nrows - 1; j++){
    Vector2d p[4];
    p[0].x = U[i][j]; 
    p[0].y = V[i][j];
    p[1].x = U[i+1][j]; 
    p[1].y = V[i+1][j];
    p[2].x = U[i+1][j+1]; 
    p[2].y = V[i+1][j+1];
    p[3].x = U[i][j+1]; 
    p[3].y = V[i][j+1];
   }
  }
  DrawGridPoints();
  return area;
 }
 // Debug functions
 void DrawCell(const Vector2d v[], const int size){
  glColor4f(1, 0, 0, 1);
  glBegin(GL_POINTS);
  for(int i = 0; i < size; i++){
   glVertex3f((v[i].x + 0.5) * 800, (v[i].y + 0.5) * 800, 0);
  }
  glEnd();
 }

 void DrawGridPoints(){
  glColor4f(1, 0, 0, 1);
  glBegin(GL_POINTS);
  for(int i = 0; i < U.size(); i++){
   for(int j = 0; j < U[i].size(); j++){
    glVertex3f((U[i][j] + 0.5) * 800, (V[i][j] + 0.5) * 800, 0);
   }
  }
  glEnd();
 }
};

class PathResampling{
 public:
  // nodes used for tracking binary structure of centerlines
  struct CenterlineNode{
      int _id;
      CenterlineNode* left;
      CenterlineNode* right;
      CenterlineNode(int id) : _id(id), left(NULL), right(NULL){}
  };

  PathResampling(const int timepoint);
  PathResampling(const int _nrows, const int _ncols, const int _numofUDspaces, const int _numofpaths);
  ~PathResampling();

  void InitPoissonPointsGenerators();
  // time steps [0, 120]
  std::vector<std::vector<Vector2d>> ReadFullEnsembleFromFiles();
  std::vector<std::vector<StormCharacteristicInterpolator::PredictionNode>> ReadFullPredictionsFromCSVFile();
  void ReadOriginalPointsOD();
  // Does resampling
  std::vector<path*> Process();
  // Reads UD spaces
  void ReadUDSpaces();
  // Reads UD and OD grids, constructs cdfsampler, for using invmap 
  void ConstructInvmaps();
  // Computes poisson disk minimum distances at individual UD spaces
  void ComputeUDMinDistances();
  // First path should be generated separatly
  void GenerateFirstPath();
  // Generates rest paths by randomly poisson disk sampling in UD spaces
  void GenerateRestPaths();

  // First generates a setof paths in the OD space maintains the correct speed variation, and a good spatial layout. Then adjusts the predicted locations along paths in individual UD spaces to obtain correct spatial distribution
  void PhysicallyBasedPathsGeneration(const std::vector<Vector4d*>& projPath,
    const std::vector<double>& errorRadii, std::vector<path*>* pathList);
  // Generates rest paths by selecting a horizontal line of samples in UD space
  // projPath is the path with the center of predictions at different points in time
  // pathList needs to be updated  
  void GenerateStructuredPaths(const std::vector<Vector4d*>& projPath, const std::vector<double>& errorRadii,
    std::vector<path*>* pathList);
  void GenerateStructuredPathsByPDSamplingInCircles(const std::vector<Vector4d*>& projPath, const std::vector<double>& errorRadii,
    std::vector<path*>* pathList);
  // 1. Generates Poisson disk subsets for individual time steps
  // 2. Projects Poisson disk subsets to OD space
  // 3. Computes cost/distance matrices of individual pairs of subsets of adjacent time steps
  //    * distance measure should consider speed, bearing, and number of crossings will be induced
  // 4. Connects subsets of adjacent time steps by optimize the corresponding distance matrices, 
  //    using Hugarian Algorithm
  void GenerateStructuredPathsByOptimizeCostMatrix(const std::vector<Vector4d*>& projPath, 
    const std::vector<double>& errorRadii,
    std::vector<path*>* pathList);
  void ConstructPathsByOptimizeCostMatrix(const std::vector<Vector4d*>& projPath);
  // Computes bearing changes matrix of all possible pairs of adjacent path segments
  std::vector<std::vector<float>> ComputeBearingChangesMatrix(const std::vector<Vector2d>& prevPoints, 
    const std::vector<Vector2d>& currPoints,
    const std::vector<Vector2d>& nextPoints);
  // Computes speed change matrix of all possible pairs of adjacent path segments
  std::vector<std::vector<float>> ComputeSpeedChangesMatrix(const std::vector<Vector2d>& prevPoints,
    const std::vector<Vector2d>& currPoints,
    const std::vector<Vector2d>& nextPoints);
  // To minimize the number of crossings, we convert the problem to minimize the total length
  // of possible path, so we compute the matrix stores lengths of individual paths
  // Here if the reference:
  // http://mks.mff.cuni.cz/kalva/putnam/psoln/psol794.html
  std::vector<std::vector<float>> ComputePathLengthMatrix(const std::vector<Vector2d>& prevPoints,
    const std::vector<Vector2d>& currPoints,
    const std::vector<Vector2d>& nextPoints);

  // Computes weighted distance metric, considering speed changes, bearing change, and (maybe) number of crossings will induce
  std::vector<std::vector<float>> ComputeWeightedDistanceMatrix(
    const std::vector<std::vector<float>>& bmatrix,
    const std::vector<std::vector<float>>& smatrix,
    const std::vector<std::vector<float>>& lmatrix);
  // Compute an optimization of the assignments
  std::vector<int> ComputeAssignment(const std::vector<std::vector<float>>& wmatrix);

  // Transforms predicted locations along hurricane forecast paths to individual UD spaces
  void ProjectPredictedLocationsToUDSpaces(const std::vector<Vector4d*>& projPath);
  void ConstructPathsFromParticleSimulation(const std::vector<Vector4d*>& projPath);
  void ReadAdjustedPointsFromFile();
  void ReadPointsFromFiles(std::vector<std::vector<Vector2d>>* points, const int numofsets,
    const string& filenameprefix);

  // Generates stuctured paths by recursively partite the space using simplicial depth center lines
  // returns a list of CenterLines
  std::vector<CenterLine> GenerateStructuredPathsByPartitingODSpace();
  // simplicialDepthValues: simplicial depth values of individual predicted locations at individual time steps
  // nlevel: the maximum levels we can reach
  int RecursivelyComputeSimplicialDepthCenterlines(
    CenterlineNode** currentnode,
    std::queue<std::vector<std::vector<Vector2d>>>* isVisited,
    std::vector<CenterLine>* centerlines,
    std::vector<std::vector<double>>* simplicialDepthValues,
    const int level,
    const int nlevels,
    const std::vector<std::vector<Vector2d>>& points);
  std::vector<CenterLine> ExtractCenterlineSubset(const int subsetsize);
  // Given a set of points, returns a vector stores the associated simplicial depth values, which have 
  // been normalized to [0, 1] by using the ranks
  std::vector<double> ComputeSimplicialDepthValues(const std::vector<Vector2d>& points);
  // Given points at individual time steps, and the corresponding SD values,
  // returns a SD centerline
  CenterLine FindSDCenterLine(const std::vector<std::vector<Vector2d>>& points, 
    const std::vector<std::vector<double>>& sdvals);
  // Given points at individual time steps, and a centerline,
  // divides the points into a left group and a right group
  CenterLine DividePointsIntoLeftRight(const std::vector<std::vector<Vector2d>>& points,
    const CenterLine& centerline, 
    std::vector<std::vector<Vector2d>>* leftpoints,
    std::vector<std::vector<Vector2d>>* rightpoints);
  std::vector<Vector2d> ComputePerpendicularDirsOfSegOfCenterLine(const CenterLine& centerline);
  // Create a dummy centerline by extending the last node
  CenterLine CreateDummyCenterline(const CenterLine& centerline);
  // Given a centerline and a point p,
  // returns -1 if the point is on the left of the centerline, 
  // returns 1 if the point is on the right of the centerline,
  // otherwise returns 0
  int LeftRightOfCenterline(const CenterLine& centerline, const Vector2d& p);
  int LeftRightOfCenterlineGlobal(const CenterLine& centerline, const Vector2d& p);
  // Check left right by considering tangencial direction at a particular time
  int LeftRightOfCenterline(const CenterLine& centerline, const Vector2d& p, const int time);
  // Same to the previous function but considering a time window
  int LeftRightOfCenterline(const CenterLine& centerline, const Vector2d& p, const int time,
    const int window);
  int LeftRightOfCenterline2(const CenterLine& centerline, const Vector2d& p, const int time,
    const int window);
  // Given a centerline, construct the left and right regions of the centerline
  QPolygon ConstructLeftRightRegionsOfCenterline(const CenterLine& centerline);
  // Given a centerline, construct the left and right regions of the centerline
  // time window controls the region of the centerline we are going to consider
  // the total interval should be timewindow * 2 + 1
  QPolygon ConstructLeftRightRegionsOfCenterline(const CenterLine& centerline,
    const int timewindow, const int currentime);
  // Given two end points of a segment, and a point,
  // returns -1 if the point is on the left of the segment,
  // returns 1 if the point is on the right of the segment,
  // otherwise returns 0
  int LeftRightOfSegment(const Vector2d& v0, const Vector2d& v1, const Vector2d& p);
  // Given the centerline, and the perpendicular directions of each segment of the centerline,
  // computes the left and right regions divided by the centerline (represented by rectangles)
  void ComputeLeftRightRegionsOfSegments(const CenterLine& centerline,
    const std::vector<Vector2d>& pdirs,
    std::vector<std::vector<Vector2d>>* leftregions,
    std::vector<std::vector<Vector2d>>* rightregions);
  // Given a set of regions, and a point,
  // return true if the point is in any of these regions
  bool IsInRegions(const std::vector<std::vector<Vector2d>>& regions, const Vector2d& p);
  // Given a region and a point,
  // return true if the point is in the region
  bool IsInRegion(const std::vector<Vector2d>& region, const Vector2d& p);
  // Given a set of centerlines, returns a topological sorted order
  std::vector<std::pair<int, float>> SortCenterlines(const std::vector<CenterLine>& centerlines);
  // Given a set of centerlines and their topolofical order,
  // returns a set of partitions
  std::vector<Partition> ComputePartitions(const std::vector<CenterLine>& centerlines,
    const std::vector<std::pair<int, float>>& sortedindices);
  // Given a point and a partition,
  // return true if the point is in the partition
  bool IsInPartition(const Vector2d& p, const Partition& partition);
  // Given a set of partitions, and selected points at different time steps,
  // groups these points to individual partitions
  std::vector<std::vector<std::vector<Vector2d>>> GroupPoints2Partitions(
    const std::vector<std::vector<Vector2d>>& points,
    const std::vector<Partition>& partitions);
  // Simplifies centerlines to remove "noises" using psimpl lib
  // http://psimpl.sourceforge.net/documentation.html
  CenterLine ReumannWitkamSimplifier(const CenterLine& cl, const float tolerance);
  CenterLine LangSimplifier(const CenterLine& cl, const float tolerance, const unsigned lookahead);
  CenterLine OpheimSimplifier(const CenterLine& cl, const float minimum, const float maximum);
  CenterLine PerpendicularDistanceSimplifier(
    const CenterLine& cl, const float tolerance, const unsigned repeat);
  CenterLine RadialDistanceSimplifier(const CenterLine& cl, const float tolerance);
  CenterLine DouglasPeuckerNSimplifier(const CenterLine& cl, const unsigned count);
  CenterLine DouglasPeuckerSimplifier(const CenterLine& cl, const float tolerance);
  CenterLine SimplifyCenterline(const CenterLine& cl, const int typeofalgorithm);
  Vector2d Deboor(const int k, const int degree, const int i, const float u, const float knots[], 
    Vector2d* coeff);
  // B Spline curve generator
  // smoothiness: num of points we would like to sample along the curve
  CenterLine BSplineCurve(const std::vector<Vector2d>& controlPolygons,
    const int smoothiness);
  void SimplifyCenterlines(); 
  // Check whether a centerline has self intersection 
  bool IsSelfIntersect(const CenterLine& cl, std::vector<Vector2d>* intersections);
  bool IsSelfIntersect(const CenterLine& cl, Vector2d* intersections,
    int* id0, int* id1);
  CenterLine RemoveSelfLoop(const CenterLine& cl);
  // Given a specific time, returns predicted locations included in the subset at this time
  std::vector<Vector2d> PointSubset(const int time);

  // Qt functions
  // Computes bounding box at indiviudal points in time
  // stored in qrect 
  void ComputeBoundingBoxes(const std::vector<std::vector<Vector2d>>& points);

  Vector2d GetStartPos();

  void GenerateNewPath();
  void GeneratePoissonDiskPointsInUDSpaces();
  void DrawSelectedPoints();
  void DrawResampledPathsWithStormCharacteristics();
  void DrawSDCenterLinesWithStormCharacteristic(const std::string& type);
  void DrawResampledPathsWithStormSizes();
  void DrawResampledPathsWithStormSizesSub();
  void DrawPathsSubset();
  void DrawGeneratedLocations(const int UDId);
  void DrawODPoints(const std::vector<Vector2d>& points);
  void DrawPointsGroup(const std::vector<std::vector<Vector2d>>& group);
  void DrawODPointsWithSimplicialDepthValues(const std::vector<Vector2d>& points, 
    const std::vector<double>& sdvals);
  void DrawDebugInfo();
  void DrawSDCenterLines();
  void DrawSDCenterLine(const CenterLine& centerline);
  void DrawSDCenterLinePointAtTimeStep(const int timestep);
  void DrawOriginalPathsEnsemble();
  void DrawOriginalPointsEnsemble(const int time);
  void DrawPartition(const Partition& partition);
  void DrawRegion(const std::vector<Vector2d>& region);
  void DrawTriangle(const Vector2d& p0, const Vector2d& p1, const Vector2d& p2);
  void DrawProjectedPointsInUDSpaces(const int UDId);
  void DrawBoundingBoxes();
  void DrawBoundingBox(const int i);
  void DrawCircle(float x, float y, float radius, const int interval = -1);
  void WriteGeneratedPointsToFiles();
  // Check whether the new formed path segment has intersections with generated paths
  bool HasIntersection(const float p0_x, const float p0_y, const float p1_x, const float p1_y, path* curPath);
  bool HasIntersection(path* curPath);
  // Check whether the new formed path has a sharp turn 
  bool HasSharpTurn(path* curPath, Vector2d newLoc);
  bool GetLineIntersection(const float p0_x, const float p0_y, const float p1_x, const float p1_y, 
    const float p2_x, const float p2_y, const float p3_x, const float p3_y, float *i_x, float *i_y);
  bool GetSegmentsIntersection(const Vector2d& p0, const Vector2d& p1, const Vector2d& q0, const Vector2d& q1, Vector2d* i0, Vector2d* i1);
  float Point2LineDistance(const Vector2d& p, const Vector2d& l0, const Vector2d& l1);
  float SignedPoint2LineDistance(const Vector2d& p, const Vector2d& l0, const Vector2d& l1);
  std::vector<int> FilterClosePoints(const std::vector<Vector2d>& points, const std::vector<float> radii, const float scalefactor = 1.0);
  void TranslateCenterLines2Geo();
  void TranslateCenterLines2Screen();

  // Functions for the experiment
  void TransformSDCenterLines();
  void OutputMeanTrack2File();

  // Utility functions
  template<typename T>
  static void PrintMatrix(const std::vector<std::vector<T>>& _matrix){
   for(const auto& m : _matrix){
    for(const auto& v : m){
     std::cout << v << "\t";
    }
    std::cout << "\n" << std::endl;
   }
  }
  template<typename T>
  static void PrintVector(const std::vector<T>& _vector){
   for(const auto& v : _vector){
    std::cout << v << " ";
   }
   std::cout << "\n" << std::endl;
  }

  // for interpolations
  void ComputeStormcharacteristicsInterpolations();

  // statistics verification
  void VerifyStatistics();
  void ComputeSDFieldFromAllEnsembleSamples();
  void ComputeSDFieldsFromOLS();
  StormCharacteristicInterpolator* BuildRBFFromFullEnsemble(const int time);
  void BuildRBFsFromFullEnsemble();
  void BuildRBFFromSubset(std::vector<Vector2d>& fullset, const std::string& subsetfilename);
  void ComputeSDFieldsFromResampling();
  void BuildRBFFromSampledFullEnsemble();
  void BuildRBFFromSampledSubset(const bool recomputeradii = false);
  std::vector<double> RecomputeRBFRadiiBasedOnRecursion();
  void ExtractLeftRightPointsOfCenterline(CenterlineNode* currentnode, std::vector<Vector2d>* points, const std::vector<CenterLine>& centerlines);

  bool IsShuffled;
  std::vector<Vector4d> subsamplingdata;

 private:
  std::vector<UDSpace> UDSpaces;
  int ncols, nrows;
  int numofUDspaces;
  int numofTimeSteps;
  int sampledTimeSteps;
  int numofpaths; // desired number of paths in the subset
  int numofRecLevels;
  int leftrightWindow;
  std::vector<path*> pathSubset;

  // Data structure for using invmap
  std::vector<ProjectMap> projmaps;

  // PoissonPointsGenerators at different points in time
  std::vector<PoissonPointsGenerator> generators;
  std::vector<float> minDistances; // poisson disk min distances at individual UD spaces

  // Vectors store generated points in UD spaces at different points in time
  std::vector<std::vector<Vector2d>> GeneratedPointsUD;
  // Vectors store generated points in OD spaces at different points in time
  std::vector<std::vector<Vector2d>> GeneratedPointsOD;

  // Vectors store the original full ensemble of points in OD space
  std::vector<std::vector<Vector2d>> OriginalPointsOD;

  // Projected predicted locations in UD spaces
  std::vector<std::vector<Vector2d>> projectedPoints;

  // Reference points in circles
  std::vector<Vector2d> ReferencePoints;

  // timer for generating seeds of PossionPointsGenerators
  myclock::time_point beginning;

  // Adjusted locations from particle simulation
  std::vector<std::vector<Vector2d>> AdjustedPoints;

  // simplicial depth centerlines
  std::vector<CenterLine> SDCenterLines;
  std::vector<CenterLine> SDCenterLinesShadow;
  std::vector<CenterLine> SDCenterLinesShadowLatLon;

  // Convex hulls of points at different points in time
  std::vector<QRectF> boundingboxes;

  StormCharacteristicInterpolator* stormcharacteristicsinterpolator;
  std::vector<StormCharacteristicInterpolator*> stormcharacteristicsinterpolators;

  int _timepoint;

  CenterlineNode* _centerlineroot;

  StormCharacteristicInterpolator::PredictionNode _beginningnode;

};

#endif
