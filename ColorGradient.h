/*
 * ColorGradient class for build a heat map
 */
#ifndef __COLOR_GRADIENT__
#define __COLOR_GRADIENT__
#include "Vector.h"
#include <vector>


class ColorGradient{
 public:
  ColorGradient(){createDefaultHeatMapGradient();}

  void createDefaultHeatMapGradient();

  void normalizeColors();

  void addColorPoint(float red, float green, float blue, float val);
  void clearGradient();

  void getColorAtValue(const double val, float &red, float &green, float &blue);

  std::vector<Vector4d> color; // r, g, b, gradient value
};

#endif
