#ifndef __PATH__CBD__H
#define __PATH__CBD__H
/*
 * contour/curve box plot
 */

#include "path.h"
#include "Matrix.h"
//#include <vtkSmartPointer.h>
//#include <vtkPoints.h>
//#include <vtkLine.h>
//#include <vtkPolygon.h>
#include <algorithm>
#include <vector>
#include <iostream>


typedef pair<double, int> my_pair;
bool sort_predD(const my_pair& left, const my_pair& right);
vector<int> getSortedIndex(const vector<double>& v);


class PathCBD{
 public:
  PathCBD(std::vector<path*> _e, Matrix _M);
  ~PathCBD();

  void Analysis();
  void OptAnalysis();
  void computeDataDepth(path* p, std::vector<path*> PathSet, int idx);

  template <typename Iterator>
   bool next_combination(const Iterator first, const Iterator k, const Iterator last);
  Matrix getCombinationMatrix( unsigned int n,  unsigned int k);

  unsigned long Choose( unsigned int n,  unsigned int k);
  bool IsElement(int p, Matrix M){
   for(int i = 0; i < M.nrows(); i++)
    if(p == M[0][i])
     return true;
   return false;
  }

  int nChoosek(int n, int k){
   if(k > n) return 0;
   if(k * 2 > n) return k = n-k;
   if(k == 0) return 0;
   int result = n;
   for(int i = 2; i <= k; i++){
    result *= (n - i + 1);
    result /= i;
   }
   return result;
  }

  //vtkSmartPointer<vtkPolygon> getTriangle(std::vector<Vector2d> v);
  //std::vector< vtkSmartPointer<vtkPolygon> > getBand(std::vector<path*> PSet);
  //void setMatrix(Matrix _M){
  // M = _M;
  //}

  Vector3d BaryCentricCoor(Vector3d x, Vector3d p0, Vector3d p1, Vector3d p2){
   Vector3d e01 = p0 - p1;
   Vector3d e02 = p2 - p0;
   Vector3d e12 = p2 - p1;
   Vector3d n = e01 % e02;
   n = n / n.norm();
   float A  = 0.5 * (e01 % e02) * n;
   float Au = 0.5 * (e01 % (x - p1)) * n;
   float Av = 0.5 * ((x - p0) % e02) * n;
   float Aw = 1 - Au - Av;
   return Vector3d(Au, Av, Aw);
  }

  int Inside(Vector3d x, Vector3d p0, Vector3d p1, Vector3d p2){
   if(x == p0 || x == p1 || x == p2)
    return 0;
   Vector3d ws = BaryCentricCoor(x, p0, p1, p2);
   if(ws[0] > 0 && ws[0] < 1 &&
     ws[1] > 0 && ws[1] < 1 &&
     ws[2] > 0 && ws[2] < 1)
    return 1;
   else
    return 0;
  }

  std::vector<int> getIdx(){ return SortedIdx; }
  std::vector<int> getSegmentIdx(int segment){
   std::vector<double> tmp;
   for(int i = 0; i < maxpathlength; i++){
    tmp.push_back(segmentProbs[segment][i]);
   }
   std::vector<int> rtn = getSortedIndex(tmp);
   return rtn;
  }

  int getMaxPathLength(){ return maxpathlength; }
  static std::vector<int> sortSegmentIdx(std::vector<double> tmp);

  void fastPathSimplexDepth();
  std::vector<double> fastSegmentSimplexDepth(const int time);
  std::vector<double> fastSegmentSimplexDepth2(const int time, std::vector<Vector2d>);
  static std::vector<double> fastSegmentSimplexDepth3(const std::vector<Vector2d> );
  static double fastPointSimplexDepth(std::vector<Vector2d> X, Vector2d P);
  static std::vector<double> constructALPHA(std::vector<Vector2d> X, Vector2d P, int& NT);
  static unsigned int mergesort(std::vector<double> alpha, int NU);
  static unsigned long long K(const unsigned int M, const unsigned int j);

  std::vector<double> getProbs(){return Probs;}

 private:
  std::vector<path*> ensembles;
  std::vector<double> Probs;
  std::vector<double*> segmentProbs;
  int maxpathlength;
  Matrix M;

  std::vector<int> SortedIdx;
  int combinationSize;

};


#endif
