/*
 * simulation.cpp
 *
 *  Created on: Jan 16, 2011
 *      Author: Jonathan Cox
 *      Clemson University
 */

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#  include <GL/glu.h>
#endif

#include <algorithm>
#include <limits.h>

#include "simulation.h"

extern int nlayers;

extern int colorClosest;
extern unsigned int counter;

extern int drawMethod;
extern int totalDrawMethods;

static int heatmapWidth =  200;
static int heatmapHeight = 200;
//static int heatmapWidth =  1024;
//static int heatmapHeight = 869;

static int drawContourMethod = -1;
static int heatmapMethod = 0;
static int mipmapSwith = 0;

simulation::simulation(int htd, double l1, double l2, double l3, double l4, double h, double a, double sda, double tl1, double tl2, double ml1, double ml2, int im){

 int i;
 std::vector<Vector2d*> toPush;

 hurToDisplay = htd;
 lonL = l1;
 lonR = l2;
 latB = l3;
 latT = l4;

 hours = h;
 alpha = a;
 pathOpacity = 0.75;

 stdDevAlpha = sda;

 totalLat = tl1;
 totalLon = tl2;
 maxLat = ml1;
 maxLon = ml2;

 for(i=0; i < 361; i++){
  bearBin.push_back(toPush);
 }
 curHur = 0;
 initMethod = im;

 turnOnErrorConeCircle = false;

}

// Draw the generated paths for each advisory
void simulation::drawGenPaths(){

 //adv->drawGenPaths();
 if( drawMethod == 0 ){
  adv->drawGenPathsRainDrop();
  if(turnOnErrorConeCircle == true)
   adv->drawErrorConeRadius();
  //adv->drawGenPathsEyePoint();
 }
 else{
  //adv->drawForecastPath(); // center line
  //adv->drawSmoothOutline();
  //if(drawContourMethod == -1)
  // adv->drawDataDepthPlot();
  //else if(drawContourMethod == 1){
  // adv->drawGenPaths();
  // adv->drawDataDepthPlotofAllTime();
  //}

  //adv->translateToGeo();
  if(heatmapMethod == 0)
  {
   //adv->renderEllipseFromFile(TimeFrame, width, height);
   //adv->renderUnionconeFromFiles(TimeFrame);
   //adv->drawDataDepthPlotofTime();
   //adv->drawDataDepthPlotRBFwithNHC(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon, TimeFrame, RBFKERNELFACTOR, NHCinputprefix + NHCfilename + "_Data.txt");
   //adv->drawErrorConeRadius();
   adv->GenerateStructuredPaths();
   //adv->GeneratePathsByResamplingUDSpaces();
   //adv->constructPathsSubset(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon, TimeFrame, RBFKERNELFACTOR);
   //adv->drawGenPaths();
//#ifdef NHC_DATA
//   cout << "here" << endl;
//   for(int i = 1; i < 7; i++)
//   {
//    adv->drawDataDepthPlotRBFwithNHC(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon, i, RBFKERNELFACTOR);
//   }
//#else
//   if(adv->pathList.size() >= 999)
//   {
//    for(int i = 1; i < 7; i++)
//    {
//     adv->drawDataDepthPlotRBFwithNHC(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon, i, RBFKERNELFACTOR);
//    }
//   }
//   else
//    adv->drawGenPaths();
//#endif
  }
  else if(heatmapMethod == 1){
   //adv->drawDataDepthPlotofTime();
   //adv->renderFromBinaryFile();
   adv->renderUnionconeFromFiles();
   //adv->drawDataDepthPlotRBFwithNHC(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon, TimeFrame);
   //adv->drawDataDepthPlotRBF(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon);
   //adv->drawPureDataDepthPlot(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon);
   //adv->drawDataDepthPlotofTime();
   //adv->drawDataDepthPlotRBF(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon);
   //adv->drawDataDepthPlotofTimeSplat(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height);
   //adv->drawHeatMapGrid(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height);
  //else if(heatmapMethod == 2)
   //adv->drawDataDepthPlotRBF(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height, totalLat, totalLon, maxLat, maxLon);
  // adv->drawSplatHeatMapGrid(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height);
   //adv->drawSmoothOutline();
  }


  //adv->drawTrueCone();
  //adv->drawSmoothOutline();
  //adv->drawDataDepthPlotofTime();
  //adv->drawGenPaths();
  //adv->drawExperimentPoints();
  //adv->drawHeatMapGrid(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height);
  //adv->drawHeatMapGridDataDepth(heatmapGrid, width / float(heatmapWidth), height / float(heatmapHeight), heatmapWidth, heatmapHeight, width, height);
 } 
 //adv->drawGenPaths();
}

int simulation::drawGenPathsTrail(int step){
 step = adv->drawGenPathsTrail(step);
 return step;
}

// Draw the heat map for each advisory
void simulation::drawHeatMap(){
 adv->drawHeatMap();
}

// Draw the forecasted path and the error cone for each advisory
void simulation::drawForecastPath(){
 //adv->drawErrorSmooth();
 //adv->drawTrueConeOutline();
 //adv->drawTrueCone();
 //adv->drawExperimentPoints();
 //adv->drawSmoothOutline();
 adv->drawSmoothCone();
 for(int i = 0; i < nlayers; i++){
  //adv->drawMultiCones(i);
  //adv->drawMultiSmoothCones(i);
 }
 //adv->drawBoxplotCones();
 //adv->drawForecastPath(); // center line
 //adv->drawErrorConeRadius();
}

// Draw the outliers of contour box plot
void simulation::drawOutliers(){
 adv->drawOutliers();
}

// Draw all of the data points from historical data
void simulation::drawDataPoints(){
 std::vector<posPoint*>::iterator posListIt;
 Vector2d draw1, draw2;
 int i;

 glColor4f(1.0, 0.0, 0.0, 0.3);
 glLineWidth(0.75);
 glPointSize(3.0);

 for(i=0; i < hurToDisplay; i++){
  if(hurToDisplay == 6){
   glViewport( (i%3)*width, (int)(floor(i/3)+1)%2*height, width, height);
  }
  for (posListIt = positionList.begin(); posListIt != positionList.end(); posListIt++) {
   draw1.x = (*posListIt)->getLon();
   draw1.y = (*posListIt)->getLat();

   draw2 = locateDestination(draw1.x, draw1.y, 15.0, 15.0, (int)(*posListIt)->getDeg());

   draw1 = translateToScreen(draw1.x, draw1.y);
   draw2 = translateToScreen(draw2.x, draw2.y);

   glBegin(GL_POINTS);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glEnd();
   glBegin(GL_LINES);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glEnd();
  }
 }
}

// Draw all of the paths from historical data
void simulation::drawDataPaths(){
 std::vector<path*>::iterator pathIt;
 std::vector<Vector2d*>::iterator pos1;
 std::vector<Vector2d*>::iterator pos2;
 Vector2d draw1, draw2;
 float lineWidth;
 int i;

 // Draw all data tracks
 for(i=0; i < hurToDisplay; i++){
  if(hurToDisplay == 6){
   glViewport( (i%3)*width, (int)(floor(i/3)+1)%2*height, width, height);
  }
  for (pathIt = dataPathList.begin(); pathIt != dataPathList.end(); pathIt++) {
   pos2 = (*pathIt)->posList.begin() + 1;
   lineWidth = 1.0;
   glColor4f((*pathIt)->getInt().x, (*pathIt)->getInt().y,
     (*pathIt)->getInt().z, 0.25);
   for (pos1 = (*pathIt)->posList.begin(); pos2
     != (*pathIt)->posList.end(); pos1++) {
    glColor4f((*pathIt)->getInt().x, (*pathIt)->getInt().y,
      (*pathIt)->getInt().z, 0.25);
    glLineWidth(lineWidth * 1.0);
    draw1.x = (*pos1)->x;
    draw1.y = (*pos1)->y;
    draw2.x = (*pos2)->x;
    draw2.y = (*pos2)->y;

    glBegin(GL_LINES);
    glVertex3f(draw1.x, draw1.y, 0.0);
    glVertex3f(draw2.x, draw2.y, 0.0);
    glEnd();
    pos2++;
   }
  }
 }
}

// Prints the contents of the bearing bin structure
void simulation::printBearBin(){
 std::vector<Vector2d*>::iterator _d;
 int i, z, total;
 int maxCount, minCount, zeroCount;
 minCount = 1000000;
 maxCount = -1;
 zeroCount = 0;
 total = 0;

 cout << "Printing information on bearing bins:\n";
 for(i = 0; i < (int)bearBin.size(); i++){
  cout << "  Bin " << i << ": " << bearBin[i].size() << " elements\n";
  z = 0;
  total += bearBin[i].size();

  // Check for minimum count
  if( (int)bearBin[i].size() < minCount ){
   minCount = bearBin[i].size();
  }

  // Check for maximum count
  if( (int)bearBin[i].size() > maxCount ){
   maxCount = bearBin[i].size();
  }
  // Check for no elements
  if( bearBin[i].size() == 0 ){
   zeroCount++;
  }

  // Loop through elements
  for(_d = bearBin[i].begin(); _d != bearBin[i].end(); _d++){
   if(z == 0){
    cout << "     ";
   }
   if(z == 10){
    cout << *(*_d) << "\n     ";
    z = 0;
   }
   else if(_d == bearBin[i].end()-1){
    cout << *(*_d) << "\n";
   }
   else{
    cout << *(*_d) << ", ";
   }
   z++;
  }
 }
 cout << "Total elements: " << total << "\n";
 cout << "   Max bin count: " << maxCount << "\n   Min bin count: " << minCount << "\n";
 cout << "   Bins with no elements: " << zeroCount << "\n";
}

void simulation::buildExp(){
 unsigned int i;

 //int index[] = {5, 3, 1, 2, 0, 4};
 int index[] = {0, 1, 2, 3, 4, 5};

 for(i=0; i < (unsigned int)(advList.size()/2); i++){
  advErrorList.push_back(index[i]); 
  advPathList.push_back(index[i]);
 }

 for(i=0; i < advErrorList.size(); i++){
  if(initMethod == 0){
   advCombList.push_back(advPathList[i]);
   advCombList.push_back(advPathList.size()+advErrorList[i]);
  }
  else{
   advCombList.push_back(advPathList.size()+advErrorList[i]);
   advCombList.push_back(advPathList[i]);
  }
 }

 //cout << "Combined list: ";
 for(i=0; i < advCombList.size(); i++){
  cout << "  " << advCombList[i];
 }
 cout << endl;

 adv = advList[abs(advCombList[curHur])];
}

void simulation::buildHeatMapGrid(){
 float dw = width / float(heatmapWidth);
 float dh = height / float(heatmapHeight);
 float w, h;
 float pw, ph;
 cout << dh << " " << dw << endl;
 cout << width << " " << height << endl;


 for(int i = 1; i <= heatmapHeight; i++){
  h = dh * i;
  ph = dh * (i - 1);
  for(int j = 1; j <= heatmapWidth; j++){
   w = dw * j;
   pw = dw * (j - 1);
   Vector2d* toPush = new Vector2d[4];
   Vector2d llc(pw, ph);
   Vector2d lrc(w, ph);
   Vector2d urc(w, h);
   Vector2d rlc(pw, h);
   toPush[0] = llc;
   toPush[1] = lrc;
   toPush[2] = urc;
   toPush[3] = rlc;
   heatmapGrid.push_back(toPush);
  }
 }
}

void simulation::setDrawContourMethod(){
 drawContourMethod *= -1;
}

void simulation::setHeatmapMethod(){
 heatmapMethod = (heatmapMethod + 1) % 3;
}


void simulation::setMorphology(){
 adv->setMorphology();
}


void simulation::adjustKernelFactor(int mode){
 adv->adjustKernelFactor(mode);
}

void simulation::MipmapSwith(){
 mipmapSwith ++;
 mipmapSwith = mipmapSwith % 3;
 adv->swithmipmap(mipmapSwith);
}

bool simulation::outputImage(){
 return adv->outputImage();
}


void simulation::setRBFKernelFactor(float f){
 RBFKERNELFACTOR = f;
}


