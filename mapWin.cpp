#include <QImage>
#include <QTimer>
#include <QEvent>
#include <QString>
#include <QFileDialog>
#include <QMouseEvent>

#include <QPixmap>
#include <QPoint>
#include <QPainter>
#include <QApplication>
#include <QDesktopWidget>
#include <QGLContext>
#include <QGLFormat>
#include <QRect>
#include <QPoint>
#include <QSize>
#include <QProcess>
#include <Magick++.h>

#include <QDebug>
#include <QPushButton>

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#  include <GL/glu.h>
#endif

// This flag used to generate images for vis group
//#define EXP_FLAG

#include <fstream>
#include <stdlib.h>
#include <climits>

#include "mapWin.h"
#include "posPoint.h"
#include "advisory.h"
#include "path.h"
#include "P6Image.h"
#include "simulation.h"
#include "Matrix.h"
#include "ui.h"
#include "PathResampling.h"

#include "gl2ps.h"

using namespace Magick;
int frameID = 0;

extern simulation * sim;
extern ui * userInt;
extern PathResampling* pathresampling;

// Map texture
extern GLuint textures[8];
extern P6Image * imageMap;

// Display mode variables
extern int displayDataPoints;
extern int displayDataPaths;
extern int displayProjectedPath;
extern int displayGenPaths;
extern int heatMap;
extern int interactive;
extern int hitButton;
extern int showCurPath;
extern int chooseForward;
extern int stall;
extern int preOnly;
extern int colorClosest;
extern int averageOnAll;

extern int totalPaths;
extern double omega;
extern double userSig;   //  Starts at error cone radius at 69 hours
extern int curAdvisory;

extern path * testPath;
extern path * curPath;

extern double speedRatio;
extern double bearRatio;

// For random number generation
extern unsigned int counter;

extern int drawMethod;
extern int totalDrawMethods;

const string outputFile("testResults.txt");

static int timecounter = -1;
static int drawcount = 0;

// NHC data repository
extern string InputPrefix;
extern string OutputPrefix;
extern string DatafileName;
extern float OilRigLoc;

extern int PathVisMode;
extern int RepSubsetSize;
extern int SampledTimePoint;

mapWin::mapWin(QWidget * parent): QGLWidget(parent){
 timer = new QTimer(this);
 connect(timer, SIGNAL(timeout()), SLOT(update()));
 timer->setInterval(0); // update every 0.01 second
 timer->setSingleShot(false);
 timer->start();

 cout << "DB: " << format().doubleBuffer() << "   rgba: " << format().rgba()
      << "   depth: " << format().depth() << endl;


 butMin.set(sim->getW()-125, sim->getH()-75);
 butMax.set(sim->getW()-25, sim->getH()-25);

 butMinBord.set(sim->getW()-127, sim->getH()-77);
 butMaxBord.set(sim->getW()-23, sim->getH()-23);

 butTextPos.set(sim->getW()-102, sim->getH()-45);
 buttonPressed = 0;
 completed = 0;
 step = 0;
 timeThreshold = 10;

 // Init time slider
 horizontalsliders = new SlidersGroup(Qt::Horizontal, tr("Time Slider"));
 // timeslider = new QSlider(Qt::Horizontal, this);
 // timeslider->setTickInterval(72);
 // timeslider->setSingleStep(1);
 // timeslider->setTickPosition(QSlider::TicksBelow);
 // timeslider->setMinimumWidth(300);
 // timeslider->move(QPoint(500, 500));

 printStartExp();
}

mapWin::~mapWin(){
 makeCurrent();
}

void mapWin::initializeGL(){
 glClearColor(0.0, 0.0, 0.0, 1.0);
 gluOrtho2D(0.0, sim->getW(), sim->getH(), 0.0);
 glClear(GL_COLOR_BUFFER_BIT);
 glEnable(GL_TEXTURE_2D);
 glPolygonMode(GL_FRONT, GL_FILL);
 glPolygonMode(GL_BACK, GL_FILL);
 glDisable(GL_DEPTH_TEST);
 glShadeModel(GL_SMOOTH);

 glGenTextures(8, textures);
 setAutoFillBackground(false);
}

void mapWin::resizeGL(int w, int h){
 glViewport(0, 0, w, h);

 glMatrixMode(GL_PROJECTION);
 glLoadIdentity();
 gluOrtho2D(0.0, w, h, 0.0);

 glMatrixMode(GL_MODELVIEW);
 glLoadIdentity();
 imageOpen();
 setFocus();
}

void mapWin::paintGL(){
 std::vector<Vector2d*>::iterator pos1;
 std::vector<Vector2d*>::iterator pos2;
 std::vector<posPoint*>::iterator posListIt;
 std::vector<path*>::iterator pathIt;
 std::vector<advisory*>::iterator advIt;
 Vector2d draw1, draw2;
 float lineWidth;


 lineWidth = 0.5;

 glClear(GL_COLOR_BUFFER_BIT);
 //if (displayProjectedPath == 1) {
 //sim->drawForecastPath();
 //}

 if(interactive == -1){
  glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
  glColor4f(0.0f, 0.0f, 0.0f, 0.0f);
  // Draw normal display with generated paths
  if(heatMap == -1){
#ifndef EXP_FLAG
   glEnable(GL_TEXTURE_2D);
   // Draw rectangle for background image
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_FILL);
   glBindTexture(GL_TEXTURE_2D, textures[0]);
   glBegin(GL_POLYGON);
   glTexCoord2f(0.0, 0.0);
   glVertex3f(0.0, 0.0, 0.0);
   glTexCoord2f(1.0, 0.0);
   glVertex3f(sim->getW(), 0.0, 0.0);
   //glTexCoord2f(1.0, 0.771484375);
   glTexCoord2f(1.0, 0.8486328125);
   glVertex3f(sim->getW(), sim->getH(), 0.0);
   glTexCoord2f(0.0, 0.8486328125);
   glVertex3f(0.0, sim->getH(), 0.0);
   glEnd();
   glDisable(GL_TEXTURE_2D);
#endif

   glEnable(GL_BLEND);
   glEnable(GL_LINE_SMOOTH);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

   // Display generated paths
   /*if(sim->getCurrentAdv() < sim->advCombList.size()/2){
     if (displayGenPaths == 1) {
     if(colorClosest > 0){
     sim->drawGenPathsClosest();
     }
     sim->drawGenPaths();
     }
     }*/

   //step = sim->drawGenPathsTrail(step);
   pathresampling->DrawDebugInfo();
   sim->drawGenPaths();
   //if(sim->outputImage())
   {
    QPixmap qpixmap = QPixmap();
    QRect qrect = QApplication::desktop()->availableGeometry(this);
    QPoint qpoint = this->mapToGlobal(this->pos());
    QSize qsize = this->size();
    //cout << qsize.height() << " " << qsize.width() << endl;
    //cout << qrect.x() << "  " << qrect.y() << " " << qrect.width() << " " << qrect.height() << endl;
    //cout << qpoint.x() << " " << qpoint.y() << endl;

    qpixmap = QPixmap::grabWindow(QApplication::desktop()->winId(),
                                  qpoint.x(), qpoint.y(), qsize.width(), qsize.height());
    //qpixmap = QPixmap::grabWidget(this);
    char* filename = new char[200];
    stringstream ss(stringstream::in | stringstream::out);
    ss << OilRigLoc;
    string tmps = ss.str();
    string str = OutputPrefix + "Images/Icon/" + tmps + "/" + DatafileName + "_" + tmps + "_Icon_%05d.png";
    const char* cstr = str.c_str();
    //if(drawcount < 10)
    // sprintf(filename, cstr + "%04d.png", drawcount);
    //else if(drawcount < 100)
    // sprintf(filename, cstr + "%04d.png", drawcount);
    //else if(drawcount < 1000)
    // sprintf(filename, cstr + "%04d.png", drawcount);
    //else if(drawcount < 10000)
    // sprintf(filename, cstr + "%4d.png", drawcount);
    sprintf(filename, cstr, drawcount - 15);
    //if(drawcount - 15 < 0)
    // qpixmap.save(filename, "jpg");
    //else
    // qpixmap.save(filename, "PNG");
    //cout << "finish outputing image: " << filename << " " << drawcount <<  endl;
    drawcount ++;
    //if(drawcount > 1200){
    // exit(0);
    //}
   }

   glDisable(GL_LINE_SMOOTH);
   glDisable(GL_BLEND);
   glEnable(GL_TEXTURE_2D);
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_FILL);
  }
  // Draw heat map
  else{
   glDisable(GL_TEXTURE_2D);
   // Draw rectangle for background image
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_FILL);
   glEnable(GL_LINE_SMOOTH);
   glEnable(GL_BLEND);
   glColor3f(0.0f, 0.0f, 0.0f);
   glBegin(GL_POLYGON);
   glVertex3f(0.0, 0.0, 0.0);
   glVertex3f(sim->getW(), 0.0, 0.0);
   glVertex3f(sim->getW(), sim->getH(), 0.0);
   glVertex3f(0.0, sim->getH(), 0.0);
   glEnd();
   // Display generated paths
   if (displayGenPaths == 1) {
    sim->drawHeatMap();
   }
  }
 }

 // In interactive mode
 glDisable(GL_TEXTURE_2D);
 glEnable(GL_BLEND);
 glEnable(GL_LINE_SMOOTH);
 glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

 // Display the projected path
 if(sim->getCurrentAdv() >= (int)(sim->advCombList.size()/2)){
  if (displayProjectedPath == 1) {
   sim->drawForecastPath();
  }
 }

 // Display all data points
 if (displayDataPoints == 1) {
  sim->drawDataPoints();
 }

 // Display all previous paths
 if (displayDataPaths == 1) {
  sim->drawDataPaths();
 }

 if(showCurPath > 0){
  drawCurPath();
 }

 //#ifndef EXP_FLAG
 //userInt->drawChips();
 //userInt->drawTargetArea(this);
 //userInt->drawSectors(this);
 //userInt->setSliderPos(sim->TimeFrame / 3 / 2 / 2);
 //userInt->setSliderPos(sim->TimeFrame);
 //userInt->drawSlider();
 //drawLabel();
 //drawNext();
 //#endif

 vector<Vector4d> colors;
 vector<string> labels;
 QPainter painter(this);
 getColorBarColorsLabels(&colors, &labels);
 if(PathVisMode == 0){
  //userInt->drawHorizontalColorBar(8, 8, 55, 20, colors, labels, this, &painter);
 }
 Vector2d hurstartpos = pathresampling->GetStartPos();
 hurstartpos = correctHurrianceLocation(hurstartpos, DatafileName, hurstartpos);
 QString hurtimestamp = getHurTimeStamp(DatafileName);
 //userInt->drawTimeStamp(int(hurstartpos.x), int(hurstartpos.y), hurtimestamp, this, &painter);

 glDisable(GL_BLEND);
 glDisable(GL_LINE_SMOOTH);

 //#ifndef EXP_FLAG
 //userInt->drawChipText(this);
 //#endif

 QImage frameimage = this->grabFrameBuffer(true);

 stringstream ss(stringstream::in | stringstream::out);
 ss << OilRigLoc;
 string tmps = ss.str();
 string str;

 if(PathVisMode == 0){
  str = OutputPrefix + "pathvis/" + DatafileName + "_completevis_15paths_" + to_string(SampledTimePoint) + "hours_" + tmps + ".png";
 } else if(PathVisMode == 1){
  str = OutputPrefix + "pathvis/" + DatafileName + "_pathonlyvis_" + to_string(RepSubsetSize) + "paths_" + to_string(SampledTimePoint) + "hours_" + tmps + ".png";
 }
 const char* cstr = str.c_str();
 //frameimage.save(cstr, "PNG", 100);
 cout << cstr << endl;
 //exit(0);
}

void mapWin::drawNext(){
 QFont f;


 // Button
 if(buttonPressed == 0){
  glColor4f(0.5, 0.5, 0.5, 0.9);
 }
 else{
  glColor4f(0.25, 0.25, 0.25, 0.9);
 }
 glBegin(GL_POLYGON);
 glVertex3f(butMin.x, butMin.y, 0);
 glVertex3f(butMax.x, butMin.y, 0);
 glVertex3f(butMax.x, butMax.y, 0);
 glVertex3f(butMin.x, butMax.y, 0);
 glEnd();

 // Border
 if(buttonPressed == 0){
  glColor4f(0.0, 0.0, 0.0, 0.9);
 }
 else{
  glColor4f(0.75, 0.75, 0.75, 0.9);
 }
 glLineWidth(3);
 glBegin(GL_LINE_LOOP);
 glVertex3f(butMinBord.x, butMinBord.y, 0);
 glVertex3f(butMaxBord.x, butMinBord.y, 0);
 glVertex3f(butMaxBord.x, butMaxBord.y, 0);
 glVertex3f(butMinBord.x, butMaxBord.y, 0);
 glEnd();

 // Text
 glDisable(GL_BLEND);
 glDisable(GL_LINE_SMOOTH);
 f = QFont();
 f.setBold(true);
 f.setPointSize(15);
 if(buttonPressed == 0){
  glColor4f(0.0, 0.0, 0.0, 1.0);
 }
 else{
  glColor4f(0.75, 0.75, 0.75, 1.0);
 }
 renderText(butTextPos.x, butTextPos.y, 0, "Next", f);
 glEnable(GL_BLEND);
 glEnable(GL_LINE_SMOOTH);
}

void mapWin::drawLabel(){
 int tickspacing = userInt->getSlider().getTickSpacing();
 Vector2d startPos = userInt->getSlider().getBarPos();
 int dt = 3;
 int textsize = 12;
 for(int i = 0; i <= 23; i += 2){
  char* text = new char[100];
  sprintf(text, "%d", i*dt);
  int spacing = i * tickspacing;
  QFont f;
  glDisable(GL_BLEND);
  glDisable(GL_LINE_SMOOTH);
  f = QFont();
  f.setBold(true);
  f.setPointSize(textsize);
  glColor4f(.4, .4, .4, .9);
  renderText(startPos.x + spacing - textsize / 2.0, startPos.y + textsize * 2.3, 0, text, f);
  glEnable(GL_BLEND);
  glEnable(GL_LINE_SMOOTH);
 }
}

void mapWin::getColorBarColorsLabels(std::vector<Vector4d> *colors, std::vector<string> *labels)
{
    colors->resize(7);
    labels->resize(7);
    for(int i = 5; i >= 1; i--){
        float r(0.0f), g(0.0f), b(0.0f), a(1.0f);
        StormCharacteristicInterpolator::StormCategoryColorMapping2(6 - i, &r, &g, &b);
        string str = "CAT ";
        str = str + to_string( 6 - i);
        colors->at(i - 1) = Vector4d(r, g, b, a);
        labels->at(i - 1) = str;
    }

    string str = "TS";
    float r(0.0f), g(0.0f), b(0.0f), a(1.0f);
    StormCharacteristicInterpolator::StormCategoryColorMapping2(7, &r, &g, &b);
    labels->at(5) = str;
    colors->at(5) = Vector4d(r, g, b, a);

    str = "TD";
    StormCharacteristicInterpolator::StormCategoryColorMapping2(6, &r, &g, &b);
    labels->at(6) = str;
    colors->at(6) = Vector4d(r, g, b, a);
}

QString mapWin::getHurTimeStamp(const string &id)
{
    if(id == string("mcrall_al092008_091018")){
        return tr("09/10/2008 16:00 CDT");
    } else if (id == string("mcrall_al092012_082612")){
        return QString("08/26/2012\n11:00 EDT");
    } else if (id == string("mcrall_al112009_110806")){
        return QString("11/08/2009 11:00 CST");
    } else if(id == string("mcrall_al122005_082706")){
        return QString("08/27/2005 05:00 EDT");
    } else if(id == string("mcrall_al182005_092206")){
        return QString("09/22/2005 04:00 CDT");
    } else {
        return QString("unknown hurricane");
    }
}

void mapWin::update(){
 unsigned int curAdv = sim->getCurrentAdv();

 int r;
 double chosen = 0.0;

 if(sim->getCurrentAdv() < (int)(sim->advCombList.size()/2)){
  if( drawMethod == 0 ){// draw points -- Le
   // if(step == 0){
   //   for(int i = 0; i < 200; i++){
   totalPaths++;

   // Determine whether to do a historical path or points in the cone
   srand ( counter );
   counter = (counter*21)%UINT_MAX;

   r = rand() % 10000000+1;
   chosen = (double)r/10000000.0;

   //if(chosen <= 0.32){//this treshold determines which model(predicted or historical) will be used-Le
   testPath = new path(sim->adv->getLat(), sim->adv->getLon(), sim->adv->getDeg(), sim->adv->getSpeed(), curAdv);
   //cout << testPath->getAge() << endl;

   int tt = userInt->getSliderCurTick();
   tt = userInt->getSliderCurTick() > 12 ? 12 : tt;

   timeThreshold = 150 - tt * 11;
   if(userInt->getSliderCurTick() > 12)
    timeThreshold = 10;
   if(userInt->getSliderCurTick() == 0)
    timeThreshold = 10e6;

   if(timecounter % timeThreshold == 0)
   {
    sim->adv->pathList.push_back(testPath);
   }
   timecounter ++;
   //}
   // else{
   //   //if(sim->adv->eyePointList.size() < 5){
   //     hurEyePoints * hep;
   //     hep = new hurEyePoints(sim->adv->pre);
   //     sim->adv->eyePointList.push_back(hep);
   //   //}
   // }
   //   }
   curAdv++;
   // }
   // if(step == 24){
   //   for(unsigned int i = 0; i < sim->adv->pathList.size(); i++){
   //     delete(sim->adv->pathList[i]);
   //   }
   //   sim->adv->pathList.clear();
   //   step = -1;
   // }
   step++;
  }
  else
  {//draw paths --Le
   totalPaths++;

   testPath = new path(sim->adv->getLat(), sim->adv->getLon(), sim->adv->getDeg(), sim->adv->getSpeed(), curAdv);
   sim->adv->pathList.push_back(testPath);
  }
 }

 sim->adv->findInConePercent();
 updateGL();
}

void mapWin::mouseMoveEvent(QMouseEvent * e){
 Vector2d dif, pos;
 e->accept();

 dif.set(e->x()-initPress.x, e->y()-initPress.y);
 pos.set(e->x(), e->y());
 userInt->moveChip(dif);
 userInt->moveSlider(pos);

 initPress.set(e->x(), e->y());

 updateGL();
}

void mapWin::mousePressEvent(QMouseEvent * e){
 Vector2d p;

 e->accept();

 p.set(e->x(), e->y());
 userInt->selectChip(p);
 userInt->checkSliderPressed(p);

 initPress.set(e->x(), e->y());

 checkButtonPress(e->x(), e->y());

 updateGL();
}

void mapWin::mouseReleaseEvent(QMouseEvent * e){
 e->accept();

 userInt->releaseChip();
 //userInt->printSectors();

 checkButtonRelease(e->x(), e->y());

 updateGL();
}

void mapWin::keyPressEvent(QKeyEvent * k){
 k->accept();
 std::vector<advisory*>::iterator advIt;

 cout << flush;

 if ( k->text().toStdString()== "1") {
  displayDataPoints = displayDataPoints * -1;
  updateGL();
 }

 if (k->text().toStdString() == "2") {
  displayDataPaths = displayDataPaths * -1;
  updateGL();
 }

 if (k->text().toStdString() == "3") {
  displayProjectedPath = displayProjectedPath * -1;
  updateGL();
 }

 if (k->text().toStdString() == "4") {
  displayGenPaths = displayGenPaths * -1;
  updateGL();
 }
 if (k->text().toStdString() == "8"){
  showCurPath = showCurPath * -1;
  updateGL();
 }
 if (k->text().toStdString() == "5"){
  timeThreshold -= 1;
  if(timeThreshold < 1)
   timeThreshold = 1;
  cout << "Time Threshold: " << timeThreshold << endl;
  updateGL();
 }
 if (k->text().toStdString() == "6"){
  timeThreshold += 1;
  cout << "Time Threshold: " << timeThreshold << endl;
  updateGL();
 }
 if (k->text().toStdString() == "7"){
  sim->turnOnErrorConeCircle = !sim->turnOnErrorConeCircle;
  if(sim->turnOnErrorConeCircle == true)
   cout << "Display error cone circle." << endl;
  updateGL();
 }

 FILE *fp;
 char filename[100];
 char pagetitle[100];
 int state = GL2PS_OVERFLOW, buffsize = 0;

 if(k->text().toStdString() == "r"){
  //sprintf(filename, "../maps/all/hurricane.%02d.svg", frameID);
  sprintf(filename, "images/cleanPath/paths.pdf");
  sprintf(pagetitle, "hurricane");
  fp = fopen(filename, "wb");
  printf("writing svg...");
  while(state == GL2PS_OVERFLOW){
   buffsize += 1024*1024;
   gl2psBeginPage(pagetitle, "hurricane", NULL, GL2PS_PDF, GL2PS_SIMPLE_SORT,
                  GL2PS_DRAW_BACKGROUND | GL2PS_USE_CURRENT_VIEWPORT,
                  GL_RGBA, 0, NULL, 0, 0, 0, buffsize, fp, filename);
   gl2psEnable(GL2PS_BLEND);
   gl2psBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   updateGL();
   gl2psDisable(GL2PS_BLEND);
   state = gl2psEndPage();
  }
  fclose(fp);
  printf("Done!\n");
  frameID ++;
 }

 if (k->text().toStdString() == "i"){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  if(speedRatio > 0){
   speedRatio -= 0.01;
   cout << "New Speed Ratio: " << speedRatio << endl;
  }
 }
 if (k->text().toStdString() == "I"){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  if(speedRatio < 1.0){
   speedRatio += 0.01;
   cout << "New Speed Ratio: " << speedRatio << endl;
  }
 }
 if (k->text().toStdString() == "o"){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  if(bearRatio > 0.0){
   bearRatio -= 0.01;
   cout << "New Bearing Ratio: " << bearRatio << endl;
  }
 }
 if (k->text().toStdString() == "O"){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  if(bearRatio < 1.0){
   bearRatio += 0.01;
   cout << "New Bearing Ratio: " << bearRatio << endl;
  }
 }

 if (k->text().toStdString() == "h" || k->text().toStdString() == "H") {
  heatMap = heatMap * -1;
  updateGL();
 }

 if (k->text().toStdString() == "[" && sim->getPathOpacity() > 0.00){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  sim->setPathOpacity(sim->getPathOpacity() - 0.01);
  cout << "New opacity: " << sim->getPathOpacity() << "\n";
 }
 if (k->text().toStdString() == "]"){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  sim->setPathOpacity(sim->getPathOpacity() + 0.01);
  cout << "New opacity: " << sim->getPathOpacity() << "\n";
 }

 if (k->text().toStdString() == "," && sim->getAlpha()  >= 0.1){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  sim->setAlpha(sim->getAlpha() - 0.1);
  cout << "New alpha: " << sim->getAlpha() << "\n";
 }
 if (k->text().toStdString() == "." && sim->getAlpha() < 1.0){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  sim->setAlpha(sim->getAlpha() + 0.1);
  cout << "New alpha: " << sim->getAlpha() << "\n";
 }

 if (k->text().toStdString() == "-"){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }

  userSig += 1.0;
  cout << "New userSig: " << userSig << "\n";
 }
 if (k->text().toStdString() == "="){
  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  if(userSig > 2.0){
   userSig -= 1.0;
  }
  cout << "New userSig: " << userSig << "\n";
 }

 if (k->text().toStdString() == "s" || k->text().toStdString() == "S"){
  //stall = stall * -1;
  sim->MipmapSwith();
 }
 if (k->text().toStdString() == "c" || k->text().toStdString() == "C"){
  sim->incCurAdv(1);
  cout << "Current advisory: " << sim->getCurrentAdv() << endl;
  drawMethod = 1;
  //userInt->reset();
  delete userInt;
  userInt = new ui();
 }
 if (k->text().toStdString() == "v" || k->text().toStdString() == "V"){
  /*for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
    (*advIt)->pathList.clear();
    }
    preOnly = preOnly * -1;
    if(preOnly > 0){
    cout << "Only using predicted data." << endl;
    }
    else{
    cout << "Using predicted and historical data." << endl;
    }*/

  for(advIt = sim->advList.begin(); advIt != sim->advList.end(); advIt++){
   (*advIt)->pathList.clear();
  }
  drawMethod = (drawMethod+1)%totalDrawMethods;
 }
 if (k->text().toStdString() == "p" || k->text().toStdString() == "P"){
  //sim->adjustKernelFactor(1);
  cout << "output desktop to image" << endl;
  //QGLFormat glf;
  //glf.setDoubleBuffer(TRUE);
  //glf.setRgba(TRUE);
  //glf.setDepth(TRUE);
  //QGLFormat::setDefaultFormat(glf);

  //QGLContext * context = new QGLContext(glf);
  //context->create();
  //context->makeCurrent();

  //QImage qimage = QGLWidget::grabFrameBuffer(true);
  //qimage.save("qtest.png", "PNG");
  QPixmap qpixmap = QPixmap();
  QRect qrect = QApplication::desktop()->availableGeometry(this);
  QPoint qpoint = this->mapToGlobal(this->pos());
  QSize qsize = this->size();
  cout << qsize.height() << " " << qsize.width() << endl;
  cout << qrect.x() << "  " << qrect.y() << " " << qrect.width() << " " << qrect.height() << endl;
  cout << qpoint.x() << " " << qpoint.y() << endl;

  qpixmap = QPixmap::grabWindow(QApplication::desktop()->winId(),
                                qpoint.x() + 1, qpoint.y() + 2, qsize.width()-5, qsize.height()-8);
  //qpixmap = QPixmap::grabWidget(this);
  //QString str = QString::fromStdString("pathvis-" + DatafileName + ".png");
  QString str = QString::fromStdString("../selectedpoints.png");
  qpixmap.save(str, "PNG", 100);
  pathresampling->IsShuffled = false;
 }
 if (k->text().toStdString() == "l" || k->text().toStdString() == "L"){
  sim->adjustKernelFactor(-1);
 }
 if (k->text().toStdString() == "b" || k->text().toStdString() == "B"){
  //colorClosest = colorClosest * -1;
  sim->setDrawContourMethod();
 }
 if (k->text().toStdString() == "z" || k->text().toStdString() == "Z"){
  sim->setHeatmapMethod();
 }
 if (k->text().toStdString() == "n" || k->text().toStdString() == "N"){
  sim->setMorphology();
 }
 if(k->text().toStdString() == "u"){
  cout << "Compute SD fields ..." << endl;
  QString program("/bin/sh");
  QString args("/home/lel/Projects/HurVis/matlab-rbf/olsselect.sh");
  QProcess process;
  process.start(program, QStringList() << args << QString("55"));
  process.waitForFinished(30000);
  QString output(process.readAllStandardOutput());
  process.close();
  qDebug() << output ;
  pathresampling->ComputeSDFieldsFromOLS();
  pathresampling->ComputeSDFieldsFromResampling();
  cout << "Finish outputing SD fields." << endl;

 }
 if (k->text().toStdString() == "x" || k->text().toStdString() == "X"){
  averageOnAll = averageOnAll * -1;
  if(averageOnAll > 0){
   cout << "Taking average of all paths." << endl;
  }
  else{
   cout << "Taking average of closest 68% paths." << endl;
  }
 }
 else if ((k->text().toStdString() == "q") || (k->text().toStdString() == "Q") || (k->key() == Qt::Key_Escape)) {
  exit(0);
 }
 updateGL();
}

void mapWin::imageBind(GLubyte * toBind, int texNum, int height, int width) {
 cout << "Binding image...." << endl;

 glBindTexture(GL_TEXTURE_2D, textures[texNum]);
 glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
 glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
 glTexImage2D(GL_TEXTURE_2D, 0, 3, height, width, 0, GL_RGB,
              GL_UNSIGNED_BYTE, toBind);


 for(int i = 1; i <= 7; i++)
 {
  char* buff = new char[20];
  sprintf(buff, "images/cat%d.png", i);
  Image image(buff);
  Blob blob;
  image.magick("RGBA");
  //image.flip();
  image.write(&blob);
  glClear(GL_COLOR_BUFFER_BIT);
  glBindTexture(GL_TEXTURE_2D, textures[i]);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexImage2D(GL_TEXTURE_2D, 0, 4, image.columns(), image.rows(), 0, GL_RGBA,
               GL_UNSIGNED_BYTE, blob.data());
 }

 glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
 glDisable(GL_TEXTURE_2D);


}

void mapWin::imageOpen(){
 cout << "Opening image...." << endl;
 FILE * in;
 //in = fopen("map_bw.ppm", "rb");
 //in = fopen("tinymap.ppm", "rb");
 //in = fopen("map_with_cities_fixed.ppm", "rb");
 in = fopen("map_with_cities_black.ppm", "rb");
 //in = fopen("croped-map2.ppm", "rb");
 // Check file open
 if (in == NULL) {
  printf("Unable to open file map_with_cities.ppm\n");
  exit(1);
 }
 imageMap = new P6Image(in);
 imageBind(imageMap->getImageData(), 0, imageMap->getHeight(), imageMap->getWidth());

 updateGL();
}

// Draw the current path being used
void mapWin::drawCurPath(){
 std::vector<posPoint*>::iterator _p1;
 std::vector<posPoint*>::iterator _p2;
 Vector2d d1, d2;

 glColor4f(0.75, 0.0, 0.75, 1.0);
 glLineWidth(1.0);

 if(curPath != NULL && showCurPath > 0){
  _p2 = curPath->positionList.begin()+1;
  for(_p1 = curPath->positionList.begin(); _p2 != curPath->positionList.end(); _p1++){
   d1 = translateToScreen((*_p1)->getLon(), (*_p1)->getLat());
   d2 = translateToScreen((*_p2)->getLon(), (*_p2)->getLat());
   glBegin(GL_LINES);
   glVertex3f(d1.x, d1.y, 0.0);
   glVertex3f(d2.x, d2.y, 0.0);
   glEnd();
   _p2++;
  }
 }
}

void mapWin::checkButtonPress(double x, double y){
 if(x < butMaxBord.x && x > butMinBord.x){
  if(y < butMaxBord.y && y > butMinBord.y){
   buttonPressed = 1;
   return;
  }
 }
 buttonPressed = 0;
 return;
}

void mapWin::checkButtonRelease(double x, double y){
 if(x < butMaxBord.x && x > butMinBord.x){
  if(y < butMaxBord.y && y > butMinBord.y){
   if(userInt->chipsPlaced() == 1){
    printResults();
    sim->incCurAdv(1);
    userInt->reset();
    completed++;
   }
  }
 }
 cout << "completed: " << completed << "     advList: " << sim->advList.size() << endl;
 if(completed == (int)(sim->advList.size())){
  printStopExp();
 }
 buttonPressed = 0;
}


// File format is as follows:
/* #EXP_START
   0 if paths | 1 if error cone
# of advisory (which hurricane)
sector , value
...
#EXP_STOP
*/
void mapWin::printStartExp(){
 fstream file;
 std::vector<advisory*>::iterator _a;

 file.open(outputFile.c_str(), fstream::out | fstream::ate | fstream::app);
 file << "#EXP_START" << endl;
 file.close();
}

void mapWin::printResults(){
 fstream file;
 std::vector<advisory*>::iterator _a;
 unsigned int i;
 unsigned int numSectors = 8;

 file.open(outputFile.c_str(), fstream::out | fstream::ate | fstream::app);
 if(sim->getCurrentAdv() < (int)(sim->advList.size()/2)){
  file << "    0  // PATHS" << endl;
 }
 else{
  file << "    1 // ERROR CONE" << endl;
 }

 file << "    " << sim->getCurrentAdv() << endl;
 for(i = 0; i < numSectors; i++){
  file << "    " << i << " , " << userInt->sectorValue(i) << endl;
 }
 file << "#EXP_STOP" << endl;
 file << endl;
 file.close();
}

void mapWin::printStopExp(){
 exit(0);
}
