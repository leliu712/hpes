#ifdef __APPLE__
# include<GLUT/glut.h>
#else
# include<GL/glut.h>
# include<GL/glu.h>
#endif

#include <cmath>
#include "MarchingSquares.h"
#include "ColorGradient.h"
#include <omp.h>

void MarchingSquares::buildBinaryGrid(const float isovalue){
 //for(int i = 0; i < grid.size(); i++){
 // values[i] = 0.0;
 //}
 //for(int i = 5; i < 15; i++){
 // for(int j = 5; j < 15; j++){
 //  int idx = getIndex(i, j);
 //  values[idx] = 1.0;
 // }
 //}
 binaryValues = new float[grid.size()];
 for(int i = 0; i < grid.size(); i++){
  if(values[i] - isovalue <= 10e-6)
   binaryValues[i] = 0;
  else
   binaryValues[i] = 1;
 }
}

void MarchingSquares::buildCellGrid(){
 cells.clear();
 for(int i = 0; i < rows-1; i++){
  for(int j = 0; j < cols-1; j++){
   int idx;
   Vector2d center;
   Vector2d c0,c1,c2,c3;
   float cval;
   Cell cell;

   idx = getIndex(i, j); 
   c0 = grid[idx][0];
   c1 = grid[idx][1];
   c2 = grid[idx][2];
   c3 = grid[idx][3];
   center = c0 + (c2 - c0) / 2.0;
   cell.corners.push_back(center);
   cval = values[idx];
   cell.cornervalues.push_back(cval);
   if(binaryValues[idx] == 1)
    cell.index += 0b1000;

   idx = getIndex(i, j+1); 
   c0 = grid[idx][0];
   c1 = grid[idx][1];
   c2 = grid[idx][2];
   c3 = grid[idx][3];
   center = c0 + (c2 - c0) / 2.0;
   cell.corners.push_back(center);
   cval = values[idx];
   cell.cornervalues.push_back(cval);
   if(binaryValues[idx] == 1)
    cell.index += 0b0100;

   idx = getIndex(i+1, j+1); 
   c0 = grid[idx][0];
   c1 = grid[idx][1];
   c2 = grid[idx][2];
   c3 = grid[idx][3];
   center = c0 + (c2 - c0) / 2.0;
   cell.corners.push_back(center);
   cval = values[idx];
   cell.cornervalues.push_back(cval);
   if(binaryValues[idx] == 1)
    cell.index += 0b0010;

   idx = getIndex(i+1, j); 
   c0 = grid[idx][0];
   c1 = grid[idx][1];
   c2 = grid[idx][2];
   c3 = grid[idx][3];
   center = c0 + (c2 - c0) / 2.0;
   cell.corners.push_back(center);
   cval = values[idx];
   cell.cornervalues.push_back(cval);
   if(binaryValues[idx] == 1)
    cell.index += 0b0001;

   cells.push_back(cell);
  }
 }
}

void MarchingSquares::lookupContourPatches(const float isovalue){
 for(int i = 0; i < cells.size(); i++){
  int idx = cells[i].index;
  Vector2d c0, c1, c2, c3;
  c0 = cells[i].corners[0];
  c1 = cells[i].corners[1];
  c2 = cells[i].corners[2];
  c3 = cells[i].corners[3];
  float v0, v1, v2, v3, centerVal;
  v0 = cells[i].cornervalues[0];
  v1 = cells[i].cornervalues[1];
  v2 = cells[i].cornervalues[2];
  v3 = cells[i].cornervalues[3];
  Vector2d endpoint;
  switch(idx){
   case 0:
   case 15:
    break;
   case 14:
    endpoint = c3 + (c0 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].patch.push_back(endpoint);
    endpoint = c3 + (c2 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c2);
    cells[i].patch.push_back(c1);
    cells[i].patch.push_back(c0);
    break;
   case 1:
    endpoint = c3 + (c0 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].patch.push_back(endpoint);
    endpoint = c3 + (c2 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c3);
    break;
   case 13:
    endpoint = c3 + (c2 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].patch.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c1);
    cells[i].patch.push_back(c0);
    cells[i].patch.push_back(c3);
    break;
   case 2:
    endpoint = c3 + (c2 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].patch.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c2);
    break;
   case 3:
    endpoint = c3 + (c0 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].patch.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c2);
    cells[i].patch.push_back(c3);
    break;
   case 12:
    endpoint = c3 + (c0 - c3) / 2.0;
    //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].patch.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c1);
    cells[i].patch.push_back(c0);
    break;
   case 4:
    endpoint = c0 + (c1 - c0) / 2.0;
    //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].patch.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c1);
    break;
   case 11:
    endpoint = c0 + (c1 - c0) / 2.0;
    //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].patch.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c2);
    cells[i].patch.push_back(c3);
    cells[i].patch.push_back(c0);
    break;
   case 5:
    centerVal = (v0 + v1 + v2 + v3) / 4.0;
    if(centerVal - isovalue > 10e-6){
     endpoint = c3 + (c0 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].patch.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c1);
     endpoint = c2 + (c1 - c2) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].patch.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c3);
    }
    else{
     endpoint = c3 + (c0 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].patch.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c3);

     endpoint = c0 + (c1 - c0) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].patch.push_back(endpoint);
     endpoint = c2 + (c1 - c2) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c1);
     cells[i].drawflag = 1;
    }
    break;
   case 6:
    endpoint = c3 + (c2 - c3) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].patch.push_back(endpoint);
    endpoint = c0 + (c1 - c0) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c1);
    cells[i].patch.push_back(c2);
    break;
   case 9:
    endpoint = c3 + (c2 - c3) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].patch.push_back(endpoint);
    endpoint = c0 + (c1 - c0) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c0);
    cells[i].patch.push_back(c3);
    break;
   case 7:
    endpoint = c3 + (c0 - c3) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].patch.push_back(endpoint);
    endpoint = c0 + (c1 - c0) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c1);
    cells[i].patch.push_back(c2);
    cells[i].patch.push_back(c3);
    break;
   case 8:
    endpoint = c3 + (c0 - c3) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].patch.push_back(endpoint);
    endpoint = c0 + (c1 - c0) / 2.0; 
    //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].patch.push_back(endpoint);
    cells[i].patch.push_back(c0);
    break;
   case 10:
    centerVal = (v0 + v1 + v2 + v3) / 4.0;
    if(centerVal - isovalue < 10e-6){
     endpoint = c3 + (c0 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].patch.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c0);

     endpoint = c3 + (c2 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].patch.push_back(endpoint);
     endpoint = c2 + (c1 - c2) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c2);
     cells[i].drawflag = 1;
    }
    else{
     endpoint = c3 + (c0 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].patch.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c2);
     endpoint = c2 + (c1 - c2) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].patch.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     //endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].patch.push_back(endpoint);
     cells[i].patch.push_back(c0);
    }
    break;


   default:
    cout << "error: no suitable case in look-up table." << endl;
  }
 }
}



void MarchingSquares::lookupContourLines(const float isovalue){
 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
#pragma omp parallel for
 for(int i = 0; i < cells.size(); i++){
  int idx = cells[i].index;
  Vector2d c0, c1, c2, c3;
  c0 = cells[i].corners[0];
  c1 = cells[i].corners[1];
  c2 = cells[i].corners[2];
  c3 = cells[i].corners[3];
  float v0, v1, v2, v3, centerVal;
  v0 = cells[i].cornervalues[0];
  v1 = cells[i].cornervalues[1];
  v2 = cells[i].cornervalues[2];
  v3 = cells[i].cornervalues[3];
  Vector2d endpoint;
  switch(idx){
   case 0:
   case 15:
    break;
   case 1:
   case 14:
    endpoint = c3 + (c0 - c3) / 2.0;
    endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].endpoints.push_back(endpoint);
    endpoint = c3 + (c2 - c3) / 2.0;
    endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].endpoints.push_back(endpoint);
    break;
   case 2:
   case 13:
    endpoint = c3 + (c2 - c3) / 2.0;
    endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].endpoints.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].endpoints.push_back(endpoint);
    break;
   case 3:
   case 12:
    endpoint = c3 + (c0 - c3) / 2.0;
    endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].endpoints.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].endpoints.push_back(endpoint);
    break;
   case 4:
   case 11:
    endpoint = c0 + (c1 - c0) / 2.0;
    endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].endpoints.push_back(endpoint);
    endpoint = c2 + (c1 - c2) / 2.0;
    endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
    cells[i].endpoints.push_back(endpoint);
    break;
   case 5:
    centerVal = (v0 + v1 + v2 + v3) / 4.0;
    if(centerVal - isovalue > 10e-6){
     endpoint = c3 + (c0 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c2 + (c1 - c2) / 2.0; 
     endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].endpoints.push_back(endpoint);
    }
    else{
     endpoint = c3 + (c0 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c2 + (c1 - c2) / 2.0; 
     endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].endpoints.push_back(endpoint);
    }
    break;
   case 6:
   case 9:
    endpoint = c3 + (c2 - c3) / 2.0; 
    endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
    cells[i].endpoints.push_back(endpoint);
    endpoint = c0 + (c1 - c0) / 2.0; 
    endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].endpoints.push_back(endpoint);
    break;
   case 7:
   case 8:
    endpoint = c3 + (c0 - c3) / 2.0; 
    endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
    cells[i].endpoints.push_back(endpoint);
    endpoint = c0 + (c1 - c0) / 2.0; 
    endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
    cells[i].endpoints.push_back(endpoint);
    break;
   case 10:
    centerVal = (v0 + v1 + v2 + v3) / 4.0;
    if(centerVal - isovalue < 10e-6){
     endpoint = c3 + (c0 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c2 + (c1 - c2) / 2.0; 
     endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].endpoints.push_back(endpoint);
    }
    else{
     endpoint = c3 + (c0 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v0, c3, c0);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c3 + (c2 - c3) / 2.0; 
     endpoint = linearInterpolation(isovalue, v3, v2, c3, c2);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c0 + (c1 - c0) / 2.0; 
     endpoint = linearInterpolation(isovalue, v0, v1, c0, c1);
     cells[i].endpoints.push_back(endpoint);
     endpoint = c2 + (c1 - c2) / 2.0; 
     endpoint = linearInterpolation(isovalue, v2, v1, c2, c1);
     cells[i].endpoints.push_back(endpoint);
    }
    break;


   default:
    cout << "error: no suitable case in look-up table." << endl;
  }
 }
}

void MarchingSquares::drawGrid(){
 glLineWidth(1.0);
 glColor4f(0, 1, 0, 0.8);
 for(int i = 0; i < grid.size(); i++){
  Vector2d draw0 = grid[i][0];
  Vector2d draw1 = grid[i][1];
  Vector2d draw2 = grid[i][2];
  Vector2d draw3 = grid[i][3];
  glBegin(GL_LINE_LOOP);
  glVertex3f(draw0.x, draw0.y, 0);
  glVertex3f(draw1.x, draw1.y, 0);
  glVertex3f(draw2.x, draw2.y, 0);
  glVertex3f(draw3.x, draw3.y, 0);
  glEnd();
 
  glPointSize(5);
  glBegin(GL_POINTS);
  glVertex3f(draw0.x, draw0.y, 0);
  glEnd();
  glBegin(GL_POINTS);
  glVertex3f(draw1.x, draw1.y, 0);
  glEnd();
  glBegin(GL_POINTS);
  glVertex3f(draw2.x, draw2.y, 0);
  glEnd();
  glBegin(GL_POINTS);
  glVertex3f(draw3.x, draw3.y, 0);
  glEnd();
 }
}

void MarchingSquares::drawLines(const float isovalue){
 ColorGradient cg;
 float red, green, blue;
 cg.getColorAtValue(isovalue, red, green, blue);
 glLineWidth(3.0);
 //glColor4f(red, green, blue, 1);
 glColor4f(1, 0, 0, 1);
 for(int i = 0; i < cells.size(); i++){
  if(cells[i].index != 5 && cells[i].index != 10){ 
   glBegin(GL_LINE_STRIP);
   for(int j = 0; j < cells[i].endpoints.size(); j++){
    Vector2d tmp = cells[i].endpoints[j];
    glVertex3f(tmp.x, tmp.y, 0.0);
   }
   glEnd();
  }
  else if(cells[i].index == 5 || cells[i].index == 10){
   Vector2d c0, c1;
   c0 = cells[i].endpoints[0];
   c1 = cells[i].endpoints[1];
   glBegin(GL_LINES);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glEnd();
   c0 = cells[i].endpoints[2];
   c1 = cells[i].endpoints[3];
   glBegin(GL_LINES);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glEnd();
  }
 }
}

void MarchingSquares::drawPatches(const float isovalue){
 ColorGradient cg;
 float red, green, blue;
 cg.getColorAtValue(isovalue, red, green, blue);
 glLineWidth(3.0);
 glColor4f(red, green, blue, 1);
 //glColor4f(1, 0, 0, 0.8);
 for(int i = 0; i < cells.size(); i++){
  if(cells[i].index != 5 && cells[i].index != 10){ 
   glBegin(GL_POLYGON);
   for(int j = 0; j < cells[i].patch.size(); j++){
    Vector2d tmp = cells[i].patch[j];
    glVertex3f(tmp.x, tmp.y, 0.0);
   }
   glEnd();
  }
 else if(cells[i].index == 5 || cells[i].index == 10){
  if(cells[i].drawflag == 0){
   glBegin(GL_POLYGON);
   for(int j = 0; j < cells[i].patch.size(); j++){
    Vector2d tmp = cells[i].patch[j];
    glVertex3f(tmp.x, tmp.y, 0.0);
   }
   glEnd();
  }
  else{
   Vector2d c0, c1, c2;
   c0 = cells[i].patch[0];
   c1 = cells[i].patch[1];
   c2 = cells[i].patch[2];
   glBegin(GL_POLYGON);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glVertex3f(c2.x, c2.y, 0.0);
   glEnd();
   c0 = cells[i].patch[3];
   c1 = cells[i].patch[4];
   c2 = cells[i].patch[5];
   glBegin(GL_POLYGON);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glVertex3f(c2.x, c2.y, 0.0);
   glEnd();
  }
 }
 }
}

void MarchingSquares::drawInsideArea(const float isovalue){
 ColorGradient cg;
 float red, green, blue;
 cg.getColorAtValue(isovalue, red, green, blue);
 glColor4f(red, green, blue, 1);
 for(int i = 0; i < cells.size(); i++){
  if(cells[i].marked == 4){
   Vector2d c0, c1, c2, c3;
   c0 = cells[i].corners[0];
   c1 = cells[i].corners[1];
   c2 = cells[i].corners[2];
   c3 = cells[i].corners[3];
   //glBegin(GL_LINE_LOOP);
   glBegin(GL_POLYGON);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glVertex3f(c2.x, c2.y, 0.0);
   glVertex3f(c3.x, c3.y, 0.0);
   glEnd();
  }
 }
}

void MarchingSquares::drawBinaryImage(){
 for(int i = 0; i < rows; i++){
  for(int j = 0; j < cols; j++){
   int idx = getIndex(i, j);
   if(binaryValues[idx] > 0){
     Vector2d draw0 = grid[idx][0];
     Vector2d draw1 = grid[idx][1];
     Vector2d draw2 = grid[idx][2];
     Vector2d draw3 = grid[idx][3];
     glBegin(GL_POLYGON);
     glVertex3f(draw0.x, draw0.y, 0);
     glVertex3f(draw1.x, draw1.y, 0);
     glVertex3f(draw2.x, draw2.y, 0);
     glVertex3f(draw3.x, draw3.y, 0);
     glEnd();
   }
  }
 }
}

void MarchingSquares::markCells(const int width, const int height){
for(int i = 0; i < rows - 1; i++){
 for(int j = 0; j < cols - 1; j++){
   int idx = getCellIndex(i, j);
   int flag = 0;
   int fl, fr, fu, fd;
   fl = fr = fu = fd = 0;

   //check left
   for(int k = j - 1; k >=0; k--){
    int left = getCellIndex(i, k);
    if(cells[left].index > 0 && cells[left].index < 15){
     fl = 1;
    }
   }

   //check right
   for(int k = j + 1; k < cols - 1; k++){
    int right = getCellIndex(i, k);
    if(cells[right].index > 0 && cells[right].index < 15){
     fr = 1;
    }
   }

   //check up
   for(int k = i - 1; k >= 0; k--){
    int up = getCellIndex(k, j);
    if(cells[up].index > 0 && cells[up].index < 15){
     fu = 1;
    }
   }

   //check down
   for(int k = i + 1; k < rows - 1; k++){
    int down = getCellIndex(k, j);
    if(cells[down].index > 0 && cells[down].index < 15){
     fd = 1;
    }
   }

   flag = fl + fr + fu + fd;
   cells[idx].marked = flag;
   //if(flag == 4)
   // cout << flag << endl;
  }
 }
}



void MarchingSquares::computeDataDensity(const float h){
 std::vector<Vector2d> centers;
 for(int i = 0; i < grid.size(); i++){
  Vector2d c0, c1;
  c0 = grid[i][0];
  c1 = grid[i][2];
  Vector2d center = c0 + (c1 - c0) / 2.0; 
  centers.push_back(center);
 }

 float* dataDensity = new float[grid.size()];
 for(int i = 0; i < grid.size(); i++){
  float sum = 0;
  for(int j = 0; j < grid.size(); j++){
   if(i == j)
    continue;
   float r = (centers[j] - centers[i]).norm();
   float m_j = values[j];
   //float W = spikyKernel(r, h);
   float W = cubicSplineKernel(r, 1.4 * r);
   float sub_sum = m_j * W; 
   sum += sub_sum;
  }
  dataDensity[i] = sum;
 }
 values = dataDensity;
}

float MarchingSquares::spikyKernel(const float r, const float h){
 if(r > 0 && r - h <= 10e-6){
  //return 15.0 / (PI*pow(h, 6)) * pow(h-r, 3);
  //return 315.0 / (64 * PI * pow(h, 9)) * pow(pow(h, 2) - pow(r, 2), 3);
  //return 15.0 / (2 * PI * pow(h, 3)) * (- pow(r, 3) / (2 * pow(h, 3) + pow(r, 2) / pow(h, 2) + h / (2 * r) - 1));
 }
 else
  return 0;
}

float MarchingSquares::cubicSplineKernel(const float r, const float h){
 float q = r / h;
 if(q >= 0 && q < 1){
  return 10.0 / (7.0 * PI * pow(h, 2)) * (1 - 3.0 / 2.0 * pow(q, 2) + 3.0 / 4.0 * pow(q, 3));
 }
 else if(q >= 1 && q <= 2){
  return 10.0 / (7.0 * PI * pow(h, 2)) * (1/4.0 * pow(2-q, 3));
 }
 else
  return 0.0;
}
















