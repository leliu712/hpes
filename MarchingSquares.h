#ifndef __MARCHING_SQUARE__
#define __MARCHING_SQUARE__
/*
 * class to implement a marching squares algorithm
 * used to constrcut iso-contourn from a heatmap
 */

#include <iostream>
#include <stdio.h>
#include <vector>
#include "Vector.h"

using namespace std;

class MarchingSquares{
 public:

  struct Cell{
   int index = 0b0000;
   std::vector<Vector2d> corners;
   std::vector<Vector2d> endpoints;
   std::vector<float> cornervalues;
   std::vector<Vector2d> patch;
   int drawflag = 0;
   int marked = -1;
  };

  MarchingSquares(float* _values, std::vector<Vector2d*> _grid, int _rows, int _cols):
   grid(_grid),
   rows(_rows),
   cols(_cols){
    values = new float[grid.size()];
    for(int i = 0; i < grid.size(); i++){
     values[i] = _values[i];
    }
   }
  ~MarchingSquares(){
   delete[] values;
   delete[] binaryValues;}

  void buildBinaryGrid(const float isovalue);
  void buildCellGrid();
  void lookupContourLines(const float isovalue);
  void lookupContourPatches(const float isovalue);
  void computeDataDensity(const float h);
  float spikyKernel(const float r, const float h);
  float cubicSplineKernel(float r, const float h);
  void markCells(const int width, const int height);

  void drawGrid();
  void drawLines(const float isovalue);
  void drawPatches(const float isovalue);
  void drawInsideArea(const float isovalue);
  void drawBinaryImage();
  void drawSingleCell(int idx){
   Vector2d c0, c1, c2, c3;
   c0 = cells[idx].corners[0];
   c1 = cells[idx].corners[1];
   c2 = cells[idx].corners[2];
   c3 = cells[idx].corners[3];
   glColor4f(1, 0, 0, 1);
   glBegin(GL_LINE_LOOP);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glVertex3f(c2.x, c2.y, 0.0);
   glVertex3f(c3.x, c3.y, 0.0);
   glEnd();
  }
  void drawCellGrid(){
   for(int i = 0; i < cells.size(); i++){
    drawSingleCell(i);
   }
  }
  void drawSingleGrid(int idx){
   Vector2d c0, c1, c2, c3;
   c0 = grid[idx][0];
   c1 = grid[idx][1];
   c2 = grid[idx][2];
   c3 = grid[idx][3];
   glColor4f(1, 1, 0, 1);
   glBegin(GL_POLYGON);
   glVertex3f(c0.x, c0.y, 0.0);
   glVertex3f(c1.x, c1.y, 0.0);
   glVertex3f(c2.x, c2.y, 0.0);
   glVertex3f(c3.x, c3.y, 0.0);
   glEnd();
  }

  void copyBinaryValues(float* copy){
   for(int i = 0; i < grid.size(); i++)
    copy[i] = binaryValues[i];
  }

  void setBinaryValues(float* binary){
   for(int i = 0; i < grid.size(); i++){
    binaryValues[i] = binary[i];
   }
  }

  void copyValues(float* vals){
   for(int i = 0; i < grid.size(); i++){
    vals[i] = values[i];
   }
  }

  void setValues(float* vals){
   for(int i = 0; i < grid.size(); i++)
    values[i] = vals[i];
  }

  int getIndex(int i, int j){
   return i * cols + j;
  }
  
  int getCellIndex(int i, int j){
   return i * (cols-1) + j;
  }

  Vector2d linearInterpolation(const float c, const float a, const float b, Vector2d p0, Vector2d p1){
   float x, y, x0, x1, y0, y1;
   x0 = p0.x; y0 = p0.y; x1 = p1.x; y1 = p1.y;
   float frac = (c-a) / (b-a);
   x = (1 - frac) * x0 + frac * x1;
   y = (1 - frac) * y0 + frac * y1;
   return Vector2d(x, y);
  }

  float norm(const float isovalue){
   float max = 0.0;
   float min = 10e6;
   for(int i = 0; i < grid.size(); i++){
    if(values[i] >= max)
     max = values[i];
    if(values[i] <= min)
     min = values[i];
   }
   return (isovalue - min) / (max - min);
  }

 private:
  float* values;
  float* binaryValues;
  std::vector<Vector2d*> grid;
  int rows, cols;
  std::vector<Cell> cells;
};


#endif
