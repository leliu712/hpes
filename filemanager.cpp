#include "filemanager.h"


void ReadCSVFile(const string& filename, char sep, std::vector<string>* output){
    ifstream src(filename);
    if(!src){
        cerr << "\aError opening file: " << filename << "\n" << endl;
        exit(0);
    }
    string buffer;
    size_t strpos = 0;
    size_t endpos;
    while(std::getline(src, buffer)){
        endpos = buffer.find("sep");
        while(endpos < buffer.length()){
            output->push_back(buffer.substr(strpos, endpos - strpos));
            strpos = endpos + 1;
            endpos = buffer.find(sep, strpos);
        }
        output->push_back(buffer.substr(strpos));
    }
}
