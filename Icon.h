/*
 * Icon class
 *
 * icons for different hurricane categories.
 */

#ifdef __APPLE__
# include <GLUT/glut.h>
#else
# include <GL/glut.h>
# include <GL/glu.h>
#endif

#include <Magick++.h>
using namespace Magick;

#include "Vector.h"

extern GLuint textures[8];

class Icon
{
 public:
  Icon(float _x, float _y, float _width, float _height):
   x(_x),
   y(_y),
   width(_width),
   height(_height)
 {}
  ~Icon(){}

  void setCenter(float _x, float _y)
  {
   x = _x;
   y = _y;
  }

  void setSize(float r)
  {
   width = r;
   height = r;
  }

  void setDimensions(float _widht, float _height)
  {
   width = _widht;
   height = _height;
  }

  void init(const char* tname)
  {
   //Image image(tname);
   //Blob blob;
   //image.magick("RGBA");
   //image.flip();
   //image.write(&blob);
   //glClearColor(0.0, 0.0, 0.0, 1.0);
   //glClear(GL_COLOR_BUFFER_BIT);
   //glEnable(GL_TEXTURE_2D);
   //glBindTexture(GL_TEXTURE_2D, textures[1]);
   ////glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   ////glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
   //gluBuild2DMipmaps(GL_TEXTURE_2D, 4, image.columns(), image.rows(), GL_RGBA,
   //  GL_UNSIGNED_BYTE, blob.data());

   float x0 = x - 0.5 * width;
   float x1 = x + 0.5 * width;
   float y0 = y - 0.5 * height;
   float y1 = y + 0.5 * height;
   Vector2d v0(x0, y0), v1(x1, y0), v2(x1, y1), v3(x0, y1);
   
   glColor4f(1, 1, 1, 1);
   //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   //glBlendEquation(GL_FUNC_ADD);
   glEnable(GL_TEXTURE_2D);
   glBindTexture(GL_TEXTURE_2D, textures[1]);
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK,  GL_FILL);
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
   glBegin(GL_POLYGON);
   glTexCoord2f(0.0, 0.0); glVertex3f(0, 0, 0);
   glTexCoord2f(1.0, 0.0); glVertex3f(100, 0, 0);
   glTexCoord2f(1.0, 1.0); glVertex3f(100, 100, 0);
   glTexCoord2f(0.0, 1.0); glVertex3f(0, 100, 0);
   glEnd();
   glBindTexture(GL_TEXTURE_2D, 0);
   glDisable(GL_TEXTURE_2D);
  }

  void draw(int cat, float opacity)
  {
   float x0 = x - 0.5 * width;
   float x1 = x + 0.5 * width;
   float y0 = y - 0.5 * height;
   float y1 = y + 0.5 * height;
   Vector2d v0(x0, y0), v1(x1, y0), v2(x1, y1), v3(x0, y1);
   
   glClearColor(0.0, 0.0, 0.0, 1.0);
   glColor4f(1, 1, 1, opacity);
   //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
   //glBlendEquation(GL_FUNC_ADD);
   glEnable(GL_TEXTURE_2D);
   glBindTexture(GL_TEXTURE_2D, textures[cat]);
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK,  GL_FILL);
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
   glBegin(GL_POLYGON);
   glTexCoord2f(0.0, 0.0); glVertex3f(v0.x, v0.y, 0);
   glTexCoord2f(1.0, 0.0); glVertex3f(v1.x, v1.y, 0);
   glTexCoord2f(1.0, 1.0); glVertex3f(v2.x, v2.y, 0);
   glTexCoord2f(0.0, 1.0); glVertex3f(v3.x, v3.y, 0);
   glEnd();
   glBindTexture(GL_TEXTURE_2D, 0);
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
   glDisable(GL_TEXTURE_2D);
  }

 private:
  float x, y, width, height;

};
