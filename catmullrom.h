/*
 * catmull rom
 */
#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>
#include "Vector.h"

using namespace std;

std::vector<Vector2d> CatmullRom(std::vector<Vector2d*> ctrls, int nctrls, int nsamples){
 if(nctrls < 4){
  cout << "Number of Control Point is less than 4." << endl; 
  exit(0);
 }

 float* x = new float[nctrls];
 float* y = new float[nctrls];
 for(int i = 0; i < nctrls; i++){
  x[i] = ctrls[i]->x; 
  y[i] = ctrls[i]->y; 
 }

 vector<Vector2d> samples;
 for(int i = 1; i < nctrls-2; i++){
  float delta = 1.0 / nsamples;
  for(int k = 0; k < nsamples; k++){
   float t = k * delta; 
   float xcr, ycr;
   xcr = x[i] + 0.5*t*(-x[i-1]+x[i+1]) 
    + t*t*(x[i-1] - 2.5*x[i] + 2*x[i+1] - 0.5*x[i+2])
    + t*t*t*(-0.5*x[i-1] + 1.5*x[i] - 1.5*x[i+1] + 0.5*x[i+2]);
   ycr = y[i] + 0.5*t*(-y[i-1]+y[i+1]) 
    + t*t*(y[i-1] - 2.5*y[i] + 2*y[i+1] - 0.5*y[i+2])
    + t*t*t*(-0.5*y[i-1] + 1.5*y[i] - 1.5*y[i+1] + 0.5*y[i+2]);
   Vector2d push(xcr, ycr);
   samples.push_back(push);
  }
 }
 return samples;
}
