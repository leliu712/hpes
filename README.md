##  Visualizing Uncertain Tropical Cyclone Predictions using Representative Samples from Ensembles of Forecast Tracks

#### By Le Liu at 06/14/2018 #### 

### Introduction ###

This repository contains research codes of a paper, 
- *Visualizing Uncertain Tropical Cyclone Predictions using Representative Samples from Ensembles of Forecast Tracks* 

which has been conditionally accepted by IEEE Vis Conference. This repository serves as a supplementary material for this submission.

In addition, this repository also contains research codes of other papers published by our group, including:
- *Cox, Jonathan, and Michael Lindell. "Visualizing uncertainty in predicted hurricane tracks." 
International Journal for Uncertainty Quantification 3.2 (2013).*
- *Liu, Le, et al. "Visualizing Time‐Specific Hurricane Predictions, with Uncertainty, from Storm Path Ensembles." 
Computer Graphics Forum. Vol. 34. No. 3. 2015.*
- *Ruginski, Ian T., et al. "Non-expert interpretations of hurricane forecast uncertainty visualizations." 
Spatial Cognition & Computation 16.2 (2016): 154-172.*
- *Liu, Le, et al. "Uncertainty Visualization by Representative Sampling from Prediction Ensembles." 
IEEE transactions on visualization and computer graphics 23.9 (2017): 2165-2178.*

Please contact Le Liu at lel@g.clemson.edu for more information.

