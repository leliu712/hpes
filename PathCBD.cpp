#include "PathCBD.h"
#include "CDFSample.h"

#include <iostream>
#include <fstream>

#include <omp.h>

#define EPS 10e-6

using namespace std;

static int sub_size = 3;

bool sort_predD(const my_pair& left, const my_pair& right){
 return left.first > right.first;
}

vector<int> getSortedIndex(const vector<double>& v){
 vector<my_pair> VIndx;
 for(unsigned int i=0; i<v.size(); i++){
  VIndx.push_back(my_pair(v[i],i));
 }
 sort(VIndx.begin(), VIndx.end(), sort_predD);
 vector<int> indx;
 for(unsigned int i=0; i<VIndx.size(); i++)
  indx.push_back(VIndx[i].second);
 return indx;
}


PathCBD::PathCBD(std::vector<path*> _e, Matrix _M):
 M(_M)
{
 //int tt = 2000;
 //long double  combsize = K(tt, int(3));
 //long double test = 1234;
 //cout << combsize << endl;
 //cout << test / combsize << endl;
 //exit(0);

 for(int i = 0; i < _e.size(); i++)
  ensembles.push_back(_e[i]);

 if(ensembles.size() <= 3){
  cerr << "The size of ensembles should be at least 4." << endl;
  return;
 }

 Probs.resize(ensembles.size(), 0.0);
 segmentProbs.resize(ensembles.size());


 //cout << "ensemble size: " << ensembles.size() << " " << segmentProbs.size() << endl;
 maxpathlength = 0;
 for(int i = 0; i < ensembles.size(); i++){
  if(maxpathlength <= ensembles[i]->posList.size())
   maxpathlength = ensembles[i]->posList.size();
 }
 //cout << "max path length: " << maxpathlength << endl;

 //Analysis();
 //OptAnalysis(); 
 

}

PathCBD::~PathCBD(){
 //for(int i = 0; i < maxpathlength; i++)
 // delete[] segmentProbs[i];

 //for(int i = 0; i < ensembles.size(); i++)
 // delete[] ensembles[i];

}


void PathCBD::Analysis(){
 //Matrix M;
 //M = getCombinationMatrix(ensembles.size(), sub_size);
 //std::vector< vtkSmartPointer<vtkPolygon> > band;
 //std::vector<path*> PathSet;
 //
 //cout << "combinations size: " << M.ncols() << endl;
 //
 //for(int i = 0; i < M.ncols(); i++){  //goes over combinations
 // PathSet.clear();
 // for(int j = 0; j < M.nrows(); j++){  //goes over choices
 //  PathSet.push_back(ensembles[M[j][i]-1]);
 // }
 // band.clear();
 // band = getBand(PathSet);
 // for(int l = 0; l < ensembles.size(); l++){
 // }
 //}
}

void PathCBD::OptAnalysis(){
 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
 cout << npro << endl;
 //cout << "combinations size: " << M.ncols() << endl;
 std::vector<path*> PathSet;

 for(int i = 0; i < M.ncols(); i++){
  PathSet.clear();
  for(int j = 0; j < M.nrows(); j++){
   //PathSet.push_back( new path(*ensembles[M[j][i] - 1]) );
   PathSet.push_back(ensembles[M[j][i] - 1]);
  }

#pragma omp parallel for
  for(int k = 0; k < ensembles.size(); k++){
   computeDataDepth(ensembles[k], PathSet, k);
  }
 }

 // normalize
 //for(int i = 0; i < ensembles.size(); i++){
 // Probs[i] /= double(M.ncols());
 // for(int j = 0; j < maxpathlength; j++)
 //  segmentProbs[i][j] /= double(M.ncols());
 //}

}

void PathCBD::computeDataDepth(path* p, std::vector<path*> PathSet, int idx){
 if(p->posList.size() < maxpathlength){
  cout << "path posList < 24." << endl;
  exit(-1);
 }

 //segmentProbs[idx] = new double[maxpathlength];
 for(int i = 0; i < maxpathlength; i++){
  Vector3d x(p->posList[i]->x, p->posList[i]->y, 0.0);
  Vector3d p0(PathSet[0]->posList[i]->x, PathSet[0]->posList[i]->y, 0.0);
  Vector3d p1(PathSet[1]->posList[i]->x, PathSet[1]->posList[i]->y, 0.0);
  Vector3d p2(PathSet[2]->posList[i]->x, PathSet[2]->posList[i]->y, 0.0);
  int inside = Inside(x, p0, p1, p2);
  //segmentProbs[idx][i] = double(inside) / double(maxpathlength);
  //Probs[idx] += segmentProbs[idx][i];
  
  double tmp = double(inside) / double(maxpathlength);
  Probs[idx] += tmp; 
 }
}

Matrix PathCBD::getCombinationMatrix( unsigned int n,  unsigned int k){
 Matrix rtn;
 std::vector<int> ints;
 for(int i = 1; i <= n; ints.push_back(i++));

 unsigned long ncombination = Choose(n, k);

 rtn = Matrix(k, ncombination);

 int counter = 0;
 do{
  for(int i = 0; i < k; i++){
   rtn[i][counter] = ints[i];
  }
  counter ++;
 }while(next_combination(ints.begin(), ints.begin() + k, ints.end()));

 //cout << rtn << endl;

 return rtn;
}

unsigned long PathCBD::Choose( unsigned int n,  unsigned int k){
 if(k > n){
  return 0;
 }
 unsigned long r = 1;
 for(unsigned long d = 1; d <= k; ++d){
  r *= n--;
  r /= d;
 }
 return r;
}

template <typename Iterator>
bool PathCBD::next_combination(const Iterator first, Iterator k, const Iterator last){
 /* Credits: Thomas Draper */
 if ((first == last) || (first == k) || (last == k))
  return false;
 Iterator itr1 = first;
 Iterator itr2 = last;
 ++itr1;
 if (last == itr1)
  return false;
 itr1 = last;
 --itr1;
 itr1 = k;
 --itr2;
 while (first != itr1){
  if (*--itr1 < *itr2){
   Iterator j = k;
   while (!(*itr1 < *j)) ++j;
   std::iter_swap(itr1,j);
   ++itr1;
   ++j;
   itr2 = k;
   std::rotate(itr1,j,last);
   while (last != j){
    ++j;
    ++itr2;
   }
   std::rotate(k,itr2,last);
   return true;
  }
 }
 std::rotate(first,k,last);
 return false;
}

/*
vtkSmartPointer<vtkPolygon> PathCBD::getTriangle(std::vector<Vector2d> v){
 // forms a vtk triangle
 vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
 for(int i = 0; i < v.size(); i++){
  polygon->GetPoints()->InsertNextPoint(v[i].x, v[i].y, 0.0);
 }
 return polygon;
}

std::vector< vtkSmartPointer<vtkPolygon> > PathCBD::getBand(std::vector<path*> PSet){
 std::vector< vtkSmartPointer<vtkPolygon> > rtn;
 vtkSmartPointer<vtkPolygon> polygon = vtkSmartPointer<vtkPolygon>::New();
 std::vector<Vector2d> PointSet;

 for(int i = 0; i < maxpathlength; i++){
  PointSet.clear();
  for(int j = 0; j < PSet.size(); j++){
   PointSet.push_back(Vector2d(PSet[j]->posList[i]->x, PSet[j]->posList[i]->y));
  }

  if(PointSet.size() == sub_size){ // right now the sub size is 3
   Vector2d p1 = PointSet[0];
   Vector2d p2 = PointSet[1];
   Vector2d p3 = PointSet[2];
   double check = (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
   if(check < 0){
    PointSet.clear();
    PointSet.push_back(p3);
    PointSet.push_back(p2);
    PointSet.push_back(p1);
   }
  }
  else{}
  polygon = getTriangle(PointSet);
  rtn.push_back(polygon);
 }

 return rtn;
}
*/

std::vector<int> PathCBD::sortSegmentIdx(std::vector<double> tmp){
 std::vector<int> rtn = getSortedIndex(tmp);
 return rtn;
}

void PathCBD::fastPathSimplexDepth(){
 for(int i = 1; i < maxpathlength; i++)
  fastSegmentSimplexDepth(i);

 float maxVal = *std::max_element(Probs.begin(), Probs.end());
 float minVal = *std::min_element(Probs.begin(), Probs.end());
 for(int i = 0; i < Probs.size(); i++)
  Probs[i] = (Probs[i] - minVal) / (maxVal - minVal);
 SortedIdx = getSortedIndex(Probs);
 ofstream myfile("PATHSD.txt", fstream::out);
 for(int i = 0; i < Probs.size(); i++)
  myfile << Probs[i] << endl;
 myfile.close();
 
 exit(0);
}

std::vector<double> PathCBD::fastSegmentSimplexDepth(const int time){
 std::vector<Vector2d> pointSet;
 std::vector<double> segmentDataDepth;
 segmentDataDepth.resize(ensembles.size(), 0.0);
 for(int j = 0; j < ensembles.size(); j++){
  Vector2d pos(ensembles[j]->posList[time]->x, ensembles[j]->posList[time]->y);
  pointSet.push_back(pos);
 }

int npro = omp_get_max_threads();
omp_set_num_threads(npro);
#pragma omp parallel for
 for(int j = 0; j < ensembles.size(); j++){
  Vector2d P(ensembles[j]->posList[time]->x, ensembles[j]->posList[time]->y);
  double depth = fastPointSimplexDepth(pointSet, P);
  Probs[j] = Probs[j] + double(depth * 1.0) / double(maxpathlength) / double(ensembles.size());
  segmentDataDepth[j] = double(depth * 1.0);
 }
 return segmentDataDepth;
}

std::vector<double> PathCBD::fastSegmentSimplexDepth2(const int time, 
  std::vector<Vector2d> evalList){
 std::vector<Vector2d> pointSet;
 std::vector<double> segmentDataDepth;
 segmentDataDepth.resize(evalList.size(), 0.0);
 for(int j = 0; j < ensembles.size(); j++){
  Vector2d pos(ensembles[j]->posList[time]->x, ensembles[j]->posList[time]->y);
  pointSet.push_back(pos);
 }

 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
#pragma omp parallel for
 for(int j = 0; j < evalList.size(); j++){
  Vector2d P = evalList[j];
  double depth = fastPointSimplexDepth(pointSet, P);
  //Probs[j] = Probs[j] + double(depth * 1.0) / double(maxpathlength) / double(ensembles.size());
  segmentDataDepth[j] = double(depth * 1.0);
 }
 return segmentDataDepth;
}

// Given a set of points, return the associated simplicial depth values
std::vector<double> PathCBD::fastSegmentSimplexDepth3( std::vector<Vector2d> evalList)
{
 std::vector<Vector2d> pointSet;
 std::vector<double> segmentDataDepth;
 segmentDataDepth.resize(evalList.size(), 0.0);

 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
#pragma omp parallel for
 for(int j = 0; j < evalList.size(); j++){
  Vector2d P = evalList[j];
  double depth = fastPointSimplexDepth(evalList, P);
  //Probs[j] = Probs[j] + double(depth * 1.0) / double(maxpathlength) / double(ensembles.size());
  segmentDataDepth[j] = double(depth * 1.0);
 }
 return segmentDataDepth;
}

/*
std::vector<double> PathCBD::fastSegmentSimplexDepth(const int time){
 std::vector<Vector2d> pointSet;
 std::vector<double> segmentDataDepth;
 segmentDataDepth.resize(ensembles.size(), 0.0);
 for(int j = 0; j < ensembles.size() / 10; j++){
  Vector2d pos(ensembles[j * 10]->posList[time]->x, ensembles[j * 10]->posList[time]->y);
  pointSet.push_back(pos);
 }

 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
#pragma omp parallel for
 for(int j = 0; j < ensembles.size(); j++){
  Vector2d P(ensembles[j]->posList[time]->x, ensembles[j]->posList[time]->y);
  int depth = fastPointSimplexDepth(pointSet, P);
  Probs[j] = Probs[j] + double(depth * 1.0) / double(maxpathlength) / double(ensembles.size());
  segmentDataDepth[j] = double(depth * 1.0) / double(maxpathlength) / double(ensembles.size());
 }
 return segmentDataDepth;
}
*/

double PathCBD::fastPointSimplexDepth(std::vector<Vector2d> X, Vector2d P){
 //construct the array ALPHA
 int NT = 0;
 int N = X.size();
 std::vector<double> alpha;
 alpha.clear();
 alpha = constructALPHA(X, P, NT);

 int NN = N - NT;
 if(NN < 1)
  return 0;
 
 if(alpha.size() == 0)
  return 0 ;

 //sort the array ALPHA
 std::sort(alpha.begin(), alpha.end());
 
 //check whether P lies outside the data cloud
 int size = alpha.size();
 double angle = alpha[0] - alpha[size - 1] + 2 * PI;
 for(int i = 2; i < size; i++)
  angle = std::max(angle, (alpha[i] - alpha[i - 1]));
 if(angle > PI + EPS)
  return 0;

 //make smallest alpha equal to zero, and compute NU = number of alpha < PI
 angle = alpha[0];
 int NU = 0;
 for(int i = 0; i < size; i++){
  alpha[i] = alpha[i] - angle;
  if(alpha[i] < (PI - EPS))
   NU++;
 }
 if(NU > size)
  return 0;
 
 //mergesort the alpha with their antipodal angles beta, and at the same time update I, F[I], and NBAD
 //return NBAD
 long double NBAD = mergesort(alpha, NU);
 long double combsize = K(size, 3);
 double depth = ((combsize) - (NBAD)) / (combsize);

 if(depth < 0){
  depth = 0;
 }

 return depth;

}

std::vector<double> PathCBD::constructALPHA(std::vector<Vector2d> X, Vector2d P, int& NT){
 std::vector<double> ALPHA;
 ALPHA.clear();
 int test = 0;
 for(int i = 0; i < X.size(); i++)
 {
  float alpha(0.0);
  Vector2d u = X[i] - P;
  float D = u.norm();
  if(D < EPS){
   NT = NT + 1;
  }
  else
  {
   u = u / D;
   if(abs(u.x) - abs(u.y) > EPS)
   {
    if(X[i].x - P.x > EPS)
    {
     alpha = asin(u.y);
     if(alpha < 0.0)
      alpha = 2 * PI + alpha;
    }
    else
     alpha = PI - asin(u.y);
   }
   else
   {
    if(X[i].y - P.y > EPS)
     alpha = acos(u.x);
    else
     alpha = 2 * PI - acos(u.x);
   }

   if(alpha > 2 * PI - EPS)
    alpha = 0.0;
   ALPHA.push_back(alpha);
  }
 }
 return ALPHA;
}


//problem here
unsigned int PathCBD::mergesort(std::vector<double> alpha, int NU){
 int NN = alpha.size();
 int JA = 0;
 int JB = 0;
 double alphaK = alpha[0];
 double betaK  = alpha[NU] - PI; // alpha[NU] is the first alpha > PI, minus PI gives the first beta
 int NF = NN;
 int NN2 = NN * 2;
 int I = NU;
 unsigned int NBAD = 0;
 for(int j = 0; j < NN2; j++){
  if(alphaK + EPS < betaK){
   NF = NF + 1;
   if(JA < NN - 1){
    JA = JA + 1;
    alphaK = alpha[JA];
   }
   else
    alphaK = 2 * PI + 1.0;
  }
  else{
   I = I + 1;
   if(I == NN){
    I = 1;
    NF = NF - NN;
   }
   NBAD = NBAD + K((NF - I), 2);
   if(JB < NN - 1){
    JB = JB + 1;
    if((JB + NU) <= NN - 1)
     betaK = alpha[JB + NU] - PI;
    else
     betaK = alpha[JB + NU - NN] + PI;
   }
   else
    betaK = PI * 2 + 1.0;
  }
 }
 return NBAD;
}

unsigned long long PathCBD::K(const unsigned int M, const unsigned int J){
 unsigned long long rtn;
 if(M < J)
  rtn = 0;
 else{
  if(J == 1){
   rtn = M;
  }
  if(J == 2){
   rtn = M * (M-1);
   rtn = rtn / 2;
  }
  if(J == 3){
   rtn = M * (M-1);
   rtn = rtn * (M-2) / 6;
  }
 }
 return rtn;
}










