/*
 * RBF interpolation
 */
#ifndef __RBF_H__
#define __RBF_H__
#include <iostream>
#include <vector>
#include "Vector.h"
#include "Matrix.h"


class RBF{
 public :
  RBF(){}
  ~RBF(){}

  RBF(const RBF &rbf)
  {
    samples = rbf.samples;
    sampleValues = rbf.sampleValues;
    Phi = rbf.Phi;
    W = rbf.W;
    F = rbf.F;
    kernelFactor = rbf.kernelFactor;
    kernelFactors = rbf.kernelFactors;
  }

  void compute(std::vector<Vector2d> trainingSet,
    std::vector<double> trainingValues, double f);

  void compute(std::vector<Vector2d> trainingSet,
    std::vector<double> trainingValues,
    std::vector<double> factors);

  double eval(Vector2d c);
  double eval(Vector2d c, double factor);
  double eval(Vector2d c, std::vector<double> factors);

  void RBFtest();
  void RBFtest(std::vector<double> factors);

  void setRBFMatrix(Matrix m);
  void setWeights(Vector2d w);
  void setValues(Vector2d v);
  long double RBFkernel(Vector2d c, Vector2d p);
  double RBFkernel(Vector2d c, Vector2d p, double factor);
  void setRBFKernelFactors(double* factors);

 //private:
  std::vector<Vector2d> samples;
  std::vector<double> sampleValues;
  Matrix Phi;
  Vector W;
  Vector F;
  double kernelFactor;
  std::vector<double> kernelFactors;
};

#endif
