/*
 * advsiory.h
 *
 *  Created on: Aug 20, 2010
 *  Jonathan Cox
 *  Clemson University
 */

#ifndef ADVSIORY_H_
#define ADVSIORY_H_

//#include <boost/shared_ptr.hpp>
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/convex_hull_2.h>
//#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
//#include <CGAL/Boolean_set_operations_2.h>
//#include <CGAL/Polygon_with_holes_2.h>
//#include <CGAL/create_offset_polygons_from_polygon_with_holes_2.h>
//#include <CGAL/Polygon_2.h>
//#include <CGAL/Straight_skeleton_builder_2.h>
//#include <CGAL/Polygon_offset_builder_2.h>
//#include <CGAL/compute_outer_frame_margin.h>
//#include <CGAL/algorithm.h>
//#include <CGAL/Delaunay_triangulation_2.h>
//#include <CGAL/Alpha_shape_2.h>
//#include <CGAL/hilbert_sort.h>
//
//typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
//typedef K::Point_2 Point_2;
//typedef std::vector<Point_2> Points;
//typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
//typedef K::FT FT;
//typedef K::Segment_2 Segment;
//typedef CGAL::Polygon_2<K> Polygon_2;
//typedef CGAL::Straight_skeleton_2<K> Ss;
//typedef CGAL::Polygon_2<K> Contour;
//
//typedef boost::shared_ptr<Polygon_2> PolygonPtr;
//typedef boost::shared_ptr<Ss> SsPtr;
//typedef boost::shared_ptr<Contour> ContourPtr;
//typedef std::vector<PolygonPtr> PolygonPtrVector;
//typedef std::vector<ContourPtr> ContourSequence;
//
////typedef Ss::Halfedge_iterator Halfedge_iterator;
////typedef Ss::Halfedge_handle   Halfedge_handle;
////typedef Ss::Vertex_handle     Vertex_handle;
//
//typedef CGAL::Straight_skeleton_builder_traits_2<K>      SsBuilderTraits;
//typedef CGAL::Straight_skeleton_builder_2<SsBuilderTraits,Ss> SsBuilder;
//typedef CGAL::Polygon_offset_builder_traits_2<K>                  OffsetBuilderTraits;
//typedef CGAL::Polygon_offset_builder_2<Ss,OffsetBuilderTraits,Contour> OffsetBuilder;
//
//typedef CGAL::Alpha_shape_vertex_base_2<K> Vb;
//typedef CGAL::Alpha_shape_face_base_2<K>  Fb;
//typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
//typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation_2;
//typedef CGAL::Alpha_shape_2<Triangulation_2>  Alpha_shape_2;
//typedef Alpha_shape_2::Alpha_shape_edges_iterator Alpha_shape_edges_iterator;

#define NHC_DATA

#include <QGLWidget>
#include <QRegion>
#include <QPoint>
#include <QPointF>

#include <iostream>
#include <vector>
#include "Matrix.h"
#include "path.h"
#include "geoFunct.h"
#include "predictedPath.h"
#include "hurEyePoints.h"
#include <cmath>
#include "RBF.h"
#include "CDFSample.h"
#include "KDTree.h"
#include "gauss.h"
//#include "PoissonDiskSample.h"
//#include <gmpxx.h>
//#include "PrePoint.h"
#include "VectorField.h"
#include "PathsGenerator.h"
#include "PathResampling.h"
#include "KDTreeDensityEstimator.h"

#ifndef E
#define E 2.718281
#endif

#define ROUND(a) ((int) (a+0.5))

static int MipmapSwith = 0;

bool sortPath(path * p1, path * p2);
bool angleSort(Vector2d v1, Vector2d v2);
static double xp(Vector2d a, Vector2d b);
bool sortVectors(Vector2d l, Vector2d r);
bool sortVector3d(Vector3d l, Vector3d r);

class advisory{
 private:
  // Position variables
  double lat;
  double lon;
  double deg;
  double speed;
  double projPathDist;
  void buildProjPath(std::vector<Vector2d*> projPathPos, double projPathDir);
  void buildProjPathFull();
  void buildErrorCone();
  int presLat, presLon;
  int numIn68;
  int onRight, onLeft;

  double inConePercent;
  double usePredictedPercent;
  double baseConePercent;

  double percBubDrawn;

  //Vector4d** posColor;
  double hurSizeMean;

  // NHC file 
  string NHCinputpre, NHCoutputpre, NHCfilename;

 public:
  std::vector<PrePoint> subPoints;
  std::vector<PrePoint> fullPoints;
  std::vector<std::vector<PrePoint>> pointsBand;
  std::vector<std::vector<PrePoint>> pointsBandBackup;
  float UDDmin;
  std::vector<float> ODDmin;
  // List of projected paths
  std::vector<path*> pathList;
  std::vector<path*> pathListRev;
  std::vector<hurEyePoints*> eyePointList;

  // The predicted path 
  predictedPath * pre;

  std::vector<path*> closestToPre;
  std::vector<path*> closestToPreStdDev;
  std::vector<Vector2d*> newAvg;

  // The projected path of a specific hurricane at a given time step
  std::vector<Vector4d*> projPath;     // (lat, lon, bearing change, speed change)
  std::vector<Vector2d*> projPathFull;
  std::vector<double> distWeightCone;

  // Error cone - Error radius based on 2010 data
  // ecSmooth contains the data used to generate the pdf functions for speed and bearing
  // change 4d vector gives (lat, lon, bearing, speed) ---- lat and lon might be
  std::vector<Vector4d*> ecSmooth1;
  std::vector<Vector4d*> ecSmooth2;

  // trueCone holds the data used to draw the representation of the NHC error cone
  // as displayed in the user study
  std::vector<Vector2d*> trueCone1;
  std::vector<Vector2d*> trueCone2;
  // additional cones with different radius
  std::vector<Vector2d*>* cones1;
  std::vector<Vector2d*>* cones2;
  // boxplot cones
  std::vector<Vector2d*> boxplotCones1;
  std::vector<Vector2d*> boxplotCones2;
  // smooth cones
  std::vector<Vector2d*> ctrlp1;
  std::vector<Vector2d*> ctrlp2;
  std::vector<Vector2d*> ctrlCone;
  std::vector<Vector2d> smoothCone1;
  std::vector<Vector2d> smoothCone2;
  std::vector<Vector2d*>*ctrlps1;
  std::vector<Vector2d*>*ctrlps2;
  std::vector<Vector2d>* smoothCones1;
  std::vector<Vector2d>* smoothCones2;

  std::vector<Vector2d*> testall;
  Vector2d testp;
  Vector2d testp1;


  // Constructor
  advisory(std::vector<Vector2d*> projPathPos, double curLat, double curLon, double curDeg, double curSpeed, string predFile);

  // Get functions
  double getLat(){ return lat; }
  double getLon(){ return lon; }
  double getDeg(){ return deg; }
  double getSpeed(){ return speed; }
  double getProjPathDist(){ return projPathDist; }

  // Draw functions
  void drawGenPaths();
  void drawGenPathsRainDrop();
  void drawGenPathsEyePoint();
  int drawGenPathsTrail(int);
  void drawHeatMap();
  void drawForecastPath();
  void drawErrorSmooth();
  void drawErrorConeRadius();

  // Draw Function -- countour box plot
  void drawOutliers();
  int isOutlier(std::vector<path*>::iterator pathIt);

  // Current point on path information
  int getPresLat(){ return presLat; }
  int getPresLon(){ return presLon; }
  void setPresLat(int i){ presLat = i; }
  void setPresLon(int i){ presLon = i; }

  // Functions for finding median and std deviation lines based on median
  void sortPath(int seg);

  // Build paths using predicted path normal distribution

  // Interpolations for error cone
  void buildECSmooth(std::vector<Vector2d> &, std::vector<Vector2d> &);
  void interpECSmooth(Vector2d p0, Vector2d p1, double h, int side);

  // Build and draw the NHC Error Cone
  void buildTrueCone();
  void buildTrueConeSide(int);
  void drawTrueCone();

  // Build and draw smooth error cone
  void buildSmoothCone();
  void buildSmoothConeSide(int side);
  void drawSmoothCone();
  void drawSmoothOutline();
  void drawMultiSmoothCones(int n);

  void findInConePercent();
  void updatePredictedPercent();
  double getInConePercent(){ return inConePercent; }
  double getPredictedPercent(){ return usePredictedPercent; }
  double getBasePercent(){ return baseConePercent; }

  void colorEncoding(Vector4d ** posColor);
  void drawCategories(double x, double y, double radius, int category, double opacity);

  void makeCones(int side, int id);
  void makeSmoothCones(int side, int id);
  void makeBoxplotCones(int side, double frac);
  void drawMultiCones(int n);
  void drawBoxplotCones();
  double computeRation(int id);
  double computeBoxplotRation(double frac);

  void drawTrueConeOutline();
  Vector2d deboor(int k, int degree, int i, float u, float knots[], Vector2d* coeff);
  void bsplineCurve(std::vector<Vector2d> controlPolygons);

  void drawCircle(float x, float y, float radius);
  void drawSolidCircle(float x, float y, float radius);

  // heatmap
  void drawHeatMapGrid(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height);
  void drawHeatMapGridDataDepth(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height);
  void drawSplatHeatMapGrid(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height);

  // funtion to generate images for percepture study
  void drawExperimentPoints();
  void findLocationBasedOnTime(float time, Vector2d& location, float& radius);

  // data depth values and contour box plot
  std::vector<int> SortedIdx;
  std::vector<int> segSortedIdx;
  Matrix M; // combination Matrix
  void drawDataDepthPlot(); // path data depth
  void drawDataDepthPlotofTime();
  void drawDataDepthPlotofTimeSplat(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height);
  void drawDataDepthPlotofAllTime();
  void drawDataDepthPlotRBF(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height, double tlat, double tlon, double mlat, double mlon);
  void drawDataDepthPlotRBFwithNHC(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height, double tlat, double tlon, double mlat, double mlon, int timeframe, float rbfkernelfactor, const string& nhcfilename);
  void drawPureDataDepthPlot(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height, double tlat, double tlon, double mlat, double mlon);

  RBF RBFatTime(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height, double tlat, double tlon, double mlat, double mlon, int timeframe, float rbfkernelfactor, std::vector<double>& rbfradii);

  void drawCGALContour(Points poly);
  void drawCGALPolygon(CGAL::Polygon_2<K> const& poly);
  void drawCGALPolygons(std::vector<boost::shared_ptr< CGAL::Polygon_2<K> > > const& polies);
  Matrix getCombinationMatrix(unsigned int n, unsigned int k);
  template <typename Iterator>
   bool next_combination(const Iterator first, const Iterator k, const Iterator last);
  unsigned long Choose(unsigned int n, unsigned int k){
   if(k > n)
    return 0;
   unsigned long r = 1;
   for(unsigned long d = 1; d <= k; ++d){
    r *= n--;
    r /= d;
   }
   return r;
  }

  Points getConvexHull(Points in){
   Points result;
   CGAL::convex_hull_2(in.begin(), in.end(), std::back_inserter(result));
   return result;
  }

  template <class OutputIterator>
   void
   alpha_edges( const Alpha_shape_2&  A,
     OutputIterator out)
   {
    for(Alpha_shape_edges_iterator it =  A.alpha_shape_edges_begin();
      it != A.alpha_shape_edges_end();
      ++it){
     *out++ = A.segment(*it);
    }
   }

  void setMorphology();

  double opacityCurve(double val, double factor, double power){
   //return (pow(1.0/(val-factor), power));
   //return (pow((val-factor), power));
   //return log(4) / (log(pow(2 * (1-val) + 1.26, 2)));
   double p = log(100.0);
   double rtn = pow(1.0/ (1.0-(val-1.0)), p);
   return rtn;
  }

  double RBFKernelCus(double R, double dis, float f);
  double RBFKernelCus(Vector2d p0, Vector2d p1);
  double RBFKernelGaussian(double dis, float f);
  long double RBFKernelGaussian(Vector2d c, Vector2d p);
  double RBFKernelThinPlateSpline(Vector2d c, Vector2d p);

  void translateToGeo();
  void adjustKernelFactor(int mode);

  std::vector<double> buildPDF(std::vector<double> vals, int size);
  std::vector<double> buildPDF(std::vector<double> depthvalue, 
    std::vector<double> indexvalue, int size);
  std::vector<double> buildCDF(std::vector<double> pdf);
  double getValueFromCDF(std::vector<double> pdf, double val, int intervalsize);

  void swithmipmap(int i){MipmapSwith = i; cout << "Mipmap: " << i << endl;}

  void readMeanTrack(const char* filename);
  std::vector< std::vector<float> > readNHCTracks(const string& filename, int framenum);
  void readErrorCone(const char* filename);
  bool outputImage();
  void renderFromBinaryFile(int timeframe);
  std::vector< std::vector<float> > readPixelColorFromFile(int i);
  void renderUnionconeFromFiles();
  void renderUnionconeFromFiles(int frameno);
  std::vector<Vector2d*> buildDensityGrid(float minX, float minY, float maxX, float maxY, float& dw, float &dh);

  void outputTracks(char* filename);
  std::vector<double> buildRBFRadiusVector(std::vector<Vector2d> pos, RBF densityRBF, double base);
  std::vector<double> buildRBFRadiusVector(std::vector<Vector2d> pos, RBF densityRBF, double base, float factor);
  double computeRBFradiusFromDensity(RBF densityRBF, Vector2d c, double base);
  float GaussianDis(float x, float delta, float miu);
  std::vector<double> buildRBFRadii(std::vector<Vector2d> pos, 
    std::vector<double> densityList, 
    std::vector<int> indicesList, double width, double factor);
  void removeDuplication(float dw, int time);
  float IDW(std::vector<Vector2d> trainingSet, std::vector<double> vals, Vector2d c);
  void fillConvexHull(int width, int height, std::vector<Points> convexhulls);
  void findConcaves(Points points, std::vector<Segment> &segments);
  void findEllipse(Points convexhull, Vector2d& center, Vector2d& axes, float& theta);

  void setPixel(int x, int y);
  void ellipsePlotPoints(int xc, int yc, int x, int y);
  void drawEllipse(int xCenter, int yCenter, int Rx, int Ry);
  void drawEllipses(std::vector<QRegion> rs, int width, int height,
    std::vector<Vector2d> Cs, std::vector<float> thetas);

  void writeEllipseToFile(std::vector<Vector2d> Cs, std::vector<float> thetas, std::vector<Vector2d> axisList, int i);
  void renderEllipseFromFile(int i, int width, int height);
  void renderEllipsesFromFile();
  void writeRBFdata(std::vector<Vector2d> data, std::vector<double> values, std::vector<double> radii);
  void computeRadCurve(float rmin, float rmax, float dmin, float dmax, 
    float& K, float& P);
  void writeDensityData(std::vector<Vector2d> P, std::vector<double> density, 
    const int frameid);
  std::vector< std::vector<float> > writeGeneratedPath();

  void generateTestData()
  {
   int N = 999;
   unsigned int counter = time(NULL);
   glPointSize(3);
   glColor4f(1, 0, 0, 1);
   glBegin(GL_POINTS);
   for(int i = 0; i < pathList.size(); i++)
   {
    for(int j = 0; j < pathList[i]->posList.size(); j++)
    {
     cout << i << " " << j << " " << *(pathList[i]->posList[j]) << endl;
     srand(counter);
     int seed  = int(rand() % 1000);
     float x = (gauss(0, 1.0, 50)) * 50 + 500;
     srand(counter);
     seed  = int(rand() % 1000);
     float y = (gauss(0, 1.0, 50)) * 50 + 400;
     pathList[i]->posList[j] = new Vector2d(x, y);
     glVertex3f(x, y, 0.0);
    }
   }
   glEnd();
  }

//  template<typename T>
//   std::vector<double> KDTreeDensity(T data, int size, int dim,
//     std::vector<int>& validlist, float threshold)
//  {
//   int K = 10;
//   KDTree kdtree;
//   kdtree.buildKDTree(kdtree.formatInput<T>(data, data.size(), dim));
//   std::vector<double> density;
//   density.resize(data.size(), 0);
//   validlist.clear();
//   validlist.resize(data.size(), 1);
//   for(int i = 0; i < size; i++)
//   {
//    if(validlist[i] != 0)
//    {
//     Point* p = kdtree.createPoint(dim);
//     for(int j = 0; j < dim; j++)
//      p->coord[j] = data[i][j];
//
//     // 11 is the K value for KNN
//     // 11 = 10 + 1, the last element in the queue is the testing point
//     // maybe 1/100 of the size of the training set
//     bounded_priority_queue<QueueTuple> bpq = kdtree.knearest(p, K+1);
//     //cout << "bpq size: " << bpq.size() << endl;
//     //bounded_priority_queue<QueueTuple> tmp(bpq);
//
//     float maxR(0);
//     // merge points too close
//     while(!bpq.queue_.empty())
//     {
//      // r is already sqr distance
//      float r = bpq.queue_.top().first;
//      maxR = max(maxR, r);
//      if(r < threshold * threshold)
//      {
//       // if a point is too close to the query
//       // set its valid flag to 0
//       int idx = 0;
//       Point* p_ = bpq.queue_.top().second;
//       idx = p_->index;
//       if(idx != i)
//        validlist[idx] = 0;
//      }
//      bpq.queue_.pop();
//     }
//
//     float area = PI * maxR;
//     float d = K / area;
//     density[i] = d;
//
//     if(dim > 2)
//     {
//      area = maxR;
//      d = K / area;
//      density[i] = d;
//     }
//
//    }
//   }
//
//   //normalize density to [0, 1];
//   float max_den = *(max_element(density.begin(), density.end()));
//   for(int i = 0; i < density.size(); i++)
//   {
//    density[i] /= max_den;
//   }
//
//   return density;
//  }

  void readDensitySubset(
    std::vector<Vector2d>& P, 
    std::vector<double>& densities, 
    const char* filename)
  {
   std::vector<Vector2d> _P;
   std::vector<double> _densities;
   ifstream myfile(filename);
   string line;
   std::vector<int> indices;
   if(myfile.is_open())
   {
    while(getline(myfile, line))
     indices.push_back(stoi(line));
    myfile.close();
   }
   for(int i = 0; i < indices.size(); i++)
   {
    _P.push_back(P[indices[i]]);
    _densities.push_back(densities[indices[i]]);
   }
   P.clear();
   densities.clear();
   P = _P;
   densities = _densities;
  }
  
  // generate subset of paths by resampling UD spaces
  void GeneratePathsByResamplingUDSpaces();
  // generate structured paths by resampling circles of uncertainty cone
  void GenerateStructuredPaths();

  void testSortPath();
  void pathSampling();
  void cleanPaths();
  void generateNewPaths();
  void generateNewPaths2();
  void generateNewPaths3(const int width, const int height);
  void recursiveFindCenterline(std::vector< std::vector<Vector2d> > samples, const Vector2d start,
    int& level,
    const std::vector<Vector2d> precenterline,
    const int lor);
  void recursiveFindCenterline2(std::vector< std::vector<Vector2d> > samples, 
    const std::vector<Vector2d> precenterline,
    const std::vector< std::vector<float> > sds,
    const Vector2d start,
    const int width, const int height);
  void recursiveFindCenterline3(std::vector< std::vector<Vector2d> > samples, const Vector2d start,
    int& level,
    const std::vector<Vector2d> precenterline,
    const int lor);
  void groupSamples(std::vector< std::vector<Vector2d> >& leftpoints, 
    std::vector< std::vector<Vector2d> >& rightpoints,
    const std::vector< std::vector<Vector2d> > samples, const std::vector<Vector2d> centerline,
    const Vector2d start);
  int checkIntersections(path* p0, path* p1);
  void swappaths(path* p0, path* p1);
  Vector2d findIntersection(const Vector2d p1, const Vector2d p2, const Vector2d p3, const Vector2d p4)
  {
   float x1 = p1.x; float x2 = p2.x; float x3 = p3.x; float x4 = p4.x;
   float y1 = p1.y; float y2 = p2.y; float y3 = p3.y; float y4 = p4.y;
   if((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4) == 0)
    return Vector2d(0, 0);
   float x = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / 
             ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
   float y = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / 
             ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
   return Vector2d(x, y);
  }

  int numofIntersections(std::vector<path*> paths)
  {
   int toReturn = 0;
   for(int i = 0; i < paths.size(); i++)
   {
    for(int j = i+1; j < paths.size(); j++)
    {
     for(int ii = 0; ii < paths[i]->posList.size() - 1; ii++)
     {
      for(int jj = 0; jj < paths[j]->posList.size() - 1; jj++)
      {
       Vector2d v0 = *paths[i]->posList[ii];
       Vector2d v1 = *paths[i]->posList[ii+1];
       Vector2d v2 = *paths[j]->posList[jj];
       Vector2d v3 = *paths[j]->posList[jj+1];
       Vector2d e0 = v1 - v0;
       Vector2d e1 = v3 - v2;
       e0 = e0 / e0.norm();
       e1 = e1 / e1.norm();
       if(ii == 0 && jj ==0){}
       else if(doIntersect(v0, v1, v2, v3) && (e0 % e1).norm() != 0)
        toReturn ++;
      }
     }
    }
   }
   return toReturn;
  }

  bool leftright(const Vector2d v, const std::vector<Vector2d> l)
  {
   int counter = 0;
   for(int i = 0; i < l.size() - 1; i++)
   {
    for(int j = i+1; j < l.size(); j++)
    {
     Vector2d p0 = l[i];
     Vector2d p1 = l[j];
     Vector2d e0 = v - p0;
     Vector2d e1 = v - p1;
     Vector3d d = e0 % e1;
     if(d[2] >= 0)
      counter ++;
    }
   }
   if(counter == 0)
    return true;
   else
    return false;
  }

  void drawRebuildSDField(const int frameid, const float b0, const float b1, const float b2);

  void writeKNNDsamples(const std::vector<Vector2d> trainingSet, 
    const std::vector<double> kdtreeden, 
    const int frameid);

  RBF DensityFieldFromSubset(const int frameid);
  VectorField extractVectorField(const int timeframe, const RBF rbf, const std::vector<double> rbfradii);
  void constructPathsSubset(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height, double tlat, double tlon, double mlat, double mlon, int timeframe, float rbfkernelfactor);
  void producePaths(const int refframe, const std::vector<RBF> rbfs, const std::vector< std::vector<double> > rbfradii);

  std::vector<RBF> RBFS;
  std::vector< std::vector<double> > RBFRADII;
  std::vector<VectorField> vectorfields;

  //PathResampling* pathresampling;
  
};

#endif /* ADVSIORY_H_ */
