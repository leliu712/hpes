CONFIG += warn_on release qt
QMAKE_CC = gcc
QMAKE_CXX = g++


SOURCES += advisory.cpp bin.cpp geoFunct.cpp gridStruct.cpp hurVis.cpp main.cpp    \
Matrix.cpp P6Image.cpp path.cpp pathSegment.cpp posPoint.cpp predictedPath.cpp     \
simulation.cpp Utility.cpp Vector.cpp velocityFunct.cpp window.cpp mapWin.cpp      \
chip.cpp sector.cpp hurEyePoints.cpp slider.cpp ui.cpp gauss.cpp gl2ps.cpp         \
ColorGradient.cpp MarchingSquares.cpp PathCBD.cpp MathMorphology.cpp Splatting.cpp \
RBF.cpp mipmap.cpp MMPyramid.cpp Sv_Raster.cpp CDFSample.cpp KDTree.cpp BPQ.cpp \
PoissonDiskSample.cpp Icon.cpp VectorField.cpp VelocityPath.cpp PathsGenerator.cpp \
PathResampling.cpp \
    slidersgroup.cpp \
    stormcharacteristicinterpolator.cpp \
    filemanager.cpp


HEADERS += advisory.h bin.h geoFunct.h gridPoint.h gridStruct.h hurVis.h           \
ImageFile.h Matrix.h P6Image.h path.h pathSegment.h posPoint.h predictedPath.h     \
simulation.h Utility.h Vector.h velocityFunct.h window.h mapWin.h chip.h sector.h  \
hurEyePoints.h slider.h ui.h gauss.h gl2ps.h catmullrom.h ColorGradient.h          \
MarchingSquares.h PathCBD.h MathMorphology.h Splatting.h RBF.h mipmap.h MMPyramid.h\
Sv_Raster.h tiff.h tiffio.h CDFSample.h KDTree.h BPQ.h PoissonDiskSample.h Icon.h  \
PrePoint.h VectorField.h VelocityPath.h PathsGenerator.h PathResampling.h          \
PoissonGenerator.h \
    slidersgroup.h \
    stormcharacteristicinterpolator.h \
    KDTreeDensityEstimator.h \
    filemanager.h

TARGET = main

QT += opengl
QT += core

INCLUDEPATH += . 

QMAKE_CFLAGS   += -g `Magick++-config --cxxflags`
QMAKE_LFLAGS   += -g `Magick++-config --ldflags`
QMAKE_CFLAGS   += -fopenmp
QMAKE_CXXFLAGS += -g `Magick++-config --cxxflags`
QMAKE_CXXFLAGS += -std=c++11 -fopenmp
QMAKE_CXXFLAGS += -Wno-sign-compare -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -Wno-sequence-point -Wno-reorder
QMAKE_CFLAGS_RELEASE -= -O2
QMAKE_CFLAGS_RELEASE += -O3 
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3

TEMPLATE = app

OBJECTS_DIR = objs
MOC_DIR = mocs

LIBS += -lGLU
# Link CGAL library
message("------>CGAL_INCLUDE_PATH: $$(CGAL_INCLUDE_PATH)")
message("------>CGAL_LIBRARY_PATH: $$(CGAL_LIBRARY_PATH)")
INCLUDEPATH += $$(CGAL_INCLUDE_PATH)
LIBS += -L$$(CGAL_LIBRARY_PATH)
LIBS += -lCGAL -lCGAL_Core -lCGAL_ImageIO
LIBS += -lgmp -lmpfr

LIBS += -lgomp -lboost_thread -ltiff -lm -lMagick++-7.Q16HDRI
