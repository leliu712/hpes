/*
 * splatting algorithm 
 */
#ifndef __SPLATTING_H__
#define __SPLATTING_H__

#include <iostream>
#include <vector>
#include "Vector.h"
#include "path.h"

class Splatting{
 public:
  Splatting(std::vector<Vector2d*> _grid, std::vector<path*> _pathList, int _time, float* _values, double _R):
   grid(_grid),
   pathList(_pathList),
   time(_time),
   R(_R){
    if(pathList.size() == 0)
     return;
    values = new float[grid.size()];
    for(int i = 0; i < grid.size(); i++)
    {
     values[i] = _values[i];
    }
    cout << pathList.size() << "."  << endl;
    //sn.resize(grid.size(), 0.0);
    //alpha.resize(grid.size(), 0.0);
    splat();
   }

  void splat();
  void update(Vector2d* _p, int idx);
  double WKernel(double dis);
  void copyTo(float* vals);

  ~Splatting(){
   delete[] values;
  }
  

 private:
  std::vector<Vector2d*> grid;
  //std::vector<double> sn; 
  //std::vector<double> alpha;
  float* values;
  std::vector<path*> pathList;
  int time;
  double R; //kernel radius
};



#endif
