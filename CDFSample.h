/*
 * CDF sample
 */
#ifndef __CDF_SAMPLE_H__
#define __CDF_SAMPLE_H__
#include <iostream>
#include <vector>
#include "Vector.h"
#include "Matrix.h"
#include "PoissonDiskSample.h"
#include "RBF.h"
#include "geoFunct.h"
#include "PrePoint.h"

#include <boost/shared_ptr.hpp>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/convex_hull_2.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/Polygon_with_holes_2.h>
#include <CGAL/create_offset_polygons_from_polygon_with_holes_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Straight_skeleton_builder_2.h>
#include <CGAL/Polygon_offset_builder_2.h>
#include <CGAL/compute_outer_frame_margin.h>
#include <CGAL/algorithm.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Alpha_shape_2.h>
#include <CGAL/hilbert_sort.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Barycentric_coordinates_2/Triangle_coordinates_2.h>
#include <CGAL/Triangulation_2.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_2 Point_2;
typedef std::vector<Point_2> Points;
typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef K::FT FT;
typedef K::Segment_2 Segment;
typedef CGAL::Polygon_2<K> Polygon_2;
typedef CGAL::Straight_skeleton_2<K> Ss;
typedef CGAL::Polygon_2<K> Contour;

typedef boost::shared_ptr<Polygon_2> PolygonPtr;
typedef boost::shared_ptr<Ss> SsPtr;
typedef boost::shared_ptr<Contour> ContourPtr;
typedef std::vector<PolygonPtr> PolygonPtrVector;
typedef std::vector<ContourPtr> ContourSequence;

//typedef Ss::Halfedge_iterator Halfedge_iterator;
//typedef Ss::Halfedge_handle   Halfedge_handle;
//typedef Ss::Vertex_handle     Vertex_handle;

typedef CGAL::Straight_skeleton_builder_traits_2<K>      SsBuilderTraits;
typedef CGAL::Straight_skeleton_builder_2<SsBuilderTraits,Ss> SsBuilder;
typedef CGAL::Polygon_offset_builder_traits_2<K>                  OffsetBuilderTraits;
typedef CGAL::Polygon_offset_builder_2<Ss,OffsetBuilderTraits,Contour> OffsetBuilder;

typedef CGAL::Alpha_shape_vertex_base_2<K> Vb;
typedef CGAL::Alpha_shape_face_base_2<K>  Fb;
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;
typedef CGAL::Delaunay_triangulation_2<K,Tds> Triangulation_2;
typedef CGAL::Alpha_shape_2<Triangulation_2>  Alpha_shape_2;
typedef Alpha_shape_2::Alpha_shape_edges_iterator Alpha_shape_edges_iterator;

typedef CGAL::Simple_cartesian<double> BKernel;
typedef BKernel::Point_2 BPoint;
typedef CGAL::Barycentric_coordinates::Triangle_coordinates_2<BKernel> Triangle_coordinates;

typedef CGAL::Triangulation_2<K> Triangulation;


static int Alpha = 200;
static unsigned int randomseed = time(NULL);

struct TNode
{
 int* v;
 int row;
 int col;
};

class CDFSample{
 public:
  CDFSample();
  CDFSample(const CDFSample & _cdfsample){
   tnodes = _cdfsample.tnodes; 
   Radii = _cdfsample.Radii;
  }
  ~CDFSample(){};

  void compute(int width, int height, int nrows, int ncols, std::vector<double> density, 
    std::vector<Vector2d> input, const int frameid);
  // sampling using WSE
  std::vector<Vector2d> SamplingByWSESelecting(float minX, float maxX, float minY, float maxY,
    int width, int height, int nrows, int ncols, std::vector<double> density, 
    std::vector<Vector2d> input, const int frameid,
    std::vector<double> sdvals,
    const float bandminx, const float bandmaxx,
    const float bandminy, const float bandmaxy);
  std::vector<Vector2d> SamplingByPDSampling(float minX, float maxX, float minY, float maxY,
    int width, int height, int nrows, int ncols, std::vector<double> density, 
    std::vector<Vector2d> input, const int frameid,
    std::vector<double> sdvals,
    const float bandminx, const float bandmaxx,
    const float bandminy, const float bandmaxy);
  void compute3(float minX, float maxX, float minY, float maxY,
    int width, int height, int nrows, int ncols, std::vector<double> density, 
    std::vector<Vector2d> input, const int frameid, std::vector<double> inputDensity);

  static void write2file(std::vector<Vector2d> data, const char* filename);
  static void write2file(std::vector<Vector2d> data, std::vector<float> vals, 
    const char* filename);
  static void write2file(std::vector<Vector2d> data, std::vector<double> vals, 
    const char* filename);
  static void write2file(std::vector<int> data, const char* filename);
  static void write2file(std::vector<float> data, const char* filename);
  static void write2file(std::vector<double> data, const char* filename);
  static void write2file(std::vector<int> index, std::vector<Vector2d> data, std::vector<double> errorRadii, const char* filename);
  static std::vector<double> readFromFile(const char* filename);
  static std::vector<Vector4d> readFromFile2(const char* filename);
  static void readFromFile(const char* filename, std::vector<Vector2d>& v0, std::vector<int>& v1);
  void write2file(std::vector< std::vector<float> > CDFX,
    std::vector< std::vector<float> > CDFY,
    const char* filename);
  void write2file2(
    std::vector< std::vector<float> > CDFX,
    std::vector< std::vector<float> > CDFY,
    const char* filename)
  {
   ofstream myfile(filename, fstream::out);
   for(int i = 0; i < CDFX.size(); i++)
    for(int j = 0; j < CDFX[i].size(); j++)
    {
     float u = CDFX[i][j];
     float v = CDFY[j][i];
     myfile << i << " " << j << " " << u << " " << v << endl;
    }
   myfile.close();
   cout << "Finish writing " << CDFX.size() * CDFX[0].size() << " entries to " << filename << endl;
  }

  void removeOutlier(std::vector<Vector2d>& samples, std::vector<Vector2d>& input,
    const int nrows, const int ncols,
    const std::vector<Vector2d> vertices,
    std::vector<double>& sdvals);

  Vector2d evalCDFinv(
    const float radius,
    const Vector2d value,
    const Vector2d p,
    const float dw,
    const float dh,
    std::vector< std::vector<float> > CDFX, 
    std::vector< std::vector<float> > CDFY,
    std::vector<Vector2d> vertices,
    const int nrows, const int ncols,
    const int flag);
  static Vector2d invmap(const Vector2d query,
    const std::vector< std::vector<float> > CDFX,
    const std::vector< std::vector<float> > CDFY,
    const std::vector<Vector2d> vertices,
    const int nrows, const int ncols);
  std::vector<float> invmapbyRBF(const std::vector<Vector2d> input,
    const std::vector<Vector2d> newspace,
    const int dim, const float lambda);
  Vector2d invmap(Vector2d query,
    const std::vector<Vector2d> CDFgrid,
    const std::vector<Vector2d> vertices);
  Vector2d forwardmap(const Vector2d& query, 
    const std::vector<Vector2d>& UDGrid,
    const std::vector<Vector2d>& ODGrid,
    const int nrows, const int cols,
    const std::vector<std::vector<float>>& U, const std::vector<std::vector<float>>& V);

  Vector2d invmap(Vector2d query,
    const std::vector<Vector2d> CDFgrid,
    const std::vector<Vector2d> vertices,
    int& flag);

  static Vector3d BaryCentricCoor(Vector3d x, Vector3d p0, Vector3d p1, Vector3d p2);
  static bool isInside(const Vector3d barycentric)
  {
   if(barycentric.x >= 0 && barycentric.y >= 0 && barycentric.z >= 0)
    return true;
   else
    return false;
  }
  static bool checkCell(Vector2d* indices, 
    const std::vector< std::vector<float> > CDFX, 
    const std::vector< std::vector<float> > CDFY, 
    const Vector2d query,
    Vector3d& barycentric,
    int& flag);

  void RBFinterp(const std::vector<Vector2d> input,
    const std::vector<double> val);

  void findTriangles(
    const int nrows,
    const int ncols)
  {
   for(int i = 0; i < nrows - 1; i++)
    for(int j = 0; j < ncols - 1; j++)
    {
     int idx[4];
     idx[0] = i * ncols + j;
     idx[1] = i * ncols + j + 1;
     idx[2] = (i + 1) * ncols + j + 1;
     idx[3] = (i + 1) * ncols + j;

     //upper triangle 0 3 2
     TNode n0; 
     n0.v = new int[3];
     n0.v[0] = idx[0]; n0.v[1] = idx[3]; n0.v[2] = idx[2];
     n0.row = i; n0.col = j;
     tnodes.push_back(n0);

     //lower triangle 0 2 1
     TNode n1;
     n1.v = new int[3];
     n1.v[0] = idx[0]; n1.v[1] = idx[2]; n1.v[2] = idx[1];
     n1.row = 1; n1.col = j;
     tnodes.push_back(n1);
    }
  }

  float findMax(const std::vector< std::vector<float> > CDFs)
  {
   int size = CDFs.size();
   float toReturn(10e-6);
   for(int i = 0; i < size; i++)
   {
    std::vector<float> cdf = CDFs[i];
    float tmp = *std::max_element(cdf.begin(), cdf.end());
    toReturn = max(tmp, toReturn);
   }
   return toReturn;
  }

  void CenterCDFSpace(std::vector< std::vector<float> >& CDFs)
  {
   float globalmax = findMax(CDFs);
   for(int i = 0; i < CDFs.size(); i++)
    for(int j = 0; j < CDFs[i].size(); j++)
     CDFs[i][j] /= globalmax;
   //printf("\n");
   //for(int i = 0; i < CDFs.size(); i++)
   //{
   // for(int j = 0; j < CDFs[i].size(); j++)
   //  printf("%1.6f\t", CDFs[i][j]);
   // printf("\n");
   //}

   for(int i = 0; i < CDFs.size(); i++)
   {
    std::vector<float> cdf = CDFs[i];
    float localmax = *std::max_element(cdf.begin(), cdf.end());
    for(int j = 0; j < cdf.size(); j++)
    {
     CDFs[i][j] -= 0.5 * localmax;
    }
   }

   //printf("\n");
   //for(int i = 0; i < CDFs.size(); i++)
   //{
   // for(int j = 0; j < CDFs[i].size(); j++)
   //  printf("%1.6f\t", CDFs[i][j]);
   // printf("\n");
   //}
   //exit(0);
  }

  void CenterCDFSpace2(std::vector< std::vector<float> >& CDFs)
  {
   float globalmax = findMax(CDFs);
   for(int i = 0; i < CDFs.size(); i++)
    for(int j = 0; j < CDFs[i].size(); j++)
     CDFs[i][j] /= globalmax;

   for(int i = 0; i < CDFs.size(); i++)
   {
    std::vector<float> cdf = CDFs[i];
    int idx0 = floor(cdf.size() * 0.5);
    int idx1 = ceil(cdf.size() * 0.5);
    float f0 = cdf[idx0];
    float f1 = cdf[idx1];
    float f = 0.5 * (f0 + f1);
    for(int j = 0; j < cdf.size(); j++)
    {
     CDFs[i][j] -= f;
    }
   }
  }

  std::vector<Vector2d> duplicateSamples(const std::vector<Vector2d> input);
  void duplicateCDFgrid(std::vector< std::vector<float> >& CDFs)
  {
   for(int i = 0; i < CDFs.size(); i++)
   {
    cout << i << ": " << endl;
    for(int j = 0; j <  CDFs[i].size(); j++)
     printf("%d:%1.11f\t", j, CDFs[i][j]);
    printf("\n");
    float localmax = *std::max_element(CDFs[i].begin(), CDFs[i].end());
    float localmin = *std::min_element(CDFs[i].begin(), CDFs[i].end());
    int low = std::upper_bound(CDFs[i].begin(), CDFs[i].end(), localmin) - CDFs[i].begin();
    //int up  = std::lower_bound(CDFs[i].begin(), CDFs[i].end(), localmax) - CDFs[i].begin();
    int up  = std::find(CDFs[i].begin(), CDFs[i].end(), localmax) - CDFs[i].begin();
    cout << low << " " << up << endl;
    cout << localmin << " " << localmax << endl;
   }
  }

  void fillBoundaries(
    const std::vector<Vector2d> cdfgrid,
    const int nrows,
    const int ncols,
    const std::vector< std::vector<float> > CDFX,
    const std::vector< std::vector<float> > CDFY,
    const std::vector<Vector2d> input,
    std::vector<Vector2d>& extrasamples,
    const int frameid);
  //{
  // std::vector<Vector2d> segments = extractEdges(cdfgrid, nrows, ncols, CDFX, CDFY, input);
  // std::vector<Vector2d> newsegments;
  // std::vector<Vector2d> directions;
  // std::vector<Vector2d> vertices;
  // std::vector<Vector2d> newvertices;
  // newvertices.clear();
  // for(int i = 0; i < segments.size() / 2; i++)
  // {
  //  Vector2d p0 = segments[2 * i];
  //  Vector2d p1 = segments[2 * i + 1];
  //  Vector2d v = Vector2d(p1[0], p1[1]) - Vector2d(p0[0], p0[1]);
  //  Vector2d dir = rotate(270 * PI / 180.0, v); 
  //  directions.push_back(dir);
  //  float rmax = PoissonDiskSample<std::vector<Vector2d>, float>::Rmax(1.0, 100); 
  //  dir = dir * rmax;
  //  Vector2d toPush = p0 + dir;
  //  newsegments.push_back(toPush);
  //  toPush = p1 + dir;
  //  newsegments.push_back(toPush);
  // }

  // //glColor4f(1, 0, 0, 1);
  // //glBegin(GL_LINE_LOOP);
  // //for(int i = 0; i < segments.size(); i++)
  // // glVertex3f((segments[i][0]+0.7)*500, (segments[i][1]+0.7)*400, 0.0);
  // //glEnd();
  // //glColor4f(0, 1, 0, 1);
  // //glBegin(GL_LINE_LOOP);
  // //for(int i = 0; i < newsegments.size(); i++)
  // // glVertex3f((newsegments[i][0]+0.7)*500, (newsegments[i][1]+0.7)*400, 0.0);
  // //glEnd();
  // //return;

  // // generate random points
  // float density = input.size() / 0.5;  //reference density 
  // std::vector<Vector2d> generatedsamples;
  // for(int i = 0; i < segments.size()/2; i += 2)
  // {
  //  //construct two triangles
  //  Vector2d e0 = newsegments[i] - segments[i];
  //  Vector2d e1 = segments[i+1] - segments[i];
  //  Vector3d tmp = e0 % e1;
  //  float area = 0.5 * tmp.norm();
  //  
  //  e0 = newsegments[i] - segments[i+1];
  //  e1 = newsegments[i+1] - segments[i+1];
  //  tmp = e0 % e1;
  //  area += 0.5 * tmp.norm();

  //  int targetsamples = density * area;
  //  cout << area << " " << targetsamples << endl;
  //  int j = 0;
  //  while(j < targetsamples)
  //  {
  //   j++;
  //   //cout << "---" << endl;
  //   //cout << segments[i] << " "<< segments[i+1] << 
  //   // newsegments[i] << " "<< newsegments[i+1] << endl;
  //   Vector2d newsample = generateRandomPoint(
  //     segments[i], segments[i+1], 
  //     newsegments[i], newsegments[i+1]);
  //   generatedsamples.push_back(newsample);
  //  }
  // }
  // write2file(generatedsamples, "generatedsamples.txt");

  // exit(0);
  // 
  //}

  std::vector<Vector2d> extractEdges(
    const std::vector<Vector2d> cdfgrid,
    const int nrows,
    const int ncols,
    const std::vector< std::vector<float> > CDFX,
    const std::vector< std::vector<float> > CDFY,
    const std::vector<Vector2d> input,
    const int frameid)
  {
   std::vector<Vector2d> edgePoints;
   Points points;
   for(int i = 0; i < input.size(); i++)
    points.push_back(Point_2(input[i].x, input[i].y));
   Points convexhull = getConvexHull(points);
   std::vector<Vector2d> toReturn;
   for(int i = 0; i < convexhull.size(); i++)
    toReturn.push_back(Vector2d(convexhull[i][0], convexhull[i][1]));

   Alpha_shape_2 A(points.begin(), points.end(), FT(0.005), Alpha_shape_2::GENERAL);
   std::vector<Segment> segments;
   alpha_edges(A, std::back_inserter(segments));
   cout << segments.size() << endl;
   cout << "optimal :" << *A.find_optimal_alpha(1)<< endl;
   std::vector<Vector2d> outputsegments;
   for(int i = 0; i < segments.size(); i++)
   {
    glColor4f(1, 0, 0, 1);
    glBegin(GL_LINES);
    Point_2 p0 = segments[i][0];
    Point_2 p1 = segments[i][1];
    //glVertex3f((p0[0]+0.7)*500, (p0[1]+0.7)*400, 0.0);
    //glVertex3f((p1[0]+0.7)*500, (p1[1]+0.7)*400, 0.0);
    glEnd();
    outputsegments.push_back(Vector2d(p0[0], p0[1]));
    outputsegments.push_back(Vector2d(p1[0], p1[1]));
   }
   string str = OutputPrefix + "InternalData/PathSamplingData/concavehull-%d.txt";
   const char* cstr = str.c_str();
   char* buff = new char[200];
   sprintf(buff, cstr, frameid);
   write2file(outputsegments, buff);
   return toReturn;
  }

  Vector2d generateRandomPoint(
    const Vector2d v0, 
    const Vector2d v1,
    const Vector2d v2,
    const Vector2d v3)
  {
   float f[4];
   float total(0);
   for(int i = 0; i < 4; i++)
   {
    srand(randomseed);
    randomseed = (randomseed * 21)%UINT_MAX;
    f[i] = (rand()%100+1) * (1.0 / 100);
    total += f[i];
    //cout << f[i] << endl;
   }
   for(int i = 0; i < 4; i++)
    f[i] /= total;
   Vector2d toReturn =  v0 * f[0] + v1 * f[1] + v2 * f[2] + v3 * f[3];
   //cout << f[0] << " " << f[1] << " " << f[2] << " " << f[3] << endl;
   //cout << v0 << " " << v1 << " " << v2 << " " << v3 << endl;
   //cout << toReturn << endl;
   return toReturn;
  }

  static std::vector<Vector2d> readTestSamples(const char* filename)
  {
   ifstream file(filename);
   if(!file.is_open())
   {
    cerr << "Bad file: " << filename << endl;
    exit(-1);
   }
   std::vector<Vector2d> data;
   while(!file.eof())
   {
    Vector2d toPush;
    file >> toPush.x >> toPush.y;
    data.push_back(toPush);
   }
   data.pop_back();
   return data;
  }

  Points getConvexHull(Points in)
  {
   Points result;
   CGAL::convex_hull_2(in.begin(), in.end(), std::back_inserter(result));
   return result;
  }

  template <class OutputIterator>
   void
   alpha_edges( const Alpha_shape_2& A,
     OutputIterator out)
   {
    for(Alpha_shape_edges_iterator it = A.alpha_shape_edges_begin(); it != A.alpha_shape_edges_end(); ++it)
     *out++ = A.segment(*it);
   }

  static Vector2d rotate(const float theta, const Vector2d v, const Vector2d center){
   Matrix2x2 R(cos(theta), -sin(theta),
     sin(theta), cos(theta));
   Vector2d u = v - center;
   Vector2d toReturn = R * u;
   toReturn = toReturn + center;
   return toReturn;
  }

  static Vector2d rotate(const float theta, const Vector2d v)
  {
   Matrix2x2 R(cos(theta), -sin(theta), 
     sin(theta), cos(theta));
   Vector2d toReturn = R * v;
   toReturn = toReturn / toReturn.norm();
   return toReturn;
  }

  template<typename T>
   std::vector<double> KDTreeDensity(T data, int size, int dim,
     std::vector<int>& validlist, float threshold)
   {
    int K = 10;
    KDTree kdtree;
    kdtree.buildKDTree(kdtree.formatInput<T>(data, data.size(), dim));
    std::vector<double> density;
    density.resize(data.size(), 0);
    validlist.clear();
    validlist.resize(data.size(), 1);
    for(int i = 0; i < size; i++)
    {
     if(validlist[i] != 0)
     {
      Point* p = kdtree.createPoint(dim);
      for(int j = 0; j < dim; j++)
       p->coord[j] = data[i][j];

      // 11 is the K value for KNN
      // 11 = 10 + 1, the last element in the queue is the testing point
      // maybe 1/100 of the size of the training set
      bounded_priority_queue<QueueTuple> bpq = kdtree.knearest(p, K+1);
      //cout << "bpq size: " << bpq.size() << endl;
      //bounded_priority_queue<QueueTuple> tmp(bpq);

      float maxR(0);
      // merge points too close
      while(!bpq.queue_.empty())
      {
       float r = bpq.queue_.top().first;
       maxR = max(maxR, r);
       if(r < threshold * threshold)
       {
        // if a point is too close to the query
        // set its valid flag to 0
        int idx = 0;
        Point* p_ = bpq.queue_.top().second;
        idx = p_->index;
        if(idx != i)
         validlist[idx] = 0;
       }
       bpq.queue_.pop();
      }

      float area = PI * maxR;
      float d = K / area;
      density[i] = d;

      if(dim > 2)
      {
       area = maxR;
       d = K / area;
       density[i] = d;
      }

     }
    }

    //normalize density to [0, 1];
    float max_den = *(max_element(density.begin(), density.end()));
    for(int i = 0; i < density.size(); i++)
    {
     density[i] /= max_den;
    }
    return density;
   }

  void writeDensityField(RBF rbf)
  {
   ofstream myfile("uy-densityfield.txt", fstream::out);
   for(float i = -0.5; i < 0.5; i += 0.01)
    for(float j = 0; j < 0.868; j += 0.01)
    {
     Vector2d p(i, j);
     float f = rbf.eval(p);
     myfile << p.x  << " " <<  p.y << " " << f << endl;
    }
   myfile.close();
   cout << "finish writing " << 200 * 200 << " entries to uy-densityfield.txt" << endl; 
  }

  void ScaleCDFs(std::vector< std::vector<float> >& CDFs,
    const float totalrange,
    const int size)
  {
   float scalefactor(0);
   float numofintervals(0);
   numofintervals = (size % 2 == 0) ? 2 : 3;
   float minrange = totalrange / float(size - 1) * numofintervals;
   for(int i = 0; i < CDFs.size(); i++)
   {
    float localmin = CDFs[i][0];
    float localmax = CDFs[i][CDFs[i].size() - 1];
    float f = localmax - localmin;
    if(f < minrange && f != 0)
    {
     float scalefactor = minrange * 0.5 / localmax; 
     cout << i << " " << localmax - localmin << " " << scalefactor << endl; 
     for(int j = 0; j < CDFs[i].size(); j++)
      CDFs[i][j] *= scalefactor;
    }
    else if(f == 0)
    {
     CDFs[i][0] = -minrange * 0.5;
     CDFs[i][CDFs[i].size()-1] = minrange * 0.5;
    }
   }
  }

  void Vcoords(
    std::vector< std::vector<float> >& CDFs, 
    const float globalmax,
    const float globalmin)
  {
   float globalcdfmax(0);
   float globalcdfmin(0);
   std::vector<float> tmp;
   for(int i = 0; i < CDFs.size(); i++)
    for(int j = 0; j < CDFs[i].size(); j++)
     tmp.push_back(CDFs[i][j]);
   globalcdfmax = *std::max_element(tmp.begin(), tmp.end());
   globalcdfmin = *std::min_element(tmp.begin(), tmp.end());
   float globalcdfrange = globalcdfmax - globalcdfmin;

   std::vector< std::vector<float> > factors;
   std::vector<float> rangefraction;
   for(int i = 0; i < CDFs.size(); i++)
   {
    std::vector<float> cdf = CDFs[i];
    // compute range
    float localmin = *std::min_element(cdf.begin(), cdf.end());
    float localmax = *std::max_element(cdf.begin(), cdf.end());
    float range = localmax - localmin;
    float c = range / globalcdfrange;
    rangefraction.push_back(c);
    std::vector<float> factor;
    float total(0);
    for(int j = 0; j < cdf.size(); j++)
    {
     if(j == 0)
      factor.push_back(0);
     else
     {
      float f = (cdf[j] - cdf[j-1]) / range * c;
      total += f;
      factor.push_back(total);
     }
    }
    factors.push_back(factor);
   }

   //for(int i = 0; i < factors.size(); i++)
   //{
   // printf("\n");
   // for(int j = 0; j < factors[i].size(); j++)
   //  printf("%f\t", factors[i][j]);
   // printf("\n");
   //}

   float globalrange = globalmax - globalmin;
   CDFs.clear();
   for(int i = 0; i < factors.size(); i++)
   {
    std::vector<float> cdf;
    float c = rangefraction[i];
    for(int j = 0; j < factors.size(); j++)
    {
     float v = globalmin + factors[i][j] * globalrange;
     //cdf.push_back(factors[i][j]);
     //v = globalmin + globalrange / (30 - 1) * j;
     cdf.push_back(v);
    }
    CDFs.push_back(cdf);
   }
   cout << "finish computing v coords." << endl;
  }

  float UDSpaceArea(const std::vector<std::vector<float>>& U, const std::vector<std::vector<float>>& V,
    const std::vector<Vector2d>& UDGrid, const int nrows, const int ncols){
   // loop row by row to compute scale factor of each grid cell
   float totalArea(0);
   for(int i = 0; i < nrows - 1; i++)
   {
    for(int j = 0; j < ncols - 1; j++)
    {
     // v0 v1
     // v3 v2
     // compute area of a grid cell in the CDF space
     Vector2d ij[4];
     ij[0] = Vector2d(i, j);
     ij[1] = Vector2d(i+1, j);
     ij[2] = Vector2d(i+1, j+1);
     ij[3] = Vector2d(i, j+1);
     float cdfarea = CDFCellArea(ij, U, V);
     totalArea += cdfarea;
    }
   }
   return totalArea;
  }

  std::vector<float> scaleFactors(
    const std::vector< std::vector<float> > CDFX,
    const std::vector< std::vector<float> > CDFY,
    const std::vector<Vector2d> vertices, 
    const int nrows, const int ncols)
  {
   std::vector<float> scalefactors;
   // loop row by row to compute scale factor of each grid cell
   for(int i = 0; i < nrows - 1; i++)
   {
    for(int j = 0; j < ncols - 1; j++)
    {
     // v0 v1
     // v3 v2
     // compute area of a grid cell in the CDF space
     Vector2d ij[4];
     ij[0] = Vector2d(i, j);
     ij[1] = Vector2d(i+1, j);
     ij[2] = Vector2d(i+1, j+1);
     ij[3] = Vector2d(i, j+1);
     float cdfarea = CDFCellArea(ij, CDFX, CDFY);

     // compute area of a grid cell in the original space 
     // the area is fixed
     //int idx[4];
     //idx[0] = nrows * i + j;
     //idx[1] = nrows * i + j + 1;
     //idx[2] = nrows * (i + 1) + j + 1;
     //idx[3] = nrows * (i + 1) + j;
     //float originalarea = OriginalCellArea(idx, vertices);
     float dw = 1.0 / (ncols - 1);
     float dh = 1.0 / (nrows - 1);
     float originalarea = dw * dh;

     float s = cdfarea / originalarea;
     scalefactors.push_back(s);

    }
   }
   return scalefactors;
  }

  float OriginalCellArea(int idx[4],
    const std::vector<Vector2d> vertices)
  {
   // v0-v1
   //  |\|
   // v3-v2
   Vector2d v[4];
   for(int i = 0; i < 4; i++)
    v[i] = vertices[idx[i]];

   Vector2d e01 = v[1] - v[0];
   Vector2d e02 = v[2] - v[0];
   Vector2d e03 = v[3] - v[0];
   float area012 = (e01 % e02).norm() * 0.5;
   float area023 = (e02 % e03).norm() * 0.5;
   float area = area012 + area023;
   return area;
  }

  float CDFCellArea(Vector2d ij[4],
    const std::vector< std::vector<float> > CDFX,
    const std::vector< std::vector<float> > CDFY)
  {
   // v0-v1
   //  |\|
   // v3-v2
   Vector2d* v = new Vector2d[4];
   for(int i = 0; i < 4; i++)
   {
    float x = CDFX[ij[i].x][ij[i].y];
    float y = CDFY[ij[i].x][ij[i].y];
    v[i].x = x;
    v[i].y = y;
   }
   //glColor4f(1, 0, 0, 1);
   //glBegin(GL_LINE_LOOP);
   //for(int i = 0; i < 4; i++){
   // glVertex3f(v[i].x * 800, v[i].y * 800, 0);
   //}
   //glEnd();

   Vector2d e01 = v[1] - v[0];
   Vector2d e02 = v[2] - v[0];
   Vector2d e03 = v[3] - v[0];
   float area012 = (e01 % e02).norm() * 0.5;
   float area023 = (e02 % e03).norm() * 0.5;
   float area = area012 + area023;
   return area;
  }

  std::vector<int> samplesinGrid(
    const std::vector<Vector2d> samples,
    const float dw, const float dh,
    const int nrows, const int ncols)
  {
   std::vector<int> indices;
   for(int i = 0; i < samples.size(); i++)
   {
    int row = std::floor(samples[i].y / dh);
    int col = std::floor(samples[i].x / dw); 
    int idx = ncols * row + col; 
    indices.push_back(idx);
   }
   return indices;
  }

  static void readNewSpace(const char* filename, 
    std::vector< std::vector<float> >& CDFX,
    std::vector< std::vector<float> >& CDFY, 
    const int nrows,
    const int ncols);

  void rebuildSDField(const std::vector<Vector2d> selectedSamples,
    RBF densityRBF,
    RBF SDRBF,
    const int timeframe,
    const float dw,
    const float rbfkernerfactor,
    const std::vector<double> rbfradii);

  void rebuildIntensityField(const std::vector<Vector2d> selectedSamples,
    RBF densityRBF,
    std::vector<double> hurInt,
    const int timeframe,
    const float dw,
    const float rbfkernerfactor);


  std::vector<Vector2d> readCategories(const char* idxfile, const char* catfile, std::vector<Vector2d>& subset);

  static float Nmi2Km(const float nmi)
  {
   return nmi / 0.53996;
  }

  static float Stormsize2Display(Vector2d lonlat, float nmi);

  std::vector<Vector2d> findFourCorners(Vector4d radiiInNmi, Vector2d loc)
  {
   std::vector<Vector2d> toReturn;
   for(int i = 0; i < 4; i++)
   {
    float km = radiiInNmi[i] / 0.53996;
    Vector2d toPush = locateDestination_2(loc.x, loc.y, km, 45 +  90 * (i + 1));
    toReturn.push_back(toPush);
   }
   return toReturn;
  }

  void drawInputwithIcon(const std::vector<Vector2d> input, 
    const int frameid);

  static int determineCat(const float intensity)
  {
   if(intensity <= 33) return 6;
   else if(intensity > 33 && intensity <= 63) return 7;
   else if(intensity > 63 && intensity <= 82) return 1;
   else if(intensity > 82 && intensity <= 95) return 2;
   else if(intensity > 95 && intensity <= 112) return 3;
   else if(intensity > 112 && intensity <= 136) return 4;
   else if(intensity > 136) return 5;
  }

  static void drawSolidCircle(float _x, float _y, float radius);

  void dynamicDraw(const int timeframe, 
    std::vector<PrePoint>& subpoints, 
    std::vector<PrePoint>& fullpoints, 
    std::vector<std::vector<PrePoint>>& pointsband, 
    std::vector<std::vector<PrePoint>>& pointsbandbackup, 
    std::vector<double> bandsVals,
    float& UDDmin, std::vector<float>& ODDmin);

  void drawSolidPolygon(std::vector<Vector2d> vts)
  {
   glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
   glBegin(GL_POLYGON);
   for(int i = 0; i < vts.size(); i++)
    glVertex3f(vts[i].x, vts[i].y, 0);
   glEnd();
   glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  }

  int DisplayFlag;
  std::vector<TNode> tnodes;
  std::vector<double> Radii;
 private:

};

#endif
