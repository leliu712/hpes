#include "slidersgroup.h"

#include <QBoxLayout>

SlidersGroup::SlidersGroup(Qt::Orientation orientation, const QString &title, QWidget *parent) :
    QGroupBox(title, parent)
{
    slider = new QSlider(orientation);
    slider->setFocusPolicy(Qt::StrongFocus);
    slider->setTickPosition(QSlider::TicksBelow);
    slider->setTickInterval(72);
    slider->setSingleStep(1);

    connect(slider, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));

    QBoxLayout::Direction direction;
    if(orientation == Qt::Horizontal){
        direction = QBoxLayout::TopToBottom;
    } else {
        direction = QBoxLayout::LeftToRight;
    }

    QBoxLayout* sliderslayout = new QBoxLayout(direction);
    sliderslayout->addWidget(slider);
    setLayout(sliderslayout);
}

void SlidersGroup::setValue(const int value)
{
    slider->setValue(value);
}

void SlidersGroup::setMinimum(const int value)
{
    slider->setMinimum(value);
}

void SlidersGroup::setMaximum(const int value)
{
    slider->setMaximum(value);
}
