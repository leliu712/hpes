/*
 * advisory.cpp
 *
 *  Created on: Aug 20, 2010
 *  Jonathan Cox
 *  Clemson University
 */

#ifdef __APPLE__
#  include <GLUT/glut.h>
#else
#  include <GL/glut.h>
#  include <GL/glu.h>
#endif


#include <cstdio>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <omp.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "advisory.h"
#include "simulation.h"
#include "ui.h"
#include "catmullrom.h"
#include "ColorGradient.h"
#include "MarchingSquares.h"
#include "PathCBD.h"
#include "MathMorphology.h"
#include "Splatting.h"
#include "mipmap.h"
#include "MMPyramid.h"
#include "PoissonDiskSample.h"

#include <unistd.h>

#include "gl2ps.h"

extern simulation * sim;
extern ui * userInt;

extern PathResampling* pathresampling;

extern double errorRadR[];

extern int averageOnAll;

Vector4d * adv;
Vector4d * adv2;

int sortInd = 0;

int nlayers = 5;
int SAMPLENUM = 3;
double BoxplotFrac = 0.92;
double TimePointRadius = 3.0;
int writeCounter = 0;
int NumofTimesteps = 6;

// for percepture study
static float time1 = 24;
static float time2 = 48;

// heatmap
static int npaths =  999; 
static float fadeConst = 0.028;
static float threshold = 200; // # of tracks in one cell

// contour boxplot
static int subsetsize = 3;
static float bandPercentage[5] = {0.3, 0.33, 0.66, 0.9, 0.9};
//static float bandPercentage[5] = {0.3, 0.33, 0.66, 0.9, 0.9};
//static float densityIntervals[5] = {0.0, .1, .3, .7, 1.0};
static float densityIntervals[4] = {0.0, .4, .7, 1.0};
//static int rbfIndices[5] = {6, 5, 4, 3, 2};
//static int rbfIndices[5] = {5, 4, 3, 2, 1};
static int rbfIndices[5] = {4, 3, 2, 1, 0};
static float ALPHA = 2000; //alpha value for alpha shape

static int morph = -1;

static double KernelRadius = 25;
static double OpacityFactor = 0.00;
static double OpacityPower = 4;

#define Edges    std::vector<Vector3d> 
#define Vertices std::vector<Vector3d> 

static Vector3d bandColors[3] = {Vector3d(1,0,0), Vector3d(1, 0.27, 0), Vector3d(0.647, 0.165, 0.165)};
static float bandColorOpc[3] = {0.6, 0.3, 0.1};
//static Vector3d bandColors[3] = {Vector3d(1,0.27,0), Vector3d(1, 0.27, 0), Vector3d(0.647, 0.165, 0.165)};
//static float bandColorOpc[3] = {0.3, 0.3, 0.0};

// rbf
//static float kernelFactor = 3.0;
static float kernelFactor = 200;
static float RBFKernelRadius = 120;

// pdf interval size
static int IntervalSize = 100;

static double maxdatadepth = 0.0;
static double mindatadepth = 0.0;

static int DensityGridRows = 10;
static int DensityGridCols = 10;

static bool opImage = false;
static bool ToFile = false;

// NHC data repositories
extern string InputPrefix;
extern string OutputPrefix;
extern string DatafileName;
extern int VisSwitch;
extern float OilRigLoc;

extern int SampledTimePoint;

//#define TestOilRigs

bool sp(path * p1, path * p2){
 double a, b;
 double bDif_1 = 1.0;
 double bDif_2 = 1.0;

 // Use determinate to find side of line
 bDif_1 = (adv2->x - adv->x)*(p1->latLonList[sortInd]->y - adv->y) - 
  (adv2->y - adv->y)*(p1->latLonList[sortInd]->x - adv->x);
 bDif_2 = (adv2->x - adv->x)*(p2->latLonList[sortInd]->y - adv->y) - 
  (adv2->y - adv->y)*(p2->latLonList[sortInd]->x - adv->x);

 bDif_1 <= 0 ? bDif_1 = 1.0 : bDif_1 = -1.0;
 bDif_2 <= 0 ? bDif_2 = 1.0 : bDif_2 = -1.0;

 bDif_1 <= 0 ? p1->setSide(1) : p1->setSide(-1);
 bDif_2 <= 0 ? p2->setSide(1) : p2->setSide(-1);

 if(sortInd < 22){
  a = bDif_1*haversine(p1->latLonList[sortInd]->x, adv->x, p1->latLonList[sortInd]->y, adv->y);
  b = bDif_2*haversine(p2->latLonList[sortInd]->x, adv->x, p2->latLonList[sortInd]->y, adv->y);
 }
 else{
  a = bDif_1*haversine(p1->latLonList[sortInd]->x, adv2->x, p1->latLonList[sortInd]->y, adv2->y);
  b = bDif_2*haversine(p2->latLonList[sortInd]->x, adv2->x, p2->latLonList[sortInd]->y, adv2->y);
 }

 //delete advVec;
 return abs(a) < abs(b);
}

static double xp(Vector2d a, Vector2d b){
 return a.x * b.y - a.y * b.x;
}

static Vector2d m_origin;
static Vector2d reference;
static Vector2d m_dreference;
bool angleSort(Vector2d a, Vector2d b){
 Vector2d da = a - m_origin;
 Vector2d db = b - m_origin;
 double detb = xp(m_dreference, db);
 if(detb == 0 && db.x * m_dreference.x + db.y * m_dreference.y >= 0)
  return false;
 double deta = xp(m_dreference, da);
 if(deta == 0 && da.x * m_dreference.x + da.y * m_dreference.y >= 0)
  return true;
 if(deta * detb >= 0)
  return xp(da, db) > 0;
 return deta > 0;
}

advisory::advisory(std::vector<Vector2d*> projPathPos,
  double curLat, double curLon, double curDeg, double curSpeed, string predFile){
 // Assign position, etc
 lat = curLat;
 lon = curLon;
 deg = curDeg;
 presLat = -1;
 presLon = -1;

 cones1 = new std::vector<Vector2d*>[nlayers];
 cones2 = new std::vector<Vector2d*>[nlayers];

 ctrlps1 = new std::vector<Vector2d*>[nlayers];
 ctrlps2 = new std::vector<Vector2d*>[nlayers];

 smoothCones1 = new std::vector<Vector2d>[nlayers];
 smoothCones2 = new std::vector<Vector2d>[nlayers];


 // Build the projected path
 buildProjPath(projPathPos, curDeg);
 buildErrorCone();
 speed = curSpeed;
 pre = new predictedPath(predFile, &ecSmooth1, &ecSmooth2);
 adv = projPath[4];
 adv2 = projPath[5];

 usePredictedPercent = 0.0;
 baseConePercent = 0.682;

 percBubDrawn = 0.0;

 hurSizeMean = 100.0;

 //M = getCombinationMatrix(npaths, subsetsize);
 // hard coded here
 const int nrows = 129;
 const int ncols = 129;
 const int numofUDspaces = 6;
 const int numofPaths = 20;
 //pathresampling = new PathResampling(nrows, ncols, numofUDspaces, numofPaths);
}

void advisory::buildProjPath(std::vector<Vector2d*> projPathPos, double projPathDir){

 Vector4d * toAdd;
 Vector2d * segAdd;
 Vector2d temp;
 Vector2d prev;
 int seg = 0;
 double time = 0.0, tempDist = 0.0;

 std::vector<Vector2d*>::iterator p1;
 std::vector<Vector2d*>::iterator p2;
 std::vector<Vector4d*>::iterator projPathIt;

 p1 = projPathPos.begin();
 p2 = projPathPos.begin()+1;

 for(p1 = projPathPos.begin(); p2 != projPathPos.end(); p1++){

  // Figure out the duration of the line segment
  if(seg == 0){
   time = 9.0;
  }
  else if(seg == 1 || seg == 2 || seg == 3){
   time = 12.0;
  }
  else if(seg == 4){
   time = 24.0;
  }
  else{
   time = 24.0;
  }

  segAdd = genVelocity((*p1)->x, (*p2)->x, (*p1)->y, (*p2)->y, time);

  toAdd = new Vector4d;
  toAdd->x = (*p1)->x;
  toAdd->y = (*p1)->y;

  if(p1 == projPathPos.begin()){
   toAdd->z = projPathDir;
  }
  else{
   toAdd->z = findBearing_2((*p1), (*p2));
  }

  tempDist = haversine((*p1)->x, (*p2)->x, (*p1)->y, (*p2)->y);
  toAdd->w = (tempDist/time)*sim->getHours();
  projPathIt = projPath.end();
  projPath.insert(projPathIt, toAdd);

  projPathDist += tempDist;

  p2++;
  seg++;

  if(p2 == projPathPos.end()){
   p1++;
   toAdd = new Vector4d((*p1)->x, (*p1)->y, toAdd->z, toAdd->w);
   projPath.push_back(toAdd);
  }
 }
 //cout << "projPath size: " << projPath.size() << endl;
 buildProjPathFull();
}

void advisory::buildProjPathFull(){
 Vector4d * projPath1, * projPath2;
 std::vector<Vector4d*>::iterator projPathIt;
 std::vector<Vector2d*>::iterator _f;

 double tempError, t, subt, setNHS, f, subt2, setNHS2, f2;
 int in1, in2, pathSeg;
 Vector2d * intPos;

 int steps = 0;

 projPathFull.clear();
 distWeightCone.clear();

 while(steps <= 69/sim->getHours()){
  t = steps*sim->getHours();
  // Find i, subt and setNHS based on the hours completed so far
  if(t <= 9){
   in1 = 0;
   in2 = 1;
   subt = 0;
   setNHS = 9;
   pathSeg = 0;
   subt2 = 0;
   setNHS2 = 9;
  }
  else if(t <= 21){
   in1 = 1;
   in2 = 2;
   subt = 9;
   setNHS = 12;
   pathSeg = 1;
   subt2 = 9;
   setNHS2 = 12;
  }
  else if(t <= 33){
   in1 = 2;
   in2 = 3;
   subt = 21;
   setNHS = 12;
   pathSeg = 2;
   subt2 = 21;
   setNHS2 = 12;
  }
  else if(t <= 45){
   in1 = 3;
   in2 = 4;
   subt = 33;
   setNHS = 12;
   pathSeg = 3;
   subt2 = 33;
   setNHS2 = 12;
  }
  else if(t <= 57){
   in1 = 4;
   in2 = 5;
   subt = 45;
   setNHS = 12;
   pathSeg = 4;
   subt2 = 45;
   setNHS2 = 24;
  }
  else{
   in1 = 5;
   in2 = 6;
   subt = 57;
   setNHS = 12;
   pathSeg = 4;
   subt2 = 45;
   setNHS2 = 24;
  }

  // Find the weight value
  f = (t - subt)/setNHS;
  f2 = (t - subt2)/setNHS2;

  // Find the begining and the end of the current line segment.
  projPathIt = projPath.begin()+pathSeg;
  projPath1 = (*projPathIt);
  projPathIt = projPath.begin()+pathSeg+1;
  projPath2 = (*projPathIt);

  // Find the interpolated position on the projected line segment
  intPos = new Vector2d;
  intPos->x = f2*projPath2->x + (1-f2)*projPath1->x;
  intPos->y = f2*projPath2->y + (1-f2)*projPath1->y;
  projPathFull.push_back(intPos);

  tempError = (1-f)*errorRadR[in1] + f*errorRadR[in2];
  distWeightCone.push_back(tempError);
  steps += 1;
 }
}


// Chris -- The fuctions buildErrorCone(), buildECSmooth(), and interpECSmooth() should give
// you what you want. =)

// Find the points along that error cone that correspond to the points given in the NHC
// Advisory
void advisory::buildErrorCone(){
 double errorRad[] = {50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
 //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
 int i;

 for(i = 0; i < projPath.size(); i++){
  cout << *(projPath[i]) << endl;
 }

 // Avg path list
 std::vector<Vector4d*>::iterator p1;
 std::vector<Vector4d*>::iterator p2;
 std::vector<Vector2d> errorCone1;
 std::vector<Vector2d> errorCone2;

 Vector2d endPoint1, endPoint2;

 Vector2d tempPoint, mapPos, * t1,  * t2;
 double tempBear, startBear, endBear;

 startBear = endBear = 0.0;

 // New std deviation using distance
 t1 = new Vector2d();
 t2 = new Vector2d();

 // Build first side
 mapPos.set((*projPath.begin())->x, (*projPath.begin())->y);
 errorCone1.push_back(mapPos);
 for(p1 = projPath.begin(), p2=projPath.begin()+1, i=0; 
   p2 != projPath.end()-1; 
   p1++, p2++, i++){

  t1->set((*p1)->x, (*p1)->y);
  t2->set((*p2)->x, (*p2)->y);
  tempBear = findBearing_2(t1, t2) - 90.0;
  if(tempBear < 0.0){
   tempBear = tempBear + 360;
  }
  tempPoint = locateDestination_2(t2->x, t2->y, errorRad[i], tempBear);
  mapPos.set(tempPoint.x, tempPoint.y);
  errorCone1.push_back(mapPos);
 }

 // Build second side
 mapPos.set((*projPath.begin())->x, (*projPath.begin())->y);
 errorCone2.push_back(mapPos);
 for(p1 = projPath.begin(), p2=projPath.begin()+1, i=0; 
   p2 != projPath.end()-1;
   p1++, p2++, i++){

  t1->set((*p1)->x, (*p1)->y);
  t2->set((*p2)->x, (*p2)->y);
  tempBear = fmod(findBearing_2(t1, t2) + 90.0, 360.0);
  tempPoint = locateDestination_2(t2->x, t2->y, errorRad[i], tempBear);
  mapPos.set(tempPoint.x, tempPoint.y);
  errorCone2.push_back(mapPos);
 }

 // Interpolate to find the points inbetween the error cone points that correspond
 // to the advisory positions.
 buildECSmooth(errorCone1, errorCone2);

 // Build the display for the NHC error cone in the user study
 buildTrueCone();

 delete(t1);
 delete(t2);
}

void advisory::buildECSmooth(std::vector<Vector2d> & errorCone1, 
  std::vector<Vector2d> & errorCone2){
 double h;
 unsigned int i;
 Vector2d p0, p1;

 // First Side
 for(i=0; i<errorCone1.size(); i++){
  p0 = errorCone1[i];
  p1 = errorCone1[i+1];

  h = 0.0;
  if(i==0){ h=9.0; }
  else if(i==1 || i==2 || i==3){ h=12.0; }
  else if(i==4){ h=24.0; }

  interpECSmooth(p0, p1, h, 0);
 }

 // Second Side
 for(i=0; i<errorCone2.size(); i++){
  p0 = errorCone2[i];
  p1 = errorCone2[i+1];

  h = 0.0;
  if(i==0){ h=9.0; }
  else if(i==1 || i==2 || i==3){ h=12.0; }
  else if(i==4){ h=24.0; }

  interpECSmooth(p0, p1, h, 1);
 }
}

// Interpolate between p0 and p1 to find additional points on the error cone
void advisory::interpECSmooth(Vector2d p0, Vector2d p1, double h, int side){
 double dist = haversine(p0.x, p1.x, p0.y, p1.y);
 double speed = dist/h;
 int segments = (int)(h/3.0);
 double disPerSeg = dist/(double)segments;

 Vector2d curPos = p0;
 double tBear = findBearing_2(&curPos, &p1);

 Vector4d * toAdd;
 int i;

 toAdd = new Vector4d(curPos.x, curPos.y, tBear, speed);
 if(side==0){ ecSmooth1.push_back(toAdd); }
 else{ ecSmooth2.push_back(toAdd); }

 for(i=0; i < segments-1; i++){
  curPos = locateDestination_2(curPos.x, curPos.y, disPerSeg, tBear);
  tBear = findBearing_2(&curPos, &p1);

  toAdd = new Vector4d(curPos.x, curPos.y, tBear, speed);
  if(side == 0){ ecSmooth1.push_back(toAdd); }
  else{ ecSmooth2.push_back(toAdd); } 
 }

 // Chris, this is tricky!
 if(h == 24){
  toAdd = new Vector4d(p1.x, p1.y, tBear, speed);
  if(side == 0){ ecSmooth1.push_back(toAdd); }
  else{ ecSmooth2.push_back(toAdd); }
 }
}

// Chris --- Ignore buildTrueCone() and buildTrueConeSide()
void advisory::buildTrueCone(){

 buildTrueConeSide(0);
 buildTrueConeSide(1);
 for(int i = 0; i < nlayers; i++){
  cout << ctrlps1[i].size() << " " << ctrlps2[i].size() << endl;
  makeSmoothCones(0, i);
  makeSmoothCones(1, i);
 }
 buildSmoothCone();

 cout << "Finished building the true cone..." << endl;
}

void advisory::makeSmoothCones(int side, int id){
 if(ctrlps1[id].size() != ctrlps2[id].size()){
  cout << ctrlps1[id].size() << " " << ctrlps2[id].size() << endl;
  cout << "control points on two sides do not match!" << endl;
  exit(0);
 }
 std::vector<Vector2d*> allCtrls;
 if(side == 0){
  for(int i = 0 ; i < ctrlps1[id].size(); i++)
   allCtrls.push_back(ctrlps1[id][i]);
 }
 else if(side == 1){
  for(int i = 0 ; i < ctrlps2[id].size(); i++)
   allCtrls.push_back(ctrlps2[id][i]);
  //for(int i = ctrlp2.size()-1 ; i >= 0; i--)
  // allCtrls.push_back(ctrlp2[i]);
 }
 int nctrl = allCtrls.size(); 
 for(int i = 0; i < nctrl; i++){
  Vector2d tmp = translateToScreen(allCtrls[i]->x, allCtrls[i]->y);
  allCtrls[i] = new Vector2d(tmp);
 }

 //add additional start and end points
 Vector2d* add1 = new Vector2d(allCtrls[0]->x, allCtrls[0]->y+1);
 Vector2d* add2 = new Vector2d(allCtrls[nctrl-1]->x, allCtrls[nctrl-1]->y+1);
 allCtrls.insert(allCtrls.begin(), add1); 
 allCtrls.push_back(add2);
 nctrl = allCtrls.size();
 if(side == 0)
  smoothCones1[id] = CatmullRom(allCtrls, nctrl, 60);
 else if(side == 1)
  smoothCones2[id] = CatmullRom(allCtrls, nctrl, 60);
}


void advisory::buildSmoothConeSide(int side){
 if(ctrlp1.size() != ctrlp2.size()){
  cout << "control points on two sides do not match!" << endl;
  exit(0);
 }
 std::vector<Vector2d*> allCtrls;

 ////draw a line for the last segment
 //if(side == 0){
 // for(int i = 0 ; i < ctrlp1.size()-1; i++)
 //  allCtrls.push_back(ctrlp1[i]);
 //}
 //else if(side == 1){
 // for(int i = 0 ; i < ctrlp2.size()-1; i++)
 //  allCtrls.push_back(ctrlp2[i]);
 // //for(int i = ctrlp2.size()-1 ; i >= 0; i--)
 // // allCtrls.push_back(ctrlp2[i]);
 //}

 //consider the last point on one side as a control point
 if(side == 0){
  for(int i = 0 ; i < ctrlp1.size(); i++)
   allCtrls.push_back(ctrlp1[i]);
 }
 else if(side == 1){
  for(int i = 0 ; i < ctrlp2.size(); i++)
   allCtrls.push_back(ctrlp2[i]);
  //for(int i = ctrlp2.size()-1 ; i >= 0; i--)
  // allCtrls.push_back(ctrlp2[i]);
 }


 int nctrl = allCtrls.size(); 
 for(int i = 0; i < nctrl; i++){
  Vector2d tmp = translateToScreen(allCtrls[i]->x, allCtrls[i]->y);
  allCtrls[i] = new Vector2d(tmp);
  //cout << *allCtrls[i] << endl;
 }

 //add additional start and end points
 //Vector2d tmp0(allCtrls[nctrl-2]->x, allCtrls[nctrl-2]->y);
 //Vector2d tmp0(projPath[projPath.size()-3]->x, projPath[projPath.size()-3]->y);
 //tmp0 = translateToScreen(tmp0.x, tmp0.y);
 Vector2d tmp0(allCtrls[nctrl-2]->x, allCtrls[nctrl-2]->y);
 Vector2d tmp1(allCtrls[nctrl-1]->x, allCtrls[nctrl-1]->y);
 Vector2d u = (tmp1 - tmp0);
 float d = (tmp1 - tmp0).norm();
 u = u / d;
 u = tmp1 + u * 10;
 Vector2d* add1 = new Vector2d(allCtrls[0]->x, allCtrls[0]->y+10);
 //Vector2d* add2 = new Vector2d(allCtrls[nctrl-1]->x, allCtrls[nctrl-1]->y-10);
 Vector2d* add2 = new Vector2d(u.x, u.y);
 allCtrls.insert(allCtrls.begin(), add1); 
 allCtrls.push_back(add2);
 nctrl = allCtrls.size();
 testp = tmp0;
 testp1 = u;

 testall = allCtrls;

 if(side == 0)
  smoothCone1 = CatmullRom(allCtrls, nctrl, 60);
 else if(side == 1)
  smoothCone2 = CatmullRom(allCtrls, nctrl, 60);

 //draw a line for the last segment
 //if(side == 0){
 // Vector2d tmp = translateToScreen(ctrlp1[ctrlp1.size()-1]->x, ctrlp1[ctrlp1.size()-1]->y);
 // smoothCone1.push_back(tmp);
 //}
 else if(side == 1){
  Vector2d tmp = translateToScreen(ctrlp2[ctrlp2.size()-1]->x, ctrlp2[ctrlp2.size()-1]->y);
  smoothCone2.push_back(tmp);
 }

}

void advisory::buildSmoothCone(){
 if(trueCone1.size() != trueCone2.size()){
  cout << "Error: two side do not match!" << endl;
  exit(0);
 }

 buildSmoothConeSide(0);
 buildSmoothConeSide(1);

}

double advisory::computeRation(int id){
 id = id + 1;
 //set original rad = 2 * std
 //e^(-d^2/(2*delta^2))
 double co = 1.0 - pow(E, -2);
 //set d = id / nlayers * rad
 double d = id / (1.0 * nlayers) * 2; 
 double tmp = d * d / 2.0;
 double cnew = 1.0 - pow(E, -tmp);
 cnew = cnew / co;
 return cnew;
}

double advisory::computeBoxplotRation(double frac){
 double co = pow(E, -2);
 // current radius is 67.7%
 // current radius is 2*std
 // co/lambda = (1-67.7%) / (1-frac)
 double lamdba = (1 - frac) / (1 - 0.677) * co;
 // e^(-x^2/2) = lamdba
 double x = log(lamdba) * (-2);
 x = sqrt(x);
 // new redius is x * std
 double newc = x / 2.0;
 return newc;
}

void advisory::makeBoxplotCones(int side, double frac){
 // Radius of the error cone using 2010 values
 double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
 //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
 //set rad = 2*std
 double ration = computeBoxplotRation(frac);
 for(int i = 0; i < 7; i++){
  errorRad[i] *= ration;
 }

 Vector2d * push;
 Vector2d p0, p1, pT;
 double l, r, R, x, d, theta, phi1, phi2;
 double bearing;

 for(unsigned int i=0; i<projPath.size()-2; i++){
  p0.set(projPath[i]->x, projPath[i]->y);
  p1.set(projPath[i+1]->x, projPath[i+1]->y);

  d = haversine(p0.x, p1.x, p0.y, p1.y);
  r = errorRad[i];
  R = errorRad[i+1];
  x = (r*d)/(R-r);
  l = sqrt((x+d)*(x+d) - R*R);
  theta = (atan2(R, l)*180.0)/M_PI;
  if(theta != theta){ theta = 0.0; }
  phi1 = (theta+90.0);
  phi2 = 180.0 - phi1;

  if(side == 0){
   bearing = findBearing_2(&p0, &p1);
   phi1 = fmod(bearing + phi1, 360.0);
   bearing = fmod(findBearing_2(&p1, &p0)+180.0, 360.0);
   phi2 = fmod(bearing + phi2, 360.0);
  }
  else{
   bearing = findBearing_2(&p0, &p1);
   phi1 = bearing - phi1;
   if(phi1 < 0.0){ phi1 += 360.0; }
   bearing = fmod(findBearing_2(&p1, &p0)+180.0, 360.0);
   phi2 = bearing - phi2;
   if(phi2 < 0.0){ phi2 += 360.0; }
  }

  // Determine points and push to the error cone
  if(side == 0){
   pT = locateDestination_2(p0.x, p0.y, r, phi1); 
   push = new Vector2d(pT.x, pT.y);
   boxplotCones1.push_back(push);
   pT = locateDestination_2(p1.x, p1.y, R, phi1); 
   push = new Vector2d(pT.x, pT.y);
   boxplotCones1.push_back(push);
  }
  else{
   pT = locateDestination_2(p0.x, p0.y, r, phi1); 
   push = new Vector2d(pT.x, pT.y);
   boxplotCones2.push_back(push);
   pT = locateDestination_2(p1.x, p1.y, R, phi1); 
   push = new Vector2d(pT.x, pT.y);
   boxplotCones2.push_back(push);
  } 
 }
 //cout << "here" << endl;
 //for(int i = 0; i < cones1[id].size(); i++)
 // std::cout << *(cones1[id])[i] << std::endl;
}



void advisory::makeCones(int side, int id){
 // Radius of the error cone using 2010 values
 double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
 //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
 //set rad = 2*std
 double ration = computeRation(id);
 for(int i = 0; i < 7; i++){
  errorRad[i] *= ration;
 }

 Vector2d * push;
 Vector2d p0, p1, pT;
 double l, r, R, x, d, theta, phi1, phi2;
 double bearing;

 Vector2d prepT1;
 Vector2d prepT2;
 for(unsigned int i=0; i<projPath.size()-2; i++){
  p0.set(projPath[i]->x, projPath[i]->y);
  p1.set(projPath[i+1]->x, projPath[i+1]->y);

  d = haversine(p0.x, p1.x, p0.y, p1.y);
  r = errorRad[i];
  R = errorRad[i+1];
  x = (r*d)/(R-r);
  l = sqrt((x+d)*(x+d) - R*R);
  theta = (atan2(R, l)*180.0)/M_PI;
  if(theta != theta){ theta = 0.0; }
  phi1 = (theta+90.0);
  phi2 = 180.0 - phi1;

  if(side == 0){
   bearing = findBearing_2(&p0, &p1);
   phi1 = fmod(bearing + phi1, 360.0);
  }
  else{
   bearing = findBearing_2(&p0, &p1);
   phi1 = bearing - phi1;
   if(phi1 < 0.0){ phi1 += 360.0; }
   bearing = fmod(findBearing_2(&p1, &p0)+180.0, 360.0);
  }

  // Determine points and push to the error cone
  if(side == 0){
   pT = locateDestination_2(p0.x, p0.y, r, phi1); 
   if(i != 0)
    pT = prepT1;
   push = new Vector2d(pT.x, pT.y);
   cones1[id].push_back(push);
   ctrlps1[id].push_back(push);
   pT = locateDestination_2(p1.x, p1.y, R, phi1); 
   push = new Vector2d(pT.x, pT.y);
   cones1[id].push_back(push);
   prepT1 = pT;
  }
  else{
   pT = locateDestination_2(p0.x, p0.y, r, phi1); 
   if(i != 0)
    pT = prepT2;
   push = new Vector2d(pT.x, pT.y);
   cones2[id].push_back(push);
   ctrlps2[id].push_back(push);
   pT = locateDestination_2(p1.x, p1.y, R, phi1); 
   push = new Vector2d(pT.x, pT.y);
   cones2[id].push_back(push);
   prepT2 = pT;
  } 
 }

 if(side == 0)
  ctrlps1[id].push_back(push);
 else
  ctrlps2[id].push_back(push);
 //cout << "here" << endl;
 //for(int i = 0; i < cones1[id].size(); i++)
 // std::cout << *(cones1[id])[i] << std::endl;
}

/*
// pure tan sample
void advisory::buildTrueConeSide(int side){
// Radius of the error cone using 2010 values
// change temp
double errorRad[] = {0.0, 50.004, 100.471, 154.302, 187.515, 283.263, 390.463};
//double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};

Vector2d * push;
Vector2d p0, p1, pT;
double l, r, R, x, d, theta, phi1, phi2;
double bearing;

Vector2d prepT1;
Vector2d prepT2;
Vector2d* last;

//for the first one
p0.set(projPath[0]->x, projPath[0]->y);
push = new Vector2d(p0.x, p0.y);
if(side == 0){
ctrlp1.push_back(push);
}
else{
ctrlp2.push_back(push);
}
for(unsigned int i=0; i<projPath.size()-2; i++){
if(side == 0)
p0.set(ctrlp1[ctrlp1.size()-1]->x, ctrlp1[ctrlp1.size()-1]->y);
else
p0.set(ctrlp2[ctrlp2.size()-1]->x, ctrlp2[ctrlp2.size()-1]->y);
p1.set(projPath[i+1]->x, projPath[i+1]->y);

d = haversine(p0.x, p1.x, p0.y, p1.y);
r = errorRad[i];
R = errorRad[i+1];
l = sqrt(d*d - R*R);
theta = (atan2(R, l)*180.0)/M_PI;
if(side == 0)
theta = findBearing_2(&p0, &p1) + (90 + theta);
else if(side == 1)
theta = findBearing_2(&p0, &p1) + (90 + theta) + 2 * (90 - theta);

pT = locateDestination_2(p1.x, p1.y, R, theta);
push = new Vector2d(pT.x, pT.y);
if(side == 0)
ctrlp1.push_back(push);
else if(side == 1)
ctrlp2.push_back(push);

// Determine points and push to the error cone
if(side == 0){
pT = locateDestination_2(p0.x, p0.y, r, phi1); 
if(i != 0)
pT = prepT1;
push = new Vector2d(pT.x, pT.y);
trueCone1.push_back(push);
//ctrlp1.push_back(push);
last = push;
pT = locateDestination_2(p1.x, p1.y, R, phi1); 
push = new Vector2d(pT.x, pT.y);
trueCone1.push_back(push);
prepT1 = pT;
}
else{
pT = locateDestination_2(p0.x, p0.y, r, phi1); 
if(i != 0)
pT = prepT2;
push = new Vector2d(pT.x, pT.y);
trueCone2.push_back(push);
last = push;
//ctrlp2.push_back(push);
pT = locateDestination_2(p1.x, p1.y, R, phi1); 
push = new Vector2d(pT.x, pT.y);
trueCone2.push_back(push);
prepT2 = pT;
} 
}


int id = projPath.size() - 2;
cout << id << endl;
//p0.set(projPath[id-1]->x, projPath[id-1]->y);
p0.set(last->x, last->y);
p1.set(projPath[id]->x, projPath[id]->y);
float h = haversine(p0.x, p1.x, p0.y, p1.y); 
Vector2d C = translateToScreen(p1.x, p1.y);
Vector2d X = translateToScreen(p0.x, p0.y);
Vector2d u = (C - X) / (C - X).norm();
r = errorRad[id];
d = sqrt(h*h - r*r);
theta = (atan2(r, d)*180.0)/M_PI;
if(side == 0)
 theta = findBearing_2(&p0, &p1) + (90 + theta);
else if(side == 1)
 theta = findBearing_2(&p0, &p1) + (90 + theta) + 2 * (90 - theta);
 pT = locateDestination_2(p1.x, p1.y, r, theta);
 push = new Vector2d(pT.x, pT.y);
 //if(side == 0)
 // ctrlp1.push_back(push);
 //else if(side == 1)
 // ctrlp2.push_back(push);

 //cout << p0 << " " << p1 << " " << h << " " << r << " " << d << theta << endl;


 //Matrix2x2 tm(cos(theta), -sin(theta), sin(theta), cos(theta));
 //Vector2d u_ = tm * u;
 //Vector2d p = p0 + d * u_;
 //p1.set(projPath[id+1]->x, projPath[id+1]->y);
 //p = locateDestination_2(p1.x, p1.y, r, 90);
 //push = new Vector2d(p.x, p.y);
 //if(side == 0)
 // ctrlp1.push_back(push);
 //else if(side == 1)
 // ctrlp2.push_back(push);
 //cout << *push << endl;

 //if(side == 0)
 // ctrlp1.push_back(push);
 //else
 // ctrlp2.push_back(push);

 for(int i = 0; i < nlayers; i++){
  makeCones(side, i);
  //makeSmoothCones(side, i);
 }
makeBoxplotCones(side, BoxplotFrac);
}
*/



// last point tan sample
void advisory::buildTrueConeSide(int side){
 // Radius of the error cone using 2010 values
 double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
 //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};

 Vector2d * push;
 Vector2d p0, p1, pT;
 double l, r, R, x, d, theta, phi1, phi2;
 double bearing;

 Vector2d prepT1;
 Vector2d prepT2;
 Vector2d* last;
 for(unsigned int i=0; i<projPath.size()-2; i++){
  p0.set(projPath[i]->x, projPath[i]->y);
  p1.set(projPath[i+1]->x, projPath[i+1]->y);

  d = haversine(p0.x, p1.x, p0.y, p1.y);
  r = errorRad[i];
  R = errorRad[i+1];
  x = (r*d)/(R-r);
  l = sqrt((x+d)*(x+d) - R*R);
  theta = (atan2(R, l)*180.0)/M_PI;
  if(theta != theta){ theta = 0.0; }
  phi1 = (theta+90.0);
  phi2 = 180.0 - phi1;

  if(side == 0){
   bearing = findBearing_2(&p0, &p1);
   phi1 = fmod(bearing + phi1, 360.0);
  }
  else{
   bearing = findBearing_2(&p0, &p1);
   phi1 = bearing - phi1;
   if(phi1 < 0.0){ phi1 += 360.0; }
   bearing = fmod(findBearing_2(&p1, &p0)+180.0, 360.0);
  }

  // Determine points and push to the error cone
  if(side == 0){
   pT = locateDestination_2(p0.x, p0.y, r, phi1); 
   if(i != 0)
    pT = prepT1;
   push = new Vector2d(pT.x, pT.y);
   trueCone1.push_back(push);
   ctrlp1.push_back(push);
   last = push;
   pT = locateDestination_2(p1.x, p1.y, R, phi1); 
   push = new Vector2d(pT.x, pT.y);
   trueCone1.push_back(push);
   prepT1 = pT;
  }
  else{
   pT = locateDestination_2(p0.x, p0.y, r, phi1); 
   if(i != 0)
    pT = prepT2;
   push = new Vector2d(pT.x, pT.y);
   trueCone2.push_back(push);
   last = push;
   ctrlp2.push_back(push);
   pT = locateDestination_2(p1.x, p1.y, R, phi1); 
   push = new Vector2d(pT.x, pT.y);
   trueCone2.push_back(push);
   prepT2 = pT;
  } 
 }


 //int id = projPath.size() - 2;
 //cout << id << endl;
 ////p0.set(projPath[id-1]->x, projPath[id-1]->y);
 //p0.set(last->x, last->y);
 //p1.set(projPath[id]->x, projPath[id]->y);
 //float h = haversine(p0.x, p1.x, p0.y, p1.y); 
 //Vector2d C = translateToScreen(p1.x, p1.y);
 //Vector2d X = translateToScreen(p0.x, p0.y);
 //Vector2d u = (C - X) / (C - X).norm();
 //r = errorRad[id];
 //d = sqrt(h*h - r*r);
 //theta = (atan2(r, d)*180.0)/M_PI;
 //cout << theta << endl;
 //if(side == 0)
 // theta = findBearing_2(&p0, &p1) + (90 + theta);
 //else if(side == 1)
 // theta = findBearing_2(&p0, &p1) + (90 + theta) + 2 * (90 - theta);
 //pT = locateDestination_2(p1.x, p1.y, r, theta);
 //push = new Vector2d(pT.x, pT.y);
 //if(side == 0)
 // ctrlp1.push_back(push);
 //else if(side == 1)
 // ctrlp2.push_back(push);

 if(side == 0)
  ctrlp1.push_back(push);
 else
  ctrlp2.push_back(push);

 for(int i = 0; i < nlayers; i++){
  makeCones(side, i);
 }
 makeBoxplotCones(side, BoxplotFrac);
}

//----------------------------- Drawing Functions ---------------------------//
void advisory::drawGenPaths(){
 std::vector<Vector2d*>::iterator pos1;
 std::vector<Vector2d*>::iterator pos2;
 std::vector<path*>::iterator pathIt;
 Vector2d draw1, draw2;
 float lineWidth;
 float tempTrans;

 // Values for testing whether the median sort is correct
 int side = 0;
 adv = projPath[4];
 adv2 = projPath[5];
 sortPath(22);

 onRight = 0;
 onLeft = 0;

 //if(pathList.size() == 999){
 // outputTracks("ourtracks.txt");
 // exit(0)
 //}


 // Display generated paths
 cout << pathList.size() << endl;
 for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
  int testSide = 0;
  pos2 = (*pathIt)->posList.begin() + 1;
  lineWidth = 0.8;
  glColor4f((*pathIt)->getInt().x, (*pathIt)->getInt().y, (*pathIt)->getInt().z, (*pathIt)->getInt().w);
  tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
  for(pos1 = (*pathIt)->posList.begin(); pos2 != (*pathIt)->posList.end(); pos1++){
   glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);
   glLineWidth(lineWidth * 1.0);
   draw1.x = (*pos1)->x;
   draw1.y = (*pos1)->y;
   draw2.x = (*pos2)->x;
   draw2.y = (*pos2)->y;
   glBegin(GL_LINES);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glEnd();
   pos2++;
   //lineWidth = lineWidth + 0.5;
   tempTrans = tempTrans - fadeConst;
  }

  testSide < 0 ? onLeft++ : onRight++;
  side++;
  (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
 }

 for(pathIt = pathList.begin(); pathIt != pathList.end();){
  if ((*pathIt)->getInt().w <= 0.0) {
   delete((*pathIt));
   pathIt = pathList.erase(pathIt);
  }
  else{
   pathIt++;
  }
 }

 //opImage = true;
 //cout << "OnLeft:  " << onLeft << "     OnRight: " << onRight << endl;
}

int advisory::isOutlier(std::vector<path*>::iterator pathIt){
 double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
 //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
 cout << (*pathIt)->posList.size() << endl;
 cout << (*pathIt)->posList[0]->x << " " << (*pathIt)->posList[0]->y << endl;
 Vector2d draw1 = translateToScreen((*projPath[0]).x, (*projPath[0]).y);
 cout << draw1.x << " " << draw1.y << endl; 
 for(int i = 0; i < (*pathIt)->posList.size(); i++){
  int j = i / 4;
 }
 exit(0);

 return 0;
}

void advisory::drawOutliers(){
 std::vector<Vector2d*>::iterator pos1;
 std::vector<Vector2d*>::iterator pos2;
 std::vector<path*>::iterator pathIt;
 Vector2d draw1, draw2;
 float lineWidth;
 float tempTrans;

 // Values for testing whether the median sort is correct
 int side = 0;
 adv = projPath[4];
 adv2 = projPath[5];
 sortPath(22);

 onRight = 0;
 onLeft = 0;

 // Display generated paths
 for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
  int testSide = 0;
  int isoutlier = 0;
  pos2 = (*pathIt)->posList.begin() + 1;
  lineWidth = 0.8;
  glColor4f((*pathIt)->getInt().x, (*pathIt)->getInt().y,
    (*pathIt)->getInt().z, (*pathIt)->getInt().w);
  tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
  isoutlier = isOutlier(pathIt);
  if(isoutlier == 0)
   continue;
  for (pos1 = (*pathIt)->posList.begin(); pos2 != (*pathIt)->posList.end(); pos1++) {
   glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y,
     tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);
   //glColor4f(tempTrans*1.0, tempTrans*0.0,
   //      tempTrans*0.0, tempTrans);
   //if(side <= med){
   if((*pathIt)->getSide() > 0){
    glColor4f(0.2, 0.2, 1.0, tempTrans * 0.5);
    if(pos2+1 == (*pathIt)->posList.end()){
     testSide--;
    }
    glColor4f(0.0, 0.0, 1.0, tempTrans * 0.5);
    glColor4f(tempTrans*0.0, tempTrans*0.0,
      tempTrans*1.0, tempTrans * 0.5);
   }
   else if((*pathIt)->getSide() < 0){
    glColor4f(0.2, 0.2, 1.0, tempTrans * 0.5);
    if(pos2+1 == (*pathIt)->posList.end()){
     testSide++;
    }
    glColor4f(tempTrans*0.0, tempTrans*0.0,
      tempTrans*1.0, tempTrans * 0.5);
    //glColor4f(0.0, 0.0, 1.0, tempTrans);
    //glColor4f(1.0, 0.0, 0.0, tempTrans);
   }
   else{
    glColor4f(0.0, 0.0, 0.0, 0.0);
   }
   glLineWidth(lineWidth * 1.0);
   draw1.x = (*pos1)->x;
   draw1.y = (*pos1)->y;
   draw2.x = (*pos2)->x;
   draw2.y = (*pos2)->y;
   glBegin(GL_LINES);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glEnd();
   pos2++;
   //lineWidth = lineWidth + 0.5;
   tempTrans = tempTrans - 0.025;
  }

  testSide < 0 ? onLeft++ : onRight++;
  side++;
  (*pathIt)->setInt((*pathIt)->getInt().w - 0.001);
  }
  for (pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  //cout << "OnLeft:  " << onLeft << "     OnRight: " << onRight << endl;
 }

 void advisory::drawGenPathsRainDrop(){
  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  Vector2d draw1, draw2;
  float tempTrans;
  float age;

  Vector2d * curPos;


  // for each path
  for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   (*pathIt)->colorEncoding();
   //tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
   tempTrans = (*pathIt)->getInt().w;
   age = (*pathIt)->getAge();
   float redVal = 0.5;
   float greenVal = 0.0;
   float blueVal = 0.0;
   float distAdd = 0.0;
   int i = 0;
   i = min(userInt->getSliderCurTick(), (int)((*pathIt)->posList.size())-1);
   i = max(i, 0);

   // a particular time point on this path
   curPos = (*pathIt)->posList[i];

   int angle;
   double angle_radians;
   double x, y;
   double x1, y1;

   float newTempTrans = tempTrans - (distAdd/1.5);
   float newDropRadius = 30.0 / 100 * (*pathIt)->sizeList[i];
   Vector4d cval = (*pathIt)->colorList[i];
   glColor4f(cval.x, cval.y, cval.z, newTempTrans);

   x1 = y1 = 0.0;

   drawCategories(curPos->x, curPos->y, newDropRadius, (*pathIt)->categoryList[i], newTempTrans);
   if(i == 0)
    continue;


   glDisable(GL_DEPTH_TEST);
   distAdd += 0.02;

   greenVal += 0.02;
   blueVal += 0.02; 

   //(*pathIt)->setInt((*pathIt)->getInt().w - ((age)*0.003));
   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
   (*pathIt)->incAge();
  }
  for (pathIt = pathList.begin(); pathIt != pathList.end();){
   if((*pathIt)->getInt().w <= 0.0){
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }
 }

 void advisory::drawGenPathsEyePoint(){
  std::vector<hurEyePoints*>::iterator _e;

  for(_e = eyePointList.begin(); _e != eyePointList.end(); _e++){
   (*_e)->drawPoints();
   (*_e)->age(0.01);
   if( (*_e)->getTrans() < 0.0 ){
    delete((*_e));
    eyePointList.erase(_e);
   }
  }
 }

 int advisory::drawGenPathsTrail(int step){
  std::vector<path*>::iterator pathIt;
  Vector2d * p1;
  Vector2d * p2;
  double trans = 1.0;
  bool drawingRad = false;

  if(step > 23){ return step; }

  for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   //cout << "Step: " << step << endl << flush;
   trans = 1.0;
   for(int curStep = step-1; curStep >= 0; curStep--){
    p1 = (*pathIt)->posList[curStep]; 
    p2 = (*pathIt)->posList[curStep+1]; 
    glLineWidth(1.0);
    //glColor4f(0.0, 0.0, 1.0, trans);
    glColor4f(0.0, 0.0, 1.0, 0.5);

    glBegin(GL_LINES);
    glVertex3f(p1->x, p1->y, 0.0);
    glVertex3f(p2->x, p2->y, 0.0);
    glEnd();
    trans -= 0.1;

    if((step == 3 || step == 7 || step == 11 || step == 15 || step == 23)
      && (curStep == step - 1)){
     drawingRad = true;
     if(percBubDrawn < 1.0){
      int angle;
      double angle_radians;
      double x, y;
      double x1 = 0.0;
      double y1 = 0.0;

      float newTempTrans = 1.0 - (0.5*percBubDrawn);
      float newDropRadius = 3 + (4*percBubDrawn);
      glColor4f(0.0, 0.0, 1.0, newTempTrans);

      glLineWidth(2.0);
      //glBegin(GL_LINES);
      glBegin(GL_POLYGON);
      for(angle = 0; angle < 360; angle += 5){
       angle_radians = angle*(double)M_PI/180.0;
       x = p2->x + newDropRadius*(double)cos(angle_radians);
       y = p2->y + newDropRadius*(double)sin(angle_radians);
       if(angle == 0){
        x1 = x; 
        y1 = y;
       }
       glVertex3f(x,y,0.0); 
      }
      glVertex3f(x1,y1,0.0); 
      glEnd();
     }
    }
   }
  }

  if(percBubDrawn >= 1.0){
   percBubDrawn = 0.0;
   drawingRad = false;
  }
  if(drawingRad == true){
   percBubDrawn += 0.1;
   step--;
   usleep(175000);
  }
  else{
   usleep(150000);
  }

  return step;
 }

 void advisory::drawHeatMap(){
  std::vector<Vector2d*>::iterator pos1;
  std::vector<Vector2d*>::iterator pos2;
  std::vector<path*>::iterator pathIt;
  Vector2d draw1, draw2;
  float lineWidth;

  pathIt = pathList.end();

  for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++) {
   int test = 0;
   pos2 = (*pathIt)->posList.begin() + 1;
   lineWidth = 1.0;
   glColor4f(1.0, 1.0, 1.0, 0.01);
   for (pos1 = (*pathIt)->posList.begin(); pos2 != (*pathIt)->posList.end();
     pos1++) {
    glLineWidth(lineWidth * 1.0);
    draw1.x = (*pos1)->x;
    draw1.y = (*pos1)->y;
    draw2.x = (*pos2)->x;
    draw2.y = (*pos2)->y;
    glBegin(GL_LINES);
    glVertex3f(draw1.x, draw1.y, 0.0);
    glVertex3f(draw2.x, draw2.y, 0.0);
    glEnd();
    cout << draw1 << " " << draw2 << endl;
    pos2++;
    test++;
   }
  }
 }

 // Draw the projected path for each advisory in the simulation
 void advisory::drawForecastPath() {
  Vector2d draw1, draw2;
  std::vector<Vector4d*>::iterator pos1;
  std::vector<Vector4d*>::iterator pos2;

  //drawTrueCone();

  //glColor4f(1.0, 1.0, 1.0, 1.0);
  glColor4f(0, 0, 0.5, 1.0);
  glLineWidth(2.0);

  pos2 = projPath.begin() + 1;
  for (pos1 = projPath.begin(); pos2 != projPath.end()-1; pos1++) {
   draw1 = translateToScreen((*pos1)->x, (*pos1)->y);
   draw2 = translateToScreen((*pos2)->x, (*pos2)->y);
   glBegin(GL_LINES);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glEnd();
   pos2++;
  }

  for(pos1 = projPath.begin(); pos1 != projPath.end()-1; pos1++){
   draw1 = translateToScreen((*pos1)->x, (*pos1)->y);
   drawCircle(draw1.x, draw1.y, TimePointRadius);
  }

 }

 void advisory::drawErrorSmooth(){
  Vector2d draw1, draw2;
  std::vector<Vector4d*>::iterator pos1_4d;
  std::vector<Vector4d*>::iterator pos2_4d;


  // Draw the smoothed side
  glColor4f(0.0, 1.0, 0.0, 0.5);
  pos2_4d = ecSmooth1.begin()+1;
  for(pos1_4d = ecSmooth1.begin(); pos2_4d != ecSmooth1.end(); pos1_4d++){
   draw1 = translateToScreen((*pos1_4d)->x, (*pos1_4d)->y);
   draw2 = translateToScreen((*pos2_4d)->x, (*pos2_4d)->y);
   glBegin(GL_LINES);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glEnd();
   pos2_4d++;
  }

  // Draw the smoothed side
  glColor4f(0.0, 1.0, 0.0, 0.5);
  pos2_4d = ecSmooth2.begin()+1;
  for(pos1_4d = ecSmooth2.begin(); pos2_4d != ecSmooth2.end(); pos1_4d++){
   draw1 = translateToScreen((*pos1_4d)->x, (*pos1_4d)->y);
   draw2 = translateToScreen((*pos2_4d)->x, (*pos2_4d)->y);
   glBegin(GL_LINES);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glEnd();
   pos2_4d++;
  }
 }

 void advisory::drawErrorConeRadius(){
  // Draw the circles
  glColor4f(1.0, 0.0, 0.0, 1.0); 
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  Vector2d draw1;

  for(unsigned int i = 1; i < projPath.size()-1; i++){
   glBegin(GL_LINES);
   for(curDeg = 0; curDeg < 360.0; curDeg++){
    draw1 = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRad[i], curDeg);
    draw1 = translateToScreen(draw1.x, draw1.y);
    glVertex3f(draw1.x, draw1.y, 0.0);
   }
   glEnd();
  }
 }


 // Functions for sorting the paths based on their distanced at a segment to the predicted path 
 void advisory::sortPath(int seg){
  sortInd = seg;

  std::sort(pathList.begin(), pathList.end(), sp);
 }

 void advisory::drawMultiCones(int idx){
  Vector2d draw1, draw2, draw3, draw4;
  std::vector<Vector2d*>::iterator _p1;
  std::vector<Vector2d*>::iterator _p2;
  std::vector<Vector2d*>::iterator _p3;
  std::vector<Vector2d*>::iterator _p4;

  double ration = computeRation(idx);

  glColor4f(0.5, 0.5, 1.0, 0.5 * (1-ration));
  glLineWidth(1.5);

  //glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendColor(0, 0, 1.0, 0.1);
  glEnable(GL_COLOR_LOGIC_OP);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _p3 = cones2[idx].begin();
  for(_p1 = cones1[idx].begin(); _p1 != cones1[idx].end()-1; _p1++){
   _p2 = _p1 + 1;
   _p4 = _p3 + 1;
   draw1 = translateToScreen((*_p1)->x, (*_p1)->y);  
   draw2 = translateToScreen((*_p2)->x, (*_p2)->y);
   draw3 = translateToScreen((*_p3)->x, (*_p3)->y);  
   draw4 = translateToScreen((*_p4)->x, (*_p4)->y);
   glLogicOp(GL_AND);
   glBegin(GL_POLYGON);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glVertex3f(draw4.x, draw4.y, 0.0);
   glVertex3f(draw3.x, draw3.y, 0.0);
   glEnd();
   //count++;
   _p3++;
  }

  // Draw the circles
  //glColor4f(0.5, 0.5, 1.0, 0.5); 
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  glLogicOp(GL_AND);
  glBegin(GL_TRIANGLE_FAN);
  Vector2d tmp = translateToScreen(projPath[id]->x, projPath[id]->y);
  glVertex3f(tmp.x, tmp.y, 0);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 1){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id]*ration, curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   glVertex3f(draw1.x, draw1.y, 0.0);
  }
  glEnd();

  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_COLOR_LOGIC_OP);
 }

 void advisory::drawBoxplotCones(){
  Vector2d draw1, draw2, draw3, draw4;
  std::vector<Vector2d*>::iterator _p1;
  std::vector<Vector2d*>::iterator _p2;
  std::vector<Vector2d*>::iterator _p3;
  std::vector<Vector2d*>::iterator _p4;

  double ration = computeBoxplotRation(BoxplotFrac);

  glColor4f(0.5, 0.5, 0.5, 0.5);
  glLineWidth(1.5);

  //glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendColor(0, 0, 1.0, 0.1);
  glEnable(GL_COLOR_LOGIC_OP);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _p3 = boxplotCones2.begin();
  for(_p1 = boxplotCones1.begin(); _p1 != boxplotCones1.end()-1; _p1++){
   _p2 = _p1 + 1;
   _p4 = _p3 + 1;
   draw1 = translateToScreen((*_p1)->x, (*_p1)->y);  
   draw2 = translateToScreen((*_p2)->x, (*_p2)->y);
   draw3 = translateToScreen((*_p3)->x, (*_p3)->y);  
   draw4 = translateToScreen((*_p4)->x, (*_p4)->y);
   glLogicOp(GL_AND);
   glBegin(GL_POLYGON);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glVertex3f(draw4.x, draw4.y, 0.0);
   glVertex3f(draw3.x, draw3.y, 0.0);
   glEnd();
   //count++;
   _p3++;
  }

  // Draw the circles
  //glColor4f(0.5, 0.5, 1.0, 0.5); 
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  glLogicOp(GL_AND);
  glBegin(GL_TRIANGLE_FAN);
  Vector2d tmp = translateToScreen(projPath[id]->x, projPath[id]->y);
  glVertex3f(tmp.x, tmp.y, 0);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 1){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id]*ration, curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   glVertex3f(draw1.x, draw1.y, 0.0);
  }
  glEnd();

  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_COLOR_LOGIC_OP);
 }

 Vector2d advisory::deboor(int k, int degree, int i, float u, 
   float knots[], Vector2d* coeff){
  if(k == 0)
   return coeff[i];
  else{
   float t = (u - knots[i]) / (knots[i+degree+1-k]-knots[i]);
   if(knots[i+degree+1-k]-knots[i] < 10e-4)
    t = 0;
   return deboor(k-1, degree, i-1, u, knots, coeff) * (1 - t)
    + deboor(k-1, degree, i, u, knots, coeff) * t;
  }
 }

 void advisory::bsplineCurve(std::vector<Vector2d> controlPolygons){
  int N = controlPolygons.size();
  int n = N - 1; //index
  int D = 3; //degree, in this example , degree = 3, cubic
  int K = D - 1;
  int KnotsNum = N+K+1;
  float knots[KnotsNum];


  Vector2d *coeff = new Vector2d[N];
  for(int i = 0; i < N; i++){
   coeff[i] = controlPolygons[i];
  }

  //cout << KnotsNum << endl;
  for(int i = 0 ; i < KnotsNum; i++){
   if(i <= K)
    knots[i] = 0.0;
   else if(i > K && i < N)
    knots[i] = i - D + 1;
   else if(i >= N && i <= N + D)
    knots[i] = n - D + 2;
  }

  //cout << N << endl;
  //for(int i = 0; i < KnotsNum; i++){
  //	cout << i << " " << knots[i] << endl;
  //}
  //cout << "-----" << endl;

  int upper = N - D + 1; //interval = [0, upper]
  int samplenum = SAMPLENUM + 2;
  //cout << upper << endl;

  glColor3f(1, 0, 0);
  glBegin(GL_LINE_STRIP);
  for(int j = 1; j < samplenum-1; j++){
   float u = 1.0 * upper / samplenum * j;

   float interval = -1;
   for(int k = 0; k < KnotsNum; k++){
    if(knots[k] > u){
     interval = k - 1;
     break;
    }
    else if(knots[k] == u){
     interval = k;
     break;
    }
   }
   //cout << u << " " << interval << endl;
   //cout << "+++" << endl;
   //Vector2d point = deboor2(K, controlPolygons, knots, u, interval);
   Vector2d point = deboor(K, K, interval, u, knots, coeff);
   glVertex2i(point.x, point.y);
   //cout << point << endl;
  }
  glEnd();
 }

 void advisory::drawSmoothOutline(){
  vector<Vector2d> all;
  //glColor4f(0, 0, 0.5, 0.3);
  glColor4f(0.5, 0.5, 1.0, 0.7);
  glLineWidth(2.0);
  glBegin(GL_LINE_STRIP);
  for(int i = 0; i < smoothCone1.size(); i++){
   Vector2d tmp = smoothCone1[i];
   glVertex3f(tmp.x, tmp.y, 0.0);
  }
  glEnd();

  glBegin(GL_LINE_STRIP);
  for(int i = 0; i < smoothCone2.size(); i++){
   Vector2d tmp = smoothCone2[i];
   glVertex3f(tmp.x, tmp.y, 0.0);
  }
  glEnd();

  //Vector2d start(projPath[projPath.size() - 3]->x, projPath[projPath.size() - 3]->y);
  //start = translateToScreen(start.x, start.y);
  //Vector2d end(ctrlp1[ctrlp1.size()-1]->x, ctrlp1[ctrlp1.size()-1]->y);
  //end = translateToScreen(end.x, end.y);
  //glBegin(GL_LINES);
  //glVertex3f(start.x, start.y, 0.0);
  //glVertex3f(end.x, end.y, 0.0);
  //glEnd();

  //end = Vector2d(ctrlp2[ctrlp2.size()-1]->x, ctrlp2[ctrlp2.size()-1]->y);
  //end = translateToScreen(end.x, end.y);
  //glBegin(GL_LINES);
  //glVertex3f(start.x, start.y, 0.0);
  //glVertex3f(end.x, end.y, 0.0);
  //glEnd();

  //glPointSize(20);
  //glBegin(GL_POINTS);
  //glVertex3f(testp.x, testp.y, 0.0);
  //glVertex3f(testp1.x, testp1.y, 0.0);
  //glEnd();


  //glPointSize(10);
  //for(int i = 0; i < ctrlp1.size(); i++){
  // glBegin(GL_POINTS);
  // Vector2d tmp = translateToScreen(ctrlp1[i]->x, ctrlp1[i]->y);
  // glVertex3f(tmp.x, tmp.y, 0.0);
  // glEnd();
  //}

  //glPointSize(10);
  //for(int i = 0; i < ctrlp2.size(); i++){
  // glBegin(GL_POINTS);
  // Vector2d tmp = translateToScreen(ctrlp2[i]->x, ctrlp2[i]->y);
  // glVertex3f(tmp.x, tmp.y, 0.0);
  // glEnd();
  //}

  Vector2d draw1, draw2;
  Vector2d neck = smoothCone2[smoothCone2.size()-1] - smoothCone1[smoothCone1.size()-1];
  draw2 = smoothCone1[smoothCone1.size()-1];
  //glBegin(GL_LINE_STRIP);
  //glVertex3f(smoothCone2[smoothCone2.size()-1].x, smoothCone2[smoothCone2.size()-1].y, 0);
  //glVertex3f(smoothCone1[smoothCone1.size()-1].x, smoothCone1[smoothCone1.size()-1].y, 0);
  //glEnd();

  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  Vector2d prev;
  prev = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id], curDeg);
  prev = translateToScreen(prev.x, prev.y);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 1){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id], curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   Vector2d tmp = draw1 - draw2;
   Vector3d xy1(tmp.x, tmp.y, 0);
   Vector3d xy2(neck.x, neck.y, 0);
   Vector3d xyz = xy1 % xy2;
   if(xyz.z < 0){
    glBegin(GL_LINES);
    glVertex3f(prev.x, prev.y, 0.0);
    glVertex3f(draw1.x, draw1.y, 0.0);
    all.push_back(draw1);
    glEnd();
   }
   prev = draw1;
  }

  Vector2d tmp1;
  float mind = 10e6;
  for(int i = 0; i < all.size(); i++){
   float d1;
   d1 = (all[i] - draw2).norm();
   if(d1 < mind){
    tmp1 = all[i];
    mind = d1;
   }
  }
  glBegin(GL_LINES);
  glVertex3f(draw2.x, draw2.y, 0.0);
  glVertex3f(tmp1.x, tmp1.y, 0.0);
  glEnd();


 }

 void advisory::drawTrueConeOutline(){
  Vector2d draw1, draw2, draw3, draw4;
  std::vector<Vector2d*>::iterator _p1;
  std::vector<Vector2d*>::iterator _p2;
  std::vector<Vector2d*>::iterator _p3;
  std::vector<Vector2d*>::iterator _p4;
  Vector2d neck;
  Vector2d right;

  // smooth cones side curve
  //int nctrls = trueCone1.size();
  //GLfloat ctrlpoints[nctrls][3];
  //for(int i = 0; i <= nctrls/2; i++){
  // draw1 = translateToScreen(trueCone1[i]->x, trueCone1[i]->y);
  // ctrlpoints[i][0] = draw1.x;
  // ctrlpoints[i][1] = draw1.y;
  // ctrlpoints[i][2] = 0.0;
  //}
  //glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, nctrls/2+1, &ctrlpoints[0][0]);
  //glEnable(GL_MAP1_VERTEX_3);
  //glColor3f(1.0, 1.0, 1.0);
  //glBegin(GL_LINE_STRIP);
  //   for (int i = 0; i <= 30; i++) 
  //      glEvalCoord1f((GLfloat) i/30.0);
  //glEnd();
  //int j = 0;
  //for(int i = nctrls/2; i < nctrls; i++){
  // draw1 = translateToScreen(trueCone1[i]->x, trueCone1[i]->y);
  // ctrlpoints[j][0] = draw1.x;
  // ctrlpoints[j][1] = draw1.y;
  // ctrlpoints[j][2] = 0.0;
  // j++;
  //}
  //glMap1f(GL_MAP1_VERTEX_3, 0.0, 1.0, 3, nctrls/2, &ctrlpoints[0][0]);
  //glEnable(GL_MAP1_VERTEX_3);
  //glColor3f(1.0, 1.0, 1.0);
  //glBegin(GL_LINE_STRIP);
  //   for (int i = 0; i <= 30; i++) 
  //      glEvalCoord1f((GLfloat) i/30.0);
  //glEnd();

  // draw outlines
  glColor4f(0, 0, 0.5, 1.0);
  glLineWidth(2.0);
  _p3 = trueCone2.begin();
  for(_p1 = trueCone1.begin(); _p1 != trueCone1.end()-1; _p1++){
   _p2 = _p1 + 1;
   _p4 = _p3 + 1;
   draw1 = translateToScreen((*_p1)->x, (*_p1)->y);  
   draw2 = translateToScreen((*_p2)->x, (*_p2)->y);
   draw3 = translateToScreen((*_p3)->x, (*_p3)->y);  
   draw4 = translateToScreen((*_p4)->x, (*_p4)->y);
   neck = draw4 - draw2;
   right = draw2;

   //glBegin(GL_LINES);
   // glVertex3f(draw1.x, draw1.y, 0.0);
   // glVertex3f(draw2.x, draw2.y, 0.0);
   //glEnd();

   glBegin(GL_LINES);
   glVertex3f(draw4.x, draw4.y, 0.0);
   glVertex3f(draw3.x, draw3.y, 0.0);
   glEnd();

   _p3++;
  }


  glColor4f(0, 0, 0.5, 1.0);
  glLineWidth(2.0);
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  Vector2d prev;
  prev = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id], curDeg);
  prev = translateToScreen(prev.x, prev.y);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 1){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id], curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   Vector2d tmp = draw1 - draw2;
   Vector3d xy1(tmp.x, tmp.y, 0);
   Vector3d xy2(neck.x, neck.y, 0);
   Vector3d xyz = xy1 % xy2;
   if(xyz.z < 0){
    glBegin(GL_LINES);
    glVertex3f(prev.x, prev.y, 0.0);
    glVertex3f(draw1.x, draw1.y, 0.0);
    glEnd();
   }
   prev = draw1;
  }
 }

 void advisory::drawMultiSmoothCones(int n){

  double ration = computeRation(n);
  glColor4f(0.5, 0.5, 1.0, 1-ration);
  glLineWidth(1.5);

  glEnable(GL_BLEND);
  glBlendColor(0.0, 0.0, 1.0, 0.5);
  glEnable(GL_COLOR_LOGIC_OP);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glLogicOp(GL_AND);

  //draw smooth cone
  if(smoothCones1[n].size() != smoothCones2[n].size()){
   cout << "two sides do not match." << endl;
   exit(0);
  }
  int nspl = smoothCones1[n].size(); 
  for(int i = 0; i < nspl - 1; i++){
   Vector2d l1, l2, r1, r2;
   r1 = smoothCones1[n][i];
   r2 = smoothCones1[n][i+1];
   l1 = smoothCones2[n][i];
   l2 = smoothCones2[n][i+1];
   glBegin(GL_POLYGON);
   glVertex3f(r1.x, r1.y, 0.0);
   glVertex3f(r2.x, r2.y, 0.0);
   glVertex3f(l2.x, l2.y, 0.0);
   glVertex3f(l1.x, l1.y, 0.0);
   glEnd();
  }

  // Draw the circles
  Vector2d draw1, draw2;
  Vector2d neck = smoothCones2[n][smoothCones2[n].size()-1] - smoothCones1[n][smoothCones1[n].size()-1];
  draw2 = smoothCones1[n][smoothCones1[n].size()-1];
  //glColor4f(0.5, 0.5, 1.0, 0.5); 
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  glLogicOp(GL_AND);
  glBegin(GL_TRIANGLE_FAN);
  Vector2d tmp = translateToScreen(projPath[id]->x, projPath[id]->y);
  glVertex3f(tmp.x, tmp.y, 0);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 1.0){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id] * ration, curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   glVertex3f(draw1.x, draw1.y, 0.0);
  }
  glEnd();

  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_COLOR_LOGIC_OP);


 }

 void advisory::drawSmoothCone(){
  glColor4f(0.5, 0.5, 1.0, 0.5);
  glLineWidth(1.5);

  glEnable(GL_BLEND);
  glBlendColor(0.0, 0.0, 1.0, 0.5);
  glEnable(GL_COLOR_LOGIC_OP);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glLogicOp(GL_AND);

  ////right side
  //glBegin(GL_LINE_STRIP);
  //for(int i = 0; i < smoothCone1.size(); i++){
  // Vector2d tmp = smoothCone1[i];
  // glVertex3f(tmp.x, tmp.y, 0.0);
  //}
  //glEnd();

  ////left side
  //glBegin(GL_LINE_STRIP);
  //for(int i = 0; i < smoothCone2.size(); i++){
  // Vector2d tmp = smoothCone2[i];
  // glVertex3f(tmp.x, tmp.y, 0.0);
  //}
  //glEnd();

  //draw smooth cone
  if(smoothCone1.size() != smoothCone2.size()){
   cout << "two sides do not match." << endl;
   exit(0);
  }
  int nspl = smoothCone1.size(); 
  for(int i = 0; i < nspl - 1; i++){
   Vector2d l1, l2, r1, r2;
   r1 = smoothCone1[i];
   r2 = smoothCone1[i+1];
   l1 = smoothCone2[i];
   l2 = smoothCone2[i+1];
   glBegin(GL_POLYGON);
   glVertex3f(r1.x, r1.y, 0.0);
   glVertex3f(r2.x, r2.y, 0.0);
   glVertex3f(l2.x, l2.y, 0.0);
   glVertex3f(l1.x, l1.y, 0.0);
   glEnd();
  }

  // Draw the circles
  Vector2d draw1, draw2;
  Vector2d neck = smoothCone2[smoothCone2.size()-1] - smoothCone1[smoothCone1.size()-1];
  draw2 = smoothCone1[smoothCone1.size()-1];
  glColor4f(0.5, 0.5, 1.0, 0.5); 
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  glLogicOp(GL_AND);
  glBegin(GL_TRIANGLE_FAN);
  Vector2d tmp = translateToScreen(projPath[id]->x, projPath[id]->y);
  glVertex3f(tmp.x, tmp.y, 0);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 0.2){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id], curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   tmp = draw1 - draw2;
   Vector3d xy1(tmp.x, tmp.y, 0);
   Vector3d xy2(neck.x, neck.y, 0);
   Vector3d xyz = xy1 % xy2;
   if(xyz.z < 0){
    glVertex3f(draw1.x, draw1.y, 0.0);
   }
  }
  glEnd();



  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_COLOR_LOGIC_OP);

 }

 void advisory::drawTrueCone(){
  Vector2d draw1, draw2, draw3, draw4;
  std::vector<Vector2d*>::iterator _p1;
  std::vector<Vector2d*>::iterator _p2;
  std::vector<Vector2d*>::iterator _p3;
  std::vector<Vector2d*>::iterator _p4;


  glColor4f(0.5, 0.5, 1.0, 0.5);
  glLineWidth(1.5);

  //glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendColor(0.0, 0.0, 1.0, 0.5);
  glEnable(GL_COLOR_LOGIC_OP);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  _p3 = trueCone2.begin();
  for(_p1 = trueCone1.begin(); _p1 != trueCone1.end()-1; _p1++){
   _p2 = _p1 + 1;
   _p4 = _p3 + 1;
   draw1 = translateToScreen((*_p1)->x, (*_p1)->y);  
   draw2 = translateToScreen((*_p2)->x, (*_p2)->y);
   draw3 = translateToScreen((*_p3)->x, (*_p3)->y);  
   draw4 = translateToScreen((*_p4)->x, (*_p4)->y);
   glLogicOp(GL_AND);
   glBegin(GL_POLYGON);
   glVertex3f(draw1.x, draw1.y, 0.0);
   glVertex3f(draw2.x, draw2.y, 0.0);
   glVertex3f(draw4.x, draw4.y, 0.0);
   glVertex3f(draw3.x, draw3.y, 0.0);
   glEnd();

   //count++;
   _p3++;
  }


  //// Draw the circles
  //glColor4f(0.5, 0.5, 1.0, 0.5); 
  //double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double curDeg = 0;
  //for(unsigned int i = 1; i < projPath.size()-1; i++){
  //  glLogicOp(GL_AND);
  //  glBegin(GL_TRIANGLE_FAN);
  //  Vector2d tmp = translateToScreen(projPath[i]->x, projPath[i]->y);
  //  glVertex3f(tmp.x, tmp.y, 0);
  //  for(curDeg = 0; curDeg <= 360.0; curDeg++){
  //    draw1 = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRad[i], curDeg);
  //    draw1 = translateToScreen(draw1.x, draw1.y);
  //    glVertex3f(draw1.x, draw1.y, 0.0);
  //  }
  //  glEnd();
  //}

  // Draw the circles
  glColor4f(0.5, 0.5, 1.0, 0.5); 
  double errorRad[] = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  //double errorRad[] = {0.0, 34.0, 60.0, 86.0, 109.0, 144.0, 179.0};
  double curDeg = 0;
  int id = projPath.size() - 2;
  glLogicOp(GL_AND);
  glBegin(GL_TRIANGLE_FAN);
  Vector2d tmp = translateToScreen(projPath[id]->x, projPath[id]->y);
  glVertex3f(tmp.x, tmp.y, 0);
  for(curDeg = 0; curDeg <= 360.0; curDeg += 1){
   draw1 = locateDestination_2(projPath[id]->x, projPath[id]->y, errorRad[id], curDeg);
   draw1 = translateToScreen(draw1.x, draw1.y);
   glVertex3f(draw1.x, draw1.y, 0.0);
  }
  glEnd();

  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_COLOR_LOGIC_OP);

 }


 void advisory::findInConePercent(){
  inConePercent = 0;
  int size = pathList.size();
  int t = 0;

  if(size > 0){
   for(unsigned int i = 0; i < pathList.size(); i++){
    if(pathList[i]->getInCone() == true){
     t++;
    }
   }
   //cout << "Percentage of paths in cone: ";
   //cout << static_cast<double>(t)/static_cast<float>(size) << endl;
   //cout << "    t: " << t << "     total: " << pathList.size() << endl;
   inConePercent = static_cast<double>(t)/static_cast<float>(size);
   //cout << inConePercent << endl << flush;
  }
  updatePredictedPercent();
 }

 void advisory::updatePredictedPercent(){
  //cout << "inConePercent: " << inConePercent << "    baseConePercent: " << baseConePercent << endl;
  if(inConePercent < baseConePercent && usePredictedPercent < 0.9995){
   usePredictedPercent += 0.0005; 
  }
  else if(inConePercent > baseConePercent && usePredictedPercent > 0.0005){
   usePredictedPercent -= 0.0005; 
  }
  //cout << "   New use predicted percent: " << usePredictedPercent << endl;
 }

 void advisory::colorEncoding(Vector4d** posColor){
  posColor = new Vector4d*[pathList.size()];
  for(int i = 0; i < pathList.size() - 1; i++){
   posColor[i] = new Vector4d[pathList[i]->posList.size()];
   for(int j = 0; j < pathList[i]->posList.size() - 1; j++){
    int category = randCategory(4, 1);
    category = category < 1 ? category = 1 : category;
    category = category > 7 ? category = 7 : category;
    posColor[i][j] = categoryColor(category);
   }
  }
 }

 void advisory::drawCategories(double _x, double _y, double radius, int category, double opacity){
  glLineWidth(2.0);
  int angle;
  double angle_radians;
  double x, y, x1, y1;
  double opacity_end = opacity / 3.0;

  switch(category){
   case 1:
   case 2:
   case 3:
    for(int i = 0; i < category; i++){
     int t = 1;
     for(angle = 0; angle < 360; angle += 5){
      angle_radians = angle*(double)M_PI/180.0;
      x = _x + radius*(double)cos(angle_radians) * (1 - 0.333 * i);
      y = _y + radius*(double)sin(angle_radians) * (1 - 0.333 * i);
      if(angle == 0){
       x1 = x; 
       y1 = y;
      }
      glColor4f(1.0, 0, 0, opacity_end + ( 2 * opacity / 3.0) / 72.0 * (angle / 5.0) );
      glLineWidth((0.5 + 3.0 / 72.0 * (angle / 5.0)));
      glBegin(GL_LINES);
      glVertex3f(x,y,0.0); 
      glVertex3f(x1,y1,0.0); 
      glEnd();
      x1 = x; 
      y1 = y;
     }
    }
    break;

   case 4:
    for(int i = 0; i < 3; i++){
     for(angle = 0; angle < 360; angle += 5){
      angle_radians = angle*(double)M_PI/180.0;
      x = _x + radius*(double)cos(angle_radians) * (1 - 0.333 * i);
      y = _y + radius*(double)sin(angle_radians) * (1 - 0.333 * i);
      if(angle == 0){
       x1 = x; 
       y1 = y;
      }
      glColor4f(1.0, 0, 0, opacity_end + ( 2 * opacity / 3.0) / 72.0 * (angle / 5.0) );
      glLineWidth((0.5 + 3.0 / 72.0 * (angle / 5.0)));
      glBegin(GL_LINES);
      glVertex3f(x,y,0.0); 
      glVertex3f(x1,y1,0.0); 
      glEnd();
      x1 = x; 
      y1 = y;
     }

     glColor4f(1.0, 0.0, 0, opacity);
     glLineWidth(1.0);
     glBegin(GL_LINES);
     angle_radians = -45*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians);
     y = _y + radius*(double)sin(angle_radians);
     glVertex3f(x, y, 0.0);
     angle_radians = -225*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians);
     y = _y + radius*(double)sin(angle_radians);
     glVertex3f(x, y, 0.0);
     glEnd();

    }
    break;

   case 5:

    for(int i = 0; i < 3; i++){
     for(angle = 0; angle < 360; angle += 5){
      angle_radians = angle*(double)M_PI/180.0;
      x = _x + radius*(double)cos(angle_radians) * (1 - 0.333 * i);
      y = _y + radius*(double)sin(angle_radians) * (1 - 0.333 * i);
      if(angle == 0){
       x1 = x; 
       y1 = y;
      }
      glColor4f(1.0, 0.0, 0.0, opacity_end + ( 2 * opacity / 3.0) / 72.0 * (angle / 5.0) );
      glLineWidth((0.5 + 3.0 / 72.0 * (angle / 5.0)));
      glBegin(GL_LINES);
      glVertex3f(x,y,0.0); 
      glVertex3f(x1,y1,0.0); 
      glEnd();
      x1 = x; 
      y1 = y;
     }

     glColor4f(1.0, 0.0, 0, opacity);
     glLineWidth(1.0);
     glBegin(GL_LINES);
     angle_radians =  90*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians);
     y = _y + radius*(double)sin(angle_radians);
     glVertex3f(x, y, 0.0);
     angle_radians =  270*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians);
     y = _y + radius*(double)sin(angle_radians);
     glVertex3f(x, y, 0.0);
     glEnd();

     glBegin(GL_LINES);
     angle_radians =  0*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians);
     y = _y + radius*(double)sin(angle_radians);
     glVertex3f(x, y, 0.0);
     angle_radians =  180*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians);
     y = _y + radius*(double)sin(angle_radians);
     glVertex3f(x, y, 0.0);
     glEnd();

    }
    break;

   case 6:
   case 7:

    //for(int i = 0; i < 3; i++){
    for(angle = 0; angle < 360; angle += 5){
     angle_radians = angle*(double)M_PI/180.0;
     x = _x + radius*(double)cos(angle_radians) ;
     y = _y + radius*(double)sin(angle_radians) ;
     if(angle == 0){
      x1 = x; 
      y1 = y;
     }
     if(category == 6)
      glColor4f(1.0, 0.5, 0, opacity_end + ( 2 * opacity / 3.0) / 72.0 * (angle / 5.0) );
     else
      glColor4f(0.0, 0.0, 1, opacity_end + ( 2 * opacity / 3.0) / 72.0 * (angle / 5.0) );

     glLineWidth((0.5 + 3.0 / 72.0 * (angle / 5.0)));
     glBegin(GL_LINES);
     glVertex3f(x,y,0.0); 
     glVertex3f(x1,y1,0.0); 
     glEnd();
     x1 = x; 
     y1 = y;
    }
    //}



    break;

   default:
    break;

  }
 }

 void advisory::drawCircle(float _x, float _y, float radius){
  int angle;
  float x, y, angle_radians;
  float x1, y1;
  for(angle = 0; angle <= 360; angle += 5){
   angle_radians = angle*(double)M_PI/180.0;
   x = _x + radius*(double)cos(angle_radians);
   y = _y + radius*(double)sin(angle_radians);
   if(angle == 0){
    x1 = x; 
    y1 = y;
   }
   glBegin(GL_LINES);
   glVertex3f(x,y,0.0); 
   glVertex3f(x1,y1,0.0); 
   glEnd();
   x1 = x; 
   y1 = y;
  }
 }

 void advisory::drawSolidCircle(float _x, float _y, float radius){
  int angle;
  float x, y, angle_radians;
  float x1, y1;
  glBegin(GL_POLYGON);
  for(angle = 0; angle <= 360; angle += 5){
   angle_radians = angle*(double)M_PI/180.0;
   x = _x + radius*(double)cos(angle_radians);
   y = _y + radius*(double)sin(angle_radians);
   if(angle == 0){
    x1 = x; 
    y1 = y;
   }
   glVertex3f(x,y,0.0); 
   glVertex3f(x1,y1,0.0); 
   x1 = x; 
   y1 = y;
  }
  glEnd();
 }

 // code for percepture study 
 void advisory::drawExperimentPoints(){
  Vector2d loc1, loc2, l1, l2, r1, r2;
  float R1, R2;
  //findLocationBasedOnTime(time1, loc1, R1);
#ifdef TestOilRigs
  findLocationBasedOnTime(24, loc2, R2);
  findLocationBasedOnTime(48, loc2, R2);
#else
  findLocationBasedOnTime(SampledTimePoint, loc2, R2);
#endif
  //glColor4f(0, 0, 0.5, 1.0);
  //glLineWidth(2.0);
  //drawCircle(loc1.x, loc1.y, TimePointRadius);
  //drawCircle(loc2.x, loc2.y, TimePointRadius);
 }

 void advisory::findLocationBasedOnTime(float time, Vector2d& location, float& radius){
  if(time > 69 || time < 0){
   cerr << "time must be in [0, 69]" << endl; 
   exit(0);
  }

  float* timepoints = new float[6];
  timepoints[0] = 0;
  timepoints[1] = 12;
  timepoints[2] = 24;
  timepoints[3] = 36;
  timepoints[4] = 48;
  timepoints[5] = 60;

  std::vector<Vector2d*> locations;
  string str = InputPrefix + DatafileName + "_Mean.txt"; 
  cout << str << endl;
  ifstream infile(str);
  float tmp_time, tmp_lon, tmp_lat, tmp_error_rad;
  while(infile >> tmp_time >> tmp_lon >> tmp_lat >> tmp_error_rad && locations.size() <= 6)
  {
   locations.push_back(new Vector2d(tmp_lon, tmp_lat));
   //Vector2d tmp_loc = translateToScreen(tmp_lon, tmp_lat);
   //cout << tmp_loc << endl;
   //glColor4f(1, 0, 0, 1);
   //drawCircle(tmp_loc.x, tmp_loc.y, TimePointRadius * 2);
  }

  //locations.push_back(new Vector2d(-85.90, 26.10));
  //locations.push_back(new Vector2d(-87.50, 27.40));
  //locations.push_back(new Vector2d(-89.00, 28.60));
  //locations.push_back(new Vector2d(-90.00, 29.50));
  //locations.push_back(new Vector2d(-90.70, 30.30));
  //locations.push_back(new Vector2d(-91.25, 31.25));
  //locations.push_back(new Vector2d(-91.80, 32.20));


  // find interval
  int start, end;
  for(int i = 0; i < 6; i++){
   float delta = time - timepoints[i];
   if(delta < 0){
    start = i-1;
    end = i;
    break;
   }
  }

  //Vector4d* tmps = projPath[start]; 
  //Vector4d* tmpe = projPath[end];
  Vector2d* tmps = locations[start];
  Vector2d* tmpe = locations[end];
  Vector2d pstart(tmps->x, tmps->y);
  Vector2d pend(tmpe->x, tmpe->y);

  float t = (time - timepoints[start]) / (timepoints[end] - timepoints[start]); 
  Vector2d ptarget = (1 - t) * pstart + t * pend;
  location = translateToScreen(ptarget.x, ptarget.y);

  //double errorRad[] = {0, 34, 60, 86, 109, 144, 179};
  double errorRad[] = {0, 34, 60, 86, 108, 144, 179};
  for(int i = 0; i < 7; i++)
   errorRad[i] = CDFSample::Nmi2Km(errorRad[i]);
  //double errorRad[] = {50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
  radius = (1 - t) * errorRad[start - 1] + t * errorRad[end - 1];
  radius = errorRad[5]; // old experiment
  radius = errorRad[int(time) / 12];

  Vector2d tmpL1 = translateToScreen(ctrlp2[start]->x, ctrlp2[start]->y);
  Vector2d tmpL2 = translateToScreen(ctrlp2[end]->x, ctrlp2[end]->y);
  Vector2d tmpL = (1 - t) * tmpL1 + t * tmpL2;

  Vector2d tmpR1 = translateToScreen(ctrlp1[start]->x, ctrlp1[start]->y);
  Vector2d tmpR2 = translateToScreen(ctrlp1[end]->x, ctrlp1[end]->y);
  Vector2d tmpR = (1 - t) * tmpR1 + t * tmpR2;

  //Vector2d* startp = new Vector2d(projPath[start]->x, projPath[start]->y);
  //Vector2d* endp = new Vector2d(projPath[end]->x, projPath[end]->y);
  Vector2d* startp = new Vector2d(locations[start]->x, locations[start]->y);
  Vector2d* endp =   new Vector2d(locations[end]->x,   locations[end]->y);
  float bear = findBearing_2(startp, endp);
  float dL = bear - 90;
  float dR = bear + 90;
  tmpL = locateDestination_2(ptarget.x, ptarget.y, radius, dL); 
  tmpR = locateDestination_2(ptarget.x, ptarget.y, radius, dR); 

  tmpL = translateToScreen(tmpL.x, tmpL.y);
  tmpR = translateToScreen(tmpR.x, tmpR.y);

  Vector2d uL = tmpL - location;
  Vector2d uR = tmpR - location;
  float disL = uL.norm();
  float disR = uR.norm();
  uL = uL / disL;
  uR = uR / disR;

  glColor4f(0.0, 0.0, 1.0, 1.0);
  glLineWidth(2.0);
  Vector2d drawL, drawR;
 
#ifdef TestOilRigs
  vector<float> oilriglist = {-1.78, -1.1, -0.6, -0.2, 0.2, 0.6, 1.1, 1.78};
  for(const auto rig : oilriglist){
     drawL = location + uL * abs(rig) * disL;
     drawR = location + uR * abs(rig) * disR;
     Vector2d rotateCenter = translateToScreen(locations.at(0)->x, locations.at(0)->y);
     if(rig < 0){
      drawL = correctHurrianceLocation(drawL, DatafileName, rotateCenter);
      drawCircle(drawL.x, drawL.y, TimePointRadius);
     }
     else if(rig > 0){
      drawR = correctHurrianceLocation(drawR, DatafileName, rotateCenter);
      drawCircle(drawR.x, drawR.y, TimePointRadius);
     }
  }
#else
  drawL = location + uL * abs(OilRigLoc) * disL;
  drawR = location + uR * abs(OilRigLoc) * disR;
  Vector2d rotateCenter = translateToScreen(locations.at(0)->x, locations.at(0)->y);
  if(OilRigLoc < 0){
   drawL = correctHurrianceLocation(drawL, DatafileName, rotateCenter);
   drawCircle(drawL.x, drawL.y, TimePointRadius);
  }
  else if(OilRigLoc > 0){
   drawR = correctHurrianceLocation(drawR, DatafileName, rotateCenter);
   drawCircle(drawR.x, drawR.y, TimePointRadius);
  }
#endif
  
  //glColor4f(0, 1, 0, 1);
  //drawCircle(tmpL.x, tmpL.y, TimePointRadius);
  //drawCircle(tmpR.x, tmpR.y, TimePointRadius);

  //glColor4f(1.0, 0.0, 0.0, 1.0); 
  //glLineWidth(2.0);
  //glBegin(GL_LINES);
  //for(int curDeg = 0; curDeg < 360.0; curDeg++){
  // Vector2d draw1 = locateDestination_2(ptarget.x, ptarget.y, radius, curDeg);
  // draw1 = translateToScreen(draw1.x, draw1.y);
  // glVertex3f(draw1.x, draw1.y, 0.0);
  //}
  //glEnd();
 }

 void advisory::drawSplatHeatMapGrid(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh,
   int width, int height){

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  Vector2d draw1, draw2;
  float tempTrans;
  float age;

  Vector2d * curPos;
  float* gradiantValues = new float[hgrid.size()];
  for(int i = 0; i < hgrid.size(); i++)
   gradiantValues[i] = 0.0;


  int time;
  // for each path
  //cout << pathList.size() << endl;
  for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   int i = 0;
   i = min(userInt->getSliderCurTick(), (int)((*pathIt)->posList.size())-1);
   i = max(i, 0);
   time = i;

   // a particular time point on this path
   curPos = (*pathIt)->posList[i];

   int idxCol = curPos->x / dw;
   int idxRow = curPos->y / dh;
   int idx = idxRow * hw + idxCol;
   if(idx < hgrid.size())
    gradiantValues[idx] += 1; 

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));

  }

  for (pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }




  std::vector<double> tmpv;
  for(int i = 0; i < hgrid.size(); i++){
   if(gradiantValues[i] != 0.0)
    tmpv.push_back(gradiantValues[i]);
  }
  std::sort(tmpv.begin(), tmpv.end());
  int bandIdx = (1 - bandPercentage[2]) * tmpv.size();

  float* rankValues = new float[hgrid.size()];
  for(int i = 0; i < hgrid.size(); i++){
   float val = gradiantValues[i];
   //val = val / float(npaths/200); //hack here!
   //gradiantValues[i] = val;
   if(gradiantValues[i] != 0.0){
    int rank = std::find(tmpv.begin(), tmpv.end(), gradiantValues[i]) - tmpv.begin();
    val = float(rank * 1.0) / tmpv.size();
   }
   rankValues[i] = val;
  }

  KernelRadius = dh * 2;
  Splatting splatting(hgrid, pathList, time, rankValues, KernelRadius);
  splatting.copyTo(rankValues);

  for(int i = 0; i < hgrid.size(); i++){
   float val = rankValues[i];
   val = val * 1.2;
   float red, green, blue;
   ColorGradient colorgradien;
   colorgradien.getColorAtValue(val, red, green, blue);

   //drawHeatMap here
   glLineWidth(1.0);
   glColor4f(red, green, blue, val); 

   if(rankValues[i] != 0.0){
    Vector2d draw0 = hgrid[i][0];
    Vector2d draw1 = hgrid[i][1];
    Vector2d draw2 = hgrid[i][2];
    Vector2d draw3 = hgrid[i][3];
    glBegin(GL_POLYGON);
    //glBegin(GL_LINE_STRIP);
    glVertex3f(draw0.x, draw0.y, 0);
    glVertex3f(draw1.x, draw1.y, 0);
    glVertex3f(draw2.x, draw2.y, 0);
    glVertex3f(draw3.x, draw3.y, 0);
    glEnd();
    //Vector2d center = draw0 + (draw2 - draw0) / 2.0;
    //drawSolidCircle(center.x, center.y, (dw + dh) / 3);
   }
  }



  // draw iso-contour lines from heatmap grid
  //float isovalue = threshold / npaths; 
  float isovalue = tmpv[bandIdx]; 
  MarchingSquares marSquares(gradiantValues, hgrid, hh, hw);
  marSquares.buildBinaryGrid(isovalue);
  marSquares.buildCellGrid();

  //marSquares.lookupContourLines(isovalue);
  //marSquares.drawLines(isovalue);

  //glColor4f(0, 0, 1, 0.5);
  //marSquares.drawBinaryImage();

  // mathematical morphology for binary image
  //Matrix element(3, 3);
  //float *binary = new float[hgrid.size()];
  //marSquares.copyBinaryValues(binary);
  //MathMorphology morphology(binary, element, hh, hw, hgrid.size());
  ////morphology.setImage(gradiantValues, hgrid.size());

  //morphology.opening();
  //element = Matrix(2, 2);
  //morphology.Element() = element;
  //morphology.dilation();

  //marSquares.setBinaryValues(morphology.getSet());

  //glColor4f(1, 0, 0, 0.5);
  //marSquares.drawBinaryImage();

  //marSquares.buildCellGrid();

  //marSquares.lookupContourLines(isovalue);
  //marSquares.drawLines(isovalue);

  // mathematical morphology for image
  float *binary = new float[hgrid.size()];
  marSquares.copyValues(binary);
  Matrix element(3, 3);
  MathMorphology morphology(binary, element, hh, hw, hgrid.size());
  if(morph == 1){
   morphology.closingImage();
   morphology.openingImage();
  }
  //morphology.erosionImage();
  marSquares.setValues(morphology.getSet());
  marSquares.buildBinaryGrid(isovalue);
  marSquares.buildCellGrid();
  marSquares.lookupContourLines(isovalue);
  marSquares.drawLines(isovalue);
 }




 void advisory::drawHeatMapGrid(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh,
   int width, int height){

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  Vector2d draw1, draw2;
  float tempTrans;
  float age;

  Vector2d * curPos;
  float* gradiantValues = new float[hgrid.size()];
  for(int i = 0; i < hgrid.size(); i++)
   gradiantValues[i] = 0.0;


  // for each path
  cout << pathList.size() << endl;
  for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   int i = 0;
   i = min(userInt->getSliderCurTick(), (int)((*pathIt)->posList.size())-1);
   i = max(i, 0);

   // a particular time point on this path
   curPos = (*pathIt)->posList[i];

   int idxCol = curPos->x / dw;
   int idxRow = curPos->y / dh;
   int idx = idxRow * hw + idxCol;
   if(idx < hgrid.size())
    gradiantValues[idx] += 1; 

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));

  }

  for (pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  std::vector<double> tmpv;
  for(int i = 0; i < hgrid.size(); i++){
   if(gradiantValues[i] != 0.0)
    tmpv.push_back(gradiantValues[i]);
  }
  std::sort(tmpv.begin(), tmpv.end());
  int bandIdx = (1 - bandPercentage[2]) * tmpv.size();

  for(int i = 0; i < hgrid.size(); i++){
   float val = gradiantValues[i];
   //val = val / float(npaths/200); //hack here!
   //gradiantValues[i] = val;
   if(gradiantValues[i] != 0.0){
    int rank = std::find(tmpv.begin(), tmpv.end(), gradiantValues[i]) - tmpv.begin();
    val = float(rank * 1.0) / tmpv.size();
   }
   float red, green, blue;
   ColorGradient colorgradien;
   colorgradien.getColorAtValue(val, red, green, blue);

   //drawHeatMap here
   glLineWidth(1.0);
   glColor4f(red, green, blue, 0.6);

   if(gradiantValues[i] != 0.0){
    Vector2d draw0 = hgrid[i][0];
    Vector2d draw1 = hgrid[i][1];
    Vector2d draw2 = hgrid[i][2];
    Vector2d draw3 = hgrid[i][3];
    glBegin(GL_POLYGON);
    //glBegin(GL_LINE_STRIP);
    glVertex3f(draw0.x, draw0.y, 0);
    glVertex3f(draw1.x, draw1.y, 0);
    glVertex3f(draw2.x, draw2.y, 0);
    glVertex3f(draw3.x, draw3.y, 0);
    glEnd();
    //Vector2d center = draw0 + (draw2 - draw0) / 2.0;
    //drawSolidCircle(center.x, center.y, (dw + dh) / 3);
   }
  }



  // draw iso-contour lines from heatmap grid
  //float isovalue = threshold / npaths; 
  float isovalue = tmpv[bandIdx]; 
  MarchingSquares marSquares(gradiantValues, hgrid, hh, hw);
  marSquares.buildBinaryGrid(isovalue);
  marSquares.buildCellGrid();

  //marSquares.lookupContourLines(isovalue);
  //marSquares.drawLines(isovalue);

  //glColor4f(0, 0, 1, 0.5);
  //marSquares.drawBinaryImage();

  // mathematical morphology for binary image
  //Matrix element(3, 3);
  //float *binary = new float[hgrid.size()];
  //marSquares.copyBinaryValues(binary);
  //MathMorphology morphology(binary, element, hh, hw, hgrid.size());
  ////morphology.setImage(gradiantValues, hgrid.size());

  //morphology.opening();
  //element = Matrix(2, 2);
  //morphology.Element() = element;
  //morphology.dilation();

  //marSquares.setBinaryValues(morphology.getSet());

  //glColor4f(1, 0, 0, 0.5);
  //marSquares.drawBinaryImage();

  //marSquares.buildCellGrid();

  //marSquares.lookupContourLines(isovalue);
  //marSquares.drawLines(isovalue);

  // mathematical morphology for image
  float *binary = new float[hgrid.size()];
  marSquares.copyValues(binary);
  Matrix element(3, 3);
  MathMorphology morphology(binary, element, hh, hw, hgrid.size());
  if(morph == 1){
   morphology.closingImage();
   morphology.openingImage();
  }
  //morphology.erosionImage();
  marSquares.setValues(morphology.getSet());
  marSquares.buildBinaryGrid(isovalue);
  marSquares.buildCellGrid();
  marSquares.lookupContourLines(isovalue);
  marSquares.drawLines(isovalue);
 }

 // draw heat map based on data depth values
 void advisory::drawHeatMapGridDataDepth(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh,
   int width, int height){

  PathCBD pathCBD(pathList, M);
  int time = 0;
  time = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
  time = max(time, 0);
  std::vector<double> tmp;
  tmp.resize(npaths, 0.0);
  if(time != 0){
   tmp = pathCBD.fastSegmentSimplexDepth(time);
   segSortedIdx = pathCBD.sortSegmentIdx(tmp);
  }
  else{
   segSortedIdx.resize(pathList.size(), 0);
  }

  double sum = 0.0;
  sum = (0.0 + double(pathList.size()) - 1) * double(pathList.size() ) / 2.0;
  sum = sum / pathList.size();

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  Vector2d draw1, draw2;
  float tempTrans;
  float age;

  Vector2d * curPos;
  float* gradiantValues = new float[hgrid.size()];
  for(int j = 0; j < hgrid.size(); j++)
   gradiantValues[j] = 0.0;


  // for each path
  int pathIdx = -1;
  cout << pathList.size() << endl;
  for (pathIt = pathList.begin(), pathIdx = 0; pathIt != pathList.end(); pathIt++, pathIdx ++){

   // a particular time point on this path
   curPos = (*pathIt)->posList[time];

   int idxCol = curPos->x / dw;
   int idxRow = curPos->y / dh;
   int idx = idxRow * hw + idxCol;
   if(idx < hgrid.size()){
    //int pos = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx) - segSortedIdx.begin();
    //int pos = 0;
    //for(int k = 0; k < SortedIdx.size(); k++){
    // if(pathIdx == SortedIdx[k])
    //  pos = k;
    //}
    //cout << pathIdx << " " << pos << endl;
    //double rankper =  (double(pathList.size() * 1.0) - double(pos * 1.0)) / sum; 
    //gradiantValues[idx] += rankper; 
    gradiantValues[idx] += tmp[pathIdx]; 
   }

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
  }

  for (pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  std::vector<double> temp;
  temp.resize(hgrid.size(), 0.0);
  int NNoneZero = 0;
  for(int i = 0; i < temp.size(); i++){
   temp[i] = gradiantValues[i];
   if(gradiantValues[i] != 0.0)
    NNoneZero ++;
  }
  std::vector<int> newSort;
  newSort = pathCBD.sortSegmentIdx(temp);

  sum = 0;
  for(int i = 0; i < NNoneZero; i++)
   sum += i;

  for(int i = 0; i < hgrid.size(); i++)
   gradiantValues[i] = 0.0;

  for(int i = 0; i < NNoneZero; i++){
   int pos = newSort[i];
   //cout << i << " " << pos << " " << temp[newSort[i]] << endl;
   gradiantValues[pos] = (double(NNoneZero * 1.0) - double(i * 1.0)) / NNoneZero;
  }




  for(int i = 0; i < hgrid.size(); i++){
   float val = gradiantValues[i];
   //val = val / float(npaths/200); //hack here!
   gradiantValues[i] = val;
   float red, green, blue;
   ColorGradient colorgradien;
   colorgradien.getColorAtValue(val, red, green, blue);

   //drawHeatMap here
   glLineWidth(1.0);
   glColor4f(red, green, blue, 0.6);

   if(gradiantValues[i] != 0.0){
    Vector2d draw0 = hgrid[i][0];
    Vector2d draw1 = hgrid[i][1];
    Vector2d draw2 = hgrid[i][2];
    Vector2d draw3 = hgrid[i][3];
    glBegin(GL_POLYGON);
    //glBegin(GL_LINE_STRIP);
    glVertex3f(draw0.x, draw0.y, 0);
    glVertex3f(draw1.x, draw1.y, 0);
    glVertex3f(draw2.x, draw2.y, 0);
    glVertex3f(draw3.x, draw3.y, 0);
    glEnd();
    //Vector2d center = draw0 + (draw2 - draw0) / 2.0;
    //drawSolidCircle(center.x, center.y, (dw + dh) / 3);
   }
  }

  // draw iso-contour lines from heatmap grid
  float isovalue = threshold / npaths; 
  int upperband = bandPercentage[2] * NNoneZero;
  isovalue = gradiantValues[newSort[upperband]];
  MarchingSquares marSquares(gradiantValues, hgrid, hh, hw);
  marSquares.buildBinaryGrid(isovalue);
  marSquares.buildCellGrid();

  //marSquares.lookupContourLines(isovalue);
  //marSquares.drawLines(isovalue);

  //glColor4f(0, 0, 1, 0.5);
  //marSquares.drawBinaryImage();

  // mathematical morphology for binary image
  //Matrix element(3, 3);
  //float *binary = new float[hgrid.size()];
  //marSquares.copyBinaryValues(binary);
  //MathMorphology morphology(binary, element, hh, hw, hgrid.size());
  ////morphology.setImage(gradiantValues, hgrid.size());

  //morphology.opening();
  //element = Matrix(2, 2);
  //morphology.Element() = element;
  //morphology.dilation();

  //marSquares.setBinaryValues(morphology.getSet());

  //glColor4f(1, 0, 0, 0.5);
  //marSquares.drawBinaryImage();

  //marSquares.buildCellGrid();

  //marSquares.lookupContourLines(isovalue);
  //marSquares.drawLines(isovalue);

  // mathematical morphology for image
  float *binary = new float[hgrid.size()];
  marSquares.copyValues(binary);
  Matrix element(3, 3);
  MathMorphology morphology(binary, element, hh, hw, hgrid.size());
  if(morph == 1){
   morphology.closingImage();
   morphology.openingImage();
  }
  //morphology.erosionImage();
  marSquares.setValues(morphology.getSet());
  marSquares.buildBinaryGrid(isovalue);
  marSquares.buildCellGrid();
  marSquares.lookupContourLines(isovalue);
  marSquares.drawLines(isovalue);

  int mean = newSort[0];
  marSquares.drawSingleGrid(mean);
 }




 template <typename Iterator>
  bool advisory::next_combination(const Iterator first, const Iterator k, const Iterator last){
   if ((first == last) || (first == k) || (last == k))
    return false;
   Iterator itr1 = first;
   Iterator itr2 = last;
   ++itr1;
   if (last == itr1)
    return false;
   itr1 = last;
   --itr1;
   itr1 = k;
   --itr2;
   while (first != itr1){
    if (*--itr1 < *itr2){
     Iterator j = k;
     while (!(*itr1 < *j)) ++j;
     std::iter_swap(itr1,j);
     ++itr1;
     ++j;
     itr2 = k;
     std::rotate(itr1,j,last);
     while (last != j){
      ++j;
      ++itr2;
     }
     std::rotate(k,itr2,last);
     return true;
    }
   }
   std::rotate(first,k,last);
   return false;
  }

 Matrix advisory::getCombinationMatrix( unsigned int n,  unsigned int k){
  Matrix rtn;
  std::vector<int> ints;
  for(int i = 1; i <= n; ints.push_back(i++));

  unsigned long ncombination = Choose(n, k);

  rtn = Matrix(k, ncombination);

  int counter = 0;
  do{
   for(int i = 0; i < k; i++){
    rtn[i][counter] = ints[i];
   }
   counter ++;
  }while(next_combination(ints.begin(), ints.begin() + k, ints.end()));

  //cout << rtn << endl;

  return rtn;
 }

 void advisory::drawDataDepthPlot(){
  if(pathList.size() < 4){
   drawGenPaths();
   return;
  }

  cout << pathList.size() << endl;
  PathCBD pathCBD(pathList, M);
  pathCBD.fastPathSimplexDepth();
  SortedIdx = pathCBD.getIdx();
  std::vector<double> tmp = pathCBD.getProbs();

  std::vector<Vector2d*>::iterator pos1;
  std::vector<Vector2d*>::iterator pos2;
  std::vector<path*>::iterator pathIt;
  Vector2d draw1, draw2;
  float lineWidth;
  float tempTrans;

  Points band;
  band.clear();
  std::vector<Vector2d> mean;

  int pathIdx = -1;
  for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   pathIdx ++;
   pos2 = (*pathIt)->posList.begin() + 1;
   lineWidth = 0.8;
   glColor4f((*pathIt)->getInt().x, (*pathIt)->getInt().y, (*pathIt)->getInt().z, (*pathIt)->getInt().w);
   tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
   for (pos1 = (*pathIt)->posList.begin(); pos2 != (*pathIt)->posList.end(); pos1++) {
    glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);


    draw1.x = (*pos1)->x;
    draw1.y = (*pos1)->y;
    draw2.x = (*pos2)->x;
    draw2.y = (*pos2)->y;
    glLineWidth(lineWidth * 1.0);

    // mean
    if(SortedIdx.size() > 0 && SortedIdx[0] == pathIdx){
     glLineWidth(lineWidth * 3.0);
     glColor4f(1, 1, 0, 1);
     mean.push_back(draw1);
     mean.push_back(draw2);
    }
    else if(SortedIdx.size() > 0){ 
     // bands
     for(int k = 0; k < SortedIdx.size() * bandPercentage[2]; k++){
      if(SortedIdx[k] == pathIdx){
       band.push_back(Point_2(draw1.x, draw1.y));
       band.push_back(Point_2(draw2.x, draw2.y));
      }
     }
    }
    else{
     glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);
    }

    int posIdx = std::find(SortedIdx.begin(), SortedIdx.end(), pathIdx) - SortedIdx.begin();
    //if(tmp[posIdx] - 10e-8 <= 0.0)
    // glColor4f(1, 0, 0, 1);
    //if(posIdx > 0.98 * SortedIdx.size())
    // glColor4f(1, 0, 0, 1);
    if(posIdx > SortedIdx.size()- 5)
     glColor4f(1, 0, 0, 1);

    glBegin(GL_LINES);
    glVertex3f(draw1.x, draw1.y, 0.0);
    glVertex3f(draw2.x, draw2.y, 0.0);
    glEnd();
    pos2++;
    //lineWidth = lineWidth + 0.5;
    tempTrans = tempTrans - fadeConst;
   }

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
  }
  for (pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  Points convexhull = getConvexHull(band);
  ColorGradient colorgradient;
  float value = 1.0 - bandPercentage[2];
  float red, green, blue;
  colorgradient.getColorAtValue(value, red, green, blue);
  glColor4f(red, green, blue, 1);
  //glColor4f(1, 0, 0, 1);
  glLineWidth(lineWidth * 3);
  glBegin(GL_LINE_LOOP);
  for(int i = 0; i < convexhull.size(); i++){
   glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
  }
  glEnd();

  glColor4f(1, 1, 0, 1);
  glBegin(GL_LINE_STRIP);
  for(int i = 0; i < mean.size(); i++)
   glVertex3f(mean[i].x, mean[i].y, 0.0);
  glEnd();
 }

 /*
    void advisory::drawDataDepthPlotofTimeSplat(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height){
//if(pathList.size() < 4){
// drawGenPaths();
// //return;
//}

float red, green, blue;
ColorGradient colorgradient;

std::vector<Vector2d*>::iterator pos;
std::vector<path*>::iterator pathIt;
float tempTrans;
Vector2d meanPoint;

Points band;
band.clear();
Polygon_2 poly;
poly.clear();

Vector2d * curPos;

PathCBD pathCBD(pathList, M);
int i = 0;
i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
i = max(i, 0);
std::vector<double> tmp;
if(i != 0){
tmp = pathCBD.fastSegmentSimplexDepth(i);
segSortedIdx = pathCBD.sortSegmentIdx(tmp);
}

// for each path
int pathIdx = -1;
cout << pathList.size() << endl;
std::vector<float> colorValues;
colorValues.resize(width * height, 0.0);

for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
pathIdx ++;
tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

// a particular time point on this path
curPos = (*pathIt)->posList[i];

if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
meanPoint = Vector2d(curPos->x, curPos->y);

if(i != 0){
std::vector<int>::iterator iit;
iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
int posIdx = iit - segSortedIdx.begin();
//double dval = tmp[segSortedIdx[posIdx]];
double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
colorgradient.getColorAtValue(value, red, green, blue);
glColor4f(red, green, blue, tempTrans);

if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0)
colorValues[width * int(curPos->y) + int(curPos->x)] = value;

if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end())
band.push_back(Point_2(curPos->x, curPos->y));
}

//drawSolidCircle(curPos->x, curPos->y, TimePointRadius*1.5);

(*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
}

for(pathIt = pathList.begin(); pathIt != pathList.end();){
if ((*pathIt)->getInt().w <= 0.0) {
 delete((*pathIt));
 pathIt = pathList.erase(pathIt);
}
else{
 pathIt++;
}
}

if(i != 0){
 //draw mean point
 //glColor4f(1, 0, 0, 1);
 //drawCircle(meanPoint.x, meanPoint.y, TimePointRadius * 2);

 Points convexhull = getConvexHull(band);
 float value = 1.0 - bandPercentage[1];
 colorgradient.getColorAtValue(value, red, green, blue);
 glColor4f(red, green, blue, 1);
 glColor4f(1, 0, 0, 1);
 glBegin(GL_LINE_LOOP);
 for(int i = 0; i < convexhull.size(); i++){
  glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
 }
 glEnd();
}

int npro = omp_get_max_threads();
omp_set_num_threads(npro);

//#pragma omp parallel for
for(int j = 0; j < width; j++){
 for(int k = 0; k < height; k++){
  Vector2d C(j, k);
  int cidx = width * k + j;
  double sn = 0.0;
  double alpha = 0.0;
  //for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
  for(int tt = 0; tt < pathList.size(); tt++){
   curPos = (pathList[tt]->posList[i]);
   //curPos = (*pathIt)->posList[i];
   Vector2d P(curPos->x, curPos->y);
   int idx = width * int(curPos->y) + int(curPos->x);
   double dis = (P - C).norm();
   if(KernelRadius > dis && P.x < width && P.y < height && P.x > 0 && P.y > 0){
    double d = colorValues[idx];
    double std = KernelRadius / 3.0;
    double part1 = pow(E, -pow(dis, 2) / (2 * pow(std, 2)));
    double part2 = cos(PI/2.0 * dis / (3 * std));
    double w = part1 * part2;
    if(w < 10e-6)
     w = 0.0;
    sn    += w * d;
    alpha += w;
   }
  }
  colorValues[cidx] = sn / alpha;
  if(alpha < 10e-6 || sn < 10e-6)
   colorValues[cidx] = 0.0;
  if(colorValues[cidx] < 10e-6)
   colorValues[cidx] = 0.0;
 }
 }

 for(int j = 0; j < width; j++){
  for(int k = 0; k < height; k++){
   int cidx = width * k + j;
   if(colorValues[cidx] > 10e-6){
    colorgradient.getColorAtValue(colorValues[cidx], red, green, blue);
    glColor4f(red, green, blue, colorValues[cidx]);
    //drawSolidCircle(j, k, KernelRadius);
    glBegin(GL_POINTS);
    glVertex3f(j, k, 0.0);
    glEnd();
   }
  }
 }
}
*/

/*
   void advisory::drawDataDepthPlotofTimeSplat(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height){
//if(pathList.size() < 4){
// drawGenPaths();
// //return;
//}

float red, green, blue;
ColorGradient colorgradient;
cout << "Radius: " << KernelRadius << endl;

std::vector<Vector2d*>::iterator pos;
std::vector<path*>::iterator pathIt;
float tempTrans;
Vector2d meanPoint;

Points band;
band.clear();
Polygon_2 poly;
poly.clear();
Points outlier;
outlier.clear();

double bandval = 0.0;

Vector2d * curPos;

PathCBD pathCBD(pathList, M);
int i = 0;
i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
i = max(i, 0);
std::vector<double> tmp;
if(i != 0){
tmp = pathCBD.fastSegmentSimplexDepth(i);
segSortedIdx = pathCBD.sortSegmentIdx(tmp);
}

// for each path
int pathIdx = -1;
cout << pathList.size() << endl;
std::vector<float> colorValues;
//colorValues.resize(width * height, 0.0);
colorValues.resize(hgrid.size(), 0.0);

for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
pathIdx ++;
tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

// a particular time point on this path
curPos = (*pathIt)->posList[i];

if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
meanPoint = Vector2d(curPos->x, curPos->y);

if(i != 0){
std::vector<int>::iterator iit;
iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
int posIdx = iit - segSortedIdx.begin();
//double dval = tmp[segSortedIdx[posIdx]];
double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
colorgradient.getColorAtValue(value, red, green, blue);
glColor4f(red, green, blue, tempTrans);

if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
int idxCol = curPos->x / dw;
int idxRow = curPos->y / dh;
int idx = idxRow * hw + idxCol;
if(idx < hgrid.size())
colorValues[idx] = value;
//colorValues[width * int(curPos->y) + int(curPos->x)] = value;
}

if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end()){
 band.push_back(Point_2(curPos->x, curPos->y));
}

if(posIdx < segSortedIdx.size() * 0.98 && iit != segSortedIdx.end())
 outlier.push_back(Point_2(curPos->x, curPos->y));
 }

//drawSolidCircle(curPos->x, curPos->y, TimePointRadius*1.5);

(*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
}

for(pathIt = pathList.begin(); pathIt != pathList.end();){
 if ((*pathIt)->getInt().w <= 0.0) {
  delete((*pathIt));
  pathIt = pathList.erase(pathIt);
 }
 else{
  pathIt++;
 }
}

if(i != 0){
 //draw mean point
 //glColor4f(1, 0, 0, 1);
 //drawCircle(meanPoint.x, meanPoint.y, TimePointRadius * 2);

 Points convexhull = getConvexHull(band);
 float value = 1.0 - bandPercentage[1];
 colorgradient.getColorAtValue(value, red, green, blue);
 glColor4f(red, green, blue, 1);
 glColor4f(1, 0, 0, 1);
 glBegin(GL_LINE_LOOP);
 for(int i = 0; i < convexhull.size(); i++){
  glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
 }
 glEnd();


}

int npro = omp_get_max_threads();
omp_set_num_threads(npro);

//#pragma omp parallel for
for(int j = 0; j < hgrid.size(); j++){
 Vector2d vertex[2];
 vertex[0] = hgrid[j][0];
 vertex[1] = hgrid[j][2];
 Vector2d C = vertex[0] + (vertex[1] - vertex[0]) / 2.0;
 double sn = 0.0;
 double alpha = 0.0;
 //for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
 for(int tt = 0; tt < pathList.size(); tt++){
  curPos = (pathList[tt]->posList[i]);
  //curPos = (*pathIt)->posList[i];
  Vector2d P(curPos->x, curPos->y);
  //int idx = width * int(curPos->y) + int(curPos->x);
  double dis = (P - C).norm();
  if(KernelRadius > dis && P.x < width && P.y < height && P.x > 0 && P.y > 0){
   int idxCol = curPos->x / dw;
   int idxRow = curPos->y / dh;
   int idx = idxRow * hw + idxCol;
   double d = colorValues[idx];
   double std = KernelRadius / 3.0;
   double part1 = pow(E, -pow(dis, 2) / (2 * pow(std, 2)));
   double part2 = cos(PI/2.0 * dis / (3 * std));
   double w = part1 * part2;
   if(w < 10e-6)
    w = 0.0;
   sn    += w * d;
   alpha += w;
  }
 }
 colorValues[j] = sn / alpha;
 if(alpha < 10e-6 || sn < 10e-6)
  colorValues[j] = 0.0;
 if(colorValues[j] < 10e-6)
  colorValues[j] = 0.0;
}

std::vector<double> sortedValues;
for(int j = 0; j < hgrid.size(); j++){
 if(colorValues[j] > 10e-6)
  sortedValues.push_back(colorValues[j]);
}
std::sort(sortedValues.begin(), sortedValues.end());


//draw outlier
Points convexhull = getConvexHull(band);
float value = sortedValues[sortedValues.size() * 0.1]; 
colorgradient.getColorAtValue(value, red, green, blue);
glColor4f(red, green, blue, value);
glBegin(GL_POLYGON);
for(int i = 0; i < convexhull.size(); i++){
 glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
}
glEnd();

//int idx50 = SortedIdx[SortedIdx.size() * 0.5];
//float dp50 = tmp[idx50];
//cout << idx50 << " " << dp50 << endl;


std::vector<Vector3d> edges;
std::vector<Vector3d> vertices;
edges.clear();
vertices.clear();
for(int j = 1; j < convexhull.size(); j++){
 Vector3d edge = Vector3d(convexhull[j][0], convexhull[j][1], 0.0) - Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0);
 edges.push_back(edge);
 vertices.push_back(Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0));
}
Vector3d edge = Vector3d(convexhull[0][0], convexhull[0][1], 0.0) - Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0);
edges.push_back(edge);
vertices.push_back(Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0));

band.clear();
for(int j = 0; j < hgrid.size(); j++){
 if(colorValues[j] > 10e-6){
  colorgradient.getColorAtValue(colorValues[j], red, green, blue);
  glColor4f(red, green, blue, colorValues[j] * 0.5);
  //glColor4f(1, 0, 0, colorValues[j]);
  //drawSolidCircle(j, k, KernelRadius);
  Vector2d corners[4];
  corners[0] = hgrid[j][0];
  corners[1] = hgrid[j][1];
  corners[2] = hgrid[j][2];
  corners[3] = hgrid[j][3];

  Vector2d c = corners[0] + (corners[2] - corners[1]) / 2.0;
  Vector3d C(c.x, c.y, 0.0);

  int inside = 0;
  for(int k = 0; k < edges.size(); k++){
   Vector3d N = C - vertices[k];
   N = N % edges[k];
   if(N.z > 0){
    inside = 1;
    break;
   }
  }

  //if(colorValues[j] > bandPercentage[1])
  if(inside == 0)
  {
   glColor4f(1, 0, 0, colorValues[j]);
   band.push_back(Point_2(corners[0].x, corners[0].y));
   band.push_back(Point_2(corners[1].x, corners[1].y));
   band.push_back(Point_2(corners[2].x, corners[2].y));
   band.push_back(Point_2(corners[3].x, corners[3].y));
  }

  glBegin(GL_POLYGON);
  glVertex3f(corners[0].x, corners[0].y, 0.0);
  glVertex3f(corners[1].x, corners[1].y, 0.0);
  glVertex3f(corners[2].x, corners[2].y, 0.0);
  glVertex3f(corners[3].x, corners[3].y, 0.0);
  glEnd();
 }
}

//convexhull = getConvexHull(band);
//glColor4f(0, 0, 0, bandPercentage[1]);
//glBegin(GL_POLYGON);
//for(int i = 0; i < convexhull.size(); i++){
// glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
//}
//glEnd();
}
*/

void advisory::drawDataDepthPlotofTimeSplat(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, int width, int height){
 //if(pathList.size() < 4){
 // drawGenPaths();
 // //return;
 //}

 float red, green, blue;
 ColorGradient colorgradient;
 cout << "Radius: " << KernelRadius << endl;

 std::vector<Vector2d*>::iterator pos;
 std::vector<path*>::iterator pathIt;
 float tempTrans;
 Vector2d meanPoint;

 Points band;
 band.clear();
 Polygon_2 poly;
 poly.clear();
 Points outlier;
 outlier.clear();

 std::vector<Points> bands;


 double bandval = 0.0;

 Vector2d * curPos;

 PathCBD pathCBD(pathList, M);
 int i = 0;
 i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
 i = max(i, 0);
 std::vector<double> tmp;
 if(i != 0){
  tmp = pathCBD.fastSegmentSimplexDepth(i);
  segSortedIdx = pathCBD.sortSegmentIdx(tmp);
 }

 // for each path
 int pathIdx = -1;
 cout << pathList.size() << endl;
 std::vector<float> colorValues;
 //colorValues.resize(width * height, 0.0);
 colorValues.resize(hgrid.size(), 0.0);

 for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
  pathIdx ++;
  tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
  glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

  // a particular time point on this path
  curPos = (*pathIt)->posList[i];

  if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
   meanPoint = Vector2d(curPos->x, curPos->y);

  if(i != 0){
   std::vector<int>::iterator iit;
   iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
   int posIdx = iit - segSortedIdx.begin();
   //double dval = tmp[segSortedIdx[posIdx]];
   double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
   colorgradient.getColorAtValue(value, red, green, blue);
   glColor4f(red, green, blue, tempTrans);

   if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
    int idxCol = curPos->x / dw;
    int idxRow = curPos->y / dh;
    int idx = idxRow * hw + idxCol;
    if(idx < hgrid.size())
     colorValues[idx] = value;
    //colorValues[width * int(curPos->y) + int(curPos->x)] = value;
   }

   if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end()){
    band.push_back(Point_2(curPos->x, curPos->y));
    bands[0].push_back(Point_2(curPos->x, curPos->y));
   }
   if(posIdx > segSortedIdx.size() * bandPercentage[1] &&
     posIdx < segSortedIdx.size() * bandPercentage[2] && iit != segSortedIdx.end()){
    bands[1].push_back(Point_2(curPos->x, curPos->y));
   }
   if(posIdx > segSortedIdx.size() * bandPercentage[2] &&
     posIdx < segSortedIdx.size() * bandPercentage[3] && iit != segSortedIdx.end()){
    bands[2].push_back(Point_2(curPos->x, curPos->y));
   }

   if(posIdx < segSortedIdx.size() * bandPercentage[4] && iit != segSortedIdx.end())
    outlier.push_back(Point_2(curPos->x, curPos->y));
  }

  //drawSolidCircle(curPos->x, curPos->y, TimePointRadius*1.5);

  (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
 }

 for(pathIt = pathList.begin(); pathIt != pathList.end();){
  if ((*pathIt)->getInt().w <= 0.0) {
   delete((*pathIt));
   pathIt = pathList.erase(pathIt);
  }
  else{
   pathIt++;
  }
 }

 // draw depth bands
 std::vector<Points> convexhulls;
 convexhulls.resize(bands.size());
 for(int j = bands.size()-1; j >= 0; j--){
  convexhulls[j] = getConvexHull(bands[j]);
  glColor4f(bandColors[j].x, bandColors[j].y, bandColors[j].z, 0.1);
  glBegin(GL_LINE_LOOP);
  //glBegin(GL_POLYGON);
  for(int k = 0; k < convexhulls[j].size(); k++){
   glVertex3f(convexhulls[j][k][0], convexhulls[j][k][1], 0.0);
  }
  glEnd();
 }

 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);

 //#pragma omp parallel for
 for(int j = 0; j < hgrid.size(); j++){
  Vector2d vertex[2];
  vertex[0] = hgrid[j][0];
  vertex[1] = hgrid[j][2];
  Vector2d C = vertex[0] + (vertex[1] - vertex[0]) / 2.0;
  double sn = 0.0;
  double alpha = 0.0;
  //for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
  for(int tt = 0; tt < pathList.size(); tt++){
   curPos = (pathList[tt]->posList[i]);
   //curPos = (*pathIt)->posList[i];
   Vector2d P(curPos->x, curPos->y);
   //int idx = width * int(curPos->y) + int(curPos->x);
   double dis = (P - C).norm();
   if(KernelRadius > dis && P.x < width && P.y < height && P.x > 0 && P.y > 0){
    int idxCol = curPos->x / dw;
    int idxRow = curPos->y / dh;
    int idx = idxRow * hw + idxCol;
    double d = colorValues[idx];
    double std = KernelRadius / 3.0;
    double part1 = pow(E, -pow(dis, 2) / (2 * pow(std, 2)));
    double part2 = cos(PI/2.0 * dis / (3 * std));
    double w = part1 * part2;
    if(w < 10e-6)
     w = 0.0;
    sn    += w * d;
    alpha += w;
   }
  }
  colorValues[j] = sn / alpha;
  if(alpha < 10e-6 || sn < 10e-6)
   colorValues[j] = 0.0;
  if(colorValues[j] < 10e-6)
   colorValues[j] = 0.0;
 }

 std::vector<double> sortedValues;
 for(int j = 0; j < hgrid.size(); j++){
  if(colorValues[j] > 10e-6)
   sortedValues.push_back(colorValues[j]);
 }
 std::sort(sortedValues.begin(), sortedValues.end());

 std::vector<Edges>    cedges;
 std::vector<Vertices> cvertices;

 std::vector<Vector3d> edges;
 std::vector<Vector3d> vertices;
 for(int k = 0; k < convexhulls.size(); k++){
  edges.clear();
  vertices.clear();
  Points convexhull = convexhulls[k];
  for(int j = 1; j < convexhull.size(); j++){
   Vector3d edge = Vector3d(convexhull[j][0], convexhull[j][1], 0.0) - Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0);
   edges.push_back(edge);
   vertices.push_back(Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0));
  }
  Vector3d edge = Vector3d(convexhull[0][0], convexhull[0][1], 0.0) - Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0);
  edges.push_back(edge);
  vertices.push_back(Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0));
  cedges.push_back(edges);
  cvertices.push_back(vertices);
 }

 std::vector<int> drawflags;
 drawflags.resize(hgrid.size(), -1);
 for(int j = 0; j < hgrid.size(); j++){
  if(colorValues[j] > 10e-6){
   colorgradient.getColorAtValue(colorValues[j], red, green, blue);
   glColor4f(red, green, blue, colorValues[j]);
   //glColor4f(1, 0, 0, colorValues[j]);
   //drawSolidCircle(j, k, KernelRadius);
   Vector2d corners[4];
   corners[0] = hgrid[j][0];
   corners[1] = hgrid[j][1];
   corners[2] = hgrid[j][2];
   corners[3] = hgrid[j][3];

   Vector2d c = corners[0] + (corners[2] - corners[1]) / 2.0;
   Vector3d C(c.x, c.y, 0.0);

   for(int tt = 0 ; tt < cedges.size(); tt++){
    edges.clear();
    vertices.clear();
    edges = cedges[tt];
    vertices = cvertices[tt];
    if(drawflags[j] == -1){
     int inside = 0;
     for(int k = 0; k < edges.size(); k++){
      Vector3d N = C - vertices[k];
      N = N % edges[k];
      if(N.z > 0){
       inside = 1;
       break;
      }
     }

     //if(colorValues[j] > bandPercentage[1])
     //if(inside == 0)
     //{
     // drawflags[j] = 0;
     // double opct = colorValues[j];
     // opct = opacityCurve(opct, OpacityFactor, OpacityPower);
     // // if(opct < 0.03)
     // //  opct = 0.2;
     // glColor4f(bandColors[tt].x, bandColors[tt].y, bandColors[tt].z, opct);
     //}
    }
   }

   glBegin(GL_POLYGON);
   glVertex3f(corners[0].x, corners[0].y, 0.0);
   glVertex3f(corners[1].x, corners[1].y, 0.0);
   glVertex3f(corners[2].x, corners[2].y, 0.0);
   glVertex3f(corners[3].x, corners[3].y, 0.0);
   glEnd();
  }
 }

 }

 void advisory::renderFromBinaryFile(int timeframe){
  int i = 0;
  i = max(userInt->getSliderCurTick(), 0);
  i = timeframe;
  char filename[100];
  sprintf(filename, "NHC_Imagefile/NHC_image.%03d.bin", i);
  cout << filename << endl;
  float* rendercolors = new float[4*1024*1024];
  ifstream myfile(filename, std::ios::binary | std::ios::in);
  myfile.read((char*)rendercolors, sizeof(float) * 4 * 1024 * 1024);
  int kk = 0;
  for(int j = 0; j < 1024; j++){
   for(int k = 0; k < 1024; k++){
    //int idx = j * 1024 + 4 * k;
    //float red   = rendercolors[idx+0];
    //float green = rendercolors[idx+1];
    //float blue  = rendercolors[idx+2];
    //float alpha = rendercolors[idx+3];
    float red   = rendercolors[4*kk+0];
    float green = rendercolors[4*kk+1];
    float blue  = rendercolors[4*kk+2];
    float alpha = rendercolors[4*kk+3];
    kk++;
    glColor4f(red, green, blue, alpha);
    glBegin(GL_POINTS);
    glVertex3f(j, k, 0.0);
    glEnd();
   }
  }
 }

 std::vector< std::vector<float> > advisory::readPixelColorFromFile(int i){
  char filename[100];
  if(i < 10)
   sprintf(filename, "NHC_imagefile12/NHC_image.00%d.bin", i);
  else if(i >= 10 && i < 100)
   sprintf(filename, "NHC_imagefile12/NHC_image.0%2d.bin", i);
  else
   sprintf(filename, "NHC_imagefile12/NHC_image.%3d.bin", i);
  cout << filename << endl;
  float* rendercolors = new float[4*1024*1024];
  ifstream myfile(filename, std::ios::binary | std::ios::in);
  myfile.read((char*)rendercolors, sizeof(float) * 4 * 1024 * 1024);
  int kk = 0;
  std::vector<float> toPush;
  toPush.resize(4, 0.0);
  std::vector< std::vector<float> > toReturn;
  for(int j = 0; j < 1024; j++){
   for(int k = 0; k < 1024; k++){
    float red   = rendercolors[4*kk+0];
    float green = rendercolors[4*kk+1];
    float blue  = rendercolors[4*kk+2];
    float alpha = rendercolors[4*kk+3];
    kk++;
    toPush[0] = red;
    toPush[1] = green;
    toPush[2] = blue;
    toPush[3] = alpha;
    toReturn.push_back(toPush);
    //if(Vector3d(toPush[0], toPush[1], toPush[2]) == bandColors[0]){
    glColor4f(red, green, blue, alpha);
    glBegin(GL_POINTS);
    glVertex3f(j, k, 0.0);
    glEnd();
    //}
   }
  }
  return toReturn;
 }

 void advisory::renderUnionconeFromFiles(int frameno){
  //std::vector< std::vector<float> > pixelColors = readPixelColorFromFile(frameno);
  opImage = true;

 }


 void advisory::renderUnionconeFromFiles(){
  std::vector< std::vector< std::vector<float> > > pixelColorsList;
  for(int i = 1; i <= 139; i++){
   std::vector< std::vector<float> > pixelColors = readPixelColorFromFile(i);
   pixelColorsList.push_back(pixelColors);
  }

  int i = 0;
  for(int j = 0; j < 1024; j++){
   for(int k = 0; k < 1024; k++){
    std::vector< std::vector<float> > colorList;
    for(int t = 0; t < pixelColorsList.size(); t++){
     std::vector<float> toPush = pixelColorsList[t][i];
     colorList.push_back(toPush);
    }
    i++;

    glColor4f(0, 0, 0, 0);

    float d = 0;
    float opacity = 0;
    for(int t = 0; t < colorList.size(); t++){
     Vector3d color(colorList[t][0], colorList[t][1], colorList[t][2]);
     d = (color - bandColors[2]).norm();
     if(d < 10e-3){
      opacity = max(opacity, colorList[t][3]);
      glColor4f(color.x, color.y, color.z, opacity);
     }
    }

    opacity = 0;
    for(int t = 0; t < colorList.size(); t++){
     Vector3d color(colorList[t][0], colorList[t][1], colorList[t][2]);
     d = (color - bandColors[1]).norm();
     if(d < 10e-3){
      opacity = max(opacity, colorList[t][3]);
      glColor4f(color.x, color.y, color.z, opacity);
     }
    }

    opacity = 0;
    for(int t = 0; t < colorList.size(); t++){
     Vector3d color(colorList[t][0], colorList[t][1], colorList[t][2]);
     d = (color - bandColors[0]).norm();
     if(d < 10e-6){
      opacity = max(opacity, colorList[t][3]);
      glColor4f(color.x, color.y, color.z, opacity);
     }
    }

    glBegin(GL_POINTS);
    glVertex3f(j, k, 0);
    glEnd();
   }
  }
  opImage = true;

  //std::vector< std::vector<float> > pixelColors = readPixelColorFromFile(21);
  //int i = 0;
  //for(int j = 0; j < 1024; j++){
  // for(int k = 0; k < 1024; k++){
  //  std::vector<float> color = pixelColors[i];
  //  i++;
  //  float red   = color[0];
  //  float green = color[1];
  //  float blue  = color[2];
  //  float alpha = color[3];
  //  //cout << bandColors[1] << endl;
  //  //if(red != 0 && green != 0 && blue != 0){
  //  // cout << red <<  " " << green << " " << blue << " " << alpha << endl;
  //  //}
  //  //if(Vector3d(red, green, blue) == bandColors[2]){
  //  float d = (Vector3d(red, green, blue) - bandColors[1]).norm();
  //  //if(red == float(bandColors[2].x) && green == float(bandColors[2].y) && blue == float(bandColors[2].z)){
  //  if(d < 10e-6){
  //   glColor4f(red, green, blue, alpha);
  //   glBegin(GL_POINTS);
  //   glVertex3f(j, k, 0);
  //   glEnd();
  //  }
  // }
  //}
 }

 // RBF with clustering with NHC only 3 colors bands 
 void advisory::drawDataDepthPlotRBFwithNHC(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
   int width, int height, 
   double tlat, double tlon, double mlat, double mlon,
   int timeframe, 
   float rbfkernelfactor, const string& nhcfilename){

  CDFSample cdfsample;

  // Reads data file
  std::vector< std::vector<float> > pathNodes;
#ifdef NHC_DATA
  pathNodes = readNHCTracks(nhcfilename, timeframe);
#else
  readNHCTracks("trackdata/ourtracks.txt", timeframe);
#endif

  // Sets mpi 
  int npro = omp_get_max_threads();
  omp_set_num_threads(npro);

  float red(0.0), green(0.0), blue(0.0);
  ColorGradient colorgradient;

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  float tempTrans;
  Vector2d meanPoint;

  Points band;
  band.clear();
  Polygon_2 poly;
  poly.clear();
  Points outlier;
  outlier.clear();

  std::vector<Points> bands;
  bands.resize(3);
  std::vector<double> bandsVals;
  bandsVals.resize(3, 1.0);

  double bandval = 0.0;

  Vector2d* curPos;
  Vector2d* latLon;

  int i = timeframe;

  //DensityGrid
  DensityGridCols = 5;
  DensityGridRows = 5;

  // compute bounding box of predicted locations
  float maxX = numeric_limits<float>::min();
  float maxY = numeric_limits<float>::min();
  float minX = numeric_limits<float>::max(); 
  float minY = numeric_limits<float>::max();
  for(int j = 0; j < pathList.size(); j++){
   Vector2d newpos = *(pathList[j]->posList[i]); 
   maxX = std::max(maxX, float(newpos.x));
   maxY = std::max(maxY, float(newpos.y));
   minX = std::min(minX, float(newpos.x));
   minY = std::min(minY, float(newpos.y));
  }
  float globalminx(minX), globalminy(minY), globalmaxx(maxX), globalmaxy(maxY);

  float boundwidth = (maxX - minX);
  float boundheight = (maxY - minY);
  float boundarea = boundwidth * boundheight;
  removeDuplication(maxX - minX, i);

  hgrid.clear();
  hgrid = buildDensityGrid(minX, minY, maxX, maxY, dw, dh);
  Vector2d LLC(minX, minY);

  PathCBD pathCBD(pathList, M);
  std::vector<double> dataDepthValue;
  //pathCBD.fastPathSimplexDepth();
  if(i != 0){
   dataDepthValue = pathCBD.fastSegmentSimplexDepth(i);
   segSortedIdx = pathCBD.sortSegmentIdx(dataDepthValue);
  }

  cout << "# of paths: " << pathList.size() << endl;
  std::vector<float> colorValues;
  colorValues.resize(hgrid.size(), 0.0);
  std::vector<float> samplesizes;
  samplesizes.resize(hgrid.size(), 0.0);
  std::vector<double> trainingSetValues;
  trainingSetValues.resize(pathList.size(), 0.0);
  std::vector<double> trainingSetIndices;
  trainingSetIndices.resize(pathList.size(), 0.0);

  std::vector<Vector2d> trainingSet;
  std::vector<double> tmpVals;
  tmpVals.clear();
  std::vector<int> indicesList;

  // for each path
  int pathIdx = -1;
  for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   pathIdx ++;
   // a particular time point on this path
   curPos = (*pathIt)->posList[i];
   latLon = (*pathIt)->latLonList[i];
   if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0){
    meanPoint = Vector2d(curPos->x, curPos->y);
   }

   if(i != 0){
    std::vector<int>::iterator iit;
    iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
    int posIdx = iit - segSortedIdx.begin();
    double depthvalue = dataDepthValue[segSortedIdx[posIdx]];
    double indexvalue = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
    trainingSetValues[pathIdx]  = depthvalue; 
    trainingSetIndices[pathIdx] = indexvalue;

    int idx;
    if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
     Vector2d tmpcurpos(curPos->x, curPos->y);
     tmpcurpos = tmpcurpos - Vector2d(minX, minY);
     int idxCol = tmpcurpos.x / dw;
     int idxRow = tmpcurpos.y / dh;
     idx = idxRow * DensityGridCols + idxCol;
     if(idx < hgrid.size()){
      colorValues[idx] += indexvalue;
      samplesizes[idx] += 1;
     }
    }
    else{
     //cout << "predicted location " << *latLon << " is out of map." << endl;
    }

    trainingSet.push_back(Vector2d(curPos->x, curPos->y));
    tmpVals.push_back(indexvalue);
    indicesList.push_back(idx);

    // construct bands
    if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end()){
     band.push_back(Point_2(curPos->x, curPos->y));
     bands[0].push_back(Point_2(curPos->x, curPos->y));
     bandsVals[0] = min(bandsVals[0], indexvalue);
    }
    if(posIdx > segSortedIdx.size() * bandPercentage[1] &&
      posIdx < segSortedIdx.size() * bandPercentage[2] && iit != segSortedIdx.end()){
     bands[1].push_back(Point_2(curPos->x, curPos->y));
     bandsVals[1] = min(bandsVals[1], indexvalue);
    }
    if(posIdx > segSortedIdx.size() * bandPercentage[2] &&
      posIdx < segSortedIdx.size() * bandPercentage[3] && iit != segSortedIdx.end()){
     bands[2].push_back(Point_2(curPos->x, curPos->y));
     bandsVals[2] = min(bandsVals[2], indexvalue);
    }

    // construct outlier
    if(posIdx < segSortedIdx.size() * bandPercentage[4] && iit != segSortedIdx.end())
     outlier.push_back(Point_2(curPos->x, curPos->y));
   }

  }

  maxX = numeric_limits<float>::min();
  maxY = numeric_limits<float>::min();
  minX = numeric_limits<float>::max(); 
  minY = numeric_limits<float>::max();
  for(int j = 0; j < bands[0].size(); j++){
   Point_2 pos = bands[0][j]; 
   maxX = std::max(float(pos[0]), maxX);
   minX = std::min(float(pos[0]), minX);
   maxY = std::max(float(pos[1]), maxY);
   minY = std::min(float(pos[1]), minY);
  }
  kernelFactor = rbfkernelfactor * (maxX - minX);

  // computer data density of grid 
  std::vector<double> dataDensityRanking;
  std::vector<double> dataDensityList;
  std::vector<double> dataDensityRankingwithoutZero;
  std::vector<int>    dataDensityIndices;
  int totalsample = 0;
  for(int j = 0; j < samplesizes.size(); j++)
   totalsample += samplesizes[j];
  for(int j = 0; j < samplesizes.size(); j++){
   dataDensityList.push_back(samplesizes[j]);
   samplesizes[j] = samplesizes[j] / totalsample;
   dataDensityRanking.push_back(samplesizes[j]);
  }
  dataDensityIndices = getSortedIndex(dataDensityRanking);

  // normalize to [0,1]
  float tmpmax = dataDensityRanking[dataDensityIndices[0]];
  for(int j = 0; j < dataDensityRanking.size(); j++)
  {
   dataDensityRanking[j] = dataDensityRanking[j] / tmpmax;
  }

  //RBF density values;
  std::vector<Vector2d> densityTrainingSet;
  std::vector<double> densityTrainingSetVal;
  std::vector<double> densityTrainingSetVal2;
  for(int j = 0; j < dataDensityRanking.size(); j++){
   Vector2d corners[4];
   corners[0] = hgrid[j][0];
   corners[1] = hgrid[j][1];
   corners[2] = hgrid[j][2];
   corners[3] = hgrid[j][3];
   Vector2d c = corners[0] + (corners[2] - corners[0]) / 2.0;
   if(dataDensityRanking[j] >= 0){
    densityTrainingSet.push_back(c);
    densityTrainingSetVal.push_back(dataDensityRanking[j]);
    densityTrainingSetVal2.push_back(dataDensityList[j]);
   }
  }
  RBF densityRBF;
  //densityRBF.compute(densityTrainingSet, densityTrainingSetVal, 0.7 * (maxX - minX));
  densityRBF.compute(densityTrainingSet, densityTrainingSetVal, boundwidth * 0.15);

  //compute KDTree density
  std::vector<int> validlist;
  std::vector<Vector2d> shadow = trainingSet;
  std::vector<double> kdtreeden = KDTreeDensity<std::vector<Vector2d>>(trainingSet, trainingSet.size(), 2, validlist, boundwidth * 0.01, 10); // 0.01 is a user defined parameter
  RBF newRBF = DensityFieldFromSubset(i);

  std::vector<Vector2d> validpoints;
  std::vector<double> validdensity;
  for(int j = 0; j < validlist.size(); j++)
  {
   if(validlist[j] != 0)
   {
    validpoints.push_back(trainingSet[j]);
    validdensity.push_back(kdtreeden[j]);
   }
  }
  int localmaxidx = std::max_element(validdensity.begin(), validdensity.end()) - validdensity.begin();
  Vector2d localmax = validpoints[localmaxidx];

  //readDensitySubset(validpoints, validdensity, "density_subset.txt"); 
  validpoints.push_back(localmax);
  validdensity.push_back(*std::max_element(validdensity.begin(), validdensity.end()));
  float rbflambda = 0.15;
  //cout << validdensity.size() << " " << validpoints.size() << endl;
  //cout << "width of bbox: " <<  boundwidth << endl;
  //cout << "radii#1: " << boundwidth * rbflambda << endl;
  densityRBF.compute(validpoints, validdensity, boundwidth * rbflambda);

  std::vector<double> KNNdensity;
  for(int j = 0; j < trainingSet.size(); j++)
  {
   float f = densityRBF.eval(trainingSet[j]);
   KNNdensity.push_back(f);
  }

  char* buf = new char[200];

  globalminx = std::floor(globalminx)-10;
  globalminy = std::floor(globalminy)-10;
  globalmaxx = std::ceil( globalmaxx)+10;
  globalmaxy = std::ceil( globalmaxy)+10;
  float localwidth  = globalmaxx - globalminx;
  float localheight = globalmaxy - globalminy;
  int tcol = 129;
  int trow = 129;
  float ddx = localwidth  / (tcol-1);
  float ddy = localheight / (trow-1);
  std::vector<Vector2d> ppl;
  std::vector<double> dl;
  std::vector<float> centerDensity;
  glPointSize(3);
  glColor4f(1, 0, 0, 1);
  glBegin(GL_POINTS);
  for(int ii = 0; ii < trow - 1; ii++){
   float yy = globalminy + ddy * ii;
   for(int jj = 0; jj < tcol - 1; jj++){
    float xx = globalminx + ddx * jj;
    Vector2d pp(xx, yy);
    float dd = densityRBF.eval((pp));
    ppl.push_back(pp);
    dl.push_back(dd);
    Vector2d gcenter(xx + 0.5 * ddx, yy + 0.5 * ddy);
    float tred, tgreen, tblue;
    colorgradient.getColorAtValue(dd, red, green, blue);
    glColor4f(red, green, blue, 1);
    //glVertex3f(gcenter.x, gcenter.y, 0.0);
    glColor4f(0, 1, 0, 1);
    //glVertex3f(xx, yy, 0);
    //float gdensity = newRBF.eval(translateToGeoCoor(gcenter));
    //centerDensity.push_back(gdensity);
    centerDensity.push_back(dd);
   }
  }
  glEnd();
  //string str = OutputPrefix + "InternalData/PathSamplingData/griddensity-%d.txt";
  //const char* cstr = str.c_str();
  //sprintf(buf, cstr, timeframe);
  //CDFSample::write2file(centerDensity, buf);
  //exit(0);
  
  //writeDensityData(ppl, dl, i);
  //CDFSample cdfsample;
  //cdfsample.compute3(globalminx, globalmaxx, globalminy, globalmaxy, localwidth, localheight, trow, tcol, dl, trainingSet, i, KNNdensity);
  float bandmaxX = 10e-6;
  float bandmaxY = 10e-6;
  float bandminX = 10e6; 
  float bandminY = 10e6;
  for(int j = 0; j < bands[2].size(); j++){
   Point_2 pos = bands[2][j]; 
   bandmaxX = std::max(float(pos[0]), bandmaxX);
   bandminX = std::min(float(pos[0]), bandminX);
   bandmaxY = std::max(float(pos[1]), bandmaxY);
   bandminY = std::min(float(pos[1]), bandminY);
  }
  //cdfsample.dynamicDraw(i, subPoints, fullPoints, pointsBand, pointsBandBackup, bandsVals);
  //return;
  std::vector<Vector2d> selectedSamples;
  selectedSamples = cdfsample.SamplingByWSESelecting(globalminx, globalmaxx, globalminy, globalmaxy, localwidth, localheight, trow, tcol, dl, trainingSet, i, tmpVals, bandminX, bandmaxX, bandminY, bandmaxY);
  //selectedSamples = cdfsample.SamplingByPDSampling(globalminx, globalmaxx, globalminy, globalmaxy, localwidth, localheight, trow, tcol, dl, trainingSet, i, tmpVals, bandminX, bandmaxX, bandminY, bandmaxY);
  exit(0);

  return;
  std::vector<double> rbfradii = buildRBFRadii(trainingSet, KNNdensity, indicesList, dw, rbfkernelfactor); 

  //char* subsetfile = new char[100];
  //char* intensityfile = new char[100];
  //sprintf(subsetfile, "NewData/NHC-OLS-idx-%d.txt", i);
  //sprintf(intensityfile, "NHCData/NHC-cats-%d.txt", i);
  //std::vector<Vector2d> aaa;
  //std::vector<Vector2d> cats = cdfsample.readCategories(subsetfile, intensityfile, aaa);
  //std::vector<double> hurInt;
  //for(int j = 0; j < cats.size(); j++)
  // hurInt.push_back(cats[j].y);
  //cdfsample.rebuildIntensityField(selectedSamples, densityRBF, hurInt, timeframe, dw, rbfkernelfactor);
  //return;
  //writeRBFdata(trainingSet, tmpVals, rbfradii);

  //std::vector<double> rbfradii = buildRBFRadii(trainingSet, dataDensityList, indicesList, dw, rbfkernelfactor); 
  //writeDensityData(trainingSet, KNNdensity, i);

  RBF rbf;
  //cout << "tmpVals:" << endl;
  //for(int j = 0; j < tmpVals.size(); j++)
  // cout << tmpVals[j] << endl;
  rbf.compute(trainingSet, tmpVals, rbfradii);
  //rbf.RBFtest(rbfradii);

  //return;

  //for(int j = 0; j < bandsVals.size(); j++)
  // cout << "band[" << j << "]: " << bandsVals[j] << endl;
  cdfsample.rebuildSDField(selectedSamples, densityRBF, rbf, timeframe, dw, rbfkernelfactor, rbfradii);


  //drawRebuildSDField(timeframe, bandsVals[0], bandsVals[1], bandsVals[2]);
  //return;

  std::vector<float> pixelvalues;
  pixelvalues.resize(width*height, 0.0);

  std::vector<Points> rbfbands;
  rbfbands.resize(3);

  // for early hours
  //if(i < 10){
  // for(int j = 0; j < 3; j++){
  //  bandsVals[j] = bandsVals[j] * 0.3;
  // }
  //}

  char filename[100];
  //sprintf(filename, "NewData/SD/SDField-%d.txt", timeframe);
  //ofstream myfile(filename, fstream::out);
  struct timespec time_start = {0, 0};
  struct timespec time_end = {0, 0};
  //clock_gettime(CLOCK_REALTIME, &time_start);
  float rendercolor[4];
  float* rendercolors = new float[width*height*4];
  int kk = 0;
  int rbfcount[4];
  rbfcount[0] = rbfcount[1] = rbfcount[2] = rbfcount[3] = 0;
  std::vector<float> formax;
  formax.clear();
  for(int j = 0; j < width; j++){
   for(int k = 0; k < height; k++){
    Vector2d c;
    c.set(j, k);
    float f(0);
    //pixel inside map area
    if(c.y < height && c.y > 0){
     f = rbf.eval(c, rbfradii);
     formax.push_back(f);
     // band color opacity is [0.02, 0.8]
     if(f >= bandsVals[2] && f <= bandsVals[1]){
      glColor4f(bandColors[2].x, bandColors[2].y, bandColors[2].z, 0.02+f*0.6);
      rendercolor[0] = bandColors[2].x;
      rendercolor[1] = bandColors[2].y;
      rendercolor[2] = bandColors[2].z;
      rendercolor[3] = 0.02+f*0.6;
      rbfbands[0].push_back(Point_2(j, k));
     }
     else if(f > bandsVals[1] && f <= bandsVals[0]){
      glColor4f(bandColors[1].x, bandColors[1].y, bandColors[1].z, 0.02+f*0.6);
      rendercolor[0] = bandColors[1].x;
      rendercolor[1] = bandColors[1].y;
      rendercolor[2] = bandColors[1].z;
      rendercolor[3] = 0.02+f*0.6;
      rbfbands[1].push_back(Point_2(j, k));
     }
     else if(f > bandsVals[0]){
      glColor4f(bandColors[0].x, bandColors[0].y, bandColors[0].z, 0.02+f*0.6);
      rendercolor[0] = bandColors[0].x;
      rendercolor[1] = bandColors[0].y;
      rendercolor[2] = bandColors[0].z;
      rendercolor[3] = 0.02+f*0.6;
      rbfbands[2].push_back(Point_2(j, k));
     }
     else{
      glColor4f(0, 0, 0, 0);
      rendercolor[0] = 0;
      rendercolor[1] = 0;
      rendercolor[2] = 0;
      rendercolor[3] = 0;
     }
    }
    else{
     glColor4f(1, 1, 1, 1);
    }
    float red, green, blue;
    colorgradient.getColorAtValue(f, red, green, blue);
    glColor4f(red, green, blue, 1);

    glBegin(GL_POINTS);
    //glVertex3f(j, k, 0.0); 
    glEnd();
    Vector2d geo_xy = translateToGeoCoor(Vector2d(j, k));
    //myfile << geo_xy.x << " " << geo_xy.y << " " << f << endl;
    rendercolors[4 * kk + 0] = rendercolor[0];
    rendercolors[4 * kk + 1] = rendercolor[1];
    rendercolors[4 * kk + 2] = rendercolor[2];
    rendercolors[4 * kk + 3] = rendercolor[3];
    kk++;
   }
  }
  //myfile.close();
  //cout << "finish writing :" << filename << endl;
  //exit(0);
  //clock_gettime(CLOCK_REALTIME, &time_end);
  //printf("eval duration: %llus %lluns\n", time_end.tv_sec - time_start.tv_sec, time_end.tv_nsec - time_start.tv_nsec);

  // draw rbf bands
  std::vector<QRegion> qregions;
  std::vector<Points> convexhulls;
  convexhulls.resize(rbfbands.size());
  std::vector<int> drawflags;
  std::vector<Vector2d> Cs;
  std::vector<float> thetas;
  std::vector<Vector2d> axisList;
  drawflags.resize(width * height, -1);
  for(int j = rbfbands.size()-1; j >= 0; j--){
   cout << "rbf bands id: " << j << endl;
   cout << "rbf bands size: " << j << " " << rbfbands[j].size() << endl;
   convexhulls[j] = getConvexHull(rbfbands[j]);
   cout << "convex hull size: " << convexhulls[j].size() << endl;

   Vector2d C, axes;
   float theta;
   findEllipse(rbfbands[j], C, axes, theta);
   Cs.push_back(C);
   thetas.push_back(theta);
   axisList.push_back(axes);
   cout << "Center: " << C << " Axes:" << axes << " Theta:" << theta << "\n" << endl;
   QRegion ER(QRect(C.x - axes.x, C.y - axes.y, 2 * axes.x, 2 * axes.y), QRegion::Ellipse);
   qregions.push_back(ER);
  }

  drawEllipses(qregions, width, height, Cs, thetas);
  drawExperimentPoints();

  writeEllipseToFile(Cs, thetas, axisList, i);
  exit(0);
  return;

  //fillConvexHull(width, height, convexhulls);


  cout << "rbf counter: " << rbfcount[0] << " " << rbfcount[1] << " " << 
   rbfcount[2] << " " << rbfcount[3] << endl;

  opImage = true;

  if(ToFile == false){
   char filename[100];
   if(i < 10)
    sprintf(filename, "NHC_Imagefile/NHC_image.00%d.bin", i);
   else if(i >= 10 && i < 100)
    sprintf(filename, "NHC_Imagefile/NHC_image.0%2d.bin", i);
   else
    sprintf(filename, "NHC_Imagefile/NHC_image.%3d.bin", i);
   ofstream myfile(filename, ios::out | ios::binary);
   myfile.write((char*)rendercolors, sizeof(float)*width*width*4);
   myfile.close();
   ToFile = true;
   cout << "finish writing NHC image to file:" << filename << endl;
  }
  exit(0);

  // draw depth bands
  //std::vector<Points> convexhulls;
  //convexhulls.resize(bands.size());
  //for(int j = bands.size()-1; j >= 0; j--){
  // convexhulls[j] = getConvexHull(bands[j]);
  // glColor4f(bandColors[j].x, bandColors[j].y, bandColors[j].z, 0.5);
  // glBegin(GL_LINE_LOOP);
  // for(int k = 0; k < convexhulls[j].size(); k++){
  //  glVertex3f(convexhulls[j][k][0], convexhulls[j][k][1], 0.0);
  // }
  // glEnd();
  //}

 }



 // // RBF with clustering with NHC 
 // void advisory::drawDataDepthPlotRBFwithNHC(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
 //   int width, int height, 
 //   double tlat, double tlon, double mlat, double mlon,
 //   int timeframe, 
 //   float rbfkernelfactor){
 //  //if(opImage)
 //  // exit(0);
 //  if(pathList.size() <= 4){
 //   drawGenPaths();
 //   return;
 //  }
 //
 //  std::vector< std::vector<Vector2d> > plist;
 //
 //  //readMeanTrack("NHCMean.txt");
 //  readNHCTracks("postNHCdata.txt");
 //  //readNHCTracks("NHCdata.txt");
 //
 //  // adjust kernelFactor
 //  //kernelFactor = 0.9 * dh;
 //
 //  int npro = omp_get_max_threads();
 //  omp_set_num_threads(npro);
 //
 //  float red, green, blue;
 //  ColorGradient colorgradient;
 //  cout << "\nwidth: " << width << " height: " << height << endl; 
 //  //cout << "RBF kernel radius: " << kernelFactor << endl;
 //  //cout << "deltaW: " << dw << " " << "deltaH: " << dh << endl; 
 //
 //  std::vector<Vector2d*>::iterator pos;
 //  std::vector<path*>::iterator pathIt;
 //  float tempTrans;
 //  Vector2d meanPoint;
 //
 //  Points band;
 //  band.clear();
 //  Polygon_2 poly;
 //  poly.clear();
 //  Points outlier;
 //  outlier.clear();
 //
 //  std::vector<Points> bands;
 //  bands.resize(3);
 //  std::vector<double> bandsVals;
 //  bandsVals.resize(3, 1.0);
 //
 //  double bandval = 0.0;
 //
 //  Vector2d* curPos;
 //  Vector2d* latLon;
 //
 //  int i = 0; 
 //  i = timeframe;
 //
 //  //DensityGrid
 //  DensityGridCols = 5;
 //  DensityGridRows = 5;
 //  
 //  // compute bounding box of predicted locations
 //  float maxX(10e-6), maxY(10e-6), minX(10e6), minY(10e6);
 //  for(int j = 0; j < pathList.size(); j++){
 //   Vector2d newpos = *(pathList[j]->posList[i]); 
 //   maxX = std::max(maxX, float(newpos.x));
 //   maxY = std::max(maxY, float(newpos.y));
 //   minX = std::min(minX, float(newpos.x));
 //   minY = std::min(minY, float(newpos.y));
 //  }
 //  hgrid.clear();
 //  hgrid = buildDensityGrid(minX, minY, maxX, maxY, dw, dh);
 //  cout << "DensityGrid: " << DensityGridCols << " X " << DensityGridRows << endl; 
 //  cout << "deltaW: " << dw << " " << "deltaH: " << dh << endl; 
 //  Vector2d LLC(minX, minY);
 //  cout << minX << " " << minY << " " << maxX << " " << maxY << endl;
 //  cout << maxX - minX << endl;
 //
 //  removeDuplication(maxX - minX, i);
 //
 //  PathCBD pathCBD(pathList, M);
 //  //i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
 //  //i = max(i, 0);
 //  std::vector<double> dataDepthValue;
 //  if(i != 0){
 //   dataDepthValue = pathCBD.fastSegmentSimplexDepth(i);
 //   segSortedIdx = pathCBD.sortSegmentIdx(dataDepthValue);
 //  }
 //
 //  //DensityGridCols = 3;
 //  //DensityGridRows = 3;
 //  //if(i >= 6){
 //  // //DensityGridCols = i / 6 + 2;
 //  // //DensityGridRows = i / 6 + 2;
 //  // DensityGridCols = 5;
 //  // DensityGridRows = 5;
 //  //}
 //
 //     //glColor4f(1, 1, 0, 1);
 //  //glBegin(GL_LINE_LOOP);
 //  //glVertex3f(minX, minY, 0.0);
 //  //glVertex3f(minX, maxY, 0.0);
 //  //glVertex3f(maxX, maxY, 0.0);
 //  //glVertex3f(maxX, minY, 0.0);
 //  //glEnd();
 //  //return;
 //
 //  //float rfrac = i / 69.0;
 //  //kernelFactor = 8 * (1 - rfrac) + rfrac * 12;
 //  //kernelFactor = rbfkernelfactor * dw;
 //  //kernelFactor = (maxX - minX) * rbfkernelfactor;
 //  //cout << "maxX - minX: " << maxX - minX << endl;
 //  //if(kernelFactor > 13)
 //  // kernelFactor = 13;
 //  //cout << "interpolated kernel factor: " << kernelFactor << endl;
 //
 //  Vector2d testPos;
 //  float testVal;
 //
 //  cout << "# of paths: " << pathList.size() << endl;
 //  std::vector<float> colorValues;
 //  colorValues.resize(hgrid.size(), 0.0);
 //  std::vector<float> samplesizes;
 //  samplesizes.resize(hgrid.size(), 0.0);
 //  std::vector<double> trainingSetValues;
 //  trainingSetValues.resize(pathList.size(), 0.0);
 //  std::vector<double> trainingSetIndices;
 //  trainingSetIndices.resize(pathList.size(), 0.0);
 //
 //  std::vector<Vector2d> trainingSet;
 //  std::vector<double> tmpVals;
 //  std::vector<int> indicesList;
 //
 //  // for each path
 //  int pathIdx = -1;
 //  for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
 //   pathIdx ++;
 //
 //   // a particular time point on this path
 //   curPos = (*pathIt)->posList[i];
 //   latLon = (*pathIt)->latLonList[i];
 //
 //   if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
 //    meanPoint = Vector2d(curPos->x, curPos->y);
 //
 //   if(i != 0){
 //    std::vector<int>::iterator iit;
 //    iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
 //    int posIdx = iit - segSortedIdx.begin();
 //    double depthvalue = dataDepthValue[segSortedIdx[posIdx]];
 //    double indexvalue = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
 //    trainingSetValues[pathIdx]  = depthvalue; 
 //    trainingSetIndices[pathIdx] = indexvalue;
 //
 //    int idx;
 //    if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
 //     Vector2d tmpcurpos(curPos->x, curPos->y);
 //     tmpcurpos = tmpcurpos - Vector2d(minX, minY);
 //     int idxCol = tmpcurpos.x / dw;
 //     int idxRow = tmpcurpos.y / dh;
 //     idx = idxRow * DensityGridCols + idxCol;
 //     if(idx < hgrid.size()){
 //      colorValues[idx] += indexvalue;
 //      samplesizes[idx] += 1;
 //     }
 //    }
 //    else{
 //     cout << "predicted location " << "[" << curPos->x << "," << curPos->y << "]" << " is out of map." << endl;
 //     exit(0);
 //    }
 //
 //    trainingSet.push_back(Vector2d(curPos->x, curPos->y));
 //    tmpVals.push_back(indexvalue);
 //    indicesList.push_back(idx);
 //
 //    // construct bands
 //    if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end()){
 //     band.push_back(Point_2(curPos->x, curPos->y));
 //     bands[0].push_back(Point_2(curPos->x, curPos->y));
 //     bandsVals[0] = min(bandsVals[0], indexvalue);
 //    }
 //    if(posIdx > segSortedIdx.size() * bandPercentage[1] &&
 //      posIdx < segSortedIdx.size() * bandPercentage[2] && iit != segSortedIdx.end()){
 //     bands[1].push_back(Point_2(curPos->x, curPos->y));
 //     bandsVals[1] = min(bandsVals[1], indexvalue);
 //    }
 //    if(posIdx > segSortedIdx.size() * bandPercentage[2] &&
 //      posIdx < segSortedIdx.size() * bandPercentage[3] && iit != segSortedIdx.end()){
 //     bands[2].push_back(Point_2(curPos->x, curPos->y));
 //     bandsVals[2] = min(bandsVals[2], indexvalue);
 //    }
 //
 //    // construct outlier
 //    if(posIdx < segSortedIdx.size() * bandPercentage[4] && iit != segSortedIdx.end())
 //     outlier.push_back(Point_2(curPos->x, curPos->y));
 //   }
 //
 //   //(*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
 //  }
 //
 //  maxX = 10e-6;
 //  maxY = 10e-6;
 //  minX = 10e6; 
 //  minY = 10e6;
 //  for(int j = 0; j < bands[0].size(); j++){
 //   Point_2 pos = bands[0][j]; 
 //   maxX = std::max(float(pos[0]), maxX);
 //   minX = std::min(float(pos[0]), minX);
 //   maxY = std::max(float(pos[1]), maxY);
 //   minY = std::min(float(pos[1]), minY);
 //  }
 //  cout << "max: " << maxX << ", min: " << minX << endl;
 //  cout << "max - min: " << maxX - minX << endl;
 //  kernelFactor = rbfkernelfactor * (maxX - minX);
 //  cout << "data depth RBF: " << kernelFactor << endl;
 //
 //  // computer data density of grid 
 //  std::vector<double> dataDensityRanking;
 //  std::vector<double> dataDensityList;
 //  std::vector<double> dataDensityRankingwithoutZero;
 //  std::vector<int>    dataDensityIndices;
 //  int totalsample = 0;
 //  for(int j = 0; j < samplesizes.size(); j++)
 //   totalsample += samplesizes[j];
 //  for(int j = 0; j < samplesizes.size(); j++){
 //   dataDensityList.push_back(samplesizes[j]);
 //   samplesizes[j] = samplesizes[j] / totalsample;
 //   dataDensityRanking.push_back(samplesizes[j]);
 //  }
 //  dataDensityIndices = getSortedIndex(dataDensityRanking);
 //
 //  // normalize to [0,1]
 //  float tmpmax = dataDensityRanking[dataDensityIndices[0]];
 //  cout << tmpmax << endl;
 //  for(int j = 0; j < dataDensityRanking.size(); j++)
 //  {
 //   //cout << dataDensityRanking[j] << " " ;
 //   dataDensityRanking[j] = dataDensityRanking[j] / tmpmax;
 //   //cout << dataDensityRanking[j] << endl;
 //  }
 //
 //  //RBF density values;
 //  std::vector<Vector2d> densityTrainingSet;
 //  std::vector<double> densityTrainingSetVal;
 //  std::vector<double> densityTrainingSetVal2;
 //  for(int j = 0; j < dataDensityRanking.size(); j++){
 //   Vector2d corners[4];
 //   corners[0] = hgrid[j][0];
 //   corners[1] = hgrid[j][1];
 //   corners[2] = hgrid[j][2];
 //   corners[3] = hgrid[j][3];
 //   Vector2d c = corners[0] + (corners[2] - corners[0]) / 2.0;
 //   //glLineWidth(0.5);
 //   //glColor4f(0, 0, 1, 1);
 //   //glBegin(GL_LINE_LOOP);
 //   //glVertex3f(corners[0].x, corners[0].y, 0.0);
 //   //glVertex3f(corners[1].x, corners[1].y, 0.0);
 //   //glVertex3f(corners[2].x, corners[2].y, 0.0);
 //   //glVertex3f(corners[3].x, corners[3].y, 0.0);
 //   //glEnd();
 //   //drawCircle(c.x, c.y, 2 * TimePointRadius);
 //   if(dataDensityRanking[j] > 0){
 //    densityTrainingSet.push_back(c);
 //    densityTrainingSetVal.push_back(dataDensityRanking[j]);
 //    densityTrainingSetVal2.push_back(dataDensityList[j]);
 //   }
 //  }
 //  RBF densityRBF;
 //  cout << maxX - minX << endl;
 //  cout << kernelFactor * (maxX - minX) << endl;;
 //  densityRBF.compute(densityTrainingSet, densityTrainingSetVal, 0.35 * (maxX - minX));
 //  cout << "density trainning set size: " << densityTrainingSet.size() << endl; 
 //  //for(int j = 0; j < densityTrainingSet.size(); j++){
 //  // float f = densityTrainingSetVal[j];
 //  // colorgradient.getColorAtValue(f, red, green, blue);
 //  // glColor4f(red, green, blue, 1);
 //  // drawCircle(densityTrainingSet[j].x, densityTrainingSet[j].y, 2 * TimePointRadius);
 //  //}
 //  //for(int j = 0; j < width; j++){
 //  // for(int k = 0; k < height; k++){
 //  //  float f = densityRBF.eval(Vector2d(j, k));
 //  //  colorgradient.getColorAtValue(f, red, green, blue);
 //  //  glColor4f(red, green, blue, 1);
 //  //  glBegin(GL_POINTS);
 //  //  glVertex3f(j, k, 0);
 //  //  glEnd();
 //  // }
 //  //}
 //  //return;
 //
 //
 //  //std::vector<Vector2d> tmp_trainingset;
 //  //std::vector<double>   tmp_vals;
 //  //for(int j = 0; j < removeFlags.size(); j++){
 //  // if(removeFlags[j] == 0){
 //  //  tmp_trainingset.push_back(trainingSet[j]);
 //  //  tmp_vals.push_back(tmpVals[j]);
 //  // }
 //  //}
 //  //trainingSet.clear();
 //  //tmpVals.clear();
 //  //trainingSet = tmp_trainingset;
 //  //tmpVals      = tmp_vals;
 //
 //  //std::vector<double> rbfradii = buildRBFRadiusVector(trainingSet, densityRBF,
 //  //  maxX - minX);
 //  std::vector<double> rbfradii = buildRBFRadii(trainingSet, dataDensityList, indicesList, dw, rbfkernelfactor); 
 //  cout << trainingSet.size() << endl;
 //
 //  RBF rbf;
 //  rbf.compute(trainingSet, tmpVals, rbfradii);
 //  //rbf.RBFtest(rbfradii);
 //
 //
 //  for(int j = 0; j < bandsVals.size(); j++)
 //   cout << "band[" << j << "]: " << bandsVals[j] << endl;
 //
 //  std::vector<float> pixelvalues;
 //  pixelvalues.resize(1024*1024, 0.0);
 //  //for(int j = 0; j < width; j++){
 //  // Vector2d c;
 //  // for(int k = 0; k < width; k++){
 //  //  c.set(j, k);
 //
 //  //  //pixel inside map area
 //  //  if(c.y < height && c.y > 0 && c.x < width && c.x > 0){
 //  //   double df = computeRBFradiusFromDensity(densityRBF, c, maxX - minX);
 //  //   double f = rbf.eval(c, df);
 //  //   pixelvalues[width * k + j] = f;
 //  //   // band color opacity is [0.02, 0.62]
 //  //   if(f >= bandsVals[2] && f <= bandsVals[1]){
 //  //    glColor4f(bandColors[2].x, bandColors[2].y, bandColors[2].z, 0.02+f*0.6);
 //  //   }
 //  //   else if(f > bandsVals[1] && f <= bandsVals[0]){
 //  //    glColor4f(bandColors[1].x, bandColors[1].y, bandColors[1].z, 0.02+f*0.6);
 //  //   }
 //  //   else if(f > bandsVals[0]){
 //  //    glColor4f(bandColors[0].x, bandColors[0].y, bandColors[0].z, 0.02+f*0.6);
 //  //   }
 //  //   else
 //  //    glColor4f(0, 0, 0, 0);
 //  //  }
 //  //  else{
 //  //   pixelvalues[width * k + j] = 0.0;
 //  //   glColor4f(1, 1, 1, 1);
 //  //  }
 //
 //  //  glBegin(GL_POINTS);
 //  //  //glVertex3f(j+(0.7* dh), k, 0.0); // 0.7 is the offset factor
 //  //  glEnd();
 //  // }
 //  //}
 //
 //  //MipmapSwith = 1;
 //  // test code
 //  Mipmap mipmap1024(width, width);
 //  mipmap1024.setMipmap(pixelvalues);
 //
 //  // 1024, 128, 64, 32, 16, 8, 4, 1
 //  RBF rbfs[7];
 //  rbfs[0] = rbf;
 //
 //  //int scaleFactor = 16;
 //  //for(int j = 1; j < 6; j++){
 //  // std::vector<double> tmpValsRBF;
 //  // std::vector<Vector2d> trainingSetRBF;
 //  // std::vector<float> pixelRBF;
 //  // std::vector<Vector2d> locRBf;
 //  // pixelRBF = mipmap1024.nextLevel(trainingSetRBF, tmpValsRBF, 
 //  //   scaleFactor, rbfs[j-1], locRBf); 
 //  // rbfs[j].compute(trainingSetRBF, tmpValsRBF, kernelFactor);
 //  // scaleFactor *= 2;
 //  // kernelFactor *= 2;
 //  // cout << "RBF level: " << j << endl;
 //  // cout << scaleFactor << " " << kernelFactor << endl;
 //  // cout << pixelRBF.size() << " " << trainingSetRBF.size() << " " << tmpValsRBF.size() << endl; 
 //  //}
 //
 //  //rbfs[5] = rbfs[4];
 //  //rbfs[4] = rbfs[3];
 //  //rbfs[3] = rbfs[2];
 //  //rbfs[2] = rbfs[1];
 //  ////rbfs[2] = rbfs[3];
 //  ////rbfs[3] = rbfs[4];
 //  ////rbfs[4] = rbfs[5];
 //  //rbfs[1] = rbfs[0];
 //  //rbfs[4] = rbfs[3];
 //
 //  float rendercolor[4];
 //  float* rendercolors = new float[1024*1024*4];
 //  int kk = 0;
 //  int mipmapidx = 4; // draw which mipmap
 //  int rbfcount[4];
 //  rbfcount[0] = rbfcount[1] = rbfcount[2] = rbfcount[3] = 0;
 //  cout << "Mipmap level: " << mipmapidx << endl;
 //  cout << "density randking list size: " << dataDensityRanking.size() << endl;
 //  if(MipmapSwith == 0)
 //  {
 //   for(int j = 0; j < width; j++){
 //    Vector2d c;
 //    for(int k = 0; k < width; k++){
 //     c.set(j, k);
 //     //pixel inside map area
 //     if(c.y < height && c.y > 0){
 //      // each pixel has a density value
 //      //float pixeldensity = densityRBF.eval(c);
 //      //float lb(0), ub(0);
 //      //float rbflb(0), rbfub(0);
 //      //for(int ii = 0; ii < 4; ii++){
 //      // if(pixeldensity < densityIntervals[ii]){
 //      //  ub = densityIntervals[ii];
 //      //  lb = densityIntervals[ii - 1];
 //      //  rbflb = rbfs[rbfIndices[ii]].eval(c);
 //      //  rbfub = rbfs[rbfIndices[ii - 1]].eval(c);
 //      //  rbfcount[ii] += 1;
 //      //  break;
 //      // }
 //      // if(pixeldensity > densityIntervals[3] || pixeldensity < densityIntervals[0])
 //      //  cout << pixeldensity << " here" << endl;
 //      //}
 //      //float ratio = (pixeldensity - lb) / (ub - lb);
 //      //double f = ratio * rbflb + (1 - ratio) * rbfub;
 //      //if(pixeldensity == 0){
 //      // f = rbfs[4].eval(c);
 //      // rbfcount[3] += 1;
 //      //}
 //      //if(pixeldensity == 1)
 //      // f = rbfs[1].eval(c);
 //
 //      float df = computeRBFradiusFromDensity(densityRBF, c, maxX - minX);
 //      float f = rbfs[0].eval(c, rbfradii);
 //      
 //      //float f = IDW(trainingSet, tmpVals, c);
 //
 //      // band color opacity is [0.02, 0.8]
 //      if(f >= bandsVals[2] && f <= bandsVals[1]){
 //       //f = rbf8.eval(c);
 //       glColor4f(bandColors[2].x, bandColors[2].y, bandColors[2].z, 0.02+f*0.6);
 //       rendercolor[0] = bandColors[2].x;
 //       rendercolor[1] = bandColors[2].y;
 //       rendercolor[2] = bandColors[2].z;
 //       rendercolor[3] = 0.02+f*0.6;
 //      }
 //      else if(f > bandsVals[1] && f <= bandsVals[0]){
 //       //f = rbf8.eval(c);
 //       glColor4f(bandColors[1].x, bandColors[1].y, bandColors[1].z, 0.02+f*0.6);
 //       rendercolor[0] = bandColors[1].x;
 //       rendercolor[1] = bandColors[1].y;
 //       rendercolor[2] = bandColors[1].z;
 //       rendercolor[3] = 0.02+f*0.6;
 //      }
 //      else if(f > bandsVals[0]){
 //       //f = rbf8.eval(c);
 //       glColor4f(bandColors[0].x, bandColors[0].y, bandColors[0].z, 0.02+f*0.6);
 //       rendercolor[0] = bandColors[0].x;
 //       rendercolor[1] = bandColors[0].y;
 //       rendercolor[2] = bandColors[0].z;
 //       rendercolor[3] = 0.02+f*0.6;
 //      }
 //      //else if(f > 1){
 //      // glColor4f(bandColors[2].x, bandColors[2].y, bandColors[2].z, 0.02+f*0.6);
 //      // rendercolor[0] = bandColors[2].x;
 //      // rendercolor[1] = bandColors[2].y;
 //      // rendercolor[2] = bandColors[2].z;
 //      // rendercolor[3] = 0.02+f*0.6;
 //      //}
 //      //else if(f < 0){
 //      // glColor4f(bandColors[0].x, bandColors[0].y, bandColors[0].z, 0.02+f*0.6);
 //      // rendercolor[0] = bandColors[0].x;
 //      // rendercolor[1] = bandColors[0].y;
 //      // rendercolor[2] = bandColors[0].z;
 //      // rendercolor[3] = 0.02+f*0.6;
 //      //}
 //      else{
 //       glColor4f(0, 0, 0, 0);
 //       rendercolor[0] = 0;
 //       rendercolor[1] = 0;
 //       rendercolor[2] = 0;
 //       rendercolor[3] = 0;
 //      }
 //     }
 //     else{
 //      glColor4f(1, 1, 1, 1);
 //     }
 //
 //     glBegin(GL_POINTS);
 //     glVertex3f(j, k, 0.0); 
 //     glEnd();
 //     rendercolors[4 * kk + 0] = rendercolor[0];
 //     rendercolors[4 * kk + 1] = rendercolor[1];
 //     rendercolors[4 * kk + 2] = rendercolor[2];
 //     rendercolors[4 * kk + 3] = rendercolor[3];
 //     kk++;
 //    }
 //   }
 //  }
 //  else if(MipmapSwith == 1)
 //  {
 //   for(int j = 0; j < width; j++){
 //    for(int k = 0; k < height; k++){
 //     Vector2d c(j, k);
 //     float f = rbfs[0].eval(c);
 //     colorgradient.getColorAtValue(f, red, green, blue);
 //     glColor4f(red, green, blue, f);
 //     glBegin(GL_POINTS);
 //     glVertex3f(j, k, 0);
 //     glEnd();
 //    }
 //   }
 //  }
 //
 //  cout << "rbf counter: " << rbfcount[0] << " " << rbfcount[1] << " " << 
 //   rbfcount[2] << " " << rbfcount[3] << endl;
 //
 //  opImage = true;
 //
 //  if(ToFile == false){
 //   char filename[100];
 //   if(i < 10)
 //    sprintf(filename, "NHC_Imagefile/NHC_image.00%d.bin", i);
 //   else if(i >= 10 && i < 100)
 //    sprintf(filename, "NHC_Imagefile/NHC_image.0%2d.bin", i);
 //   else
 //    sprintf(filename, "NHC_Imagefile/NHC_image.%3d.bin", i);
 //   ofstream myfile(filename, ios::out | ios::binary);
 //   myfile.write((char*)rendercolors, sizeof(float)*width*width*4);
 //   myfile.close();
 //   ToFile = true;
 //   cout << "finish writing NHC image to file:" << filename << endl;
 //  }
 //
 //  //cout << MipmapSwith << endl;
 //  //if(MipmapSwith == 0){
 //  // for(int j = 0; j < width; j++){
 //  //  for(int k = 0; k < height; k++){
 //  //   int idx = k * width + j;
 //  //   float f = pixelvalues[idx];
 //  //   colorgradient.getColorAtValue(f, red, green, blue);
 //  //   glColor4f(red, green, blue, f);
 //  //   glBegin(GL_POINTS);
 //  //   glVertex3f(j, k, 0);
 //  //   glEnd();
 //  //  }
 //  // }
 //  //}
 //  //else if(MipmapSwith == 1){
 //  // //for(int j = 0; j < pixel64.size(); j++){
 //  // // Vector2d c = loc64[j];
 //  // // float f = pixel64[j];
 //  // // colorgradient.getColorAtValue(f, red, green, blue);
 //  // // glColor4f(red, green, blue, 1);
 //  // // drawCircle(c.x, c.y, TimePointRadius);
 //  // //}
 //  // for(int j = 0; j < width; j++){
 //  //  for(int k = 0; k < height; k++){
 //  //   Vector2d c(j, k);
 //  //   float f = rbf64.eval(c);
 //  //   colorgradient.getColorAtValue(f, red, green, blue);
 //  //   glColor4f(red, green, blue, f);
 //  //   glBegin(GL_POINTS);
 //  //   glVertex3f(j, k, 0);
 //  //   glEnd();
 //  //  }
 //  // }
 //  //}
 //  //else if(MipmapSwith == 2){
 //  // //for(int j = 0; j < pixel8.size(); j++){
 //  // // Vector2d c = loc8[j];
 //  // // float f = pixel8[j];
 //  // // colorgradient.getColorAtValue(f, red, green, blue);
 //  // // glColor4f(red, green, blue, 1);
 //  // // drawCircle(c.x, c.y, TimePointRadius);
 //  // //}
 //  // for(int j = 0; j < width; j++){
 //  //  for(int k = 0; k < height; k++){
 //  //   Vector2d c(j, k);
 //  //   float f = rbf8.eval(c);
 //  //   colorgradient.getColorAtValue(f, red, green, blue);
 //  //   glColor4f(red, green, blue, f);
 //  //   glBegin(GL_POINTS);
 //  //   glVertex3f(j, k, 0);
 //  //   glEnd();
 //  //  }
 //  // }
 //  //}
 //
 //  // draw depth bands
 //  //std::vector<Points> convexhulls;
 //  //convexhulls.resize(bands.size());
 //  //for(int j = bands.size()-1; j >= 0; j--){
 //  // convexhulls[j] = getConvexHull(bands[j]);
 //  // glColor4f(bandColors[j].x, bandColors[j].y, bandColors[j].z, 0.5);
 //  // glBegin(GL_LINE_LOOP);
 //  // for(int k = 0; k < convexhulls[j].size(); k++){
 //  //  glVertex3f(convexhulls[j][k][0], convexhulls[j][k][1], 0.0);
 //  // }
 //  // glEnd();
 //  //}
 //
 // }



 // RBF with clustering 
 void advisory::drawDataDepthPlotRBF(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
   int width, int height, 
   double tlat, double tlon, double mlat, double mlon){
  if(pathList.size() <= 4){
   drawGenPaths();
   return;
  }

  // adjust kernelFactor
  //kernelFactor = 0.9 * dh;
  kernelFactor = 12;

  int npro = omp_get_max_threads();
  omp_set_num_threads(npro);

  float red, green, blue;
  ColorGradient colorgradient;
  cout << "\nwidth: " << width << " height: " << height << endl; 
  cout << "RBF kernel radius: " << kernelFactor << endl;
  cout << "deltaW: " << dw << " " << "deltaH: " << dh << endl; 

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  float tempTrans;
  Vector2d meanPoint;

  Points band;
  band.clear();
  Polygon_2 poly;
  poly.clear();
  Points outlier;
  outlier.clear();

  std::vector<Points> bands;
  bands.resize(3);
  std::vector<double> bandsVals;
  bandsVals.resize(3, 1.0);

  double bandval = 0.0;

  Vector2d* curPos;
  Vector2d* latLon;

  PathCBD pathCBD(pathList, M);
  int i = 0;
  i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
  i = max(i, 0);
  std::vector<double> dataDepthValue;
  if(i != 0){
   dataDepthValue = pathCBD.fastSegmentSimplexDepth(i);
   segSortedIdx = pathCBD.sortSegmentIdx(dataDepthValue);
  }

  Vector2d testPos;
  float testVal;

  cout << "# of paths: " << pathList.size() << endl;
  std::vector<float> colorValues;
  colorValues.resize(hgrid.size(), 0.0);
  std::vector<float> samplesizes;
  samplesizes.resize(hgrid.size(), 0.0);
  std::vector<double> trainingSetValues;
  trainingSetValues.resize(pathList.size(), 0.0);
  std::vector<double> trainingSetIndices;
  trainingSetIndices.resize(pathList.size(), 0.0);

  std::vector<Vector2d> trainingSet;
  std::vector<double> tmpVals;

  // for each path
  int pathIdx = -1;
  for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   pathIdx ++;
   tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
   glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

   // a particular time point on this path
   curPos = (*pathIt)->posList[i];
   latLon = (*pathIt)->latLonList[i];

   if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
    meanPoint = Vector2d(curPos->x, curPos->y);

   if(i != 0){
    std::vector<int>::iterator iit;
    iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
    int posIdx = iit - segSortedIdx.begin();
    double depthvalue = dataDepthValue[segSortedIdx[posIdx]];
    double indexvalue = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
    trainingSetValues[pathIdx]  = depthvalue; 
    trainingSetIndices[pathIdx] = indexvalue;

    if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
     int idxCol = curPos->x / dw;
     int idxRow = curPos->y / dh;
     int idx = idxRow * hw + idxCol;
     if(idx < hgrid.size()){
      colorValues[idx] += indexvalue;
      samplesizes[idx] += 1;
     }
    }

    trainingSet.push_back(Vector2d(curPos->x, curPos->y));
    tmpVals.push_back(indexvalue);

    // construct bands
    if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end()){
     band.push_back(Point_2(curPos->x, curPos->y));
     bands[0].push_back(Point_2(curPos->x, curPos->y));
     bandsVals[0] = min(bandsVals[0], indexvalue);
    }
    if(posIdx > segSortedIdx.size() * bandPercentage[1] &&
      posIdx < segSortedIdx.size() * bandPercentage[2] && iit != segSortedIdx.end()){
     bands[1].push_back(Point_2(curPos->x, curPos->y));
     bandsVals[1] = min(bandsVals[1], indexvalue);
    }
    if(posIdx > segSortedIdx.size() * bandPercentage[2] &&
      posIdx < segSortedIdx.size() * bandPercentage[3] && iit != segSortedIdx.end()){
     bands[2].push_back(Point_2(curPos->x, curPos->y));
     bandsVals[2] = min(bandsVals[2], indexvalue);
    }

    // construct outlier
    if(posIdx < segSortedIdx.size() * bandPercentage[4] && iit != segSortedIdx.end())
     outlier.push_back(Point_2(curPos->x, curPos->y));
   }

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
  }

  for(pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  // computer data density of grid 
  std::vector<double> dataDensityRanking;
  std::vector<double> dataDensityRankingwithoutZero;
  std::vector<int>    dataDensityIndices;
  int totalsample = 0;
  for(int j = 0; j < samplesizes.size(); j++)
   totalsample += samplesizes[j];
  for(int j = 0; j < samplesizes.size(); j++){
   samplesizes[j] = samplesizes[j] / totalsample;
   dataDensityRanking.push_back(samplesizes[j]);
  }
  //dataDensityRanking.push_back(0.0);
  dataDensityIndices = getSortedIndex(dataDensityRanking);
  for(int j = 0; j < dataDensityIndices.size(); j++){
   if(dataDensityRanking[dataDensityIndices[j]] > 0){
    dataDensityRankingwithoutZero.push_back(dataDensityIndices[j]);
   }
  }
  // normalize to [0,10]
  float tmpmax = dataDensityRanking[dataDensityIndices[0]];
  cout << tmpmax << endl;
  for(int j = 0; j < dataDensityRanking.size(); j++)
  {
   dataDensityRanking[j] = dataDensityRanking[j] / tmpmax;
  }
  //for(int j = 0; j < dataDensityIndices.size(); j++)
  // if(dataDensityRanking[dataDensityIndices[j]] != 0)
  // cout << dataDensityRanking[dataDensityIndices[j]] << " "<< dataDensityIndices[j]<< endl;
  //cout << "++++++++++++++++++++" << endl;
  //exit(0);

  //RBF density values;
  std::vector<Vector2d> densityTrainingSet;
  std::vector<double> densityTrainingSetVal;
  for(int j = 0; j < dataDensityRanking.size(); j++){
   Vector2d corners[4];
   corners[0] = hgrid[j][0];
   corners[1] = hgrid[j][1];
   corners[2] = hgrid[j][2];
   corners[3] = hgrid[j][3];
   Vector2d c = corners[0] + (corners[2] - corners[0]) / 2.0;
   if(dataDensityRanking[j] != 0){
    densityTrainingSet.push_back(c);
    densityTrainingSetVal.push_back(dataDensityRanking[j]);
   }
  }
  RBF densityRBF;
  densityRBF.compute(densityTrainingSet, densityTrainingSetVal, 1.5 * dh);

  //for(int j = 0; j < width; j++){
  // for(int k = 0; k < height; k++){
  //  float f = densityRBF.eval(Vector2d(j, k));
  //  colorgradient.getColorAtValue(f, red, green, blue);
  //  glColor4f(red, green, blue, 1);
  //  glBegin(GL_POINTS);
  //  glVertex3f(j, k, 0);
  //  glEnd();
  // }
  //}
  //return;

  //// build clustered training data set;
  //std::vector<Vector2d> trainingSet;
  //std::vector<double> tmpVals;
  //for(int j = 0; j < hgrid.size(); j++){
  // Vector2d corners[4];
  // corners[0] = hgrid[j][0];
  // corners[1] = hgrid[j][1];
  // corners[2] = hgrid[j][2];
  // corners[3] = hgrid[j][3];
  // Vector2d c = corners[0] + (corners[2] - corners[1]) / 2.0;
  // if(colorValues[j] > 0.01){
  //  trainingSet.push_back(c);
  //  tmpVals.push_back(colorValues[j]/samplesizes[j]); 
  //  //glColor4f(0, 0, 0, 1);
  //  //drawCircle(c.x, c.y, TimePointRadius);
  // }
  //}

  RBF rbf;
  rbf.compute(trainingSet, tmpVals, kernelFactor);


  for(int j = 0; j < bandsVals.size(); j++)
   cout << "band[" << j << "]: " << bandsVals[j] << endl;

  std::vector<float> pixelvalues;
  pixelvalues.resize(width*height, 0.0);
  for(int j = 0; j < width; j++){
   Vector2d c;
   for(int k = 0; k < width; k++){
    c.set(j, k);

    //pixel inside map area
    if(c.y < height && c.y > 0 && c.x < width && c.x > 0){
     double f = rbf.eval(c);
     if(f != 0)
      pixelvalues[width * k + j] = f;
     // band color opacity is [0.02, 0.62]
     if(f >= bandsVals[2] && f <= bandsVals[1]){
      glColor4f(bandColors[2].x, bandColors[2].y, bandColors[2].z, 0.02+f*0.6);
     }
     else if(f > bandsVals[1] && f <= bandsVals[0]){
      glColor4f(bandColors[1].x, bandColors[1].y, bandColors[1].z, 0.02+f*0.6);
     }
     else if(f > bandsVals[0]){
      glColor4f(bandColors[0].x, bandColors[0].y, bandColors[0].z, 0.02+f*0.6);
     }
     else
      glColor4f(0, 0, 0, 0);
    }
    else{
     pixelvalues[width * k + j] = 0.0;
     glColor4f(1, 1, 1, 1);
    }

    glBegin(GL_POINTS);
    //glVertex3f(j+(0.7* dh), k, 0.0); // 0.7 is the offset factor
    //glVertex3f(j, k, 0);
    glEnd();
   }
  }

  // test code
  Mipmap mipmap1024(width, width);
  mipmap1024.setMipmap(pixelvalues);

  // 1024, 128, 64, 32, 16, 8, 4, 1
  RBF rbfs[7];
  rbfs[0] = rbf;

  int scaleFactor = 16;
  for(int j = 1; j < 6; j++){
   std::vector<double> tmpValsRBF;
   std::vector<Vector2d> trainingSetRBF;
   std::vector<float> pixelRBF;
   std::vector<Vector2d> locRBf;
   pixelRBF = mipmap1024.nextLevel(trainingSetRBF, tmpValsRBF, 
     scaleFactor, rbfs[j-1], locRBf); 
   rbfs[j].compute(trainingSetRBF, tmpValsRBF, kernelFactor);
   scaleFactor *= 2;
   kernelFactor *= 2;
   cout << scaleFactor << " " << kernelFactor << endl;
  }

  int mipmapidx = 4; // draw which mipmap
  int rbfcount[4];
  rbfcount[0] = rbfcount[1] = rbfcount[2] = rbfcount[3] = 0;
  cout << "Mipmap level: " << mipmapidx << endl;
  cout << dataDensityRanking.size() << endl;
  if(MipmapSwith == 0)
  {
   for(int j = 0; j < width; j++){
    Vector2d c;
    for(int k = 0; k < width; k++){
     c.set(j, k);
     //pixel inside map area
     if(c.y < height && c.y > 0){
      // each pixel has a density value
      float pixeldensity = densityRBF.eval(c);
      float lb(0), ub(0);
      float rbflb(0), rbfub(0);
      for(int ii = 0; ii < 4; ii++){
       if(pixeldensity < densityIntervals[ii]){
        ub = densityIntervals[ii];
        lb = densityIntervals[ii - 1];
        rbflb = rbfs[rbfIndices[ii]].eval(c);
        rbfub = rbfs[rbfIndices[ii - 1]].eval(c);
        rbfcount[ii] += 1;
        //cout << pixeldensity << " LRBF" << rbfIndices[ii] << " URBF" << rbfIndices[ii-1] << endl;
        break;
       }
       if(pixeldensity > densityIntervals[3] || pixeldensity < densityIntervals[0])
        cout << pixeldensity << " here" << endl;
      }
      float ratio = (pixeldensity - lb) / (ub - lb);
      double f = ratio * rbflb + (1 - ratio) * rbfub;
      if(pixeldensity == 0){
       f = rbfs[4].eval(c);
       rbfcount[3] += 1;
      }
      if(pixeldensity == 1){
       f = rbfs[1].eval(c);
      }

      //float f = 0.0;
      //if(pixeldensity == densityIntervals[0])
      // f = rbfs[6].eval(c); 
      //else if(pixeldensity > densityIntervals[0] && 
      //  pixeldensity <= densityIntervals[1]){
      // ub = densityIntervals[1];
      // lb = densityIntervals[0];
      // rbflb = rbfs[6].eval(c);
      // rbfub = rbfs[5].eval(c);
      // float ratio = (pixeldensity - lb) / (ub - lb);
      // f = ratio * rbflb + (1 - ratio) * rbfub;
      //}
      //else if(pixeldensity > densityIntervals[1] &&
      //  pixeldensity <= densityIntervals[2]){
      // ub = densityIntervals[2];
      // lb = densityIntervals[1];
      // rbflb = rbfs[5].eval(c);
      // rbfub = rbfs[4].eval(c);
      // float ratio = (pixeldensity - lb) / (ub - lb);
      // f = ratio * rbflb + (1 - ratio) * rbfub;
      //}
      //else if(pixeldensity > densityIntervals[2] &&
      //  pixeldensity < densityIntervals[3]){
      // ub = densityIntervals[3];
      // lb = densityIntervals[2];
      // rbflb = rbfs[4].eval(c);
      // rbfub = rbfs[3].eval(c);
      // float ratio = (pixeldensity - lb) / (ub - lb);
      // f = ratio * rbflb + (1 - ratio) * rbfub;
      //}
      //else if(pixeldensity == densityIntervals[3])
      // f = rbfs[3].eval(c);


      //double f = rbfs[mipmapidx].eval(c);
      //if(f >= bandsVals[1]){
      // f = rbfs[1].eval(c);
      // rbfcount1++;
      //}
      //else if(f > bandsVals[1] && f <= bandsVals[2]){
      // f = rbfs[3].eval(c);
      // rbfcount3++;
      //}
      //else{
      // f = rbfs[5].eval(c);
      // rbfcount5++;
      //}
      //int idxCol = c.x / dw;
      //int idxRow = c.y / dh;
      //int idx = idxRow * hw + idxCol;
      //float densityR = std::find(dataDensityRankingwithoutZero.begin(),
      //  dataDensityRankingwithoutZero.end(), idx) -
      // dataDensityRankingwithoutZero.begin();
      //densityR = densityR * (1.0 / dataDensityRankingwithoutZero.size());
      //double f = 0.0;
      //if(densityR <= bandPercentage[1]){
      // f = rbfs[1].eval(c);
      // rbfcount1++;
      //}
      //else if(densityR > bandPercentage[1] && densityR <= bandPercentage[2]){
      // f = rbfs[3].eval(c);
      // rbfcount3++;
      //}
      //else{
      // f = rbfs[5].eval(c);
      // rbfcount5++;
      //}

      // band color opacity is [0.02, 0.62]
      if(f >= bandsVals[2] && f <= bandsVals[1]){
       //f = rbf8.eval(c);
       glColor4f(bandColors[2].x, bandColors[2].y, bandColors[2].z, 0.02+f*0.6);
      }
      else if(f > bandsVals[1] && f <= bandsVals[0]){
       //f = rbf8.eval(c);
       glColor4f(bandColors[1].x, bandColors[1].y, bandColors[1].z, 0.02+f*0.6);
      }
      else if(f > bandsVals[0]){
       //f = rbf8.eval(c);
       glColor4f(bandColors[0].x, bandColors[0].y, bandColors[0].z, 0.02+f*0.6);
      }
      else{
       glColor4f(0, 0, 0, 0);
      }
     }
     else{
      glColor4f(1, 1, 1, 1);
     }

     glBegin(GL_POINTS);
     //glVertex3f(j+(0.7* dh), k, 0.0); // 0.7 is the offset factor
     glVertex3f(j, k, 0.0); // 0.7 is the offset factor
     glEnd();
    }
   }
  }
  else if(MipmapSwith == 1)
  {
   for(int j = 0; j < width; j++){
    for(int k = 0; k < height; k++){
     Vector2d c(j, k);
     float f = rbfs[0].eval(c);
     colorgradient.getColorAtValue(f, red, green, blue);
     glColor4f(red, green, blue, f);
     glBegin(GL_POINTS);
     glVertex3f(j, k, 0);
     glEnd();
    }
   }
  }

  cout << "rbf counter: " << rbfcount[0] << " " << rbfcount[1] << " " << 
   rbfcount[2] << " " << rbfcount[3] << endl;

  //cout << MipmapSwith << endl;
  //if(MipmapSwith == 0){
  // for(int j = 0; j < width; j++){
  //  for(int k = 0; k < height; k++){
  //   int idx = k * width + j;
  //   float f = pixelvalues[idx];
  //   colorgradient.getColorAtValue(f, red, green, blue);
  //   glColor4f(red, green, blue, f);
  //   glBegin(GL_POINTS);
  //   glVertex3f(j, k, 0);
  //   glEnd();
  //  }
  // }
  //}
  //else if(MipmapSwith == 1){
  // //for(int j = 0; j < pixel64.size(); j++){
  // // Vector2d c = loc64[j];
  // // float f = pixel64[j];
  // // colorgradient.getColorAtValue(f, red, green, blue);
  // // glColor4f(red, green, blue, 1);
  // // drawCircle(c.x, c.y, TimePointRadius);
  // //}
  // for(int j = 0; j < width; j++){
  //  for(int k = 0; k < height; k++){
  //   Vector2d c(j, k);
  //   float f = rbf64.eval(c);
  //   colorgradient.getColorAtValue(f, red, green, blue);
  //   glColor4f(red, green, blue, f);
  //   glBegin(GL_POINTS);
  //   glVertex3f(j, k, 0);
  //   glEnd();
  //  }
  // }
  //}
  //else if(MipmapSwith == 2){
  // //for(int j = 0; j < pixel8.size(); j++){
  // // Vector2d c = loc8[j];
  // // float f = pixel8[j];
  // // colorgradient.getColorAtValue(f, red, green, blue);
  // // glColor4f(red, green, blue, 1);
  // // drawCircle(c.x, c.y, TimePointRadius);
  // //}
  // for(int j = 0; j < width; j++){
  //  for(int k = 0; k < height; k++){
  //   Vector2d c(j, k);
  //   float f = rbf8.eval(c);
  //   colorgradient.getColorAtValue(f, red, green, blue);
  //   glColor4f(red, green, blue, f);
  //   glBegin(GL_POINTS);
  //   glVertex3f(j, k, 0);
  //   glEnd();
  //  }
  // }
  //}

  // draw depth bands
  //std::vector<Points> convexhulls;
  //convexhulls.resize(bands.size());
  //for(int j = bands.size()-1; j >= 0; j--){
  // convexhulls[j] = getConvexHull(bands[j]);
  // glColor4f(bandColors[j].x, bandColors[j].y, bandColors[j].z, 0.5);
  // glBegin(GL_LINE_LOOP);
  // for(int k = 0; k < convexhulls[j].size(); k++){
  //  glVertex3f(convexhulls[j][k][0], convexhulls[j][k][1], 0.0);
  // }
  // glEnd();
  //}

 }





 //// RBF
 //void advisory::drawDataDepthPlotRBF(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
 //  int width, int height, 
 //  double tlat, double tlon, double mlat, double mlon){
 // if(pathList.size() <= 4){
 //  drawGenPaths();
 //  return;
 // }
 //
 // int npro = omp_get_max_threads();
 // omp_set_num_threads(npro);
 //
 // float red, green, blue;
 // ColorGradient colorgradient;
 // cout << "Radius: " << KernelRadius << endl;
 // cout << "Factor: " << kernelFactor << endl;
 //
 // std::vector<Vector2d*>::iterator pos;
 // std::vector<path*>::iterator pathIt;
 // float tempTrans;
 // Vector2d meanPoint;
 //
 // Points band;
 // band.clear();
 // Polygon_2 poly;
 // poly.clear();
 // Points outlier;
 // outlier.clear();
 //
 // std::vector<Points> bands;
 // bands.resize(3);
 //
 // double bandval = 0.0;
 //
 // Vector2d* curPos;
 // Vector2d* latLon;
 //
 // PathCBD pathCBD(pathList, M);
 // int i = 0;
 // i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
 // i = max(i, 0);
 // std::vector<double> tmp;
 // if(i != 0){
 //  tmp = pathCBD.fastSegmentSimplexDepth(i);
 //  segSortedIdx = pathCBD.sortSegmentIdx(tmp);
 // }
 //
 // // RBF matrix
 // Matrix Phi(pathList.size(), pathList.size());
 // // RBF weights vector
 // Vector W;
 // W.setsize(pathList.size());
 // // RBF sample values
 // Vector F;
 // F.setsize(pathList.size());
 //
 // Vector2d testPos;
 // float testVal;
 //
 // // for each path
 // int pathIdx = -1;
 // cout << pathList.size() << endl;
 // std::vector<float> colorValues;
 // //colorValues.resize(width * height, 0.0);
 // colorValues.resize(hgrid.size(), 0.0);
 //
 // for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
 //  pathIdx ++;
 //  tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
 //  glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);
 //
 //  // a particular time point on this path
 //  curPos = (*pathIt)->posList[i];
 //  latLon = (*pathIt)->latLonList[i];
 //
 //  if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
 //   meanPoint = Vector2d(curPos->x, curPos->y);
 //
 //  if(i != 0){
 //   std::vector<int>::iterator iit;
 //   iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
 //   int posIdx = iit - segSortedIdx.begin();
 //   //double dval = tmp[segSortedIdx[posIdx]];
 //   double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
 //   //colorgradient.getColorAtValue(value, red, green, blue);
 //   //glColor4f(red, green, blue, tempTrans);
 //
 //   //if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
 //   // int idxCol = curPos->x / dw;
 //   // int idxRow = curPos->y / dh;
 //   // int idx = idxRow * hw + idxCol;
 //   // if(idx < hgrid.size())
 //   //  colorValues[idx] = value;
 //   //}
 //
 //   // construct bands
 //   if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end()){
 //    band.push_back(Point_2(curPos->x, curPos->y));
 //    bands[0].push_back(Point_2(curPos->x, curPos->y));
 //   }
 //   if(posIdx > segSortedIdx.size() * bandPercentage[1] &&
 //      posIdx < segSortedIdx.size() * bandPercentage[2] && iit != segSortedIdx.end()){
 //    bands[1].push_back(Point_2(curPos->x, curPos->y));
 //   }
 //   if(posIdx > segSortedIdx.size() * bandPercentage[2] &&
 //      posIdx < segSortedIdx.size() * bandPercentage[3] && iit != segSortedIdx.end()){
 //    bands[2].push_back(Point_2(curPos->x, curPos->y));
 //   }
 //
 //   // construct outlier
 //   if(posIdx < segSortedIdx.size() * bandPercentage[4] && iit != segSortedIdx.end())
 //    outlier.push_back(Point_2(curPos->x, curPos->y));
 //
 //   // fill rbf sample value vector
 //   F[pathIdx] = value;
 //
 //   // construct rbf matrix
 //   //Vector2d pos0 = *(latLon);
 //   //Vector2d pos0 = *curPos;
 //   Vector2d pos0 = translateToGeoCoor(Vector2d(curPos->x, curPos->y));
 //   for(int j = 0; j < pathList.size(); j++){
 //    Vector2d pos1(pathList[j]->posList[i]->x, pathList[j]->posList[i]->y);
 //    //Vector2d pos1 = *(pathList[j]->latLonList[i]);
 //    pos1 = translateToGeoCoor(pos1);
 //    //cout << pos1 << " " << *(pathList[j]->latLonList[i]) << "@@@ " << endl;
 //    //double w = RBFKernelCus(pos0, pos1);
 //    //cout << pos0 << " " << pos1 << endl;
 //    double w = RBFKernelGaussian(pos0, pos1);
 //    //double w = RBFKernelThinPlateSpline(pos0, pos1);
 //    Phi[pathIdx][j] = w;
 //    //cout << pos0 << " " << pos1 << endl;
 //    //cout << dis << " " << endl;
 //    //cout << kernelFactor << endl;
 //    //cout << w << endl;
 //   }
 //
 //   if(pathIdx == 5){
 //    testPos = pos0;
 //    testVal = value;
 //   }
 //
 //  }
 //  (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
 //  //if((*pathIt)->posList.size() != (*pathIt)->latLonList.size()){
 //  // cout << (*pathIt)->posList.size() << " " << (*pathIt)->latLonList.size() << " " << "! " << endl; 
 //  // exit(0);
 //  //}
 // }
 //
 // // compute rbf weights
 // W = Phi.inv() * F;
 //
 // //cout << "F: " << endl;
 // //cout << F << endl;
 // //cout << "Phi: " << endl;
 // //cout << Phi << endl;
 // //cout << "W: " << endl;
 // //cout << W << endl;
 // //cout << width << " " << height << endl;
 // //cout << mlat << " " << mlon << endl;
 // //cout << tlat << " " << tlon << endl;
 //
 // /*
 // // rbf interpolation
 // for(int j = 0; j < width; j++){
 //  Vector2d c;
 //  c.x = mlon - (tlon / float(1.0 * width)) * j;
 //  for(int k = 0; k < height; k++){
 //   c.y = mlat - (tlat / float(1.0 * height)) * k; 
 //   //c.set(j, k);
 //   double f = 0.0;
 //
 //   for(int ii = 0; ii < pathList.size(); ii++){
 //    Vector2d p = *(pathList[ii]->posList[i]);
 //    p = translateToGeoCoor(p);
 //    double phi = RBFKernelGaussian(c, p);
 //    //cout << (c-p).norm() << "\t" << c << "\t" << p <<  endl;
 //    double tmpv = W[ii] * phi;
 //    //printf("pre f: %015.15f\t phi: %015.15f\t W[ii]: %015.15f\t phi*W[ii]: %015.15f\n", f, phi, W[ii], tmpv);
 //    //f = f + W[ii] * phi; 
 //    f += tmpv;
 //    //"\t" << phi << "\t" << W[ii] << "\t" << phi * W[ii] << "\t" << f << endl;
 //    //printf("%15.15f\t %15.15f\t %15.15f\t %15.15f\n", phi, W[ii], phi*W[ii], f);
 //    //printf("aft f: %015.15f\n", f);
 //   }
 //
 //   //printf("%015.15f\n", f);
 //   //cout << "-------" << endl;
 //   if(f < 0)
 //    f = 0;
 //   if(f > 1)
 //    f = 1;
 //   colorgradient.getColorAtValue(f, red, green, blue);
 //   glColor4f(red, green, blue, 1);
 //   //glColor4f(f, 0, 0, f);
 //   glBegin(GL_POINTS);
 //   glVertex3f(j, k, 0.0);
 //   glEnd();
 //  }
 // }
 // */
 //
 // //// test rbf matrix
 // double f = 0;
 // Vector2d c = testPos;
 // cout << W.getn() << " "<< pathList.size() << endl;
 // for(int k = 0; k < pathList.size(); k++){
 //  //Vector2d p(pathList[k]->latLonList[i]->x, pathList[k]->latLonList[i]->y);
 //  Vector2d p(pathList[k]->posList[i]->x, pathList[k]->posList[i]->y);
 //  p = translateToGeoCoor(p);
 //  double phi = RBFKernelGaussian(c, p);
 //  double tmp = W[k] * phi;
 //  f += tmp;
 // }
 // cout << c << " " << testPos << endl;
 // cout << f << " " << testVal << "  " << endl;
 //
 // for(pathIt = pathList.begin(); pathIt != pathList.end();){
 //  if ((*pathIt)->getInt().w <= 0.0) {
 //   delete((*pathIt));
 //   pathIt = pathList.erase(pathIt);
 //  }
 //  else{
 //   pathIt++;
 //  }
 // }
 //
 // // draw depth bands
 // std::vector<Points> convexhulls;
 // convexhulls.resize(bands.size());
 // for(int j = bands.size()-1; j >= 0; j--){
 //  convexhulls[j] = getConvexHull(bands[j]);
 //  glColor4f(bandColors[j].x, bandColors[j].y, bandColors[j].z, 0.1);
 //  glBegin(GL_LINE_LOOP);
 //  //glBegin(GL_POLYGON);
 //  for(int k = 0; k < convexhulls[j].size(); k++){
 //   glVertex3f(convexhulls[j][k][0], convexhulls[j][k][1], 0.0);
 //  }
 //  glEnd();
 // }
 //
 //
 ////for(int j = 0; j < hgrid.size(); j++){
 //// Vector2d vertex[2];
 //// vertex[0] = hgrid[j][0];
 //// vertex[1] = hgrid[j][2];
 //// Vector2d C = vertex[0] + (vertex[1] - vertex[0]) / 2.0;
 //// double sn = 0.0;
 //// double alpha = 0.0;
 //// //for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
 //// for(int tt = 0; tt < pathList.size(); tt++){
 ////  curPos = (pathList[tt]->posList[i]);
 ////  //curPos = (*pathIt)->posList[i];
 ////  Vector2d P(curPos->x, curPos->y);
 ////  //int idx = width * int(curPos->y) + int(curPos->x);
 ////  double dis = (P - C).norm();
 ////  if(KernelRadius > dis && P.x < width && P.y < height && P.x > 0 && P.y > 0){
 ////   int idxCol = curPos->x / dw;
 ////   int idxRow = curPos->y / dh;
 ////   int idx = idxRow * hw + idxCol;
 ////   double d = colorValues[idx];
 ////   double std = KernelRadius / 3.0;
 ////   double part1 = pow(E, -pow(dis, 2) / (2 * pow(std, 2)));
 ////   double part2 = cos(PI/2.0 * dis / (3 * std));
 ////   double w = part1 * part2;
 ////   if(w < 10e-6)
 ////    w = 0.0;
 ////   sn    += w * d;
 ////   alpha += w;
 ////  }
 //// }
 //// colorValues[j] = sn / alpha;
 //// if(alpha < 10e-6 || sn < 10e-6)
 ////  colorValues[j] = 0.0;
 //// if(colorValues[j] < 10e-6)
 ////  colorValues[j] = 0.0;
 //// }
 //
 // //std::vector<double> sortedValues;
 // //for(int j = 0; j < hgrid.size(); j++){
 // // if(colorValues[j] > 10e-6)
 // //  sortedValues.push_back(colorValues[j]);
 // //}
 // //std::sort(sortedValues.begin(), sortedValues.end());
 //
 // //std::vector<Edges>    cedges;
 // //std::vector<Vertices> cvertices;
 // //
 // //std::vector<Vector3d> edges;
 // //std::vector<Vector3d> vertices;
 // //for(int k = 0; k < convexhulls.size(); k++){
 // // edges.clear();
 // // vertices.clear();
 // // Points convexhull = convexhulls[k];
 // // for(int j = 1; j < convexhull.size(); j++){
 // //  Vector3d edge = Vector3d(convexhull[j][0], convexhull[j][1], 0.0) - Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0);
 // //  edges.push_back(edge);
 // //  vertices.push_back(Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0));
 // // }
 // // Vector3d edge = Vector3d(convexhull[0][0], convexhull[0][1], 0.0) - Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0);
 // // edges.push_back(edge);
 // // vertices.push_back(Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0));
 // // cedges.push_back(edges);
 // // cvertices.push_back(vertices);
 // //}
 //
 // //std::vector<int> drawflags;
 // //drawflags.resize(hgrid.size(), -1);
 // //for(int j = 0; j < hgrid.size(); j++){
 // // if(colorValues[j] > 10e-6){
 // //  colorgradient.getColorAtValue(colorValues[j], red, green, blue);
 // //  glColor4f(red, green, blue, colorValues[j]);
 // //  //glColor4f(1, 0, 0, colorValues[j]);
 // //  //drawSolidCircle(j, k, KernelRadius);
 // //  Vector2d corners[4];
 // //  corners[0] = hgrid[j][0];
 // //  corners[1] = hgrid[j][1];
 // //  corners[2] = hgrid[j][2];
 // //  corners[3] = hgrid[j][3];
 //
 // //  Vector2d c = corners[0] + (corners[2] - corners[1]) / 2.0;
 // //  Vector3d C(c.x, c.y, 0.0);
 //
 // //  for(int tt = 0 ; tt < cedges.size(); tt++){
 // //   edges.clear();
 // //   vertices.clear();
 // //   edges = cedges[tt];
 // //   vertices = cvertices[tt];
 // //   if(drawflags[j] == -1){
 // //    int inside = 0;
 // //    for(int k = 0; k < edges.size(); k++){
 // //     Vector3d N = C - vertices[k];
 // //     N = N % edges[k];
 // //     if(N.z > 0){
 // //      inside = 1;
 // //      break;
 // //     }
 // //    }
 //
 // //    //if(colorValues[j] > bandPercentage[1])
 // //    if(inside == 0)
 // //    {
 // //     drawflags[j] = 0;
 // //     double opct = colorValues[j];
 // //     opct = opacityCurve(opct, OpacityFactor, OpacityPower);
 // //     // if(opct < 0.03)
 // //     //  opct = 0.2;
 // //     glColor4f(bandColors[tt].x, bandColors[tt].y, bandColors[tt].z, opct);
 // //    }
 // //   }
 // //  }
 //
 // //  glBegin(GL_POLYGON);
 // //  glVertex3f(corners[0].x, corners[0].y, 0.0);
 // //  glVertex3f(corners[1].x, corners[1].y, 0.0);
 // //  glVertex3f(corners[2].x, corners[2].y, 0.0);
 // //  glVertex3f(corners[3].x, corners[3].y, 0.0);
 // //  glEnd();
 // // }
 // //}
 //
 //}

 void advisory::drawPureDataDepthPlot(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
   int width, int height, 
   double tlat, double tlon, double mlat, double mlon){
  if(pathList.size() < 4){
   drawGenPaths();
   return;
  }

  float red, green, blue;
  ColorGradient colorgradient;

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  float tempTrans;
  Vector2d meanPoint;

  Points band;
  band.clear();
  Polygon_2 poly;
  poly.clear();

  Vector2d * curPos;

  std::vector<Vector2d> pixels;
  for(int j = 0; j < height; j++)
   for(int k = 0; k < width; k++)
    pixels.push_back(Vector2d(j, k));

  PathCBD pathCBD(pathList, M);
  int i = 0;
  i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
  i = max(i, 0);
  std::vector<double> tmp;
  if(i != 0){
   //tmp = pathCBD.fastSegmentSimplexDepth(i);
   //segSortedIdx = pathCBD.sortSegmentIdx(tmp);
   tmp = pathCBD.fastSegmentSimplexDepth2(i, pixels);
   segSortedIdx = pathCBD.sortSegmentIdx(tmp);
  }

  double maxv(0.0), minv(0.0);
  for(int j = 0; j < tmp.size(); j++){
   maxv = tmp[j] ? tmp[j] > maxv : maxv;
   minv = tmp[j] ? tmp[j] < minv : minv;
  }
  cout << "max: " << maxv << " " << "min: " << minv << endl;

  for(int j = 0; j < tmp.size(); j++){
   cout << tmp[segSortedIdx[j]] << endl;
  }

  //for(int j = 0; j < height; j++){
  // for(int k = 0; k < width; k++){
  //  int pidx = j * width + k;
  //  std::vector<int>::iterator iit;
  //  iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pidx);
  //  int posIdx = iit - segSortedIdx.begin();
  //  double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
  //  colorgradient.getColorAtValue(value, red, green, blue);
  //  glColor4f(red, green, blue, tempTrans);
  //  glBegin(GL_POINTS);
  //  glVertex3f(j, k, 0.0);
  //  glEnd();
  // }
  //}

  // for each path
  int pathIdx = -1;
  cout << pathList.size() << endl;
  for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   pathIdx ++;

   tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
   glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

   // a particular time point on this path
   curPos = (*pathIt)->posList[i];
   //curPos = (*pathIt)->latLonList[i];

   //if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
   // meanPoint = Vector2d(curPos->x, curPos->y);

   //if(i != 0){
   // std::vector<int>::iterator iit;
   // iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
   // int posIdx = iit - segSortedIdx.begin();
   // //double dval = tmp[segSortedIdx[posIdx]];
   // double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
   // colorgradient.getColorAtValue(value, red, green, blue);
   // glColor4f(red, green, blue, tempTrans);

   // if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end())
   //  band.push_back(Point_2(curPos->x, curPos->y));
   //}

   //cout << "----" << endl;
   //cout << *curPos << endl;
   //Vector2d ttmp = translateToScreen(curPos->x, curPos->y);
   //curPos = new Vector2d(ttmp.x, ttmp.y);
   //cout << ttmp << endl;
   //cout << *curPos << endl;
   drawSolidCircle(curPos->x, curPos->y, TimePointRadius*1.5);

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
  }

  for(pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  //if(i != 0){
  // Points convexhull = getConvexHull(band);
  // float value = 1.0 - bandPercentage[1];
  // colorgradient.getColorAtValue(value, red, green, blue);
  // glColor4f(red, green, blue, 1);
  // glColor4f(1, 0, 0, 1);
  // glBegin(GL_LINE_LOOP);
  // for(int i = 0; i < convexhull.size(); i++){
  //  glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
  // }
  // glEnd();
  //}
 }




 void advisory::drawDataDepthPlotofTime(){
  if(pathList.size() < 4){
   drawGenPaths();
   return;
  }

  float red, green, blue;
  ColorGradient colorgradient;

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  float tempTrans;
  Vector2d meanPoint;

  Points band;
  band.clear();
  Polygon_2 poly;
  poly.clear();

  Vector2d * curPos;

  PathCBD pathCBD(pathList, M);
  int i = 0;
  i = min(userInt->getSliderCurTick(), (int)pathCBD.getMaxPathLength() - 1);
  i = max(i, 0);
  std::vector<double> tmp;
  if(i != 0){
   tmp = pathCBD.fastSegmentSimplexDepth(i);
   segSortedIdx = pathCBD.sortSegmentIdx(tmp);
  }

  // for each path
  int pathIdx = -1;
  cout << pathList.size() << endl;
  for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   pathIdx ++;

   tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
   glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

   // a particular time point on this path
   curPos = (*pathIt)->posList[i];
   //curPos = (*pathIt)->latLonList[i];

   if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
    meanPoint = Vector2d(curPos->x, curPos->y);

   if(i != 0){
    std::vector<int>::iterator iit;
    iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
    int posIdx = iit - segSortedIdx.begin();
    //double dval = tmp[segSortedIdx[posIdx]];
    double value = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
    colorgradient.getColorAtValue(value, red, green, blue);
    glColor4f(red, green, blue, tempTrans);

    if(posIdx < segSortedIdx.size() * bandPercentage[1] && iit != segSortedIdx.end())
     band.push_back(Point_2(curPos->x, curPos->y));
   }

   //cout << "----" << endl;
   //cout << *curPos << endl;
   //Vector2d ttmp = translateToScreen(curPos->x, curPos->y);
   //curPos = new Vector2d(ttmp.x, ttmp.y);
   //cout << ttmp << endl;
   //cout << *curPos << endl;
   drawSolidCircle(curPos->x, curPos->y, TimePointRadius*1.5);

   (*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
  }

  for(pathIt = pathList.begin(); pathIt != pathList.end();){
   if ((*pathIt)->getInt().w <= 0.0) {
    delete((*pathIt));
    pathIt = pathList.erase(pathIt);
   }
   else{
    pathIt++;
   }
  }

  if(i != 0){
   //draw mean point
   //glColor4f(1, 0, 0, 1);
   //drawCircle(meanPoint.x, meanPoint.y, TimePointRadius * 2);

   Points convexhull = getConvexHull(band);
   float value = 1.0 - bandPercentage[1];
   colorgradient.getColorAtValue(value, red, green, blue);
   glColor4f(red, green, blue, 1);
   glColor4f(1, 0, 0, 1);
   glBegin(GL_LINE_LOOP);
   for(int i = 0; i < convexhull.size(); i++){
    glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
   }
   glEnd();
  }
 }

 void advisory::drawDataDepthPlotofAllTime(){
  if(pathList.size() < 4){
   drawGenPaths();
   return;
  }

  float red, green, blue;
  ColorGradient colorgradient;

  std::vector<Vector2d*>::iterator pos;
  std::vector<path*>::iterator pathIt;
  float tempTrans;
  std::vector<Vector2d> meanPoints;

  Points band;
  band.clear();
  Polygon_2 poly;
  poly.clear();
  Points band2;
  band2.clear();
  Points outlier;
  outlier.clear();

  Points outlier1;
  outlier1.clear();
  Points outlier2;
  outlier2.clear();
  Points outlier3;
  outlier3.clear();
  Points outlier4;
  outlier4.clear();

  Vector2d * curPos;

  PathCBD pathCBD(pathList, M);

  cout << pathList.size() << endl;
  for(int t = 1; t < 24; t++){
   std::vector<double> tmp = pathCBD.fastSegmentSimplexDepth(t);
   segSortedIdx.clear();
   segSortedIdx = pathCBD.sortSegmentIdx(tmp);
   int pathIdx = -1;
   for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
    pathIdx ++;
    curPos = (*pathIt)->posList[t];
    int posIdx = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx) - segSortedIdx.begin();
    // mean
    if(posIdx == 0)
     meanPoints.push_back(Vector2d(curPos->x, curPos->y));

    // bands
    if(posIdx < segSortedIdx.size() * bandPercentage[1] && posIdx != segSortedIdx.size() - 1){
     band.push_back(Point_2(curPos->x, curPos->y));
     poly.push_back(Point_2(curPos->x, curPos->y));
    }
    else if(posIdx > segSortedIdx.size() * bandPercentage[1] && posIdx < segSortedIdx.size() * bandPercentage[2] && posIdx !=segSortedIdx.size() - 1)
     band2.push_back(Point_2(curPos->x, curPos->y));

    // outlier
    //if(tmp[posIdx] - 10e-6 <= 0.0)
    if(t == 23)
     outlier.push_back(Point_2(curPos->x, curPos->y));
    //if(posIdx == segSortedIdx.size() - 1)
    // outlier1.push_back(Point_2(curPos->x, curPos->y));
    //if(posIdx == segSortedIdx.size() - 2)
    // outlier2.push_back(Point_2(curPos->x, curPos->y));
    //if(posIdx == segSortedIdx.size() - 3)
    // outlier3.push_back(Point_2(curPos->x, curPos->y));
    //if(posIdx == segSortedIdx.size() - 4)
    // outlier4.push_back(Point_2(curPos->x, curPos->y));

   }
  }

  // for each path
  int pathIdx = -1;
  //cout << pathList.size() << endl;
  for (pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
   pathIdx ++;

   tempTrans = (*pathIt)->getInt().w*sim->getPathOpacity();
   glColor4f(tempTrans*(*pathIt)->getInt().x, tempTrans*(*pathIt)->getInt().y, tempTrans*(*pathIt)->getInt().z, tempTrans * 0.5);

   //(*pathIt)->setInt((*pathIt)->getInt().w - 1.0 / float(npaths));
  }


  std::vector<Vector2d> bsplinePoints;
  std::vector<Vector2d*> bsp;

  float lineWidth = 0.8;
  Alpha_shape_2 A(band.begin(), band.end(), FT(ALPHA), Alpha_shape_2::GENERAL);

  std::vector<Segment> segments;
  alpha_edges(A, std::back_inserter(segments));

  float value = 1.0 - bandPercentage[1];
  colorgradient.getColorAtValue(value, red, green, blue);
  glColor4f(red, green, blue, 1);
  //glColor4f(1, 0, 0, 1);
  for(int i = 0; i < segments.size(); i++){
   glLineWidth(lineWidth * 3);
   glBegin(GL_LINES);
   Point_2 p0 = segments[i][0];
   Point_2 p1 = segments[i][1];
   glVertex3f(p0[0], p0[1], 0.0);
   glVertex3f(p1[0], p1[1], 0.0);
   glEnd();
   bsplinePoints.push_back(Vector2d(p0[0], p0[1]));
   bsplinePoints.push_back(Vector2d(p1[0], p1[1]));
  }

  Alpha_shape_2 A2(band2.begin(), band2.end(), FT(ALPHA), Alpha_shape_2::GENERAL);
  std::vector<Segment> segments2;
  alpha_edges(A2, std::back_inserter(segments2));
  value = 1.0 - bandPercentage[2];
  colorgradient.getColorAtValue(value, red, green, blue);
  glColor4f(red, green, blue, 1);
  //glColor4f(0, 1, 0, 1);
  for(int i = 0; i < segments2.size(); i++){
   glLineWidth(lineWidth * 3);
   glBegin(GL_LINES);
   Point_2 p0 = segments2[i][0];
   Point_2 p1 = segments2[i][1];
   glVertex3f(p0[0], p0[1], 0.0);
   glVertex3f(p1[0], p1[1], 0.0);
   glEnd();
  }

  //Alpha_shape_2 A3(outlier.begin(), outlier.end(), FT(ALPHA), Alpha_shape_2::GENERAL);
  //std::vector<Segment> segments3;
  //alpha_edges(A3, std::back_inserter(segments3));
  //glColor4f(1, 0, 0, 1);
  //for(int i = 0; i < segments3.size(); i++){
  // glLineWidth(lineWidth * 3);
  // glBegin(GL_LINES);
  // Point_2 p0 = segments3[i][0];
  // Point_2 p1 = segments3[i][1];
  // glVertex3f(p0[0], p0[1], 0.0);
  // glVertex3f(p1[0], p1[1], 0.0);
  // glEnd();
  //}

  Points convexhull = getConvexHull(outlier);
  glColor4f(1, 0, 0, 0.1);
  glBegin(GL_POLYGON);
  for(int i = 0; i < convexhull.size(); i++){
   glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
  }
  glEnd();


  glColor4f(1, 1, 0, 1);
  glLineWidth(lineWidth * 3.0);
  glBegin(GL_LINE_STRIP);
  for(int i = 0; i < meanPoints.size(); i++){
   glVertex3f(meanPoints[i].x, meanPoints[i].y, 0.0);
  }
  glEnd();


  //m_origin.set(0,0);
  //reference = translateToScreen(projPath[3]->x, projPath[3]->y);
  ////reference = Vector2d(200, 200);
  //m_origin = reference - Vector2d(1, 1);
  //m_dreference = reference - m_origin;
  //std::sort(bsplinePoints.begin(), bsplinePoints.end(), angleSort);

  ////glColor4f(0, 0, 1, 1);
  ////glBegin(GL_LINE_LOOP);
  ////for(int i = 0; i < bsplinePoints.size(); i++)
  //// glVertex3f(bsplinePoints[i].x, bsplinePoints[i].y, 0.0);
  ////glEnd();

  //std::vector<Vector2d> tmpbs;
  //for(int i = 0; i < bsplinePoints.size(); i++){
  // if(tmpbs.size() == 0){
  //  tmpbs.push_back(bsplinePoints[i]);
  // }
  // else{
  //  Vector2d tp1 = tmpbs.back();
  //  Vector2d tp2 = bsplinePoints[i];
  //  if(tp1.x != tp2.x && tp1.y != tp2.y)
  //   tmpbs.push_back(tp2);
  // }
  //}

  //tmpbs.push_back(tmpbs.front());

  //for(int i = 0; i < tmpbs.size(); i++){
  // Vector2d* tmpv = new Vector2d(tmpbs[i][0], tmpbs[i][1]);
  // bsp.push_back(tmpv);
  //}

  //std::vector<Vector2d> bspline = CatmullRom(bsp, bsp.size(), 20);
  //glColor4f(0, 0, 1, 1);
  //for(int i = 1; i < bspline.size(); i++)
  //{
  // glBegin(GL_LINES);
  // Vector2d p0 = bspline[i];
  // Vector2d p1 = bspline[i - 1];
  // glVertex3f(p0.x, p0.y, 0.0);
  // glVertex3f(p1.x, p1.y, 0.0);
  // glEnd();
  //}




  //draw mean point
  //glColor4f(1, 0, 0, 1);
  //drawCircle(meanPoint.x, meanPoint.y, TimePointRadius * 2);

  //Points convexhull = getConvexHull(band);
  ////float value = 1.0 - bandPercentage;
  ////colorgradient.getColorAtValue(value, red, green, blue);
  ////glColor4f(red, green, blue, 1);
  //glColor4f(1, 0, 0, 1);
  //glBegin(GL_LINE_LOOP);
  //for(int i = 0; i < convexhull.size(); i++){
  // glVertex3f(convexhull[i][0], convexhull[i][1], 0.0);
  //}
  //glEnd();

  //drawCGALContour(band);


  //for (pathIt = pathList.begin(); pathIt != pathList.end();){
  // if ((*pathIt)->getInt().w <= 0.0) {
  //  delete((*pathIt));
  //  pathIt = pathList.erase(pathIt);
  // }
  // else{
  //  pathIt++;
  // }
  //}
 }



 void advisory::drawCGALContour(Points star){
  double offset = 3;
  cout << star.size() << endl;
  boost::optional<double> margin = CGAL::compute_outer_frame_margin(star.begin(), star.end(), offset);
  if(margin){
   CGAL::Bbox_2 bbox = CGAL::bbox_2(star.begin(), star.end());
   double fxmin = bbox.xmin() - *margin;
   double fxmax = bbox.xmax() + *margin;
   double fymin = bbox.ymin() - *margin;
   double fymax = bbox.ymax() + *margin;

   Point_2 frame[4] = { Point_2(fxmin, fymin),
    Point_2(fxmax, fymin),
    Point_2(fxmax, fymax),
    Point_2(fxmin, fymax)};

   // Instantiate the skeleton builder
   SsBuilder ssb ;
   // Enter the frame
   ssb.enter_contour(frame,frame+4);
   // Enter the polygon as a hole of the frame (NOTE: as it is a hole we insert it in the opposite orientation)
   ssb.enter_contour(star.rbegin(),star.rend());
   // Construct the skeleton
   boost::shared_ptr<Ss> ss = ssb.construct_skeleton();
   // Proceed only if the skeleton was correctly constructed.
   if ( ss )
   {
    cout << "here" << endl;

    // Instantiate the container of offset contours
    ContourSequence offset_contours ;
    // Instantiate the offset builder with the skeleton
    OffsetBuilder ob(*ss);
    // Obtain the offset contours
    ob.construct_offset_contours(offset, std::back_inserter(offset_contours));
    // Locate the offset contour that corresponds to the frame
    // That must be the outmost offset contour, which in turn must be the one
    // with the largetst unsigned area.
    ContourSequence::iterator f = offset_contours.end();
    double lLargestArea = 0.0 ;
    for (ContourSequence::iterator i = offset_contours.begin(); i != offset_contours.end(); ++ i  )
    {
     double lArea = CGAL_NTS abs( (*i)->area() ) ; //Take abs() as  Polygon_2::area() is signed.
     if ( lArea > lLargestArea )
     {
      f = i ;
      lLargestArea = lArea ;
     }
    }
    // Remove the offset contour that corresponds to the frame.
    offset_contours.erase(f);
    cout << offset_contours.size() << " ++++"   << endl;
    drawCGALPolygons(offset_contours);
   }

  }
 }

 void advisory::drawCGALPolygon(CGAL::Polygon_2<K> const& poly){
  glColor4f(0, 1, 0, 1);
  glBegin(GL_LINE_LOOP);
  Polygon_2::Vertex_iterator vi;
  for(vi = poly.vertices_begin(); vi != poly.vertices_end(); vi++){
   Point_2 p = *vi;
   glVertex3f(p[0], p[1], 0.0);
  }
  glEnd();
 }

 void advisory::drawCGALPolygons(std::vector<boost::shared_ptr< CGAL::Polygon_2<K> > > const& polies){
  PolygonPtrVector::const_iterator pi;
  for(pi = polies.begin(); pi != polies.end(); ++pi)
   drawCGALPolygon(**pi);
 }


 void advisory::setMorphology(){
  morph *= -1;
 }


 double advisory::RBFKernelCus(double R, double dis, float f){
  double std = R / f;
  double part1 = pow(E, -pow(dis, 2) / (2 * pow(std, 2)));
  double part2 = cos(PI/2.0 * dis / (3 * std));
  double w = part1 * part2;
  return w;
 }

 double advisory::RBFKernelCus(Vector2d p0, Vector2d p1){
  double std = KernelRadius / kernelFactor;
  double dis = (p0 - p1).normsqr();
  double part1 = pow(E, -dis / (2 * pow(std, 2)));
  //double part1 = exp(-dis / (2 * Sqr(std)));
  double part2 = cos(PI/2.0 * dis / (kernelFactor * std));
  double w = part1 * part2;
  return w;
 }

 double advisory::RBFKernelGaussian(double dis, float f){
  //return exp(-Sqr(dis/f));
  //return exp(-pow((dis/f),2));
  return exp(-dis / Sqr(f));
 }

 long double advisory::RBFKernelGaussian(Vector2d c, Vector2d p){
  double r = (c-p).norm();
  return exp(-Sqr(r) / (2 * Sqr(kernelFactor)));
  //return exp(r * r / (kernelFactor * kernelFactor));
 }

 double advisory::RBFKernelThinPlateSpline(Vector2d c, Vector2d p){
  double dis = (c - p).norm();
  return Sqr(dis) * log(dis);
 }


 void advisory::translateToGeo(){
  for(int i = 0; i < pathList.size(); i++){
   for(int j = 0; j < pathList[i]->posList.size(); j++){
    //cout << "---" << endl;
    //cout << *(pathList[i]->latLonList[j]) << endl;
    //cout << *(pathList[i]->posList[j]) << endl; 
    Vector2d tmp = translateToGeoCoor(*(pathList[i]->posList[j]));
    //pathList[i]->posList[j] = new Vector2d(pathList[i]->latLonList[i]->x, pathList[i]->latLonList[i]->y);
    pathList[i]->posList[j] = new Vector2d(tmp.x, tmp.y);
    //cout << *(pathList[i]->posList[j]) << endl; 
   }
  }
 }

 void advisory::adjustKernelFactor(int mode){
  if(mode == 1)
   kernelFactor += 0.01;
  else if(mode == -1)
   kernelFactor -= 0.01;
 }

 std::vector<double> advisory::buildPDF(std::vector<double> vals, int size){
  std::vector<double> toReturn;
  std::vector<int> interval;
  interval.resize(size, 0);
  double delta = (maxdatadepth - mindatadepth) / size;
  for(int i = 0; i < vals.size(); i++){
   int idx = vals[i] / delta;
   interval[idx] ++;
  }

  for(int i = 0; i < interval.size(); i++){
   double f = double(interval[i]*1.0) / (vals.size() * 1.0);
   //double f = interval[i];
   toReturn.push_back(f);
  }
  return toReturn;
 }

 std::vector<double> advisory::buildPDF(std::vector<double> depthvalue, 
   std::vector<double> indexvalue, int size){
  std::vector<double> toReturn;
  std::vector<double> interval;
  interval.resize(size, 0);
  double delta = (maxdatadepth - mindatadepth) / size;
  for(int i = 0; i < depthvalue.size(); i++){
   int idx = depthvalue[i] / delta;
   interval[idx] += indexvalue[idx]; 
  }

  for(int i = 0; i < interval.size(); i++){
   //double f = double(interval[i]*1.0) / (vals.size() * 1.0);
   double f = interval[i];
   toReturn.push_back(f);
   cout << delta * i << "," << f << endl;
  }
  return toReturn;
 }



 std::vector<double> advisory::buildCDF(std::vector<double> pdf){
  std::vector<double> cdf;
  int size = pdf.size();
  cdf.resize(size, 0.0);
  double sum = 0.0;
  double a = mindatadepth, b = maxdatadepth;
  sum += pdf[0];
  for(int i = 1; i < size-1; i++){
   sum += 2 * pdf[i];
  }
  sum += pdf[size - 1];
  double totalArea = (b - a) / (2 * size) * sum;

  int k = 1;
  sum = 0.0;
  for(int i = 1; i < size; i++){
   double av = a + ((k-1) * (1.0/pdf.size())) * (b - a);
   double bv = a + (k * (1.0/pdf.size())) * (b - a);
   double fa = pdf[i];
   double fb = pdf[i-1];
   double area = (bv-av) * ((fa+fb)/2.0);
   area = area / totalArea;
   sum += area;
   cdf[i] = sum;
   k++;
  }
  return cdf;
 }

 double advisory::getValueFromCDF(std::vector<double> cdf, double val, 
   int intervalsize){
  double delta = (maxdatadepth - mindatadepth) / intervalsize;
  int idx = 0;
  for(int i = 0; i < cdf.size(); i++){
   if(cdf[i] > val){
    idx = i;
    break;
   }
  }
  int pre = idx - 1;
  int suc = idx;
  double preY = cdf[pre];
  double sucY = cdf[suc];
  double preX = delta * pre;
  double sucX = delta * suc;
  //double X = (val - preY) / (sucY - preY) * (sucX - preX) + preX;
  double h = (val - preX) / (sucX - preX) * (sucY - preY);
  h = preY + h;


  //for(int i = 0; i < cdf.size(); i++)
  // cout << i << ": " << cdf[i] << endl;

  cout << "------" << endl;
  cout << delta << " " << pre << " " << suc << endl;
  cout << preY << " " << val << " " << sucY << endl;
  cout << preX << " " << h << " " << sucX << endl;
  return h;
 }

 //double advisory::getValueFromPDF(std::vector<double> pdf, double val){
 // double toReturn;
 // double totalArea = 0.0;
 // for(int i = 0; i < pdf.size(); i++){
 //  totalArea += pdf[i];
 // }
 // int size = pdf.size();
 // double prevsum(0.0), sum(0.0);
 // int k = 0;
 // for(int i = 0; i < size && size > 0; i++){
 //  prevsum = sum;
 //  sum += pdf[i];
 //  if(sum > val)
 //   break;
 //  k++;
 // }
 //
 // double newVal = val * totalArea;
 // double areaDif = sum - prevsum;
 // double areaRatio = sum - newVal;
 // if(areaDif != 0){
 //  areaRatio = areaRatio / areaDif;
 // }
 // else
 //  areaRatio = 0.0;
 //
 // return toReturn;
 //}


 void advisory::readMeanTrack(const char* filename){
  ifstream data(filename);
  if(!data.is_open()){
   cerr << "Bad file!" << endl;
   exit(-1);
  }
  std::vector<Vector2d> pos;
  float time, lat, lon;
  while(!data.eof()){
   data >> time >> lon >> lat;
   Vector2d temp = translateToScreen(lon, lat);
   pos.push_back(temp);
  }

  glColor4f(1, 0, 0, 1);
  glBegin(GL_LINE_STRIP);
  for(int i = 0; i < pos.size(); i++){
   glVertex3f(pos[i].x, pos[i].y, 0.0);
  }
  glEnd();

 }

 /*
    std::vector< std::vector<Vector2d> > advisory::readNHCTracks(const char* filename){
    int t = 0;
    t = userInt->getSliderCurTick();
    t = max(t, 0);
    pathList.clear();
    ifstream data(filename);
    if(!data.is_open()){
    cerr << "Bad file!" << endl;
    exit(-1);
    }
    std::vector< std::vector<Vector2d> > plist;
    std::vector<Vector2d> pos;
    std::vector<int> randList;
    for(int i = 0; i < 200; i++){
    srand(i+1);
    randList.push_back(rand() % 1000);
    }
    int pathNum = 0;
    int total = 0;
    float htime, lat, lon;
    while(!data.eof()){
    data >> htime >> lon >> lat;
    Vector2d temp = translateToScreen(lon, lat);
    if(htime < 72 && int(htime) % 3 == 0)
    pos.push_back(temp);
    if(htime == 120){
    pathNum += 1;
    plist.push_back(pos);
    pos.clear();
    }
    }
    std::vector< std::vector<Vector2d> > rplist;
    for(int i = 0; i < randList.size(); i++){
    rplist.push_back(plist[randList[i]]);
    }
    plist.clear();
    plist = rplist;

    for(int i = 0; i < plist.size(); i++)
    pathList.push_back(new path(0, 0, 0, 0, 0));

    for(int i = 0; i < plist.size(); i++){
    pathList[i]->posList.clear();
    for(int j = 0; j < plist[i].size(); j++){
    Vector2d temp = plist[i][j];
    Vector2d* toPush = new Vector2d(temp.x, temp.y);
    pathList[i]->posList.push_back(toPush);
    }
    }

 //  for(int i = 0; i < pathList.size() - 1; i++){
 //  glBegin(GL_LINE_STRIP);
 //  for(int j = 0; j < pathList[i]->posList.size(); j++){
 //  Vector2d* temp = pathList[i]->posList[j];
 //  glVertex3f(temp->x, temp->y, 0.0);
 //  }
 //  glEnd();
 //  }

 //for(int i = 0; i < pathList.size() - 1; i++){
 // Vector2d* temp = pathList[i]->posList[t];
 // glColor4f(1, 0, 0, 1);
 // drawCircle(temp->x, temp->y, .5 * TimePointRadius);
 //}

 return plist;
 }
 */

 std::vector< std::vector<float> > advisory::readNHCTracks(const string& filename, int framenum){
  cout << filename << endl;
  int t = 0;
  t = userInt->getSliderCurTick();
  t = max(t, 0);
  pathList.clear();
  ifstream data(filename);
  if(!data.is_open()){
   cerr << "Bad file!" << endl;
   exit(-1);
  }
  std::vector< std::vector<Vector2d> > plist;
  std::vector<Vector2d> pos;
  std::vector<int> randList;
  //for(int i = 0; i < 200; i++){
  // srand(i+1);
  // randList.push_back(rand() % 450);
  //}
  for(int i = 0; i < 450; i += 2){
   randList.push_back(i);
  }
  int pathNum = 0;
  int total = 0;
  float htime, lat, lon;
  Vector2d pretemp;
  while(!data.eof()){
   data >> htime >> lon >> lat;
   Vector2d temp = translateToScreen(lon, lat);

#ifdef NHC_DATA
   if(htime >= 0 && htime <= 72 && (htime - int(htime)) == 0 && int(htime) % 12 == 0){
    //if(htime >= 0 && htime <= 69){
    //Vector2d itemp = (pretemp + temp) * 0.5;
    //pos.push_back(itemp);
    pos.push_back(temp);
   }
#else
   if(htime >= 0 && htime < 69 && int(htime) % 12 == 0){
    //Vector2d itemp = (pretemp + temp) * 0.5;
    //pos.push_back(itemp);
    pos.push_back(temp);
   }
   if(htime == 69)
    pos.push_back(temp);
#endif

#ifdef NHC_DATA
   if(htime ==72){
    pathNum += 1;
    plist.push_back(pos);
    pos.clear();
   }
#else
   //if(htime == 120){
   if(htime ==69){
    pathNum += 1;
    plist.push_back(pos);
    pos.clear();
   }
#endif


   pretemp = temp;
   }

   //std::vector< std::vector<Vector2d> > rplist;
   //for(int i = 0; i < randList.size(); i++){
   // rplist.push_back(plist[randList[i]]);
   //}
   //plist.clear();
   //plist = rplist;

   for(int i = 0; i < plist.size(); i++)
    pathList.push_back(new path(0, 0, 0, 0, 0));

   for(int i = 0; i < plist.size(); i++){
    pathList[i]->posList.clear();
    for(int j = 0; j < plist[i].size(); j++){
     Vector2d temp = plist[i][j];
     Vector2d* toPush = new Vector2d(temp.x, temp.y);
     pathList[i]->posList.push_back(toPush);
    }
   }

   pathList.pop_back();

   ofstream myfile("CDFSPACE/PATH.txt", fstream::out);
   glLineWidth(0.8);
   glColor4f(0, 0, 0.7, 0.2);
   std::vector< std::vector<float> > pathNode;
   for(int i = 0; i < pathList.size(); i++){
    glBegin(GL_LINE_STRIP);
    std::vector<float> nodes;
    nodes.clear();
    //cout << "points: " << pathList[i]->posList.size() << endl;
    for(int j = 0; j < pathList[i]->posList.size(); j++){
     Vector2d* temp = pathList[i]->posList[j];
     //glVertex3f(temp->x, temp->y, 0.0);
     nodes.push_back(temp->x);
     nodes.push_back(temp->y);
     myfile << temp->x << " " << temp->y << " ";
    }
    //cout << nodes.size() << endl;
    pathNode.push_back(nodes);
    glEnd();
    myfile << endl;
   }
   myfile.close();

   for(int i = 0; i < pathList.size() - 1; i++){
    Vector2d* temp = pathList[i]->posList[framenum];
    glColor4f(0, 0, 1, 1);
    //cout << *temp << endl;
    //drawSolidCircle(temp->x, temp->y, 1 * TimePointRadius);
   }

   //cout << "Num of path of NHC's prediction: " << pathList.size() << endl;
   //return plist;
   return pathNode;
   }

   bool advisory::outputImage(){
    return opImage;
   }

   std::vector<Vector2d*> advisory::buildDensityGrid(float minX, float minY, float maxX, float maxY, float& dw, float& dh){
    std::vector<Vector2d*> toReturn;
    float gwidth  = abs(maxX - minX);
    float gheight = abs(maxY - minY);
    dw = gwidth  / DensityGridCols;
    dh = gheight / DensityGridRows;

    float w, h, pw, ph;
    for(int i = 1; i <= DensityGridCols; i++){
     w  = dw * i;
     pw = dw * (i-1);
     h  = dh * i;
     ph = dh * (i - 1);
     for(int j = 1; j <= DensityGridRows; j++){
      w  = dw * j;
      pw = dw * (j-1);
      Vector2d* toPush = new Vector2d[4];
      Vector2d urc(w, ph);
      Vector2d ulc(pw, ph);
      Vector2d llc(pw, h);
      Vector2d lrc(w, h);
      llc = Vector2d(minX, minY) + llc;
      ulc = Vector2d(minX, minY) + ulc;
      urc = Vector2d(minX, minY) + urc;
      lrc = Vector2d(minX, minY) + lrc;
      //if(i == 1){
      // glColor4f(0, 0, 1, 1);
      // glBegin(GL_LINE_LOOP);
      // glVertex3f(llc.x, llc.y, 0);
      // glVertex3f(ulc.x, ulc.y, 0);
      // glVertex3f(urc.x, urc.y, 0);
      // glVertex3f(lrc.x, lrc.y, 0);
      // glEnd();
      //}
      toPush[0] = llc;
      toPush[1] = ulc;
      toPush[2] = urc;
      toPush[3] = lrc;
      toReturn.push_back(toPush);
     }
    }
    return toReturn;
   }

   void advisory::outputTracks(char* filename){
    ofstream myfile(filename, fstream::out);
    for(int i = 0; i < pathList.size(); i++){
     //cout << pathList[i]->posList.size() << endl; 
     for(int j = 0; j < pathList[i]->latLonList.size(); j++){
      int time = 3 * j;
      Vector2d pos(pathList[i]->latLonList[j]->x, pathList[i]->latLonList[j]->y);
      myfile << time << " " << pos.x << " " << pos.y << endl;

     }
    }
    myfile.close();

   }


   std::vector<double> advisory::buildRBFRadiusVector(std::vector<Vector2d> pos, 
     RBF densityRBF, double base){
    std::vector<double> toReturn;
    float red, green, blue;
    ColorGradient colorgradient;
    for(int i = 0; i < pos.size(); i++){
     double f = computeRBFradiusFromDensity(densityRBF, pos[i], base);
     toReturn.push_back(f);
    }
    return toReturn;
   }

   double advisory::computeRBFradiusFromDensity(RBF densityRBF, Vector2d c, double base){
    double df = densityRBF.eval(c);
    // normalize to [0.1, 0.7]
    //double f = (1.0 - df) * 0.7 + df * 0.90;
    //f = (1.0 - f) *  base;

    //float f = df * 1;
    //f = GaussianDis(1.0 - f, 0.2, 0);
    //f = 1.0 - f;
    //if(f < 0.3)
    // f = 0.3;
    //if(f > 0.7)
    // f = 0.7;
    //f *= base * 0.28;

    float f = df * 3;
    float lamda = 1.0;
    f = lamda * exp(-lamda * f);
    //f = 1.0 - f;
    if(f <  0.2)
     f = 0.2;
    if(f > 0.6)
     f = 0.6;
    f *= base * 0.35;

    return f;
   }

   float advisory::GaussianDis(float x, float sigma, float miu){
    sigma = pow(0.2, 0.5);
    miu  = 0;
    float d = x - miu;
    return (1.0 / (sigma * pow(2 * PI, 0.5)) * exp(-(d * d) / (2 * 0.2))); 

   }

   std::vector<double> advisory::buildRBFRadii(std::vector<Vector2d> pos, 
     std::vector<double> densityList, std::vector<int> indicesList, 
     double width, 
     double factor){
    std::vector<double> toReturn;
    float rmax(0.0), rmin(0.0), dmax(0.0), dmin(0.0);
    rmax = 1024 / 16.0 / 3.16 * 0.7;
    rmin = width * 0.1;
    std::vector<double> tmp = densityList;
    sort(tmp.begin(), tmp.end());
    for(int i = 0; i < tmp.size(); i++){
     if(tmp[i] != 0.0){
      dmin = tmp[i];
      break;
     }
    }
    //dmax = *max_element(densityList.begin(), densityList.end());
    //dmin = *min_element(densityList.begin(), densityList.end()); 
    dmax = tmp[int(tmp.size() * 0.98)];
    float K(0), P(0);
    //computeRadCurve(rmin, rmax, dmin, dmax, K, P);
    for(int i = 0; i < pos.size(); i++){
     int idx = indicesList[i];
     double density = densityList[idx];
     double f = sqrt(density);
     f = width / f * factor; 
     //f = width / density * factor; 
     //f = K / pow(density, P);
     //f = width / density * factor; 
     //f = width / (density * density) * factor;
     //cout << i << " " << idx << " " << density << " " << f << " " << endl;
     toReturn.push_back(f);
    }
    return toReturn;
   }

   void advisory::computeRadCurve(float rmin, float rmax, float dmin, float dmax, 
     float& K, float& P){
    // K / pow(dmin, p) = rmax
    // K / pow(dmax, p) = rmin
    // log(K) - P(log(dmin)) = log(rmax)
    // log(K) - P(log(dmax)) = log(rmin)
    float a(log(dmin)), b(log(rmax)), c(log(dmax)), d(log(rmin));
    P = (b-d) / (c-a);
    K = exp(b + P * a);
    //cout << "a & b & c & d: " << a << " " << b << " " << c << " " << d << endl;
    //cout << "K & P: " << K << " " << P << endl;
    //cout << "dmax & dmin: " << dmax << " " << dmin << endl;
    //cout << "rmax: " << rmax << " " <<  K / pow(dmin, P) << endl;
    //cout << "rmin: " << rmin << " " <<  K / pow(dmax, P) << endl;
   }

   void advisory::removeDuplication(float dw, int time){
    std::vector<int> removeFlags;
    removeFlags.resize(pathList.size(), 0);
    //for(int i = 0; i < pathList.size(); i++){
    // pathList[i]->removeFlag = 0;
    //}
    for(int i = 0; i < pathList.size() - 1; i++){
     Vector2d p0 = *(pathList[i]->posList[time]);
     for(int j = i+1; j < pathList.size(); j++){
      Vector2d p1 = *(pathList[j]->posList[time]);
      double f = (p0 - p1).norm();
      if(f < dw * 0.001){
       removeFlags[i] += 1;
       continue;
      }
     }
    }

    std::vector<path*> tmp;
    for(int i = 0; i < pathList.size(); i++){
     if(removeFlags[i] == 0){
      tmp.push_back(pathList[i]);
     }
    }

    pathList.clear();
    pathList = tmp;

    //for(int i = 0; i < pathList.size(); i++){
    // Vector2d p = *(pathList[i]->posList[time]);
    // glColor4f(1, 0, 0, 1);
    // drawCircle(p.x, p.y, TimePointRadius);
    //}

    //cout << pathList.size() << endl;
   }

   float advisory::IDW(std::vector<Vector2d> trainingSet, std::vector<double> vals, Vector2d X){
    float p = 2;
    float toReturn = 0;
    float sum0 = 0;
    float sum1 = 0;
    for(int i = 0; i < trainingSet.size(); i++){
     Vector2d C = trainingSet[i];
     float d = (X - C).norm();
     if(d == 0){
      toReturn = vals[i]; 
      return toReturn;
     }
     else{
      float w = 1.0 /  pow(d, p);
      sum0 += w * vals[i];
      sum1 += w;
     }
    }
    toReturn = sum0 / sum1;
    return toReturn;
   }

   void advisory::fillConvexHull(int width, int height, std::vector<Points> convexhulls){
    std::vector<Edges> cedges;
    std::vector<Vertices> cvertices;
    std::vector<Vector3d> edges;
    std::vector<Vector3d> vertices;

    for(int i = 0; i < convexhulls.size(); i++){
     edges.clear();
     vertices.clear();
     Points convexhull = convexhulls[i];
     for(int j = 1; j < convexhull.size(); j++){
      Vector3d edge = Vector3d(convexhull[j][0], convexhull[j][1], 0.0) 
       - Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0);
      edges.push_back(edge);
      vertices.push_back(Vector3d(convexhull[j-1][0], convexhull[j-1][1], 0.0));
     }
     Vector3d edge = Vector3d(convexhull[0][0], convexhull[0][1], 0.0) - 
      Vector3d(convexhull[convexhull.size()-1][0], convexhull[convexhull.size()-1][1], 0.0);
     edges.push_back(edge);
     vertices.push_back(Vector3d(convexhull[convexhull.size()-1][0], 
        convexhull[convexhull.size()-1][1], 0.0));
     cedges.push_back(edges);
     cvertices.push_back(vertices);
    }

    std::vector<int> drawflags;
    int counter = -1;
    drawflags.resize(2 * width * 2 * height, -1);
    for(int i = -width/2; i < 1.5 * width; i++){
     for(int j = -height/2; j < 1.5 * height; j++){
      counter ++;
      int idx = j * width + i;
      Vector3d C(i, j, 0.0);
      for(int k = cedges.size()-1; k >= 0 ; k--){
       edges.clear();
       vertices.clear();
       edges = cedges[k];
       vertices = cvertices[k];
       if(drawflags[counter] == -1){
        int inside = 0;
        for(int t = 0; t < edges.size(); t++){
         Vector3d N = C - vertices[t];
         N = N % edges[t];
         if(N.z > 0){
          inside = 1;
          break;
         }
        }
        if(inside == 0){
         drawflags[counter] = 0;
         glColor4f(bandColors[2 - k].x, bandColors[2 - k].y, bandColors[2 - k].z, bandColorOpc[2 - k]);
        }
        else{
         glColor4f(0, 0, 0, 0);
        }
       }
      }

      glBegin(GL_POINTS);
      glVertex3f(C.x, C.y, C.z);
      glEnd();
     }
    }


   }


   void advisory::findConcaves(Points points, std::vector<Segment> &segments){
    Alpha_shape_2 A(points.begin(), points.end(), FT(ALPHA), Alpha_shape_2::GENERAL);
    segments.clear();
    alpha_edges(A, std::back_inserter(segments));
   }

   void advisory::findEllipse(Points convexhull, Vector2d& center, Vector2d& axes,
     float& theta){
    float m00(0), m01(0), m10(0), m11(0), m20(0), m02(0);
    for(int i = 0; i < convexhull.size(); i++){
     float x = convexhull[i][0];
     float y = convexhull[i][1];
     m00 += 1;
     m10 += x;
     m01 += y;
     m11 += x * y;
     m20 += x * x;
     m02 += y * y;
    }
    float xc = m10 / m00;
    float yc = m01 / m00;
    float a = m20 / m00 - xc * xc;
    float b = 2 * (m11 / m00 - xc * yc);
    float c = m02 / m00 - yc * yc;
    center.x = xc;
    center.y = yc;
    theta = 0.5 * atan2(b, a - c);
    //theta = RadToDeg(theta);
    float w = sqrt(3 * (a + c - sqrt(b * b + (a - c) * (a - c))));
    float l = sqrt(3 * (a + c + sqrt(b * b + (a - c) * (a - c))));
    axes.x = l;
    axes.y = w;
   }


   void advisory::setPixel(int x, int y){
    glColor4f(1, 0, 0, 1);
    glBegin(GL_POINTS);
    glVertex3f(x, y, 0);
    glEnd();
   }

   void advisory::ellipsePlotPoints(int xc, int yc, int x, int y){
    setPixel(xc + x, yc + y);
    setPixel(xc - x, yc + y);
    setPixel(xc + x, yc - y);
    setPixel(xc - x, yc - y);
   }


   void advisory::drawEllipse(int xCenter, int yCenter, int Rx, int Ry){
    int Rx2 = Rx*Rx;
    int Ry2 = Ry*Ry;
    int twoRx2 = 2 * Rx2;
    int twoRy2 = 2 * Ry2;
    int p;
    int x = 0;
    int y = Ry;
    int px = 0;
    int py = twoRx2 * y;

    ellipsePlotPoints(xCenter, yCenter, x, y);

    /* For Region 1 */
    p = ROUND(Ry2 - (Rx2*Ry) + (0.25) * Rx2);
    while(px < py){
     x++;
     px += twoRy2;
     if(p < 0){
      p += Ry2 + px;
     }else{
      y--;
      py -= twoRx2;
      p += Ry2 + px - py;
     }
     ellipsePlotPoints(xCenter, yCenter, x, y);
    }

    /* For Region 2*/
    p = ROUND(Ry2 * (x + 0.5)*(x + 0.5) + Rx2 * (y - 1)*(y - 1) - Rx2 * Ry2);
    while(y > 0){
     y--;
     py -= twoRx2;
     if(p > 0){
      p += Rx2 - py;
     }else{
      x++;
      px += twoRy2;
      p += Rx2 - py + px;
     }
     ellipsePlotPoints(xCenter, yCenter, x, y);
    }
   }


   void advisory::drawEllipses(std::vector<QRegion> rs, int width, int height, 
     std::vector<Vector2d> Cs, std::vector<float> thetas){
    std::vector<int> flags;
    std::vector<Points> points;
    points.resize(3);
    flags.resize(2 * width * 2 * height, -1);
    int counter = -1;
    for(int i = -width/2; i < 1.5 * width; i++){
     for(int j = -height/2; j < 1.5 * height; j++){
      QPoint p(i, j);
      counter ++ ;
      int idx = j * width + i;
      for(int k = 0; k < rs.size(); k++){
       if(rs[k].contains(p) && flags[counter] == -1){
        Vector2d temp(i, j);
        temp = temp - Cs[k];
        Matrix2x2 RM(cos(thetas[k]), -sin(thetas[k]),
          sin(thetas[k]),  cos(thetas[k]));
        temp = RM * temp;
        temp = temp + Cs[k];

        points[k].push_back(Point_2(temp.x, temp.y));

        //glColor4f(bandColors[k].x, bandColors[k].y, bandColors[k].z, bandColorOpc[k]);
        //glMatrixMode(GL_MODELVIEW);
        //glLoadIdentity();
        //glPushMatrix();
        //glTranslatef(Cs[k].x, Cs[k].y, 0.0);
        //glRotatef(thetas[k], 0, 0, 1);
        //glTranslatef(-Cs[k].x, -Cs[k].y, 0.0);
        //glPointSize(2.0);
        //glBegin(GL_POINTS);
        //glVertex3f(i, j, 0.0);
        //glEnd();
        //glPopMatrix();

        flags[counter] == 1;
       }
      }
     }
    }

    std::vector<Points> convexhulls;
    convexhulls.resize(3);
    for(int i = rs.size() - 1; i >= 0; i--){
     convexhulls[i] = getConvexHull(points[2-i]);
    }
    fillConvexHull(width, height, convexhulls);
   }

   void advisory::writeEllipseToFile(std::vector<Vector2d> Cs, std::vector<float> thetas, 
     std::vector<Vector2d> axisList, int i){
    char filename[100];
    string str = OutputPrefix + "Ellipses/ellipse.%d.bin";
    const char* cstr = str.c_str();
    sprintf(filename, cstr, i);
    ofstream myfile(filename, fstream::out);

    for(int j = 0; j < Cs.size(); j++){
     myfile << Cs[j].x << " " << Cs[j].y << " " << thetas[j] << " " 
      << axisList[j].x << " " << axisList[j].y << endl;
    }
    myfile.close();
    cout << "Finish write ellipse to file: " << filename << endl;
    if(writeCounter == 1){
     //exit(0);
    }
    writeCounter ++;
   }

   void advisory::renderEllipsesFromFile(){
   }

   void advisory::renderEllipseFromFile(int i, int width, int height){
    char filename[100];
    string str = OutputPrefix + "Ellipses/ellipse.%d.bin";
    const char* cstr = str.c_str();
    sprintf(filename, cstr, i);
    cout << filename << endl;
    std::vector<Vector2d> Cs;
    std::vector<float> thetas;
    std::vector<Vector2d> axisList;
    ifstream data(filename);
    while(!data.eof()){
     Vector2d C;
     float theta;
     Vector2d axis;
     data >> C.x >> C.y >> theta >> axis.x >> axis.y;  
     Cs.push_back(C);
     thetas.push_back(theta);
     axisList.push_back(axis);
    }

    Cs.pop_back();
    thetas.pop_back();
    axisList.pop_back();

    std::vector<QRegion> qregions;
    for(int j = 0; j < Cs.size(); j++){
     cout << Cs[j] << " " << thetas[j] << " " << axisList[j] << endl;
     Vector2d C = Cs[j];
     Vector2d axis = axisList[j];
     float theta = thetas[j];
     QRegion ER(QRect(C.x - axis.x, C.y - axis.y, 2 * axis.x, 2 * axis.y), QRegion::Ellipse);
     qregions.push_back(ER);
    }

    drawEllipses(qregions, width, height, Cs, thetas);
    drawExperimentPoints();

    opImage = true;

   }

   void advisory::writeRBFdata(std::vector<Vector2d> data, std::vector<double> values, 
     std::vector<double> radii){
    char filename[100];
    sprintf(filename, "NHCData/RBFdata.txt");
    ofstream myfile(filename, fstream::out);
    int size = data.size();
    for(int i = 0; i < size; i++){
     myfile << data[i].x << " " << data[i].y << " " << values[i] << " " << radii[i] << endl;
    }
    myfile.close();
    cout << "finish writing " << data.size() << " RBF data." << endl;
    exit(0);
   }

   void advisory::writeDensityData(std::vector<Vector2d> P, std::vector<double> density,
     const int frameid){
    char filename[100];
    sprintf(filename, "CDFSPACE/knnsamples.txt", frameid);
    ofstream myfile(filename, fstream::out);
    int size = P.size();
    for(int i = 0; i < size; i++){
     //myfile << P[i].x << " " << P[i].y << " " << density[i] << endl;
     myfile << P[i].x << " " << P[i].y << endl;
    }
    myfile.close();
    cout << "finish writing " << P.size() << " densities to " << filename << endl;
   }

   //std::vector<double> advisory::KDTreeDensity(std::vector<Vector2d> data, int size, int dim,
   //  std::vector<int>& validlist, float threshold)
   //{
   // int K = 10;
   // KDTree kdtree;
   // kdtree.buildKDTree(kdtree.formatInput< std::vector<Vector2d> >(data, data.size(), dim));
   // std::vector<double> density;
   // density.resize(data.size(), 0);
   // validlist.clear();
   // validlist.resize(data.size(), 1);
   // for(int i = 0; i < size; i++)
   // {
   //  if(validlist[i] != 0)
   //  {
   //   Point* p = kdtree.createPoint(dim);
   //   for(int j = 0; j < dim; j++)
   //    p->coord[j] = data[i][j];
   //   
   //   // 11 is the K value for KNN
   //   // 11 = 10 + 1, the last element in the queue is the testing point
   //   // maybe 1/100 of the size of the training set
   //   bounded_priority_queue<QueueTuple> bpq = kdtree.knearest(p, K+1);
   //   //bounded_priority_queue<QueueTuple> tmp(bpq);
   //
   //   float maxR(0);
   //   // merge points too close
   //   while(!bpq.queue_.empty())
   //   {
   //    float r = bpq.queue_.top().first;
   //    maxR = max(maxR, r);
   //    if(r < threshold * threshold)
   //    {
   //     // if a point is too close to the query
   //     // set its valid flag to 0
   //     int idx = 10;
   //     Point* p_ = bpq.queue_.top().second;
   //     idx = p_->index;
   //     if(idx != i)
   //      validlist[idx] = 0;
   //    }
   //    bpq.queue_.pop();
   //   }
   //
   //   float area = PI * maxR;
   //   float d = K / area;
   //   density[i] = d;
   //  }
   // }
   // 
   // //normalize density to [0, 1];
   // float max_den = *(max_element(density.begin(), density.end()));
   // for(int i = 0; i < density.size(); i++)
   //  density[i] /= max_den;
   //
   // return density;
   //}


   std::vector< std::vector<float> > advisory::writeGeneratedPath(){
    ofstream myfile("CDFSPACE/MYPATH.txt", fstream::out);
    glLineWidth(0.8);
    glColor4f(0, 0, 0.7, 0.2);
    std::vector< std::vector<float> > pathNode;
    for(int i = 0; i < pathList.size(); i++){
     glBegin(GL_LINE_STRIP);
     std::vector<float> nodes;
     nodes.clear();
     cout << "points: " << pathList[i]->posList.size() << endl;
     for(int j = 0; j < 23; j += 4){
      Vector2d* temp = pathList[i]->posList[j];
      glVertex3f(temp->x, temp->y, 0.0);
      nodes.push_back(temp->x);
      nodes.push_back(temp->y);
      myfile << temp->x << " " << temp->y << " ";
     }
     cout << nodes.size() << endl;
     pathNode.push_back(nodes);
     glEnd();
     myfile << endl;
    }
    myfile.close();
    cout << "draw MY lines" << endl;
    cout << pathNode.size() << endl;

    //cout << "Num of path of NHC's prediction: " << pathList.size() << endl;
    //return plist;
    if(pathList.size() == 999)
     exit(0);
    return pathNode;
   }

   bool sortVectors(Vector2d l, Vector2d r)
   {
    return (l.y > r.y);
   }

   bool sortVector3d(Vector3d l, Vector3d r)
   {
    return (l.z < r.z);
   }

   void advisory::testSortPath()
   {
    char* buff = new char[50];
    std::vector< std::vector<Vector2d> > samples;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToGeoCoor(*pathList[0]->posList[0]));
    std::vector< std::vector<Points> > bands;
    Vector2d start(-85.9, 26.1);
    drawCircle(translateToScreen(start.x, start.y).x, translateToScreen(start.x, start.y).y, TimePointRadius);
    for(int i = 1; i <= 6; i++)
    {
     sprintf(buff, "NHCData/OLS-selected-%d.txt", i);
     std::vector<Vector2d> p = CDFSample::readTestSamples(buff);
     samples.push_back(p);
    }
    for(int i = 1; i <= 6; i++)
    {
     PathCBD pathCBD(pathList, M);
     std::vector<double> dataDepthValue = pathCBD.fastSegmentSimplexDepth(i);
     std::vector<int> sortSegmentIdx = pathCBD.sortSegmentIdx(dataDepthValue);
     Vector2d c = translateToGeoCoor(*pathList[sortSegmentIdx[0]]->posList[i]);
     centerline.push_back(c);

     std::vector<Points> allbands;
     Points band;

     for(int j = 0; j < 10; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 10; j < 20; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 20; j < 30; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 30; j < 40; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 40; j < 50; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 50; j < 100; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 100; j < 200; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 200; j < 300; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 300; j < 400; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 400; j < 500; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 500; j < 600; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 600; j < 700; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 700; j < 800; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     for(int j = 800; j < 900; j++)
     {
      Vector2d tmp  = translateToGeoCoor(*pathList[sortSegmentIdx[j]]->posList[i]);
      band.push_back(Point_2(tmp.x, tmp.y));
     }
     band.push_back(Point_2(-85.9, 26.1));
     allbands.push_back(band);
     band.clear();

     bands.push_back(allbands);
    }

    std::vector<Points> convexhulls;
    std::vector< std::vector<Segment> > contours;

    for(int i = 0; i < bands[0].size(); i++)
    {
     Points tmp;
     tmp.clear();
     for(int j = 0; j < 6; j++)
     {
      for(int k = 0; k < bands[j][i].size(); k++)
       tmp.push_back(bands[j][i][k]);
     }
     convexhulls.push_back(getConvexHull(tmp));
     cout << tmp.size() << endl;

     //Alpha_shape_2 A(tmp.begin(), tmp.end(), FT(2), Alpha_shape_2::GENERAL);
     //std::vector<Segment> segments;
     //segments.clear();
     //alpha_edges(A, std::back_inserter(segments));
     //contours.push_back(segments);
     //cout << "optimal " << i << " :" << *A.find_optimal_alpha(1) << endl;
     //break;
    }

    for(int i = 0; i < convexhulls.size(); i++)
    {
     glBegin(GL_LINE_LOOP);
     for(int j = 0; j < convexhulls[i].size(); j++)
     {
      Vector2d draw = translateToScreen(convexhulls[i][j][0], convexhulls[i][j][1]);
      //drawCircle(draw[0], draw[1], TimePointRadius);
      glVertex3f(draw.x, draw.y, 0);
     }
     glEnd();
    }

    glBegin(GL_LINE_STRIP);
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d toDraw = translateToScreen(centerline[i].x, centerline[i].y);
     glVertex3f(toDraw.x, toDraw.y, 0.0);
    }
    glEnd();

    //for(int i = 0; i < contours.size(); i++)
    //{
    // for(int j = 0; j < contours[i].size(); j++)
    // {
    //  Point_2 p0 = contours[i][j][0];
    //  Point_2 p1 = contours[i][j][1];
    //  Vector2d draw1 = translateToScreen(p0[0], p0[1]);
    //  Vector2d draw2 = translateToScreen(p1[0], p1[1]);
    //  glColor4f(1, 0, 0, 1);
    //  glBegin(GL_LINES);
    //  glVertex3f(draw1.x, draw1.y, 0);
    //  glVertex3f(draw2.x, draw2.y, 0);
    //  glEnd();
    // }
    //}

   }

   void advisory::pathSampling()
   {
    int npaths = 50;
    char* buff = new char[50];
    std::vector< std::vector<Vector2d> > samples;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToGeoCoor(*pathList[0]->posList[0]));
    std::vector< std::vector<Points> > bands;
    Vector2d start(-85.9, 26.1);
    drawCircle(translateToScreen(start.x, start.y).x, translateToScreen(start.x, start.y).y, TimePointRadius);
    for(int i = 1; i <= 6; i++)
    {
     sprintf(buff, "NHCData/OLS-selected-%d.txt", i);
     std::vector<Vector2d> p = CDFSample::readTestSamples(buff);
     samples.push_back(p);
    }

    pathList.clear();
    for(int i = 0; i < npaths; i++)
    {
     pathList.push_back(new path(0, 0, 0, 0, 0));
     pathList[i]->posList.clear();
     pathList[i]->latLonList.clear();
    }
    for(int i = 0; i < npaths; i++)
    {
     Vector2d* toPush;
     //toPush = new Vector2d(-85.9, 26.1);
     //pathList[i]->latLonList.push_back(toPush);
     //toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
     //pathList[i]->posList.push_back(toPush);
     for(int j = 0; j < samples.size(); j++)
     {
      toPush = new Vector2d(samples[j][i]);
      pathList[i]->latLonList.push_back(toPush);
      toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
      pathList[i]->posList.push_back(toPush);
     }
    }

    PathCBD pathCBD(pathList, M);
    std::vector<double> dataDepthValue;
    dataDepthValue.resize(npaths);
    for(int i = 0; i < 1; i++)
    {
     std::vector<double> tmpdataDepthValue = pathCBD.fastSegmentSimplexDepth(3);
     for(int j = 0; j < npaths; j++)
      dataDepthValue[j] += tmpdataDepthValue[j]; 
    }
    std::vector<int> sortSegmentIdx = pathCBD.sortSegmentIdx(dataDepthValue);

    std::vector<path*> goodpaths;
    goodpaths.push_back(pathList[sortSegmentIdx[0]]);
    std::vector<path*> goodleftpaths;
    goodleftpaths.push_back(pathList[sortSegmentIdx[0]]);
    std::vector<path*> goodrightpaths;
    goodrightpaths.push_back(pathList[sortSegmentIdx[0]]);

    std::vector<path*> leftpaths;
    std::vector<path*> rightpaths;
    std::vector<int>   leftsd;
    std::vector<int>   rightsd;
    for(int i = 0; i < sortSegmentIdx.size(); i++)
    {
     int idx = sortSegmentIdx[i];
     Vector2d p  = *pathList[idx]->posList[0];
     Vector2d v0 = *goodpaths[0]->posList[0];
     Vector2d v1 = *goodpaths[0]->posList[1];
     Vector2d e0 = v0 - p;
     Vector2d e1 = v1 - p;
     Vector3d dir = e0 % e1;
     dir = dir / dir.norm();
     if(dir[2] == -1)
     {
      leftpaths.push_back(pathList[idx]);
      leftsd.push_back(sortSegmentIdx.size() - i);
     }
     else if(dir[2] == 1)
     {
      rightpaths.push_back(pathList[idx]);
      rightsd.push_back(sortSegmentIdx.size() - i);
     }
    }

    std::vector<int> leftintervals;
    std::vector<int> rightintervals;
    std::vector<int> goodleftsd;
    std::vector<int> goodrightsd;
    int np = 0;
    for(int i = 1; i < leftpaths.size(); i++)
    {
     int flag = 0;
     for(int j = 0; j < goodleftpaths.size(); j++)
      flag += checkIntersections(goodleftpaths[j], leftpaths[i]);
     if(flag == 0)
     {
      goodpaths.push_back(leftpaths[i]);
      goodleftpaths.push_back(leftpaths[i]);
      goodleftsd.push_back(leftsd[i]);
      leftintervals.push_back(np);
      np = 0;
     }
     else
      np ++;
    }

    for(int i = 1; i < rightpaths.size(); i++)
    {
     int flag = 0;
     for(int j = 0; j < goodrightpaths.size(); j++)
      flag += checkIntersections(goodrightpaths[j], rightpaths[i]);
     if(flag == 0)
     {
      goodpaths.push_back(rightpaths[i]);
      goodrightpaths.push_back(rightpaths[i]);
      goodrightsd.push_back(rightsd[i]);
      rightintervals.push_back(np);
      np = 0;
     }
     else
      np ++;
    }

    int sum = std::accumulate(goodleftsd.begin(), goodleftsd.end(), 0);
    sum = std::accumulate(goodrightsd.begin(), goodrightsd.end(), sum);

    std::vector<path*> newleftpaths;
    for(int i = 0; i < goodleftpaths.size() - 1; i++)
    {
     if(i == 0)
      newleftpaths.push_back(goodleftpaths[i]);
     else
      newleftpaths.push_back(goodleftpaths[i+1]);
     //for(int j = 0; j < goodleftpaths[i]->posList.size(); j++)
     //{
     // Vector2d draw0 = *goodleftpaths[i]->posList[j];
     // Vector2d draw1 = *goodleftpaths[i+1]->posList[j];
     // glBegin(GL_LINES);
     // glVertex3f(draw0.x, draw0.y, 0);
     // glVertex3f(draw1.x, draw1.y, 0);
     // glEnd();
     //}
     int pathnum = npaths * (goodleftsd[i] * (1.0 / sum));
     cout << "left" << endl;
     cout << i << " "<< goodleftsd[i] << " " << sum << endl;
     cout << pathnum << endl;
     if(pathnum == 0){}
     else
     {
      for(int j = 0; j < pathnum ; j++)
      {
       path* newpath = new path(0, 0, 0, 0, 0);
       newpath->posList.clear();
       for(int k = 0; k < goodleftpaths[i]->posList.size(); k++)
       {
        Vector2d delta = (*goodleftpaths[i]->posList[k] - *goodleftpaths[i+1]->posList[k]);
        float dis = delta.norm();
        Vector2d u = delta / dis;
        float udis = dis * (1.0 / (pathnum));
        Vector2d v = *goodleftpaths[i+1]->posList[k] + u * udis * (j + 1); 
        Vector2d* toPush = new Vector2d(v.x, v.y);
        newpath->posList.push_back(toPush);
       }
       newleftpaths.push_back(newpath);
      }
     }
    }

    std::vector<path*> newrightpaths;
    for(int i = 0; i < goodrightpaths.size() - 1; i++)
    {
     if(i == 0)
      newrightpaths.push_back(goodrightpaths[i]);
     else
      newrightpaths.push_back(goodrightpaths[i+1]);
     //for(int j = 0; j < goodrightpaths[i]->posList.size(); j++)
     //{
     // Vector2d draw0 = *goodrightpaths[i]->posList[j];
     // Vector2d draw1 = *goodrightpaths[i+1]->posList[j];
     // glBegin(GL_LINES);
     // glVertex3f(draw0.x, draw0.y, 0);
     // glVertex3f(draw1.x, draw1.y, 0);
     // glEnd();
     //}
     int pathnum = npaths * (goodrightsd[i] * (1.0 / sum));
     cout << "right" << endl;
     cout << i << " "<< goodrightsd[i] << " " << sum << endl;
     cout << pathnum << endl;
     if(pathnum == 0){}
     else
     {
      for(int j = 0; j < pathnum; j++)
      {
       path* newpath = new path(0, 0, 0, 0, 0);
       newpath->posList.clear();
       for(int k = 0; k < goodrightpaths[i]->posList.size(); k++)
       {
        Vector2d delta = (*goodrightpaths[i]->posList[k] - *goodrightpaths[i+1]->posList[k]);
        float dis = delta.norm();
        Vector2d u = delta / dis;
        float udis = dis * (1.0 / (pathnum));
        Vector2d v = *goodrightpaths[i+1]->posList[k] + u * udis * (j + 1); 
        Vector2d* toPush = new Vector2d(v.x, v.y);
        newpath->posList.push_back(toPush);
       }
       newrightpaths.push_back(newpath);
      }
     }
    }


    //cout << goodpaths.size() << endl;
    //float red, green, blue;
    //ColorGradient colorgradien;
    //for(int i = 0; i < goodpaths.size(); i++)
    //{
    // //float val = i * 1.0 / goodpaths.size();
    // //colorgradien.getColorAtValue(val, red, green, blue);
    // //glColor4f(red, green, blue, 1);
    // glBegin(GL_LINE_STRIP);
    // Vector2d todraw = translateToScreen(start.x, start.y);
    // glVertex3f(todraw.x, todraw.y, 0);
    // for(int j = 0; j < goodpaths[i]->posList.size(); j++)
    //  glVertex3f(goodpaths[i]->posList[j]->x, goodpaths[i]->posList[j]->y, 0.0);
    // glEnd();
    //}

    //for(int i = 0; i < pathList.size(); i++)
    //{
    // glColor4f(1, 0, 0, 1);
    // glBegin(GL_LINE_STRIP);
    // for(int j = 0; j < pathList[i]->posList.size(); j++)
    //  glVertex3f(pathList[i]->posList[j]->x, pathList[i]->posList[j]->y, 0.0);
    // glEnd();
    //}

    //for(int i = 0; i < goodleftpaths.size(); i++)
    //{
    // glColor4f(1, 0, 0, 1);
    // glBegin(GL_LINE_STRIP);
    // for(int j = 0; j < goodleftpaths[i]->posList.size(); j++)
    //  glVertex3f(goodleftpaths[i]->posList[j]->x, goodleftpaths[i]->posList[j]->y, 0.0);
    // glEnd();
    //}

    //for(int i = 0; i < goodrightpaths.size(); i++)
    //{
    // glColor4f(1, 0, 0, 1);
    // glBegin(GL_LINE_STRIP);
    // for(int j = 0; j < goodrightpaths[i]->posList.size(); j++)
    //  glVertex3f(goodrightpaths[i]->posList[j]->x, goodrightpaths[i]->posList[j]->y, 0.0);
    // glEnd();
    //}

    for(int i = 0; i < newleftpaths.size(); i++)
    {
     glColor4f(1, 0, 0, 1);
     glBegin(GL_LINE_STRIP);
     Vector2d todraw = translateToScreen(start.x, start.y);
     glVertex3f(todraw.x, todraw.y, 0);
     for(int j = 0; j < newleftpaths[i]->posList.size(); j++)
      glVertex3f(newleftpaths[i]->posList[j]->x, newleftpaths[i]->posList[j]->y, 0.0);
     glEnd();
    }

    for(int i = 0; i < newrightpaths.size(); i++)
    {
     glColor4f(1, 0, 0, 1);
     glBegin(GL_LINE_STRIP);
     Vector2d todraw = translateToScreen(start.x, start.y);
     glVertex3f(todraw.x, todraw.y, 0);
     for(int j = 0; j < newrightpaths[i]->posList.size(); j++)
      glVertex3f(newrightpaths[i]->posList[j]->x, newrightpaths[i]->posList[j]->y, 0.0);
     glEnd();
    }
    return;
   }

   int advisory::checkIntersections(path* p0, path* p1)
   {
    int intersections = 0;
    for(int i = 0; i < p0->posList.size() - 1; i++)
    {
     Vector2d v00 = *p0->posList[i];
     Vector2d v01 = *p0->posList[i+1];
     for(int j = 0; j < p1->posList.size() - 1; j++)
     {
      Vector2d v10 = *p1->posList[j];
      Vector2d v11 = *p1->posList[j+1];
      if(std::max(v00.x, v01.x) < std::min(v10.x, v11.x)){}
      else if(std::max(v00.y, v01.y) < std::min(v10.y, v11.y)){}
      else if(std::max(v10.y, v11.y) < std::min(v00.y, v01.y)){}
      else if(std::max(v10.x, v11.x) < std::min(v00.x, v01.x)){}
      else
      {
       Vector2d e0 = v10 - v00;
       Vector2d e1 = v11 - v00;
       Vector3d d0 = e0 % e1;
       d0 = d0 / d0.norm();
       e0 = v10 - v01;
       e1 = v11 - v01;
       Vector3d d1 = e0 % e1;
       d1 = d1 / d1.norm();
       if(d0[2] != d1[2])
        intersections ++;
      }
     }
    }
    return intersections;
   }

   void advisory::cleanPaths()
   {
    int npaths = 50;
    char* buff = new char[50];
    std::vector< std::vector<Vector2d> > samples;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToGeoCoor(*pathList[0]->posList[0]));
    std::vector< std::vector<Points> > bands;
    Vector2d start(-85.9, 26.1);
    //Vector2d start(-85, 24.5);
    drawCircle(translateToScreen(start.x, start.y).x, translateToScreen(start.x, start.y).y, TimePointRadius);
    for(int i = 1; i <= 6; i++)
    {
     sprintf(buff, "NHCData/PDS-selected-%d.txt", i);
     std::vector<Vector2d> p = CDFSample::readTestSamples(buff);
     samples.push_back(p);
    }

    pathList.clear();
    for(int i = 0; i < npaths; i++)
    {
     pathList.push_back(new path(0, 0, 0, 0, 0));
     pathList[i]->posList.clear();
     pathList[i]->latLonList.clear();
    }
    for(int i = 0; i < npaths; i++)
    {
     Vector2d* toPush;
     toPush = new Vector2d(-85.9, 26.1);
     //toPush = new Vector2d(-85, 24.5);
     pathList[i]->latLonList.push_back(toPush);
     toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
     pathList[i]->posList.push_back(toPush);
     for(int j = 0; j < samples.size(); j++)
     {
      toPush = new Vector2d(samples[j][i]);
      pathList[i]->latLonList.push_back(toPush);
      toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
      pathList[i]->posList.push_back(toPush);
     }
    }

    PathCBD pathCBD(pathList, M);
    std::vector<double> dataDepthValue;
    dataDepthValue.resize(npaths);
    for(int i = 0; i < 1; i++)
    {
     std::vector<double> tmpdataDepthValue = pathCBD.fastSegmentSimplexDepth(3);
     for(int j = 0; j < npaths; j++)
      dataDepthValue[j] += tmpdataDepthValue[j]; 
    }
    std::vector<int> sortSegmentIdx = pathCBD.sortSegmentIdx(dataDepthValue);

    std::vector<path*> goodpaths;
    goodpaths.push_back(pathList[sortSegmentIdx[0]]);
    std::vector<path*> goodleftpaths;
    goodleftpaths.push_back(pathList[sortSegmentIdx[0]]);
    std::vector<path*> goodrightpaths;
    goodrightpaths.push_back(pathList[sortSegmentIdx[0]]);

    std::vector<path*> leftpaths;
    std::vector<path*> rightpaths;
    std::vector<int>   leftsd;
    std::vector<int>   rightsd;
    for(int i = 0; i < sortSegmentIdx.size(); i++)
    {
     int idx = sortSegmentIdx[i];
     Vector2d p  = *pathList[idx]->posList[0];
     Vector2d v0 = *goodpaths[0]->posList[0];
     Vector2d v1 = *goodpaths[0]->posList[1];
     Vector2d e0 = v0 - p;
     Vector2d e1 = v1 - p;
     Vector3d dir = e0 % e1;
     dir = dir / dir.norm();
     if(dir[2] == -1 && leftpaths.size() < 30)
     {
      path* newpath = new path(0, 0, 0, 0, 0);
      newpath->posList.clear();
      for(int j = 0; j < pathList[idx]->posList.size(); j++)
      {
       Vector2d toPush = *pathList[idx]->posList[j];
       Vector2d* tt = new Vector2d(toPush.x, toPush.y);
       newpath->posList.push_back(tt);
      }
      leftpaths.push_back(newpath);
      leftsd.push_back(sortSegmentIdx.size() - i);
     }
     else if(dir[2] == 1)
     {
      path* newpath = new path(0, 0, 0, 0, 0);
      newpath->posList.clear();
      for(int j = 0; j < pathList[idx]->posList.size(); j++)
      {
       Vector2d toPush = *pathList[idx]->posList[j];
       Vector2d* tt = new Vector2d(toPush.x, toPush.y);
       newpath->posList.push_back(tt);
      }
      rightpaths.push_back(newpath);
      rightsd.push_back(sortSegmentIdx.size() - i);
     }
    }

    int counter = 0;
    while(counter < 1000)
    {
     counter ++;
     cout << "here" << counter << endl;
back:
     int totalinter = 0;
     totalinter = numofIntersections(pathList);
     cout << "num of inter" << totalinter << endl;
     //if(totalinter == 0 || totalinter == 380)
     if(totalinter == 0)
      goto stop;

     for(int i = 0; i < pathList.size(); i++)
     {
      for(int j = i+1; j < pathList.size(); j++)
      {
       for(int ii = 0; ii < pathList[i]->posList.size() - 1; ii++)
       {
        for(int jj = 0; jj < pathList[j]->posList.size() - 1; jj++)
        {
         Vector2d v00 = *pathList[i]->posList[ii];
         Vector2d v01 = *pathList[i]->posList[ii+1];
         Vector2d v10 = *pathList[j]->posList[jj];
         Vector2d v11 = *pathList[j]->posList[jj+1];
         Vector2d e0 = v01 - v00;
         Vector2d e1 = v11 - v10;
         e0 = e0 / e0.norm();
         e1 = e1 / e1.norm();
         //cout << i << " " << ii << " " << j << " " << jj << endl;
         if(ii == 0 && jj == 0){}
         else if(doIntersect(v00, v01, v10, v11) == true && (e0 % e1).norm() != 0)
         {
          //cout << e0 << " " << e1 << endl;
          //cout << e0 % e1 << endl;
          //cout << v00 << " " << v01 << " " << v10 << " " << v11 << endl;
          pathList[j]->posList[jj+1] = new Vector2d(v01.x, v01.y);
          pathList[i]->posList[ii+1] = new Vector2d(v11.x, v11.y);
          //cout << *pathList[i]->posList[ii] << *pathList[i]->posList[ii+1] 
          // << *pathList[j]->posList[jj] << *pathList[j]->posList[jj+1] << endl;
          goto back;
         }
        }
       }
      }
     }
    }
stop:

    // remove self intersections
    for(int i = 0; i < pathList.size(); i++)
    {
     std::vector<Vector2d> tmpList;
     for(int j = 0; j < pathList[i]->posList.size(); j++)
     {
      Vector2d p = *pathList[i]->posList[j];
      tmpList.push_back(p);
     }
     std::vector<double> disList;
     Vector2d c = tmpList[0];
     for(int j = 0; j < tmpList.size(); j++)
     {
      Vector2d p = tmpList[j];
      double dis = (p - c).norm();
      disList.push_back(dis);
     }
     std::vector<my_pair> pairs;
     for(int j = 0; j < tmpList.size(); j++)
     {
      my_pair mp(disList[j], j);
      pairs.push_back(mp);
     }
     std::sort(pairs.begin(), pairs.end(), sort_predD);
     pathList[i]->posList.clear();
     for(int j = 0; j < tmpList.size(); j++)
     {
      int idx = pairs[j].second;
      Vector2d* p = new Vector2d(tmpList[idx].x, tmpList[idx].y);
      pathList[i]->posList.push_back(p);
     }
    }

    for(int i = 0; i < sortSegmentIdx.size(); i++)
    {
     float red, green, blue;
     ColorGradient colorgradien;
     colorgradien.getColorAtValue(1 - i * 1.0 / pathList.size(), red, green, blue);
     //glColor4f(1, 0, 0, 1);
     glColor4f(red, green, blue, 1);
     glLineWidth(0.1);
     glBegin(GL_LINE_STRIP);
     //Vector2d c = translateToScreen(start.x, start.y);
     //c = (c - Vector2d(300, 300)) * 2 + Vector2d(0, 100);
     //glVertex3f(c.x, c.y, 0.0);
     int idx = sortSegmentIdx[i];
     for(int j = 0; j < pathList[idx]->posList.size(); j++)
     {
      Vector2d p = *pathList[idx]->posList[j];
      //p = p - Vector2d(300, 300);
      //p = p * 1.5 + Vector2d(0, 100);
      glVertex3f(p.x, p.y, 0);
      //cout << i << " " << j << " " << *leftpaths[i]->posList[j] << endl;
     }
     glEnd();
    }

    //glColor4f(1, 0, 0, 1);
    //glBegin(GL_LINES);
    //glVertex3f(487.342, 468.086, 0);
    //glVertex3f(498.401, 484.348, 0);
    //glEnd();

    //glColor4f(0, 0, 1, 1);
    //glBegin(GL_LINES);
    //glVertex3f(508.507, 499.052, 0);
    //glVertex3f(503.465, 491.748, 0);
    //glEnd();

    //glColor4f(0, 1, 0, 1);
    //glBegin(GL_LINES);
    //glVertex3f(489.193, 438.531, 0);
    //glVertex3f(483.116, 429.152,0);
    //glEnd();

    //glColor4f(0, 1, 0, 1);
    //glBegin(GL_LINES);
    //glVertex3f(429.86, 368.114, 0);
    //glVertex3f(464.617, 405.356, 0);
    //glEnd();

    //glColor4f(0, 0, 1, 1);
    //glBegin(GL_LINE_STRIP);
    //for(int j = 0; j < leftpaths[1]->posList.size(); j++)
    //{
    // glVertex3f(leftpaths[1]->posList[j]->x, leftpaths[1]->posList[j]->y, 0.0);
    // cout << "12 " << *leftpaths[12]->posList[j] << endl;
    //}
    //glEnd();

    //glColor4f(1, 0, 0, 1);
    //glBegin(GL_LINE_STRIP);
    //for(int j = 0; j < leftpaths[4]->posList.size(); j++)
    //{
    // glVertex3f(leftpaths[4]->posList[j]->x, leftpaths[4]->posList[j]->y, 0.0);
    // cout << "18 " << *leftpaths[18]->posList[j] << endl;
    //}
    //glEnd();



    //for(int i = 0; i < 5; i++)
    //{
    // float red, green, blue;
    // ColorGradient colorgradien;
    // colorgradien.getColorAtValue(1 - i * 1.0 / 5.0, red, green, blue);
    // //glColor4f(1, 0, 0, 1);
    // glColor4f(red, green, blue, 1);
    // glBegin(GL_LINE_STRIP);
    // for(int j = 0; j < leftpaths[i]->posList.size(); j++)
    // {
    //  Vector2d p = *leftpaths[i]->posList[j];
    //  p = p - Vector2d(300, 300);
    //  p = p * 2;
    //  glVertex3f(p.x, p.y, 0);
    //  //cout << i << " " << j << " " << *leftpaths[i]->posList[j] << endl;
    // }
    // glEnd();
    //}

    //for(int i = 0; i < leftpaths.size(); i++)
    //{
    // for(int j = 0; j < leftpaths[i]->posList.size(); j++)
    // {
    //  Vector2d p = *leftpaths[i]->posList[j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}
    //glColor4f(1, 0, 0, 1);
    //glBegin(GL_LINE_STRIP);
    //for(int j = 0; j < leftpaths[12]->posList.size(); j++)
    // glVertex3f(leftpaths[12]->posList[j]->x, leftpaths[12]->posList[j]->y, 0.0);
    //glEnd();

    //glColor4f(1, 0, 0, 1);
    //glBegin(GL_LINE_STRIP);
    //for(int j = 0; j < leftpaths[18]->posList.size(); j++)
    // glVertex3f(leftpaths[18]->posList[j]->x, leftpaths[18]->posList[j]->y, 0.0);
    //glEnd();

   }


   void advisory::swappaths(path* p0, path* p1)
   {

   }

   void advisory::generateNewPaths()
   {
    int npaths = 50;
    char* buff = new char[50];
    std::vector< std::vector<Vector2d> > samples;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToGeoCoor(*pathList[0]->posList[0]));
    std::vector< std::vector<Points> > bands;
    Vector2d start(-85.9, 26.1);
    drawCircle(translateToScreen(start.x, start.y).x, translateToScreen(start.x, start.y).y, TimePointRadius);
    for(int i = 1; i <= 6; i++)
    {
     sprintf(buff, "NHCData/OLS-selected-%d.txt", i);
     std::vector<Vector2d> p = CDFSample::readTestSamples(buff);
     samples.push_back(p);
    }

    pathList.clear();
    for(int i = 0; i < npaths; i++)
    {
     pathList.push_back(new path(0, 0, 0, 0, 0));
     pathList[i]->posList.clear();
     pathList[i]->latLonList.clear();
    }
    for(int i = 0; i < npaths; i++)
    {
     Vector2d* toPush;
     //toPush = new Vector2d(-85.9, 26.1);
     //pathList[i]->latLonList.push_back(toPush);
     //toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
     //pathList[i]->posList.push_back(toPush);
     for(int j = 0; j < samples.size(); j++)
     {
      toPush = new Vector2d(samples[j][i]);
      pathList[i]->latLonList.push_back(toPush);
      toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
      pathList[i]->posList.push_back(toPush);
     }
    }

    PathCBD pathCBD(pathList, M);
    std::vector<double> dataDepthValue;
    dataDepthValue.resize(npaths);
    for(int i = 0; i < 1; i++)
    {
     std::vector<double> tmpdataDepthValue = pathCBD.fastSegmentSimplexDepth(3);
     for(int j = 0; j < npaths; j++)
      dataDepthValue[j] += tmpdataDepthValue[j]; 
    }
    std::vector<int> sortSegmentIdx = pathCBD.sortSegmentIdx(dataDepthValue);

    std::vector<path*> goodpaths;
    goodpaths.push_back(pathList[sortSegmentIdx[0]]);

    // find segements' boundaries
    std::vector<Vector2d> dirs;
    for(int i = 0; i < goodpaths[0]->posList.size() - 1; i++)
    {
     Vector2d p0 = *goodpaths[0]->posList[i];
     Vector2d p1 = *goodpaths[0]->posList[i+1];
     Vector2d dir = p1 - p0;
     dir = dir / dir.norm();
     double degree = DegToRad(90); 
     Matrix2x2 rm(std::cos(degree), std::sin(-degree),
       std::sin(degree), std::cos(degree));
     Vector2d u = dir * rm;
     if(i == 0 || i == goodpaths[0]->posList.size() - 2)
      dirs.push_back(u);
     dirs.push_back(u);
    }

    for(int i = 0; i < goodpaths[0]->posList.size(); i++)
    {
     Vector2d p1 = *goodpaths[0]->posList[i];
     Vector2d u = dirs[i] * 200;
     glColor4f(0, 1, 0, 1);
     glBegin(GL_LINES);
     glVertex3f(p1.x, p1.y, 0.0);
     glVertex3f(p1.x + u.x, p1.y + u.y, 0.0);
     glEnd();
    }

    std::vector<Vector2d> allsamples;
    for(int i = 0; i < samples.size(); i++)
    {
     for(int j = 0; j < samples[i].size(); j++)
      allsamples.push_back(samples[i][j]);
    }
    std::vector< std::vector<Vector2d> > leftsamplegroups;
    std::vector< std::vector<Vector2d> > rightsamplegroups;
    leftsamplegroups.resize(goodpaths[0]->posList.size() - 1);
    rightsamplegroups.resize(goodpaths[0]->posList.size() - 1);

    for(int i = 1; i < goodpaths[0]->posList.size() - 1; i++)
    {
     Vector2d p1 = *goodpaths[0]->posList[i];
     Vector2d p2 = *goodpaths[0]->posList[i] + dirs[i];
     Vector2d p3 = *goodpaths[0]->posList[i+1];
     Vector2d p4 = *goodpaths[0]->posList[i+1] + dirs[i+1];
     Vector2d cross = findIntersection(p1, p2, p3, p4);
     glColor4f(0, 0, 1, 1);
     glBegin(GL_LINE_LOOP);
     glVertex3f(p1.x, p1.y, 0);
     glVertex3f(p3.x, p3.y, 0);
     glVertex3f(cross.x, cross.y, 0);
     glEnd();
     cout << i << " " << cross << endl;

    }
    for(int i = 0; i < allsamples.size(); i++)
    {
     Vector2d v = allsamples[i];
     v = translateToScreen(v.x, v.y);
     for(int j = 0; j < goodpaths[0]->posList.size() - 1; j++)
     {
      // triangles
      Vector2d p0, p1, p2;
      Vector3d ws0, ws1;
      p0 = *goodpaths[0]->posList[j];
      p1 = *goodpaths[0]->posList[j] + dirs[j] * 50;
      p2 = *goodpaths[0]->posList[j+1];
      ws0 = CDFSample::BaryCentricCoor(v, p0, p1, p2);
      //if(j == 2)
      //{
      //glColor4f(0, 0, 1, 1);
      //glBegin(GL_LINE_LOOP);
      //glVertex3f(p0.x, p0.y, 0);
      //glVertex3f(p1.x, p1.y, 0);
      //glVertex3f(p2.x, p2.y, 0);
      //glEnd();
      //}

      p0 = *goodpaths[0]->posList[j];
      p1 = *goodpaths[0]->posList[j+1];
      p2 = *goodpaths[0]->posList[j+1] + dirs[j+1] * 50;
      ws1 = CDFSample::BaryCentricCoor(v, p0, p1, p2);
      //if(j == 2)
      //{
      //glColor4f(0, 0, 1, 1);
      //glBegin(GL_LINE_LOOP);
      //glVertex3f(p0.x, p0.y, 0);
      //glVertex3f(p1.x, p1.y, 0);
      //glVertex3f(p2.x, p2.y, 0);
      //}
      if(CDFSample::isInside(ws0) || CDFSample::isInside(ws1))
      {
       leftsamplegroups[j].push_back(v);
       break;
      }


      p0 = *goodpaths[0]->posList[j];
      p1 = *goodpaths[0]->posList[j] - dirs[j] * 50;
      p2 = *goodpaths[0]->posList[j+1];
      ws0 = CDFSample::BaryCentricCoor(v, p0, p1, p2);
      p0 = *goodpaths[0]->posList[j];
      p1 = *goodpaths[0]->posList[j+1];
      p2 = *goodpaths[0]->posList[j+1] - dirs[j] * 50;
      ws1 = CDFSample::BaryCentricCoor(v, p0, p1, p2);
      if(CDFSample::isInside(ws0) || CDFSample::isInside(ws1))
      {
       rightsamplegroups[j].push_back(v);
      }
     }
    }

    std::vector< std::vector<Vector3d> > sortedGroups;
    sortedGroups.resize(leftsamplegroups.size());
    for(int i = 0; i < leftsamplegroups.size(); i++)
    {
     Vector2d p0 = *goodpaths[0]->posList[i];
     Vector2d p1 = *goodpaths[0]->posList[i+1];
     Vector2d u = p1 - p0;
     u = u / u.norm();
     for(int j = 0; j < leftsamplegroups[i].size(); j++)
     {
      Vector2d p = leftsamplegroups[i][j];
      Vector2d v = p - p0;
      float d = v * u;
      Vector2d r = u * d;
      Vector2d t = v - r;
      float dis = t.norm();
      Vector3d toPush(p.x, p.y, dis);
      sortedGroups[i].push_back(toPush);
     }
    }

    for(int i = 0; i < sortedGroups.size(); i++)
     std::sort(sortedGroups[i].begin(), sortedGroups[i].end(), sortVector3d);

    //for(int i = 0; i < sortedGroups.size(); i++)
    //{
    // for(int j = 0; j < sortedGroups[i].size(); j++)
    // {
    //  Vector3d p = sortedGroups[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}

    std::vector<int> sizes;
    for(int i = 0; i < sortedGroups.size(); i++)
     sizes.push_back(sortedGroups[i].size());
    int minsize = *std::min_element(sizes.begin(), sizes.end());
    cout << minsize << endl;

    //for(int i = 0; i < minsize; i++)
    //{
    // glBegin(GL_LINE_STRIP);
    // for(int j = 0; j < sortedGroups.size(); j++)
    // {
    //  Vector3d p = sortedGroups[j][i];
    //  Vector2d dir = dirs[j+1] * p.z;
    //  Vector2d q = *goodpaths[0]->posList[j+1] + dir;
    //  if(p.z != 0.0)
    //  {
    //   glVertex3f(p.x, p.y, 0);
    //   if(j != sortedGroups.size() - 1)
    //    glVertex3f(q.x, q.y, 0);
    //  }
    // }
    // glEnd();
    //}

    //for(int i = 0; i < sortedGroups[2].size(); i++)
    //{
    // glColor4f(1, 0, 0, 1);
    // Vector3d p = sortedGroups[2][i];
    // drawCircle(p.x, p.y, TimePointRadius);
    //}

    //for(int i = 0; i < sortedGroups[3].size(); i++)
    //{
    // glColor4f(0, 0, 1, 1);
    // Vector3d p = sortedGroups[3][i];
    // drawCircle(p.x, p.y, TimePointRadius);
    //}


    //for(int i = 0; i < leftsamplegroups.size(); i++)
    //{
    // for(int j = 0; j < leftsamplegroups[i].size(); j++)
    // {
    //  Vector2d p = leftsamplegroups[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}


    glColor4f(0, 0, 1, 1);
    glBegin(GL_LINE_STRIP);
    for(int i = 0; i < goodpaths[0]->posList.size(); i++)
     glVertex3f(goodpaths[0]->posList[i]->x, goodpaths[0]->posList[i]->y, 0.0);
    glEnd();
   }

   void advisory::generateNewPaths2()
   {
    cout << "here" << endl;
    int npaths = 100;
    int nseg = 6;
    char* buff = new char[50];
    std::vector< std::vector<Vector2d> > samples;
    std::vector<Vector2d> centerline;
    std::vector< std::vector<Points> > bands;
    //Vector2d start(-85.9, 26.1); //nhc
    Vector2d start(-85, 24.5);
    centerline.push_back(start);
    drawCircle(translateToScreen(start.x, start.y).x, translateToScreen(start.x, start.y).y, TimePointRadius);
    for(int i = 1; i <= nseg; i++)
    {
     sprintf(buff, "OURData/PDS-selected-%d.txt", i);
     std::vector<Vector2d> p = CDFSample::readTestSamples(buff);
     samples.push_back(p);
    }
    //for(int i = 1; i <= nseg; i++)
    //{
    // std::vector<Vector2d> sample;
    // for(int j = 0; j < pathList.size(); j++)
    // {
    //  Vector2d p = *pathList[j]->posList[i];
    //  p = translateToGeoCoor(p);
    //  sample.push_back(p);
    //  //sample.push_back(*pathList[j]->posList[i]);
    // }
    // samples.push_back(sample);
    //}

    pathList.clear();
    for(int i = 0; i < npaths; i++)
    {
     pathList.push_back(new path(0, 0, 0, 0, 0));
     pathList[i]->posList.clear();
     pathList[i]->latLonList.clear();
    }
    for(int i = 0; i < npaths; i++)
    {
     Vector2d* toPush;
     //toPush = new Vector2d(start.x, start.y);
     //pathList[i]->latLonList.push_back(toPush);
     //toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
     //pathList[i]->posList.push_back(toPush);
     for(int j = 0; j < samples.size(); j++)
     {
      toPush = new Vector2d(samples[j][i]);
      pathList[i]->latLonList.push_back(toPush);
      toPush = new Vector2d(translateToScreen(toPush->x, toPush->y));
      pathList[i]->posList.push_back(toPush);
     }
    }

    PathCBD pathCBD(pathList, M);
    std::vector< std::vector<double> > dataDepthValues;
    dataDepthValues.resize(nseg);
    for(int i = 0; i < nseg; i++)
    {
     std::vector<double> tmpdataDepthValue = pathCBD.fastSegmentSimplexDepth(i);
     for(int j = 0; j < nseg; j++)
      dataDepthValues[i].push_back(tmpdataDepthValue[j]); 
    }
    std::vector< std::vector<int> >sortSegmentIdx;
    sortSegmentIdx.resize(nseg);
    for(int i = 0; i < nseg; i++)
     sortSegmentIdx[i] = pathCBD.sortSegmentIdx(dataDepthValues[i]);

    for(int i = 0; i < nseg; i++)
    {
     int idx = sortSegmentIdx[i][0];
     Vector2d p = samples[i][idx];
     centerline.push_back(p);
    }

    // translate all samples to screen
    for(int i = 0; i < samples.size(); i++)
     for(int j = 0; j < samples[i].size(); j++)
      samples[i][j] = translateToScreen(samples[i][j].x, samples[i][j].y);
    for(int i = 0; i < centerline.size(); i++)
     centerline[i] = translateToScreen(centerline[i].x, centerline[i].y);

    int level = 0;
    recursiveFindCenterline3(samples, start, level, centerline, 0);

    glBegin(GL_LINE_STRIP);
    glColor4f(1, 0, 0, 1);
    for(int i = 0; i < centerline.size(); i++)
     glVertex3f(centerline[i].x, centerline[i].y, 0);
    glEnd();
    glColor4f(0, 0, 1, 1);
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d p = centerline[i];
     drawCircle(p.x, p.y, TimePointRadius);
    }
    std::vector< std::vector<Vector2d> > leftpoints;
    std::vector< std::vector<Vector2d> > rightpoints;
   }


   void advisory::recursiveFindCenterline(std::vector< std::vector<Vector2d> > samples,
     const Vector2d start, int& level,
     const std::vector<Vector2d> precenterline,
     const int lor)
   {
    level++;
    cout << "LEVEL: " <<  level << endl;
    //if(level > 70)
    // return;
    for(int i = 0; i < samples.size(); i++)
     if(samples[i].size() <= 10)
      return;
    //cout << "samples size: " << samples.size() << endl;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToScreen(start.x, start.y));
    for(int i = 0; i < samples.size(); i++)
    {
     //cout << "sample " << i << " size:" << samples[i].size() << endl;
     std::vector<double> datadepth = PathCBD::fastSegmentSimplexDepth3(samples[i]);
     std::vector<int> sortSegmentIdx = PathCBD::sortSegmentIdx(datadepth);
     //for(int j = 0; j < sortSegmentIdx.size(); j++)
     //{
     // int idx = sortSegmentIdx[j];
     // float val = 1 - j * 1.0 / sortSegmentIdx.size();
     // datadepth[idx] = val;
     //}

     // compute bbox
     std::vector<double> xlist;
     std::vector<double> ylist;
     Points points;
     points.clear();
     for(int j = 0; j < samples[i].size(); j++)
     {
      xlist.push_back(samples[i][j].x);
      ylist.push_back(samples[i][j].y);
      points.push_back(Point_2(samples[i][j].x, samples[i][j].y));
     }

     for(int j = 0; j < precenterline.size(); j++)
      points.push_back(Point_2(precenterline[j].x, precenterline[j].y));

     double minx = *min_element(xlist.begin(), xlist.end());
     double maxx = *max_element(xlist.begin(), xlist.end());
     double miny = *min_element(ylist.begin(), ylist.end());
     double maxy = *max_element(ylist.begin(), ylist.end());

     // find concave hull
     Alpha_shape_2 A(points.begin(), points.end(), FT(1), Alpha_shape_2::GENERAL);
     std::vector<Segment> segments;
     alpha_edges(A, std::back_inserter(segments));
     float optalpha = *A.find_optimal_alpha(1);
     Alpha_shape_2 B(points.begin(), points.end(), FT(optalpha), Alpha_shape_2::GENERAL);
     segments.clear();
     alpha_edges(B, std::back_inserter(segments));
     std::vector< std::vector<Vector2d> > triangles;
     for(int j = 0; j < segments.size(); j++)
     {
      Point_2 p0 = segments[j][0];
      Point_2 p1 = segments[j][1];
      //glColor4f(1, 0, 0, 1);
      //glBegin(GL_LINES);
      //glVertex3f(p0[0], p0[1], 0);
      //glVertex3f(p1[0], p1[1], 0);
      //glEnd();
      std::vector<Vector2d> triangle;
      int idx = sortSegmentIdx[0];
      Vector2d v = samples[i][idx];
      triangle.push_back(v);
      triangle.push_back(Vector2d(p0[0], p0[1]));
      triangle.push_back(Vector2d(p1[0], p1[1]));
      triangles.push_back(triangle);
     }

     //for(int j = 0; j < triangles.size(); j++)
     //{
     // glColor4f(0, 0, 1, 1);
     // glBegin(GL_LINE_LOOP);
     // for(int k = 0; k < triangles[j].size(); k++)
     //  glVertex3f(triangles[j][k].x, triangles[j][k].y, 0);
     // glEnd();
     //}

     //build rbf
     //double radii = 0.5 * sqrt((maxx - minx + maxy - miny) * 0.5);
     double radii = 0.1 * ((maxx - minx + maxy - miny) * 0.5);
     RBF rbf;
     rbf.compute(samples[i], datadepth, radii);

     //eval bbox
     Vector2d c;
     double maxval(-1);
     for(int j = minx; j < maxx; j++)
     {
      for(int k = miny; k < maxy; k++)
      {
       Vector2d p(j, k);
       for(int l = 0; l < triangles.size(); l++)
       {
        Vector3d ws = CDFSample::BaryCentricCoor(p, triangles[l][0], triangles[l][1], triangles[l][2]);
        if(CDFSample::isInside(ws))
        {
         double f = rbf.eval(Vector2d(j, k));
         //float red, green, blue;
         //ColorGradient colorgradien;
         //colorgradien.getColorAtValue(f, red, green, blue);
         //glColor4f(red, green, blue, f);
         //glBegin(GL_POINTS);
         //glVertex3f(j, k, 0);
         //glEnd();
         if(f > maxval)
         {
          c = Vector2d(j, k);
          maxval = f;
         }
         break;
        }
       }
       //double f = rbf.eval(Vector2d(j, k));
       ////if(level == 3)
       ////{
       //// float red, green, blue;
       //// ColorGradient colorgradien;
       //// colorgradien.getColorAtValue(f, red, green, blue);
       //// glColor4f(red, green, blue, f);
       //// glBegin(GL_POINTS);
       //// glVertex3f(j, k, 0);
       ////}
       ////glEnd();
       //if(f > maxval)
       //{
       // c = Vector2d(j, k);
       // maxval = f;
       //}
      }
     }
     centerline.push_back(c);
    }

    //group samples
    std::vector< std::vector<Vector2d> >leftpoints;
    std::vector< std::vector<Vector2d> >rightpoints;
    groupSamples(leftpoints, rightpoints, samples, centerline, start);
    for(int i = 0; i < leftpoints.size(); i++)
     cout << "leftpoints " << i << " size:" << leftpoints[i] .size() << endl;
    recursiveFindCenterline(leftpoints, start, level, centerline, 1);
    recursiveFindCenterline(rightpoints, start, level,centerline, 2);

    glBegin(GL_LINE_STRIP);
    glColor4f(1, 0, 0, 1);
    for(int i = 0; i < centerline.size(); i++)
     glVertex3f(centerline[i].x, centerline[i].y, 0);
    glEnd();
    glColor4f(0, 0, 1, 1);
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d p = centerline[i];
     drawCircle(p.x, p.y, TimePointRadius);
    }

    //glColor4f(0, 0, 1, 1);
    //for(int i = 0; i < leftpoints.size(); i++)
    //{
    // for(int j = 0; j < leftpoints[i].size(); j++)
    // {
    //  Vector2d p = leftpoints[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}

    //glColor4f(1, 0, 0, 1);
    //for(int i = 0; i < rightpoints.size(); i++)
    //{
    // for(int j = 0; j < rightpoints[i].size(); j++)
    // {
    //  Vector2d p = rightpoints[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}

    //for(int i = 0; i < samples.size(); i++)
    // for(int j = 0; j < samples[i].size(); j++)
    // {
    //  Vector2d p = samples[i][j];
    //  glColor4f(1, 0, 0, 1);
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }

   }


   void advisory::groupSamples(std::vector< std::vector<Vector2d> >& leftpoints, 
     std::vector< std::vector<Vector2d> >& rightpoints,
     const std::vector< std::vector<Vector2d> > samples, const std::vector<Vector2d> centerline,
     const Vector2d start)
   {
    leftpoints.clear();
    rightpoints.clear();
    // find perpendicular directions of segements'  
    std::vector<Vector2d> dirs;
    for(int i = 0; i < centerline.size()-1; i++)
    {
     Vector2d p0 = centerline[i];
     Vector2d p1 = centerline[i+1];
     Vector2d dir = p1 - p0;
     dir = dir / dir.norm();
     glColor4f(0, 0, 1, 1);
     double degree = DegToRad(90); 
     Matrix2x2 rm(std::cos(degree), std::sin(-degree),
       std::sin(degree), std::cos(degree));
     Vector2d u = dir * rm;
     if(i == centerline.size() - 2)
      dirs.push_back(u);
     dirs.push_back(u);
    }

    std::vector< std::vector<Vector2d> > lboxes;
    std::vector< std::vector<Vector2d> > rboxes;
    lboxes.resize(6);
    rboxes.resize(6);
    for(int i = 0; i < centerline.size() - 1; i++)
    {
     Vector2d p0 = centerline[i];
     Vector2d p1 = centerline[i+1];
     Vector2d p2 = p0 + dirs[i] * 1000;
     Vector2d p3 = p1 + dirs[i+1] * 1000;
     lboxes[i].push_back(p0);
     lboxes[i].push_back(p1);
     lboxes[i].push_back(p2);
     lboxes[i].push_back(p3);
     p2 = p0 - dirs[i] * 1000;
     p3 = p1 - dirs[i+1] * 1000;
     rboxes[i].push_back(p0);
     rboxes[i].push_back(p1);
     rboxes[i].push_back(p2);
     rboxes[i].push_back(p3);
    }

    leftpoints.clear();
    rightpoints.clear();
    leftpoints.resize(samples.size());
    rightpoints.resize(samples.size());
    for(int i = 0; i < samples.size(); i++)
    {
     for(int j = 0; j < samples[i].size(); j++)
     {
      Vector2d p = samples[i][j];
      bool flag = false;
      for(int k = 0; k < lboxes.size(); k++)
      {
       Vector2d v0, v1, v2, v3;
       v0 = lboxes[k][0];
       v1 = lboxes[k][1];
       v2 = lboxes[k][3];
       Vector3d ws0 = CDFSample::BaryCentricCoor(p, v0, v1, v2);
       v0 = lboxes[k][0];
       v1 = lboxes[k][3];
       v2 = lboxes[k][2];
       Vector3d ws1 = CDFSample::BaryCentricCoor(p, v0, v1, v2);
       if(CDFSample::isInside(ws0) || CDFSample::isInside(ws1))
       {
        leftpoints[i].push_back(p);
        flag = true;
        break;
       }

       v0 = rboxes[k][0];
       v1 = rboxes[k][1];
       v2 = rboxes[k][3];
       ws0 = CDFSample::BaryCentricCoor(p, v0, v1, v2);
       v0 = rboxes[k][0];
       v1 = rboxes[k][3];
       v2 = rboxes[k][2];
       ws1 = CDFSample::BaryCentricCoor(p, v0, v1, v2);
       if(CDFSample::isInside(ws0) || CDFSample::isInside(ws1))
       {
        rightpoints[i].push_back(p);
        flag = true;
        break;
       }
      }
      if(flag == false)
      {
       Vector2d v0 = centerline[0];
       Vector2d v1 = centerline[1];
       Vector2d v2 = centerline[centerline.size() - 2];
       Vector2d v3 = centerline[centerline.size() - 1];
       float dis0 = (p - v0).norm();
       float dis1 = (p - v2).norm();
       Vector2d e0, e1;
       if(dis0 <= dis1)
       {
        e0 = p - v0;
        e1 = p - v1;
       }
       else
       {
        e0 = p - v2;
        e1 = p - v3;
       }
       Vector3d d = e0 % e1;
       d = d / d.norm();
       if(d[2] == -1)
        leftpoints[i].push_back(p);
       else
        rightpoints[i].push_back(p);
      }
     }
    }
    //glColor4f(0, 0, 1, 1);
    //for(int i = 0; i < leftpoints[3].size(); i++)
    //{
    // Vector2d p = leftpoints[3][i];
    // drawCircle(p.x + 10, p.y, TimePointRadius);
    //}
   }

   void advisory::generateNewPaths3(const int width, const int height)
   {
    int npaths = 10;
    int nseg = 6;
    char* buff = new char[50];
    std::vector< std::vector<Vector2d> > samples;
    std::vector<Vector2d> centerline;
    //Vector2d start(-85.9, 26.1); //nhc
    Vector2d start(-85, 24.5);
    centerline.push_back(translateToScreen(start.x, start.y));
    drawCircle(translateToScreen(start.x, start.y).x, translateToScreen(start.x, start.y).y, TimePointRadius);
    for(int i = 1; i <= nseg; i++)
    {
     sprintf(buff, "OURData/PDS-selected-%d.txt", i);
     std::vector<Vector2d> p = CDFSample::readTestSamples(buff);
     samples.push_back(p);
    }

    std::vector< std::vector<Vector2d> > areas;
    std::vector< std::vector<float> > sds;

    // compute sd fields
    for(int i = 0; i < samples.size(); i++)
    {
     sprintf(buff, "OURData/PDS-SD-field-%d.txt", i+1);
     std::vector<double> datadepth = PathCBD::fastSegmentSimplexDepth3(samples[i]);
     std::vector<int> sortSegmentIdx = PathCBD::sortSegmentIdx(datadepth);
     // compute bbox
     std::vector<double> xlist;
     std::vector<double> ylist;
     for(int j = 0; j < samples[i].size(); j++)
     {
      samples[i][j] = translateToScreen(samples[i][j].x, samples[i][j].y);
      xlist.push_back(samples[i][j].x);
      ylist.push_back(samples[i][j].y);
     }
     double minx = *min_element(xlist.begin(), xlist.end());
     double maxx = *max_element(xlist.begin(), xlist.end());
     double miny = *min_element(ylist.begin(), ylist.end());
     double maxy = *max_element(ylist.begin(), ylist.end());
     //glColor4f(1, 0, 0, 1);
     //glBegin(GL_LINE_LOOP);
     //glVertex3f(minx, miny, 0);
     //glVertex3f(maxx, miny, 0);
     //glVertex3f(maxx, maxy, 0);
     //glVertex3f(minx, maxy, 0);
     //glEnd();

     ColorGradient colorgradien;
     float red, green, blue;

     ////build rbf and write to files
     //ofstream myfile(buff, fstream::out);
     //double radii = 0.05 * ((maxx - minx + maxy - miny) * 0.5);
     //RBF rbf;
     //rbf.compute(samples[i], datadepth, radii);
     //for(int j = 0; j < height; j++)
     //{
     // for(int k = 0; k < width; k++)
     // {
     //  Vector2d p(k, j);
     //  float f = rbf.eval(p);
     //  colorgradien.getColorAtValue(f, red, green, blue);
     //  glColor4f(red, green, blue, 1);
     //  glBegin(GL_POINTS);
     //  glVertex3f(p.x, p.y, 0);
     //  glEnd();
     //  myfile << f << endl;
     // }
     //}
     //myfile.close();
     //cout << "Finish writing " << buff << endl;

     // read SD field from file
     ifstream file(buff);
     if(!file.is_open())
     {
      cerr << "Bad file: " << buff << endl;
      exit(-1);
     }
     std::vector<float> sd;
     while(!file.eof())
     {
      float toPush;
      file >> toPush;
      sd.push_back(toPush);
     }
     file.close();
     sds.push_back(sd);

     float maxval = -1;
     Vector2d c;
     std::vector<Vector2d> area;
     for(int j = 0; j < height; j++)
     {
      for(int k = 0; k < width; k++)
      {
       area.push_back(Vector2d(k, j));
       int idx = width * j + k;
       float f = sd[idx]; 
       if(f > maxval)
       {
        c = Vector2d(k, j);
        maxval = f;
       }
       //colorgradien.getColorAtValue(f, red, green, blue);
       //glColor4f(red, green, blue, 1);
       //glBegin(GL_POINTS);
       //glVertex3f(k, j, 0);
       //glEnd();
      }
     }
     centerline.push_back(c);
     areas.push_back(area);
    }

    recursiveFindCenterline2(areas, centerline, sds, start, width, height);

    glColor4f(1, 0, 0, 1);
    glBegin(GL_LINE_STRIP);
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d p = centerline[i];
     glVertex3f(p.x, p.y, 0);
    }
    glEnd();
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d p = centerline[i];
     drawCircle(p.x, p.y, TimePointRadius);
    }

   }

   void advisory::recursiveFindCenterline2(std::vector< std::vector<Vector2d> > samples,
     const std::vector<Vector2d> precenterline,
     const std::vector< std::vector<float> > sds,
     const Vector2d start, 
     const int width,
     const int height)
   {
    for(int i = 0; i < samples.size(); i++)
     if(samples[i].size() <= 10)
      return;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToScreen(start.x, start.y));
    for(int i = 0; i < samples.size(); i++)
    {
     std::vector<double> xlist;
     std::vector<double> ylist;
     Points points;
     points.clear();
     for(int j = 0; j < samples[i].size(); j++)
     {
      xlist.push_back(samples[i][j].x);
      ylist.push_back(samples[i][j].y);
      points.push_back(Point_2(samples[i][j].x, samples[i][j].y));
     }

     for(int j = 0; j < precenterline.size(); j++)
      points.push_back(Point_2(precenterline[j].x, precenterline[j].y));

     double minx = *min_element(xlist.begin(), xlist.end());
     double maxx = *max_element(xlist.begin(), xlist.end());
     double miny = *min_element(ylist.begin(), ylist.end());
     double maxy = *max_element(ylist.begin(), ylist.end());
     Vector2d center(maxx - minx, maxy - miny);

     //// find concave hull
     //Alpha_shape_2 A(points.begin(), points.end(), FT(1), Alpha_shape_2::GENERAL);
     //std::vector<Segment> segments;
     //alpha_edges(A, std::back_inserter(segments));
     //float optalpha = *A.find_optimal_alpha(1);
     //Alpha_shape_2 B(points.begin(), points.end(), FT(optalpha), Alpha_shape_2::GENERAL);
     //segments.clear();
     //alpha_edges(B, std::back_inserter(segments));
     //std::vector< std::vector<Vector2d> > triangles;
     //for(int j = 0; j < segments.size(); j++)
     //{
     // Point_2 p0 = segments[j][0];
     // Point_2 p1 = segments[j][1];
     // //glColor4f(1, 0, 0, 1);
     // //glBegin(GL_LINES);
     // //glVertex3f(p0[0], p0[1], 0);
     // //glVertex3f(p1[0], p1[1], 0);
     // //glEnd();
     // std::vector<Vector2d> triangle;
     // Vector2d v = center;
     // triangle.push_back(v);
     // triangle.push_back(Vector2d(p0[0], p0[1]));
     // triangle.push_back(Vector2d(p1[0], p1[1]));
     // triangles.push_back(triangle);
     //}

     //for(int j = 0; j < triangles.size(); j++)
     //{
     // glColor4f(0, 0, 1, 1);
     // glBegin(GL_LINE_LOOP);
     // for(int k = 0; k < triangles[j].size(); k++)
     //  glVertex3f(triangles[j][k].x, triangles[j][k].y, 0);
     // glEnd();
     //}

     Vector2d c;
     double maxval(-1);
     for(int j = miny; j < maxy; j++)
     {
      for(int k = minx; k < maxx; k++)
      {
       Vector2d p(k, j);
       //for(int l = 0; l < triangles.size(); l++)
       //{
       // Vector3d ws = CDFSample::BaryCentricCoor(p, triangles[l][0], triangles[l][1], triangles[l][2]);
       // if(CDFSample::isInside(ws))
       // {
       int idx = width * j + k;
       float f = sds[i][idx];
       //float red, green, blue;
       //ColorGradient colorgradien;
       //colorgradien.getColorAtValue(f, red, green, blue);
       //glColor4f(red, green, blue, f);
       //glBegin(GL_POINTS);
       //glVertex3f(j, k, 0);
       //glEnd();
       if(f > maxval)
       {
        c = Vector2d(k, j);
        maxval = f;
       }
       //  break;
       // }
       //}
      }
     }
     centerline.push_back(c);
    }

    //group samples
    std::vector< std::vector<Vector2d> >leftpoints;
    std::vector< std::vector<Vector2d> >rightpoints;
    groupSamples(leftpoints, rightpoints, samples, centerline, start);
    for(int i = 0; i < leftpoints.size(); i++)
     cout << "leftpoints " << i << " size:" << leftpoints[i] .size() << endl;
    recursiveFindCenterline2(leftpoints, centerline, sds, start, width, height);
    //recursiveFindCenterline2(rightpoints,centerline, sds, start, width, height);

    glBegin(GL_LINE_STRIP);
    glColor4f(0, 0, 1, 1);
    for(int i = 0; i < centerline.size(); i++)
     glVertex3f(centerline[i].x, centerline[i].y, 0);
    glEnd();
    glColor4f(0, 0, 1, 1);
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d p = centerline[i];
     drawCircle(p.x, p.y, TimePointRadius);
    }

    glColor4f(0, 0, 1, 1);
    for(int i = 0; i < leftpoints.size(); i++)
    {
     for(int j = 0; j < leftpoints[i].size(); j++)
     {
      Vector2d p = leftpoints[i][j];
      drawCircle(p.x, p.y, TimePointRadius);
     }
    }

    //glColor4f(1, 0, 0, 1);
    //for(int i = 0; i < rightpoints.size(); i++)
    //{
    // for(int j = 0; j < rightpoints[i].size(); j++)
    // {
    //  Vector2d p = rightpoints[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}

    //for(int i = 0; i < samples.size(); i++)
    // for(int j = 0; j < samples[i].size(); j++)
    // {
    //  Vector2d p = samples[i][j];
    //  glColor4f(1, 0, 0, 1);
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }

   }

   void advisory::recursiveFindCenterline3(std::vector< std::vector<Vector2d> > samples,
     const Vector2d start, int& level,
     const std::vector<Vector2d> precenterline,
     const int lor)
   {
    level++;
    cout << "LEVEL: " <<  level << endl;
    //if(level > 70)
    // return;
    for(int i = 0; i < samples.size(); i++)
     if(samples[i].size() <= 2)
      return;
    //cout << "samples size: " << samples.size() << endl;
    std::vector<Vector2d> centerline;
    centerline.push_back(translateToScreen(start.x, start.y));
    for(int i = 0; i < samples.size(); i++)
    {
     //cout << "sample " << i << " size:" << samples[i].size() << endl;
     std::vector<double> datadepth = PathCBD::fastSegmentSimplexDepth3(samples[i]);
     std::vector<int> sortSegmentIdx = PathCBD::sortSegmentIdx(datadepth);
     //for(int j = 0; j < sortSegmentIdx.size(); j++)
     //{
     // int idx = sortSegmentIdx[j];
     // float val = 1 - j * 1.0 / sortSegmentIdx.size();
     // datadepth[idx] = val;
     //}

     // compute bbox
     std::vector<double> xlist;
     std::vector<double> ylist;
     Points points;
     points.clear();
     for(int j = 0; j < samples[i].size(); j++)
     {
      xlist.push_back(samples[i][j].x);
      ylist.push_back(samples[i][j].y);
      points.push_back(Point_2(samples[i][j].x, samples[i][j].y));
     }

     if(level == 1)
      centerline = precenterline;
     else
     {
      for(int j = 0; j < precenterline.size(); j++)
       points.push_back(Point_2(precenterline[j].x, precenterline[j].y));

      double minx = *min_element(xlist.begin(), xlist.end());
      double maxx = *max_element(xlist.begin(), xlist.end());
      double miny = *min_element(ylist.begin(), ylist.end());
      double maxy = *max_element(ylist.begin(), ylist.end());

      // find media
      Vector2d c;
      std::vector<my_pair> pairs;
      for(int j = 0; j < samples[i].size(); j++)
      {
       Vector2d e, v0, v1, p;
       if(i != samples.size() - 1)
       {
        v0 = precenterline[i];
        v1 = precenterline[i+1];
       }
       else
       {
        v0 = precenterline[i-1];
        v1 = precenterline[i];
       }
       e = v1 - v0; 
       e = e / e.norm();
       p = samples[i][j];
       Vector2d l = p - v0;
       float d0 =  l * e;
       Vector2d l0 = e * d0;
       Vector2d l1 = l - l0;
       float dis = abs(l1.norm());
       my_pair mp(dis, j);
       pairs.push_back(mp);
       //if(level == 2)
       //{
       //glBegin(GL_LINES);
       //glVertex3f(samples[i][j].x, samples[i][j].y, 0);
       //glVertex3f((samples[i][j] - l1).x, (samples[i][j] - l1).y, 0);
       //glEnd();
       //}
      }
      std::sort(pairs.begin(), pairs.end(), sort_predD);
      int idx = pairs[int(pairs.size()/2)].second;
      c = samples[i][idx];
      centerline.push_back(c);
     }
    }

    //group samples
    std::vector< std::vector<Vector2d> >leftpoints;
    std::vector< std::vector<Vector2d> >rightpoints;
    groupSamples(leftpoints, rightpoints, samples, centerline, start);
    for(int i = 0; i < leftpoints.size(); i++)
     cout << "leftpoints " << i << " size:" << leftpoints[i] .size() << endl;
    recursiveFindCenterline3(leftpoints, start, level, centerline, 1);
    recursiveFindCenterline3(rightpoints, start, level,centerline, 2);

    glBegin(GL_LINE_STRIP);
    glColor4f(1, 0, 0, 1);
    for(int i = 0; i < centerline.size(); i++)
     glVertex3f(centerline[i].x, centerline[i].y, 0);
    glEnd();
    glColor4f(0, 0, 1, 1);
    for(int i = 0; i < centerline.size(); i++)
    {
     Vector2d p = centerline[i];
     drawCircle(p.x, p.y, TimePointRadius);
    }

    //glColor4f(0, 0, 1, 1);
    //for(int i = 0; i < leftpoints.size(); i++)
    //{
    // for(int j = 0; j < leftpoints[i].size(); j++)
    // {
    //  Vector2d p = leftpoints[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}

    //glColor4f(1, 0, 0, 1);
    //for(int i = 0; i < rightpoints.size(); i++)
    //{
    // for(int j = 0; j < rightpoints[i].size(); j++)
    // {
    //  Vector2d p = rightpoints[i][j];
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }
    //}

    //for(int i = 0; i < samples.size(); i++)
    // for(int j = 0; j < samples[i].size(); j++)
    // {
    //  Vector2d p = samples[i][j];
    //  glColor4f(1, 0, 0, 1);
    //  drawCircle(p.x, p.y, TimePointRadius);
    // }

   }

   void advisory::drawRebuildSDField(const int frameid,
     const float b0, const float b1, const float b2)
   {
    char* buff = new char[100];
    //sprintf(buff, "NHCData/SD/OLS-NHC-rebuild-SD2-%d.txt", frameid);
    sprintf(buff, "NewData/SD/OLS-NHC-rebuild-SD2-%d.txt", frameid);
    cout << buff << endl;
    ifstream data(buff);
    if(!data.is_open()){
     cerr << "Bad file:" << buff;
     exit(-1);
    }
    std::vector<Vector3d> samples;
    std::vector<float> values;
    while(!data.eof()){
     float x, y, z;
     data >> x >> y >> z;
     //if(z <= 0.01)
     // z = 0;
     {
      samples.push_back(Vector3d(x, y, z));
      values.push_back(z);
     }
    }
    float localmax = *std::max_element(values.begin(), values.end());
    float localmin = *std::min_element(values.begin(), values.end());
    float inteval = localmax - localmin;
    //for(int i = 0; i < values.size(); i++)
    //{
    // float frac = (values[i] - localmin) / inteval;
    // values[i] =  1 * frac;
    //}

    std::vector<float> temp;
    for(int i = 0; i < values.size(); i++)
    {
     if(values[i] != 0)
      temp.push_back(values[i]);
    }
    std::sort(temp.begin(), temp.end());
    std::reverse(temp.begin(), temp.end());
    float bb0, bb1, bb2;
    bb0 = temp[int(temp.size() * 0.33)];
    bb1 = temp[int(temp.size() * 0.66)];
    bb2 = temp[int(temp.size() * 0.99)];
    cout << bb0 << " " << bb1 << " " << bb2 << endl;

    float red, green, blue;
    ColorGradient colorgradien;
    for(int i = 0; i < samples.size(); i++)
    {
     Vector3d tmp = samples[i];
     //Vector2d location  = translateToScreen(tmp.x, tmp.y);
     Vector2d location(tmp.x, tmp.y);
     //colorgradien.getColorAtValue(tmp.z, red, green, blue);
     //float opc = 0.7 * tmp.z / localmax;
     //float opc = 0.05 * (1 - values[i]) + 0.3 * values[i];
     float opc = 0;
     if(tmp.z > bb0)
     {
      red = 1; green = 0; blue = 0; opc = 0.6;
     }
     else if(tmp.z > bb1 && tmp.z <= bb0)
     {
      red = 1; green = 0.27; blue = 0; opc = 0.3;
     }
     else if(tmp.z > bb2 && tmp.z <= bb1)
     {
      red = 0.647; green = 0.165; blue = 0.165; opc = 0.1;
     }
     else
     {
      red = 0;  green = 0;  blue = 0; opc = 0;
     }

     glColor4f(red, green, blue, opc);
     glBegin(GL_POINTS);
     glVertex3f(location.x, location.y, 0);
     glEnd();
    }
   }

   void advisory::writeKNNDsamples(const std::vector<Vector2d> trainingSet, 
     const std::vector<double> kdtreeden, 
     const int frameid)
   {
    char* buff = new char[100];
    sprintf(buff, "NewData/samples-%d.txt", frameid);
    std::vector<Vector2d> latlon;
    std::vector<float> nonzeros;
    float red, green, blue;
    ColorGradient colorgradien;
    for(int j = 0; j < trainingSet.size(); j++)
    {
     colorgradien.getColorAtValue(kdtreeden[j], red, green, blue);
     glColor4f(red, green, blue, 1);
     drawSolidCircle(trainingSet[j].x, trainingSet[j].y, TimePointRadius*1.5);
     if(kdtreeden[j] > 10e-4)
     {
      latlon.push_back(translateToGeoCoor(trainingSet[j]));
      nonzeros.push_back(kdtreeden[j]);
     }
    }
    CDFSample::write2file(latlon, buff);
    sprintf(buff, "NewData/knndensity-%d.txt", frameid);
    CDFSample::write2file(nonzeros, buff);
    exit(0);
   }

   RBF advisory::DensityFieldFromSubset(const int frameid)
   {
    char* buff = new char[100];
    sprintf(buff, "NewData/OLS-subset-%d.txt", frameid);
    ifstream file(buff);
    if(!file.is_open())
    {
     cerr << "Bad file:" << buff << endl;
     exit(-1);
    }
    std::vector<Vector2d> latlon;
    std::vector<float> bounding;
    while(!file.eof())
    {
     Vector2d toPush;
     file >> toPush.x >> toPush.y;
     latlon.push_back(toPush);
     bounding.push_back(toPush.x);
    }
    latlon.pop_back();
    bounding.pop_back();
    float locmax = *std::max_element(bounding.begin(), bounding.end());
    float locmin = *std::min_element(bounding.begin(), bounding.end());
    float w = std::abs(locmax - locmin);

    sprintf(buff, "NewData/OLS-subset-values-%d.txt", frameid);
    ifstream file2(buff);
    if(!file2.is_open())
    {
     cerr << "Bad file:" << buff << endl;
     exit(-1);
    }
    std::vector<double> densities;
    std::vector<double> radii;
    while(!file2.eof())
    {
     float val(0), radius(0);
     file2 >> val >> radius;
     densities.push_back(val);
     radii.push_back(radius);
    }
    densities.pop_back();
    radii.pop_back();
    sprintf(buff, "NewData/DF-new-%d.txt", frameid);
    cout << latlon.size() << " " << densities.size() << " " << radii.size() << endl;
    float red, green, blue;
    ColorGradient colorgradien;

    //for(int i = 0; i < densities.size(); i++)
    //{
    // //float r = 7.0 * 0.011 / sqrt(densities[i]);
    // radii[i] = radii[i] * 7;
    // cout << radii[i] << endl;
    //}

    RBF rbf;
    rbf.compute(latlon, densities, w * 0.15);
    //rbf.compute(latlon, densities, radii);
    //std::vector<Vector2d> locs;
    //std::vector<float> vals;
    //for(float i = -95.3; i <= 85.66; i += 0.05)
    //{
    // for(float j = 26.26; j <= 35; j += 0.05)
    // {
    //  locs.push_back(Vector2d(i, j));
    //  float f = rbf.eval(Vector2d(i, j));
    //  vals.push_back(f);
    //  colorgradien.getColorAtValue(f, red, green, blue);
    //  Vector2d pos = translateToScreen(i, j);
    //  glColor4f(red, green, blue, 1);
    //  drawSolidCircle(pos.x, pos.y, TimePointRadius);
    // }
    //}
    //CDFSample::write2file(locs, vals, buff);
    return rbf;
   }

   VectorField advisory::extractVectorField(const int timeframe, const RBF rbf, 
     const std::vector<double> rbfradii)
   {
    std::vector<VelocityPath> paths;
    for(int i = 0; i < pathList.size(); i++)
    {
     VelocityPath toPush(pathList[i]);
     paths.push_back(toPush);
    }
    VectorField vectorfield(paths, rbf, rbfradii, 20, 20);
    vectorfield.buildAt(timeframe);
    return vectorfield;
   }

   void advisory::producePaths(const int refframe, const std::vector<RBF> rbfs, 
     const std::vector< std::vector<double> > rbfradii)
   {
    // rbf should be rbfs at different time instances
    if(vectorfields.empty())
    {
     for(int i = 0; i < NumofTimesteps; i++)
      vectorfields.push_back(extractVectorField(i+1, rbfs[i], rbfradii[i]));
    }

    char* buff = new char[100];
    sprintf(buff, "demoDisplayData/OD-selected-%d.txt", refframe);
    std::vector<Vector2d> refsamples = CDFSample::readTestSamples(buff);
    PathsGenerator pathgenerator(vectorfields);
    pathgenerator.RefSamples() = refsamples;
    pathgenerator.Generate(refframe-1); // indices start from 0, but refframe starts from 1
   }

   RBF advisory::RBFatTime(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
     int width, int height, 
     double tlat, double tlon, double mlat, double mlon,
     int timeframe, 
     float rbfkernelfactor,
     std::vector<double>& rbfradii){

    CDFSample cdfsample;
    std::vector< std::vector<Vector2d> > plist;

#ifdef NHC_DATA
    readNHCTracks("post2NHCdata.txt", timeframe);
#else
    readNHCTracks("trackdata/ourtracks.txt", timeframe);
#endif
    int npro = omp_get_max_threads();
    omp_set_num_threads(npro);

    float red, green, blue;
    ColorGradient colorgradient;

    std::vector<Vector2d*>::iterator pos;
    std::vector<path*>::iterator pathIt;
    float tempTrans;
    Vector2d meanPoint;

    Points band;
    band.clear();
    Polygon_2 poly;
    poly.clear();
    Points outlier;
    outlier.clear();

    std::vector<Points> bands;
    bands.resize(3);
    std::vector<double> bandsVals;
    bandsVals.resize(3, 1.0);

    double bandval = 0.0;

    Vector2d* curPos;
    Vector2d* latLon;

    int i = 0; 
    i = timeframe;

    //DensityGrid
    DensityGridCols = 5;
    DensityGridRows = 5;

    // compute bounding box of predicted locations
    float maxX(10e-6), maxY(10e-6), minX(10e6), minY(10e6);
    for(int j = 0; j < pathList.size(); j++){
     Vector2d newpos = *(pathList[j]->posList[i]); 
     maxX = std::max(maxX, float(newpos.x));
     maxY = std::max(maxY, float(newpos.y));
     minX = std::min(minX, float(newpos.x));
     minY = std::min(minY, float(newpos.y));
    }
    float globalminx(minX), globalminy(minY), globalmaxx(maxX), globalmaxy(maxY);

    float boundwidth = (maxX - minX);
    float boundheight = (maxY - minY);
    float boundarea = boundwidth * boundheight;
    removeDuplication(maxX - minX, i);

    hgrid.clear();
    hgrid = buildDensityGrid(minX, minY, maxX, maxY, dw, dh);
    Vector2d LLC(minX, minY);

    PathCBD pathCBD(pathList, M);
    std::vector<double> dataDepthValue;
    if(i != 0){
     dataDepthValue = pathCBD.fastSegmentSimplexDepth(i);
     segSortedIdx = pathCBD.sortSegmentIdx(dataDepthValue);
    }

    Vector2d testPos;
    float testVal;

    std::vector<float> colorValues;
    colorValues.resize(hgrid.size(), 0.0);
    std::vector<float> samplesizes;
    samplesizes.resize(hgrid.size(), 0.0);
    std::vector<double> trainingSetValues;
    trainingSetValues.resize(pathList.size(), 0.0);
    std::vector<double> trainingSetIndices;
    trainingSetIndices.resize(pathList.size(), 0.0);

    std::vector<Vector2d> trainingSet;
    std::vector<double> tmpVals;
    tmpVals.clear();
    std::vector<int> indicesList;

    // for each path
    int pathIdx = -1;
    for(pathIt = pathList.begin(); pathIt != pathList.end(); pathIt++){
     pathIdx ++;

     // a particular time point on this path
     curPos = (*pathIt)->posList[i];
     latLon = (*pathIt)->latLonList[i];

     if(segSortedIdx.size() > 0 && segSortedIdx[0] == pathIdx && i != 0)
      meanPoint = Vector2d(curPos->x, curPos->y);

     if(i != 0){
      std::vector<int>::iterator iit;
      iit = std::find(segSortedIdx.begin(), segSortedIdx.end(), pathIdx);
      int posIdx = iit - segSortedIdx.begin();
      double depthvalue = dataDepthValue[segSortedIdx[posIdx]];
      double indexvalue = double((segSortedIdx.size() - posIdx)*1.0) / double(segSortedIdx.size());
      trainingSetValues[pathIdx]  = depthvalue; 
      trainingSetIndices[pathIdx] = indexvalue;

      int idx;
      if(curPos->y < height && curPos->y > 0 && curPos->x < width && curPos->x > 0){
       Vector2d tmpcurpos(curPos->x, curPos->y);
       tmpcurpos = tmpcurpos - Vector2d(minX, minY);
       int idxCol = tmpcurpos.x / dw;
       int idxRow = tmpcurpos.y / dh;
       idx = idxRow * DensityGridCols + idxCol;
       if(idx < hgrid.size()){
        colorValues[idx] += indexvalue;
        samplesizes[idx] += 1;
       }
      }
      else{
       cout << "predicted location " << *latLon << " is out of map." << endl;
      }
      trainingSet.push_back(Vector2d(curPos->x, curPos->y));
      tmpVals.push_back(indexvalue);
      indicesList.push_back(idx);

     }
    }

    //RBF density values;
    RBF densityRBF;
    //compute KDTree density
    std::vector<int> validlist;
    std::vector<double> kdtreeden = KDTreeDensity<std::vector<Vector2d>>(trainingSet, trainingSet.size(), 2, validlist, boundwidth * 0.01); // 0.01 is a user defined parameter

    std::vector<Vector2d> validpoints;
    std::vector<double> validdensity;
    for(int j = 0; j < validlist.size(); j++)
    {
     if(validlist[j] != 0)
     {
      validpoints.push_back(trainingSet[j]);
      validdensity.push_back(kdtreeden[j]);
     }
    }
    int localmaxidx = std::max_element(validdensity.begin(), validdensity.end()) - validdensity.begin();
    Vector2d localmax = validpoints[localmaxidx];
    validpoints.push_back(localmax);
    validdensity.push_back(*std::max_element(validdensity.begin(), validdensity.end()));
    float rbflambda = 0.15;
    densityRBF.compute(validpoints, validdensity, boundwidth * rbflambda);

    std::vector<double> KNNdensity;
    for(int j = 0; j < trainingSet.size(); j++)
    {
     float f = densityRBF.eval(trainingSet[j]);
     KNNdensity.push_back(f);
     //float red, green, blue;
     //colorgradient.getColorAtValue(f, red, green, blue);
    }
    rbfradii.clear();
    rbfradii = buildRBFRadii(trainingSet, KNNdensity, indicesList, dw, rbfkernelfactor); 

    RBF rbf;
    rbf.compute(trainingSet, tmpVals, rbfradii);
    //producePaths(timeframe, rbf, rbfradii);
    return rbf;
   }

   void advisory::constructPathsSubset(std::vector<Vector2d*> hgrid, float dw, float dh, float hw, float hh, 
     int width, int height, 
     double tlat, double tlon, double mlat, double mlon,
     int timeframe, 
     float rbfkernelfactor){
    //std::vector< std::vector<double> > rbfradii;
    //std::vector<RBF> rbfs;
    if(RBFS.empty() && RBFRADII.empty())
    {
     for(int i = 1; i <= NumofTimesteps; i++)
     {
      std::vector<double> tmp;
      tmp.clear();
      RBF rbf = RBFatTime(hgrid, dw, dh, hw, hh, width, height, tlat, tlon, mlat, mlon, i, rbfkernelfactor, tmp); 
      RBFRADII.push_back(tmp);
      RBFS.push_back(rbf);
     }
    }
    //producePaths(timeframe, rbfs, rbfradii);
    producePaths(timeframe, RBFS, RBFRADII);
   }

void advisory::GeneratePathsByResamplingUDSpaces(){
 pathresampling->Process();
}

void advisory::GenerateStructuredPaths(){
 std::vector<double> errorRad = {0.0, 50.004, 100.471, 144.302, 187.515, 283.263, 390.463};
 //pathresampling->GenerateStructuredPaths(projPath, errorRad, &pathList);
 //pathresampling->GenerateStructuredPathsByPDSamplingInCircles(projPath, errorRad, &pathList);
 
 // Hurricane katrina
 // http://www.nhc.noaa.gov/archive/2005/mar/al122005.fstadv.016.shtml?
 std::vector<Vector4d*> centerline;
 centerline.push_back(new Vector4d(-84.4, 24.4, 0, 0));
 centerline.push_back(new Vector4d(-85.4, 24.5, 0, 0));
 centerline.push_back(new Vector4d(-87.0, 25.0, 0, 0));
 centerline.push_back(new Vector4d(-88.7, 26.0, 0, 0));
 centerline.push_back(new Vector4d(-89.9, 27.4, 0, 0));
 centerline.push_back(new Vector4d(-90.0, 31.5, 0, 0));
 
 //pathresampling->PhysicallyBasedPathsGeneration(centerline, errorRad, &pathList);
 //pathresampling->GenerateStructuredPathsByOptimizeCostMatrix(centerline, errorRad, &pathList);
 //pathresampling->GenerateStructuredPathsByPartitingODSpace();
 //pathresampling->DrawDebugInfo();
 //pathresampling->DrawOriginalPathsEnsemble();

  //double curDeg = 0;
  //Vector2d draw1;

  //for(unsigned int i = 1; i < projPath.size()-1; i++){
  // glBegin(GL_LINES);
  // for(curDeg = 0; curDeg < 360.0; curDeg++){
  //  draw1 = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRad[i], curDeg);
  //  draw1 = translateToScreen(draw1.x, draw1.y);
  //  glVertex3f(draw1.x, draw1.y, 0.0);
  // }
  // glEnd();
  //}

}

 
