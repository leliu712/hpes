#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <vector>
#include <string>
#include <cassert>
#include <fstream>
#include <iostream>

using namespace std;


template<typename T, typename S>
void WritePoints2CSVFile(const string& ofilename,const int n, vector<T>* points, vector<S>* v0 = NULL, vector<S>* v1 = NULL){
    if(v0){
     assert(points->size() == v0->size());
    }
    if(v1){
     assert(points->size() == v1->size());
    }
    std::fstream ofile;
    ofile.open(ofilename, ios::out);
    //ofile << "x,y,z,w" << endl;
    for(unsigned int i =0; i < points->size(); i++){
        for(int j = 0; j < n; j++){
            ofile << points->at(i)[j] << ", ";
        }
        if(v0){
            ofile << v0->at(i) << ", ";
        } else {
            ofile << 0.0f << ", ";
        }
        if(v1){
            ofile << v1->at(i);
        } else {
            ofile << 0.0f;
        }
        ofile << endl;
    }
    ofile.close();
    cout << "Finish writing points to " << ofilename << endl;
}

void ReadCSVFile(const string& filename, char sep, std::vector<string>* output);





#endif // FILEMANAGER_H
