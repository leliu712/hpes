#include "Splatting.h"
#include <cmath>
#include <omp.h>

#define EE 2.718281

#include <QImage>
#include <QTimer>
#include <QEvent>
#include <QString>
#include <QFileDialog>
#include <QMouseEvent>

#include <QPixmap>
#include <QPoint>
#include <QPainter>

#include <QPushButton>



#ifdef __APPLE__
# include<GLUT/glut.h>
#else
# include<GL/glut.h>
# include<GL/glu.h>
#endif

void Splatting::splat(){
 //cout << WKernel(0) << endl;
 //cout << WKernel(.5 * R) << endl;
 //cout << WKernel(.7 * R) << endl;
 //cout << WKernel(R) << endl;
 //exit(0);
 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
#pragma omp parallel for
 for(int i = 0; i < grid.size(); i++){
  Vector2d corners[4];
  corners[0] = grid[i][0];
  corners[1] = grid[i][1];
  corners[2] = grid[i][2];
  corners[3] = grid[i][3];
  Vector2d center = corners[0] + (corners[2] - corners[0]) / 2.0;
  Vector2d* tmp = new Vector2d(center.x, center.y);
  update(tmp, i);
 }
}

void Splatting::update(Vector2d* _p, int idx){
 double sn = 0.0;
 double alpha = 0.0;
 Vector2d p(_p->x, _p->y);
 for(int i = 0; i < grid.size(); i++){
  Vector2d corners[4];
  corners[0] = grid[i][0];
  corners[1] = grid[i][1];
  corners[2] = grid[i][2];
  corners[3] = grid[i][3];
  Vector2d center = corners[0] + (corners[2] - corners[0]) / 2.0;
  double dis = (p - center).norm();
  if(R >= dis){
   double d = values[i];
   double w = 0.0;
   w = WKernel(dis);
   sn    += w * d;
   alpha += w;
  }
 }
 values[idx] = sn / alpha;
 if(alpha < 10e-6 || sn < 10e-6)
  values[idx] = 0.0;
 if(values[idx] < 10e-6)
  values[idx] = 0.0;
}

double Splatting::WKernel(double dis){
 double std = R / 3.0;
 double part1 = pow(EE, -pow(dis, 2) / (2 * pow(std, 2)));
 double part2 = cos(PI/2.0 * dis / (3 * std));
 double w = part1 * part2;
 if(w < 10e-6)
  w = 0.0;
 return w;
}

void Splatting::copyTo(float* vals){
 for(int i = 0; i < grid.size(); i++){
  //if(vals[i] != 0)
  // cout << vals[i] << " " << values[i] << endl;
  vals[i] = values[i];
 }
}
