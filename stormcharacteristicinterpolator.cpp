#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#include <GL/glu.h>
#endif

#include "filemanager.h"
#include "stormcharacteristicinterpolator.h"

#include "PathCBD.h" // for sipmlicial depth computation
#include "ColorGradient.h"
#include "CDFSample.h"
#include "KDTreeDensityEstimator.h"
#include "filemanager.h"

#include <omp.h>

#include <QImage>
#include <QColor>
#include <QPoint>
#include <QRgb>
#include <QProcess>
#include <QDebug>
#include <QStringList>

#include <utility>
#include <unordered_set>
#include <boost/functional/hash.hpp>

using namespace std;

extern string OutputPrefix;
extern string DatafileName;

// Debug

//#define OUTPUT_SCALAR_FIELD
//#define RUN_MATLAB_OLS

StormCharacteristicInterpolator::StormCharacteristicInterpolator(std::vector<StormCharacteristicInterpolator::PredictionNode> &predictions, const int timepoint) :
    _predictions(predictions)
{
    //PrintPredictions();
    // First computes rbf from full ensemble
    ParsePredictions();
    ComputeBBox();
    vector<int> validlist = ComputeKDNNDensity();
    // correct predictions based on knn valid list
    CorrectPredictions(validlist);
    ComputeRBFRadiiUsingKNNDensity();
    ComputeSimplicialDepth();
#ifdef OUTPUT_SCALAR_FIELD
    ComputeSDRBF(timepoint);
    ComputeSizeRBF(timepoint);
    ComputeIntensityRBF(timepoint);
#endif
    //for(unsigned int i = 0; i < _stormintensities.size(); i++){
    //    Vector2d p = _locations.at(i);
    //    float f = _intensityrbf.eval(p, _rbfradii);
    //    cout << i << " " << _stormintensities.at(i) << " " << f << endl;
    //}
    //exit(0);

    // Output original locations with sd value to file
    string csvfile = OutputPrefix + "StatisticsVerification/" + "originallocations-sd-" + to_string(timepoint) + ".csv";
    WritePoints2CSVFile<Vector2d, double>(csvfile, 2, &Locations(), &SDValues(), &RBFRadii());
    // Output original locations with sizes to file
    csvfile = OutputPrefix + "StatisticsVerification/" + "originallocations-size-" + to_string(timepoint) + ".csv";
    WritePoints2CSVFile<Vector2d, double>(csvfile, 2, &Locations(), &StormSizes(), &RBFRadii());
    // Output original locations with intensities to file
    csvfile = OutputPrefix + "StatisticsVerification/" + "originallocations-intensity-" + to_string(timepoint) + ".csv";
    WritePoints2CSVFile<Vector2d, double>(csvfile, 2, &Locations(), &StormIntensities(), &RBFRadii());

#ifdef RUN_MATLAB_OLS
    // run ols selection on storm characteristics
    OLSSelection(timepoint, QString("sd"));
    OLSSelection(timepoint, QString("size"));
    OLSSelection(timepoint, QString("intensity"));
#endif

    qDebug() << ">>>>Start building RBFs";
    BuildRBFFromSubset(timepoint, "sd");
    qDebug() << ">>>>sd";
    BuildRBFFromSubset(timepoint, "size");
    qDebug() << ">>>>size";
    BuildRBFFromSubset(timepoint, "intensity");
    qDebug() << ">>>>intensity";
    qDebug() << ">>>>Finish building RBFs";
#ifdef OUTPUT_SCALAR_FIELD
    WriteReproducedField2Image(timepoint, "sd", _subsdrbf);
    WriteReproducedField2Image(timepoint, "size", _sizerbf);
    WriteReproducedField2Image(timepoint, "intensity", _intensityrbf);
#endif
}

StormCharacteristicInterpolator::StormCharacteristicInterpolator(const std::vector<Vector2d> &locations, QString* ofilename, std::vector<Vector2d> *externalpoints, std::vector<Vector2d>* bbox, const int timepoint) :
    _locations(locations)
{
    if(!externalpoints){
        ComputeBBox();
    } else {
        ComputeBBoxFromPointSet(*externalpoints);
    }
    ComputeKDNNDensity();
    // update locations as kdtvalidpoints
    _locations = _validKNNlocations;
    ComputeRBFRadiiUsingKNNDensity();
    ComputeSimplicialDepth();
    ComputeSDRBF(timepoint);

    if(bbox != NULL){
        _bbox = *bbox;
    }
    if(ofilename){
        WriteScalarField2Image(_sdrbf,_sdradii, true, *ofilename);
    } else{
        WriteScalarField2Image(_sdrbf,_sdradii, true);
    }

    // Output original locations with sd value to file
    string csvfile = OutputPrefix + "StatisticsVerification/" + "originallocations-sd-" + to_string(timepoint) + ".csv";
    WritePoints2CSVFile<Vector2d, double>(csvfile, 2, &Locations(), &SDValues(), &RBFRadii());

    // run ols selection on SD
    OLSSelection(timepoint, QString("sd"));

    //Process(ofilename);
}

void StormCharacteristicInterpolator::ParsePredictions()
{
    cout << "Parse predictions ..." << endl;
    //cout << _predictions.size() << endl;
    //unordered_set<pair<double, double>, boost::hash<pair<double, double>>> is_visited;
    //vector<PredictionNode> tmp;
    //for(int i = 0; i < _predictions.size(); i++){
    //    Vector2d p = _predictions.at(i)._loc;
    //    pair<double, double> mypair(p.x, p.y);
    //    if(is_visited.count(mypair)){
    //        continue;
    //    }
    //    is_visited.insert(mypair);
    //    tmp.push_back(_predictions.at(i));
    //    _locations.push_back(p);
    //    _stormsizes.push_back(_predictions.at(i)._size);
    //    _stormintensities.push_back(_predictions.at(i)._intensity);
    //}
    //_predictions = tmp;
    //cout << _predictions.size() << endl;
    _locations.resize(_predictions.size());
    _stormintensities.resize(_predictions.size());
    _stormsizes.resize(_predictions.size());
    for(unsigned int i = 0; i < _predictions.size(); i++){
        _locations.at(i) = _predictions.at(i)._loc;
        _stormintensities.at(i) = _predictions.at(i)._intensity;
        _stormsizes.at(i) = _predictions.at(i)._size;
    }
    cout << "Finish parsering predictions. " << endl;
}

void StormCharacteristicInterpolator::CorrectPredictions(const std::vector<int> validlist)
{
    vector<PredictionNode> tmp;
    for(unsigned int i = 0; i < validlist.size(); i++){
        if(validlist[i] == 0){
            continue;
        }
        tmp.push_back(_predictions.at(i));
    }
    _predictions = tmp;
    ParsePredictions();
}

void StormCharacteristicInterpolator::BuildRBFFromSubset(const int timepoint, const string &type)
{
 string subsetfilename = OutputPrefix + "StatisticsVerification/OLS-selected-samples-" + type + "-" + to_string(timepoint) + ".csv";
 vector<string> datastring;
 ReadCSVFile(subsetfilename, ',', &datastring);
 cout << "Read subset of sd from " << subsetfilename << endl;
 vector<double> values;
 vector<double> rbfradii;
 vector<Vector2d> points;
 // Remove duplicated points
 unordered_set<pair<double, double>, boost::hash<pair<double, double>>> pointset;
 for(auto& c : datastring){
  istringstream ss(c);
  vector<double> vals;
  string token;
  while(getline(ss, token, ',')){
   double f = stod(token);
   vals.push_back(f);
  }
  // the first value is index, and matlab index starts from 1
  int index = int(vals[0]) - 1;
  if(index > _locations.size()){
      continue;
  }
  Vector2d sample = _locations.at(index);
  pair<double, double> mypair(sample.x, sample.y);
  if(pointset.count(mypair)){
      continue;
  }
  pointset.insert(mypair);
  points.push_back(_locations.at(index));
  cout << "matlab index: " << index << "; " << _sdvalues.size() << " " << _stormsizes.size() << " " << _stormintensities.size() << endl;
  if(type == "sd"){
      values.push_back(_sdvalues.at(index));
  } else if (type == "size"){
      values.push_back(_stormsizes.at(index));
  } else if (type == "intensity"){
      values.push_back(_stormintensities.at(index));
  } else {
      cerr << type << " is not a valid storm charateristic." << endl;
      exit(-1);
  }
  rbfradii.push_back(_rbfradii.at(index));
 }

 assert(points.size() == rbfradii.size() && points.size() == values.size());
 if(type == "sd"){
     _subsdrbf.compute(points, values, rbfradii);
     _sdradii = rbfradii;
 } else if (type == "size"){
     _subsizerbf.compute(points, values, rbfradii);
     _sizeradii = rbfradii;
 } else if (type == "intensity"){
     _subintensityrbf.compute(points, values, rbfradii);
     _intensityradii = rbfradii;
 } else {
     cerr << "unknown storm characteristc " << type << endl;
     exit(0);
 }
 return;

 //RBF rbf;
 //rbf.compute(points, values, rbfradii);
 ////if(timepoint == 5 && type == "intensity"){
 ////for(unsigned int i = 0; i < points.size(); i++){
 ////    cout << points.at(i) << " " << values.at(i) << " " << rbfradii.at(i) << endl;
 ////}
 ////exit(0);
 ////}
 //return rbf;
}

void StormCharacteristicInterpolator::Process(QString *ofilename)
{
}

void StormCharacteristicInterpolator::OLSSelection(const int timepoint, const QString &type)
{
    qDebug() << ">>>>" << "Start OLS selection on " << type;
    // run OLS matlab script
    QString program("/bin/sh");
    string strargs = "/home/lel/Projects/HurVis/matlab-rbf/olsselect.sh";
    QString args(QString::fromStdString(strargs));
    QProcess process;
    process.start(program, QStringList() << args << QString::fromStdString(to_string(timepoint)) << type << QString::fromStdString(DatafileName));
    if(!process.waitForFinished(30000)){
        cerr << "Matlab script was not executed correctly." << endl;
    }
    QString output(process.readAllStandardOutput());
    qDebug() << output;
    process.close();
    //string subsetfile = OutputPrefix + "StatisticsVerification/OLS-selected-samples-" + to_string(_timepoint) + ".csv";
    qDebug() << "<<<<" << "Finish OLS selection on " << type;
}

void StormCharacteristicInterpolator::ComputeSimplicialDepth()
{
    vector<double> sdvalues = PathCBD::fastSegmentSimplexDepth3(_locations);
    // Normalizes to [0, 1] using rankings
    vector<int> sortedindices = PathCBD::sortSegmentIdx(sdvalues);
    for(unsigned int i = 0; i < sortedindices.size(); i++){
        sdvalues[sortedindices[i]] = 1.0 - i * 1.0 / sortedindices.size();
    }
    _sdvalues = sdvalues;
}

void StormCharacteristicInterpolator::ComputeBBox()
{
  // compute bounding box of predicted locations
  float maxX = numeric_limits<float>::min();
  float maxY = numeric_limits<float>::min();
  float minX = numeric_limits<float>::max();
  float minY = numeric_limits<float>::max();
  for(unsigned int i = 0; i < _locations.size(); i++){
   Vector2d p = _locations[i];
   maxX = std::max(maxX, float(p.x));
   maxY = std::max(maxY, float(p.y));
   minX = std::min(minX, float(p.x));
   minY = std::min(minY, float(p.y));
  }

  _bbox.clear();
  _bbox.resize(2);
  _bbox[0] = Vector2d(minX, minY);
  _bbox[1] = Vector2d(maxX, maxY);
}

void StormCharacteristicInterpolator::ComputeBBoxFromPointSet(const std::vector<Vector2d> &points)
{
  // compute bounding box of external points set
  float maxX = numeric_limits<float>::min();
  float maxY = numeric_limits<float>::min();
  float minX = numeric_limits<float>::max();
  float minY = numeric_limits<float>::max();
  for(unsigned int i = 0; i < points.size(); i++){
   Vector2d p = points[i];
   maxX = std::max(maxX, float(p.x));
   maxY = std::max(maxY, float(p.y));
   minX = std::min(minX, float(p.x));
   minY = std::min(minY, float(p.y));
  }

  _bbox.clear();
  _bbox.resize(2);
  _bbox[0] = Vector2d(minX, minY);
  _bbox[1] = Vector2d(maxX, maxY);
}

void StormCharacteristicInterpolator::ComputeBBoxFromPointSet(const std::vector<Vector2d> &points, std::vector<Vector2d> *bbox)
{
  // compute bounding box of external points set
  float maxX = numeric_limits<float>::min();
  float maxY = numeric_limits<float>::min();
  float minX = numeric_limits<float>::max();
  float minY = numeric_limits<float>::max();
  for(unsigned int i = 0; i < points.size(); i++){
   Vector2d p = points[i];
   maxX = std::max(maxX, float(p.x));
   maxY = std::max(maxY, float(p.y));
   minX = std::min(minX, float(p.x));
   minY = std::min(minY, float(p.y));
  }

  bbox->clear();
  bbox->resize(2);
  bbox->at(0) = Vector2d(minX, minY);
  bbox->at(1) = Vector2d(maxX, maxY);
}

std::vector<int> StormCharacteristicInterpolator::ComputeKDNNDensity()
{
    vector<int> validlist;
    float width = _bbox[1].x - _bbox[0].x;
    float scalefactor = 0.02;
    vector<double> density = KDTreeDensity<vector<Vector2d>>(_locations, _locations.size(), 2, validlist, width * scalefactor, 10);
    assert(density.size() == _locations.size());
    _validKNNlocations.clear();
    _knndensities.clear();
    for(unsigned int i = 0; i < validlist.size(); i++){
        if(validlist[i] == 0){
            continue;
        }
        _validKNNlocations.push_back(_locations[i]);
        _knndensities.push_back(density[i]);
    }
    assert(_validKNNlocations.size() == _knndensities.size());
    return validlist;
}

void StormCharacteristicInterpolator::ComputeKDNNDensity(const std::vector<Vector2d> &points, const std::vector<Vector2d> &bbox, std::vector<Vector2d> *validpoints, std::vector<double> *densities)
{
    vector<int> validlist;
    vector<Vector2d> validKDTLocations;
    vector<double> kdtdensities;
    float width = bbox[1].x - bbox[0].x;
    float scalefactor = 0.01;
    vector<double> density = KDTreeDensity<vector<Vector2d>>(points, points.size(), 2, validlist, width * scalefactor, 10);
    assert(density.size() == points.size());
    validKDTLocations.clear();
    kdtdensities.clear();
    for(unsigned int i = 0; i < validlist.size(); i++){
        if(validlist[i] == 0){
            continue;
        }
        validKDTLocations.push_back(points[i]);
        kdtdensities.push_back(density[i]);
    }
    assert(validKDTLocations.size() == kdtdensities.size());
    *validpoints = validKDTLocations;
    *densities = kdtdensities;
}

void StormCharacteristicInterpolator::ComputeKNNDensityRBF()
{
    float width = _bbox[1].x - _bbox[0].x;
    float rbflambda = 0.001;
    _kdtdensityrbf.compute(_validKNNlocations, _knndensities, rbflambda);
}

void StormCharacteristicInterpolator::ComputeRBFRadiiUsingKNNDensity()
{
    double width = _bbox[1].x - _bbox[0].x;
    double cellwidth = width * 0.1; // need to fix
    double rbfkernalfactor = 0.3;
    _rbfradii.clear();
    _rbfradii.resize(_locations.size());
    for(unsigned int i = 0; i < _locations.size(); i++){
        Vector2d p = _locations[i];
        double density = _knndensities[i];
        //density = density == 0 ? numeric_limits<double>::max() : density;
        if(density == 0){
            cerr << "0 density" << endl;
            exit(-1);
        }
        double f = sqrt(density);
        f = cellwidth / f * rbfkernalfactor;
        _rbfradii[i] = f;
    }
}

void StormCharacteristicInterpolator::ComputeRBFRadiiUsingKNNDensity(const std::vector<Vector2d> &bbox, std::vector<Vector2d> &points, std::vector<double> &densities, std::vector<double> *rbfradii)
{
    double width = bbox[1].x - bbox[0].x;
    double cellwidth = width * 0.1; // need to fix
    double rbfkernalfactor = 0.3;
    rbfradii->clear();
    rbfradii->resize(points.size());
    for(unsigned int i = 0; i < points.size(); i++){
        double density = densities[i];
        density = density == 0 ? numeric_limits<double>::max() : density;
        double f = sqrt(density);
        f = cellwidth / f * rbfkernalfactor;
        f = f < 10e-6 ? f = 0 : f;
        rbfradii->at(i) = f;
    }
}

RBF StormCharacteristicInterpolator::ComputeRBF(const std::vector<Vector2d> &samples, const std::vector<double> &values, const std::vector<double> &radii)
{
    RBF rbf;
    rbf.compute(samples, values, radii);
    return rbf;
}

void StormCharacteristicInterpolator::ComputeSDRBF(const int timepoint)
{
    //_sdrbf.compute(_locations, _sdvalues, _rbfradii);
    _sdrbf = ComputeRBF(_locations, _sdvalues, _rbfradii);
#ifdef OUTPUT_SCALAR_FIELD
    string str = OutputPrefix + "StatisticsVerification/" + to_string(timepoint) + "-fullfield-sd" + ".png";
    QString ofilename(QString::fromStdString(str));
    WriteScalarField2Image(_sdrbf, _rbfradii, true, ofilename);
#endif
}

void StormCharacteristicInterpolator::ComputeSizeRBF(const int timepoint)
{
    _sizerbf = ComputeRBF(_locations, _stormsizes, _rbfradii);
    _sizerbf.compute(_locations, _stormsizes, 3);
#ifdef OUTPUT_SCALAR_FIELD
    string str = OutputPrefix + "StatisticsVerification/" + to_string(timepoint) + "-fullfield-size" + ".png";
    QString ofilename(QString::fromStdString(str));
    WriteScalarField2Image(_sizerbf, _rbfradii, true, ofilename);
#endif
}

void StormCharacteristicInterpolator::ComputeIntensityRBF(const int timepoint)
{
    _intensityrbf = ComputeRBF(_locations, _stormintensities, _rbfradii);
#ifdef OUTPUT_SCALAR_FIELD
    string str = OutputPrefix + "StatisticsVerification/" + to_string(timepoint) + "-fullfield-intensity" + ".png";
    QString ofilename(QString::fromStdString(str));
    WriteScalarField2Image(_intensityrbf, _rbfradii, true, ofilename);
#endif
}

bool StormCharacteristicInterpolator::IsAjacent(const int i, const int j)
{
    int ii = i == 6 ? -1 : i;
    ii = i == 7 ? 0 : i;
    int jj = j == 6 ? -1 : j;
    jj = j == 7 ? 0 : j;
    return abs(ii - jj) == 1 ? true : false;
}

int StormCharacteristicInterpolator::StormCategory(const double windspeed, const bool needCorrect)
{
    // category boundaries
    vector<float> cb = {33.0f, 63.0f, 82.0f, 95.0f, 112.0f, 136.0f};
    vector<float> delta = {cb[0], cb[1] - cb[0], cb[2] - cb[1], cb[3] - cb[2], cb[4] - cb[3], cb[5] - cb[4]};
    float factor = 0.10;
    int res = 0;
    if(windspeed <= cb[0]){
        res = 6;
        if(needCorrect && windspeed > cb[0] - delta[0] * factor){
            res = 7;
        }
    } else if(windspeed > cb[0] && windspeed <= cb[1]){
        res = 7;
        if(needCorrect && windspeed < cb[0] + delta[1] * factor){
            res = 6;
        } else if(needCorrect && windspeed > cb[1] - delta[1] * factor){
            res = 1;
        }
    } else if(windspeed > cb[1] && windspeed <= cb[2]){
        res = 1;
        if(needCorrect && windspeed < cb[1] + delta[2] * factor){
            res = 7;
        } else if(needCorrect && windspeed > cb[2] - delta[2] * factor){
            res = 2;
        }
    } else if(windspeed > cb[2] && windspeed <= cb[3]){
        res = 2;
        if(needCorrect && windspeed < cb[2] + delta[3] * factor){
            res = 1;
        } else if(needCorrect && windspeed > cb[3] - delta[3] * factor){
            res = 3;
        }
    } else if(windspeed > cb[3] && windspeed <= cb[4]){
        res = 3;
        if(needCorrect && windspeed < cb[3] + delta[4] * factor){
            res = 2;
        } else if(needCorrect && windspeed > cb[4] - delta[4] * factor){
            res = 4;
        }
    } else if(windspeed > cb[4] && windspeed <= cb[5]){
        res = 4;
        if(needCorrect && windspeed < cb[4] + delta[5] * factor){
            res = 3;
        } else if(needCorrect && windspeed > cb[5] - delta[5] * factor){
            res = 5;
        }
    } else if(windspeed > cb[5]){
        res = 5;
        if(needCorrect && windspeed < cb[5] + delta[5] * factor){
            res = 4;
        }
    } else {
        cerr << "unknown wind speed " << windspeed << endl;
        exit(0);
    }
    return res;
}


// This color schema is not used
void StormCharacteristicInterpolator::StormCategoryColorMapping(const int category, float *r, float *g, float *b)
{
    switch(category){
    case 6:
        *r = 129.0f / 255.0f;
        *g = 15.0f / 255.0f;
        *b = 124.0f / 255.0f;
        return;
    case 7:
        *r = 77.0f / 255.0f;
        *g = 0.0f / 255.0f;
        *b = 75.0f / 255.0f;
        return;
    case 1:
        *r = 253.0f / 255.0f;
        *g = 141.0f / 255.0f;
        *b = 60.0f / 255.0f;
        return;
    case 2:
        *r = 252.0f / 255.0f;
        *g = 78.0f / 255.0f;
        *b = 42.0f / 255.0f;
        return;
    case 3:
        *r = 227.0f / 255.0f;
        *g = 26.0f / 255.0f;
        *b = 28.0f / 255.0f;
        return;
    case 4:
        *r = 189.0f / 255.0f;
        *g = 0.0f / 255.0f;
        *b = 38.0f / 255.0f;
        return;
    case 5:
        *r = 128.0f / 255.0f;
        *g = 0.0f / 255.0f;
        *b = 38.0f / 255.0f;
        return;
    default:
        cerr << "unknown storm category " << category << endl;
        *r = 1;
        *b = 0;
        *g = 0;
        exit(0);
        return;
    }

}

void StormCharacteristicInterpolator::StormCategoryColorMapping2(const int category, float *r, float *g, float *b)
{
    switch(category){
    // tropical depression -- green
    case 6:
        *r = 90.0f / 255.0f;
        *g = 174.0f / 255.0f;
        *b = 97.0f / 255.0f;
        return;
    // tropical storm -- grey
    case 7:
        *r = 0.4f;
        *g = 0.4f;
        *b = 0.4f;
        return;
    // 1 -- purple
    case 1:
        *r = 153.0f / 255.0f;
        *g = 112.0f / 255.0f;
        *b = 171.0f / 255.0f;
        return;
    // 2 -- brown
    case 2:
        *r = 191.0f / 255.0f;
        *g = 129.0f / 255.0f;
        *b = 45.0f / 255.0f;
        return;
    // 3 -- orange
    case 3:
        *r = 224.0f / 255.0f;
        *g = 130.0f / 255.0f;
        *b = 20.0f / 255.0f;
        return;
    // 4 -- pink
    case 4:
        *r = 255.0f / 255.0f;
        *g = 127.0f / 255.0f;
        *b = 127.0f / 255.0f;
        return;
    // 5 -- red
    case 5:
        *r = 255.0f / 255.0f;
        *g = 80.0f / 255.0f;
        *b = 80.0f / 255.0f;
        return;
    default:
        cerr << "unknown storm category " << category << endl;
        *r = 1;
        *b = 0;
        *g = 0;
        exit(0);
        return;
    }

}

void StormCharacteristicInterpolator::WriteReproducedField2Image(const int timepoint, const string &type, RBF rbf)
{
    string str = OutputPrefix + "StatisticsVerification/" + to_string(timepoint) + "-subfield-" + type + ".png";
    QString subfieldname(QString::fromStdString(str));
    vector<double> radii;
    if(type == "sd"){
        radii = _sdradii;
    } else if (type == "size"){
        radii = _sizeradii;
    } else if (type == "intensity"){
        radii = _intensityradii;
    }
    WriteScalarField2Image(rbf, radii, true, subfieldname);
}

void StormCharacteristicInterpolator::WriteScalarField2Image(RBF rbf, const std::vector<double> radii, const bool withradii, const QString &filename, const int imagewidth)
{
    vector<Vector2d> bigbbox(2);
    Vector2d p(-100, 40);
    p = translateToScreen(p.x,p.y);
    Vector2d q(-75, 20);
    q = translateToScreen(q.x, q.y);
    glColor4f(1, 0, 0, 1);
    bigbbox[0] = p;
    bigbbox[1] = q;
    //const float width = _bbox[1].x - _bbox[0].x;
    //const float height = _bbox[1].y - _bbox[0].y;
    //const Vector2d llc(_bbox[0]);
    const float width = bigbbox[1].x - bigbbox[0].x;
    const float height = bigbbox[1].y - bigbbox[0].y;
    const Vector2d llc(bigbbox[0]);
    const float aspectratio = height / width;
    const int imageheight = imagewidth * aspectratio;
    const float fx = width / float(imagewidth);
    const float fy = height / float(imageheight);
    cout << width << " " << height << endl;
    ColorGradient colorgradient;
    float r(.0f), g(.0f), b(.0f);
    QImage oimage(imagewidth, imageheight, QImage::Format_RGB888);
    oimage.fill(QColor(Qt::white).rgb());
    int npro = omp_get_max_threads();
    omp_set_num_threads(npro);
    vector<vector<double>> interpolatedvalues(imagewidth, vector<double>(imageheight));
    double maxval(numeric_limits<double>::min());
    double minval(numeric_limits<double>::max());
#pragma omp parallel for private(r, g, b)
    for(int i = 0; i < imagewidth; i++){
        for(int j = 0; j < imageheight; j++){
            Vector2d p(i * fx + llc.x, j * fy + llc.y);
            float f(0.0f);
            //omp_set_lock(&writelock);
            if(withradii){
                f = rbf.eval(p, radii);
                interpolatedvalues.at(i).at(j) = f;
                maxval = f > maxval ? f : maxval;
                minval = f < minval ? f : minval;
                //colorgradient.getColorAtValue(f, r, g, b);
                //oimage.setPixel(i, j, qRgb(int(r * 255), int(g * 255), int(b * 255)));
            } else {
                f = rbf.eval(p);
                interpolatedvalues.at(i).at(j) = f;
                maxval = f > maxval ? f : maxval;
                minval = f < minval ? f : minval;
                //colorgradient.getColorAtValue(f, r, b, g);
                //oimage.setPixel(i, j, qRgb(int(r * 255), int(g * 255), int(b * 255)));
            }
        }
    }

    for(int i = 0; i < imagewidth; i++){
        for(int j = 0; j < imageheight; j++){
            double f = interpolatedvalues.at(i).at(j);
            f = (f - minval) / (maxval - minval);
            colorgradient.getColorAtValue(f, r, b, g);
            oimage.setPixel(i, j, qRgb(int(r * 255), int(g * 255), int(b * 255)));
        }
    }

    oimage.save(filename);
}

void StormCharacteristicInterpolator::DrawLocationWithScalarValues(const std::vector<Vector2d>& locations, const std::vector<double>& values)
{
    assert(locations.size() == values.size());
    double minval = *min_element(values.begin(), values.end());
    double maxval = *max_element(values.begin(), values.end());
    ColorGradient colorgradient;
    float r(.0f), g(.0f), b(.0f);
    for(unsigned int i = 0; i < locations.size(); i++){
        Vector2d p = locations[i];
        float f = values[i];
        f = (f - minval) / (maxval - minval);
        colorgradient.getColorAtValue(f, r, g, b);
        glColor4f(r, g, b, 1);
        CDFSample::drawSolidCircle(p.x, p.y, 3);
    }
}

void StormCharacteristicInterpolator::DrawScalarField(RBF rbf, const bool withradii)
{
    ColorGradient colorgradient;
    float r(.0f), g(.0f), b(.0f);
    int npro = omp_get_max_threads();
    omp_set_num_threads(npro);
    for(int i = int(_bbox[0].x); i < int(_bbox[1].x); i++){
        for(int j = int(_bbox[0].y); j < int(_bbox[1].y); j++){
            Vector2d p(i, j);
            float f(0.0f);
            if(withradii){
                f = rbf.eval(p, _rbfradii);
            } else {
                f = rbf.eval(p);
            }
            colorgradient.getColorAtValue(f, r, g, b);
            glColor4f(r,g,b,1);
            glBegin(GL_POINTS);
            glVertex3f(p.x, p.y, 0);
            glEnd();
        }
    }
}

void StormCharacteristicInterpolator::DrawReproducedScalarValues(RBF rbf)
{
    ColorGradient colorgradient;
    float r(.0f), g(.0f), b(.0f);
    for(const auto& p : _locations){
        float f = rbf.eval(p, _rbfradii);
        colorgradient.getColorAtValue(f, r, g, b);
        glColor4f(r, g, b, 1);
        CDFSample::drawSolidCircle(p.x, p.y, 3);
    }
}

void StormCharacteristicInterpolator::PrintPredictions()
{
    for(unsigned int i = 0; i < _predictions.size(); i++){
        cout << i << " ";
        cout << _predictions.at(i)._loc << " "
             << _predictions.at(i)._size << " "
             << _predictions.at(i)._intensity << endl;
    }
}
