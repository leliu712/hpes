#include <unistd.h>
#include <time.h>
#include <omp.h>
#include "RBF.h"
#include <Eigen/Dense>
#include <Eigen/Core>
#include <Eigen/SparseCore>
#include <Eigen/IterativeLinearSolvers>


uint32_t getTick(){
 timespec ts;
 unsigned theTick = 0U;
 //clock_gettime(CLOCK_REALTIME, &ts);
 theTick  = ts.tv_nsec / 1000000;
 theTick += ts.tv_sec * 1000;
 return theTick;
}

void RBF::compute(std::vector<Vector2d> trainingSet,
  std::vector<double> trainingValues, double f){
 struct timespec time_start={0, 0};
 struct timespec time_end  ={0, 0};
 //clock_gettime(CLOCK_REALTIME, &time_start);
 //:cout << "RBF training set size: " << trainingSet.size() << endl;
 samples = trainingSet;
 sampleValues = trainingValues;
 int size = samples.size();
 Phi = Matrix(size, size);
 W.setsize(size);
 F.setsize(size);
 kernelFactor = f;

 // use eigen instead of dhouse vector and matrix
 Eigen::MatrixXd Phi2(size, size);
 Eigen::VectorXd F2(size);
 Eigen::VectorXd W2(size);

 // set values
 for(int j = 0; j < samples.size(); j++)
  F2[j] = sampleValues[j]; 

 // build RBF matrix 
 for(int j = 0; j < samples.size(); j++){
  Vector2d pos0 = samples[j];
  for(int k = 0; k < samples.size(); k++){
   Vector2d pos1 = samples[k];
   double w = RBFkernel(pos0, pos1);
   Phi2(j, k) = w;
  }
 }

 // compute rbf weights
 //W = Phi.inv() * F;
 double lamda = 0.1;
 Eigen::MatrixXd I(size, size);
 for(int j = 0; j < size; j++){
  for(int k = 0; k < size; k++){
   if(j == k)
    I(j, k) = 1;
   else
    I(j, k) = 0;
  }
 }
 //I.print(7,7);

 Phi2 = Phi2 + I;
 Eigen::MatrixXd I2(size, size);
 for(int j = 0; j < size; j++){
  for(int k = 0; k < size; k++){
   if(j == k)
    I2(j,k) = 1;
   else
    I2(j,k) = 0;
  }
 }

 //W2 = Phi2.fullPivHouseholderQr().solve(F2);
 W2 = (Phi2.transpose() * Phi2 + lamda * I2).inverse() * Phi2.transpose() * F2;

 for(int j = 0; j < size; j++){
  W[j] = W2(j);
 }

 //clock_gettime(CLOCK_REALTIME, &time_end);
 //printf("duration: %llus %lluns\n", time_end.tv_sec-time_start.tv_sec, time_end.tv_nsec - time_start.tv_nsec);

 //W = (Phi.transpose() * Phi + lamda * I).inv() * Phi.transpose() * F;

 //cout << Phi << endl;
 //cout << "testphi" << endl;
 //cout << Phi.inv() << endl;
 //cout << W << endl;
 //cout << I << endl;
}

void RBF::compute(std::vector<Vector2d> trainingSet,
  std::vector<double> trainingValues, std::vector<double> factors){
 //cout << "RBF training set size: " << trainingSet.size() << endl;
 samples.clear();
 samples.clear();
 samples = trainingSet;
 sampleValues = trainingValues;
 int size = samples.size();
 //Phi = Matrix(size, size);
 W.setsize(size);
 //F.setsize(size);
 for(int i= 0; i < size; i++)
  W[i] = 0.0;

 // set values
 //for(int j = 0; j < samples.size(); j++)
 // F[j] = sampleValues[j]; 

 // use eigen instead of dhouse vector and matrix
 Eigen::VectorXd F2(samples.size());
 for(int j = 0; j < samples.size(); j++)
 {
  F2[j] = sampleValues[j]; 
  //int tmp = int(F2[j] * 1000);
  //F2[j] = tmp * 0.001;
 }

 // build RBF matrix 
 //for(int j = 0; j < samples.size(); j++){
 // Vector2d pos0 = samples[j];
 // for(int k = 0; k < samples.size(); k++){
 //  Vector2d pos1 = samples[k];
 //  double w = RBFkernel(pos0, pos1, factors[k]);
 //  Phi[j][k] = w;
 // }
 //}
 ////cout << Phi << endl;

 Eigen::MatrixXd Phi2(samples.size(), samples.size());
 Phi2 = Eigen::MatrixXd::Zero(samples.size(), samples.size());
 for(int j = 0; j < samples.size(); j++){
  Vector2d pos0 = samples[j];
  for(int k = 0; k < samples.size(); k++){
   Vector2d pos1 = samples[k];
   double w = RBFkernel(pos0, pos1, factors[k]);
   //int tmp = int(w * 1000);
   //w = tmp * 0.001;
   Phi2(j,k) = w;
  }
 }

 // compute rbf weights
 //W = Phi.inv() * F;
 double lamda = 0.1;
 //Matrix I(size, size);
 //for(int j = 0; j < size; j++){
 // for(int k = 0; k < size; k++){
 //  if(j == k)
 //   I[j][k] = 1;
 //  else
 //   I[j][k] = 0;
 // }
 //}

 Eigen::MatrixXd I2(size, size);
 for(int j = 0; j < size; j++){
  for(int k = 0; k < size; k++){
   if(j == k)
    I2(j,k) = 1;
   else
    I2(j,k) = 0;
  }
 }

 //I.print(7,7);
 // W = (Phi.transpose() * Phi + lamda * I).inv() * Phi.transpose() * F;

 //Phi2 = Phi2 + I2 * lamda;
 Eigen::VectorXd W2(size);
 W2 = Eigen::VectorXd::Zero(size);
 W2 = (Phi2.transpose() * Phi2 + lamda * I2).inverse() * Phi2.transpose() * F2;
 //W2 = Phi2.fullPivHouseholderQr().solve(F2);

 for(int j = 0; j < size; j++){
  W[j] = W2(j);
  //int tmp = int(W[j] * 1000);
  //W[j] = tmp * 0.001;
 }
}

double RBF::eval(Vector2d c){
 double f = 0.0;
 for(int i = 0; i < samples.size(); i++){
  Vector2d p = samples[i];
  double phi = RBFkernel(c, p);
  double tmp = W[i] * phi;
  f += tmp;
 }
 if(f < 0)
  f = 0;
 if(f > 1)
  f = 1;
 return f;
}

double RBF::eval(Vector2d c, std::vector<double> factors){
 double f = 0.0;
 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
 int i = 0;
 //#pragma omp parallel for private(i)
 for(i = 0; i < samples.size(); i++){
  Vector2d p = samples[i];
  double phi = RBFkernel(c, p, factors[i]);
  double tmp = W[i] * phi;
  //if(tmp < 10e-5 && tmp > 0)
  // tmp = 10e-5;
  //if(tmp > 10e5)
  // tmp = 10e5;
  //if(tmp < 0 && tmp > -10e-5)
  // tmp = -10e-5;
  //if(tmp < -10e5)
  // tmp = -10e5;
  f += tmp;
 }

 //if(f < 0){
 // f = 0;
 //}
 //if(f > 1){
 // f = 1;
 //}
 return f;
}

void RBF::setRBFMatrix(Matrix m){
 Phi = m;
}

void RBF::setWeights(Vector2d w){
 W = w;
}

void RBF::setValues(Vector2d v){
 F = v;
}

long double RBF::RBFkernel(Vector2d c, Vector2d p){
 double r = (c - p).norm();
 //cout <<  exp(-Sqr(r) / Sqr(kernelFactor)) << endl;
 //return exp(-Sqr(r) / (2 * Sqr(kernelFactor)));
 return exp(-(c-p).normsqr() / ( 2 * (kernelFactor * kernelFactor)));
}

double RBF::RBFkernel(Vector2d c, Vector2d p, double factor){
 int tmp = int(factor * 1000);
 factor = tmp * 0.001;
 double r = (c - p).norm();
 //cout <<  exp(-Sqr(r) / Sqr(kernelFactor)) << endl;
 //return exp(-Sqr(r) / (2 * Sqr(kernelFactor)));
 return exp(-(c-p).normsqr() / (2 * (factor * factor)));
}

void RBF::RBFtest(){
 for(int i = 0; i < samples.size(); i++){
  double f = 0;
  Vector2d c = samples[i];
  for(int j = 0; j < samples.size(); j++){
   Vector2d p = samples[j];
   double phi = RBFkernel(c, p);
   f += W[j] * phi;
  }
  if(float(sampleValues[i]) != float(f))
   cout << i << ": " << sampleValues[i] << " != " << f << endl;
 }
}

void RBF::RBFtest(std::vector<double> factors){
 for(int i = 0; i < samples.size(); i++){
  double f = 0;
  Vector2d c = samples[i];
  for(int j = 0; j < samples.size(); j++){
   Vector2d p = samples[j];
   double phi = RBFkernel(c, p, factors[j]);
   f += W[j] * phi;
  }
  if(float(sampleValues[i]) != float(f))
   cout << i << ": " << sampleValues[i] << " != " << f << endl;
 }
}
