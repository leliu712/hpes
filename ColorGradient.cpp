#include "ColorGradient.h"

void ColorGradient::createDefaultHeatMapGradient(){
 color.clear();
 //color.push_back(Vector4d(0, 0, 1, 0.0f));
 //color.push_back(Vector4d(0, 1, 1, 0.25f));
 //color.push_back(Vector4d(0, 1, 0, 0.5f));
 //color.push_back(Vector4d(1, 1, 0, 0.75f));
 //color.push_back(Vector4d(1, 0, 0, 1.0f));

 // gray
 color.push_back(Vector4d(37,37,37, 0.0f));
 color.push_back(Vector4d(99,99,99, 0.25f));
 color.push_back(Vector4d(150,150,150, 0.5f));
 color.push_back(Vector4d(204,204,204, 0.75f));
 color.push_back(Vector4d(247,247,247, 1.0f));

 normalizeColors();

 // red to orange
 //color.push_back(Vector4d(103,0,13, 0.0f));
 //color.push_back(Vector4d(165,15,21, 0.25f));
 //color.push_back(Vector4d(203,24,29, 0.5f));
 //color.push_back(Vector4d(239,59,44, 0.75f));
 //color.push_back(Vector4d(251,106,74, 1.0f));
 //for(auto& c : color){
 //    c.x = c.x * 1.0 / 255.0f;
 //    c.y = c.y * 1.0 / 255.0f;
 //    c.z = c.z * 1.0 / 255.0f;
 //}

}

void ColorGradient::normalizeColors()
{
  for(auto& c : color){
      c.x = c.x * 1.0 / 255.0f;
      c.y = c.y * 1.0 / 255.0f;
      c.z = c.z * 1.0 / 255.0f;
  }
}

void ColorGradient::addColorPoint(float red, float green, float blue, float val){
 for(int i = 0; i < color.size(); i++){
  if(val < color[i].w){
   color.insert(color.begin()+1, Vector4d(red, green, blue, val));
   return;
  }
 }
 color.push_back(Vector4d(red, green, blue, val));
}

void ColorGradient::clearGradient(){
 color.clear();
}

// input val is between [0,1]
void ColorGradient::getColorAtValue(const double val, float &red, float &green, float &blue){
 if(color.size() == 0)
  return;

 for(int i = 0; i < color.size(); i++){
  Vector4d &currC = color[i];
  if(val < currC.w){
   Vector4d &prevC = color[max(0, i-1)];
   double valDiff = (prevC.w - currC.w);
   double fracBetween = (valDiff == 0) ? 0 : (val - currC.w) / valDiff;
   red   = (prevC.x - currC.x) * fracBetween + currC.x;
   green = (prevC.y - currC.y) * fracBetween + currC.y;
   blue  = (prevC.z - currC.z) * fracBetween + currC.z;
   return;
  }
 }
 red   = color.back().x;
 green = color.back().y;
 blue  = color.back().z;
 return;
}
