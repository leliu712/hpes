/*
 * VectorField Class
 */
#ifndef __VECTOR_FIELD__
#define __VECTOR_FIELD__

#ifdef __APPLE__
# include <GLUT/glut.h>
#else
# include <GL/glut.h>
# include <GL/glu.h>
#endif

#define GLOBAL

#include "VelocityPath.h"
#include "RBF.h"
#include "ColorGradient.h"

class VectorField{
 public:
  VectorField(){};
  VectorField(std::vector<VelocityPath> _paths, const RBF _rbf, const std::vector<double> _rbfradii, 
    const int _nrows, const int _ncols):
   paths(_paths),
   rbf(_rbf),
   nrows(_nrows),
   ncols(_ncols),
   rbfradii(_rbfradii)
 {}
  VectorField(const VectorField& _v)
  {
   bins = _v.Bins();
   paths = _v.Paths();
   nrows = _v.Nrows();
   ncols = _v.Ncols();
   rbf = _v.Rbf();
   rbfradii = _v.RBFRadii();
  }

  ~VectorField(){};

  int findBinIdx(const Vector2d p, const int timeframe)
  {
   float maxX(-10e6), maxY(-10e-6), minX(10e6), minY(10e6);
   for(int i = 0; i < paths.size(); i++)
   {
    VelocityPath path = paths[i];
    std::vector<VelocityPoint> samples = path.Samples();
#ifdef GLOBAL
    for(int j = 0; j < path.Size(); j++)
    {
     Vector2d pos = samples[j].Pos();
     maxX = std::max(maxX, float(pos.x));
     maxY = std::max(maxY, float(pos.y));
     minX = std::min(minX, float(pos.x));
     minY = std::min(minY, float(pos.y));
    }
#else
    Vector2d pos = samples[timeframe].Pos();
    maxX = std::max(maxX, float(pos.x));
    maxY = std::max(maxY, float(pos.y));
    minX = std::min(minX, float(pos.x));
    minY = std::min(minY, float(pos.y));
#endif
   }
   if(p.x < minX || p.x > maxX || p.y < minY || p.y > maxY)
    return -1;

   Vector2d LLC(minX, minY), URC(maxX, maxY);
   float dx = (maxX - minX) / (ncols );
   float dy = (maxY - minY) / (nrows );
   float lx = (p.x - LLC.x);
   float ly = (p.y - LLC.y);
   int idxx = lx / dx;
   int idxy = ly / dy;
   idxx = idxx >= ncols ? idxx = ncols - 1 : idxx;
   idxy = idxy >= nrows ? idxy = nrows - 1 : idxy;
   int idxBin = ncols * idxy + idxx;
   return idxBin;
  }

  void buildAt(const int timeframe)
  {
   bins.clear();
   float maxX(-10e6), maxY(-10e-6), minX(10e6), minY(10e6);
   for(int i = 0; i < paths.size(); i++)
   {
    VelocityPath path = paths[i];
    std::vector<VelocityPoint> samples = path.Samples();
#ifdef GLOBAL
    for(int j = 0; j < path.Size(); j++)
    {
     Vector2d pos = samples[j].Pos();
     maxX = std::max(maxX, float(pos.x));
     maxY = std::max(maxY, float(pos.y));
     minX = std::min(minX, float(pos.x));
     minY = std::min(minY, float(pos.y));
    }
#else
    Vector2d pos = samples[timeframe].Pos();
    maxX = std::max(maxX, float(pos.x));
    maxY = std::max(maxY, float(pos.y));
    minX = std::min(minX, float(pos.x));
    minY = std::min(minY, float(pos.y));
#endif
   }

   Vector2d LLC(minX, minY), URC(maxX, maxY);
   float dx = (maxX - minX) / (ncols );
   float dy = (maxY - minY) / (nrows );

   for(int i = 0; i < nrows; i++)
   {
    for(int j = 0; j < ncols; j++)
    {
     Bin toPush;
     Vector2d binLLC = LLC + Vector2d(dx * j, dy * i);
     Vector2d binURC = binLLC + Vector2d(dx, dy);
     toPush.LLC() = binLLC;
     toPush.URC() = binURC;
     Vector2d c = (binLLC + binURC) * 0.5;
     c = translateToScreen(c.x, c.y); // this rbf is measured by screen coordinate
     float sd = rbf.eval(c, rbfradii);
     toPush.SD() = sd;
     bins.push_back(toPush);
    }
   }

   for(int i = 0; i < paths.size(); i++)
   {
    VelocityPath path = paths[i];
    std::vector<VelocityPoint> samples;
    for(int j = 0; j < path.Samples().size(); j++)
     samples.push_back(path.Samples()[j]);

    Vector2d pos = samples[timeframe].Pos();
    float lx = (pos.x - LLC.x);
    float ly = (pos.y - LLC.y);
    int idxx = lx / dx;
    int idxy = ly / dy;
    idxx = idxx >= ncols ? idxx = ncols - 1 : idxx;
    idxy = idxy >= nrows ? idxy = nrows - 1 : idxy;
    int idxBin = ncols * idxy + idxx;
    (bins[idxBin].Samples()).push_back(samples[timeframe]);
   }

   for(int i = 0; i < bins.size(); i++)
    bins[i].findVector();

   //drawBins();
  }

  void buildFullPaths()
  {
   bins.clear();
   float maxX(-10e6), maxY(-10e-6), minX(10e6), minY(10e6);
   for(int i = 0; i < paths.size(); i++)
   {
    VelocityPath path = paths[i];
    std::vector<VelocityPoint> samples = path.Samples();
    for(int j = 0; j < path.Size(); j++)
    {
     Vector2d pos = samples[j].Pos();
     maxX = std::max(maxX, float(pos.x));
     maxY = std::max(maxY, float(pos.y));
     minX = std::min(minX, float(pos.x));
     minY = std::min(minY, float(pos.y));
    }
   }

   Vector2d LLC(minX, minY), URC(maxX, maxY);
   float dx = (maxX - minX) / (ncols );
   float dy = (maxY - minY) / (nrows );

   for(int i = 0; i < nrows; i++)
   {
    for(int j = 0; j < ncols; j++)
    {
     Bin toPush;
     Vector2d binLLC = LLC + Vector2d(dx * j, dy * i);
     Vector2d binURC = binLLC + Vector2d(dx, dy);
     toPush.LLC() = binLLC;
     toPush.URC() = binURC;
     bins.push_back(toPush);
    }
   }

   for(int i = 0; i < paths.size(); i++)
   {
    VelocityPath path = paths[i];
    std::vector<VelocityPoint> samples;
    for(int j = 0; j < path.Samples().size(); j++)
     samples.push_back(path.Samples()[j]);
    for(int j = 0; j < path.Size(); j++)
    {
     Vector2d pos = samples[j].Pos();
     float lx = (pos.x - LLC.x);
     float ly = (pos.y - LLC.y);
     int idxx = lx / dx;
     int idxy = ly / dy;
     idxx = idxx >= ncols ? idxx = ncols - 1 : idxx;
     idxy = idxy >= nrows ? idxy = nrows - 1 : idxy;
     int idxBin = ncols * idxy + idxx;
     (bins[idxBin].Samples()).push_back(samples[j]);
    }
   }

   for(int i = 0; i < bins.size(); i++)
    bins[i].findVector();

   drawBins();
  }

  void drawBins()
  {
   for(int i = 0; i < bins.size(); i++)
   {
    drawBin(bins[i]);
    //drawBinSamples(bins[i]);
    drawBinVector(bins[i]);
   }
  }

  void drawBin(const int idx)
  {
   drawBinVector(bins[idx]);
   drawBin(bins[idx]);
  }

  void drawBin(const Bin bin)
  {
   Vector2d llc = bin.LLC();
   Vector2d urc = bin.URC();
   llc = translateToScreen(llc.x, llc.y);
   urc = translateToScreen(urc.x, urc.y);
   float sd = bin.SD();
   float red, green, blue;
   ColorGradient colorgradient;
   colorgradient.getColorAtValue(sd, red, green, blue);
   glColor4f(red, green, blue, 1);
   glBegin(GL_LINE_LOOP);
   glVertex3f(llc.x, llc.y, 0);
   glVertex3f(urc.x, llc.y, 0);
   glVertex3f(urc.x, urc.y, 0);
   glVertex3f(llc.x, urc.y, 0);
   glEnd();
  }

  void drawBinSamples(const Bin bin)
  {
   for(int i = 0; i < bin.Samples().size(); i++)
   {
    Vector2d p = (bin.Samples())[i].Pos();
    p = translateToScreen(p.x, p.y);
    glPointSize(2);
    glColor4f(1, 0, 0, 1);
    glBegin(GL_POINTS);
    glVertex3f(p.x, p.y, 0);
    glEnd();
   }
  }

  void drawBinVector(const Bin bin)
  {
   if(bin.Samples().size() == 0)
    return;
   Vector2d llc = bin.LLC();
   Vector2d urc = bin.URC();
   Vector2d c = (urc + llc) * 0.5;
   float d = (urc - llc).norm() * 0.3;
   Vector2d v = bin.V();
   //Vector2d q = c + d * v;
   Vector2d q = c + v;
   c = translateToScreen(c.x, c.y);
   q = translateToScreen(q.x, q.y);
   float sd = bin.SD();
   float red, green, blue;
   ColorGradient colorgradient;
   colorgradient.getColorAtValue(sd, red, green, blue);
   glColor4f(red, green, blue, 1);
   glPointSize(4);
   glBegin(GL_POINTS);
   glVertex3f(c.x, c.y, 0);
   glEnd();
   glBegin(GL_LINES);
   glVertex3f(c.x, c.y, 0);
   glVertex3f(q.x, q.y, 0);
   glEnd();
  }

  const std::vector<VelocityPath>& Paths() const {return paths;}
  std::vector<VelocityPath>& Path() {return paths;}

  const std::vector<Bin>& Bins() const { return bins;}
  std::vector<Bin>& Bins() { return bins;}

  const int& Nrows() const {return nrows;}
  int& Nrows() {return nrows;}

  const int& Ncols() const {return ncols;}
  int& Ncols() {return ncols;}

  const RBF& Rbf() const {return rbf;}
  RBF& Rbf() {return rbf;}

  const std::vector<double>& RBFRadii() const {return rbfradii;}
  std::vector<double>& RBFRadii() {return rbfradii;}

 private:
  std::vector<VelocityPath> paths;
  std::vector<Bin> bins;
  int nrows, ncols;
  RBF rbf;
  std::vector<double> rbfradii;
};

#endif
