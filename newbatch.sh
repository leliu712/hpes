#!/bin/bash
#declare -a hurrSequence=( "mcrall_al092008_091018" "mcrall_al092012_082612" "mcrall_al112009_110806" "mcrall_al122005_082706" "mcrall_al182005_092206")
declare -a hurrSequence=("mcrall_al092012_082612" "mcrall_al112009_110806")
declare -a radii=("0.2" "0.6" "1.1" "1.78" "-0.2" "-0.6" "-1.1" "-1.78")
declare -a timepoints=("24" "48")
declare -a paths=("63" "7" "15")

for npaths in "${paths[@]}"
do
 for hurId in "${hurrSequence[@]}"
 do
  for errorRadius in "${radii[@]}"
  do
   for timepoint in "${timepoints[@]}"
   do
    ./main 0 1 6 0.15 /media/lel/New\ Volume/Research/New-NHC-Data/processed/ /media/lel/New\ Volume/Research/New-NHC-Data/Vis/ $hurId 1 $errorRadius $timepoint 1 $npaths > output$timepoint.txt
   done
  done
 done
done

for hurId in "${hurrSequence[@]}"
do
 for errorRadius in "${radii[@]}"
 do
  for timepoint in "${timepoints[@]}"
  do
   ./main 0 1 6 0.15 /media/lel/New\ Volume/Research/New-NHC-Data/processed/ /media/lel/New\ Volume/Research/New-NHC-Data/Vis/ $hurId 1 $errorRadius $timepoint 0 15
  done
 done
done
