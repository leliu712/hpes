#include "ColorGradient.h"
#include "PathCBD.h" // contains functions to compute simplicial depth
#include "psimpl.h"

#include "filemanager.h"
#include "PathResampling.h"

#include <QLineF>
#include <QPointF>
#include <QProcess>
#include <QPainter>
#include <QDebug>
#include <sstream>
#include <utility>
#include <cmath>
#include <numeric>

extern std::string DatafileName;
extern std::string InputPrefix;
extern int PathVisMode;
extern int RepSubsetSize;

static bool IsCenterLineComputed = false;
static bool IsRandomShuffled = false;

enum SimplifyAlgorithm{
    DONOTAPPLY = 0,
    REUMANNWITKAM,
    LANG,
    OPHEIM,
    PERPENDICULARDISTANCE,
    RADIALDISTANCE,
    DOUGLASPEUCKERN,
    DOUGLASPEUCKER,
    BSPLINECURVE
};

int AppliedSimplifyAlgorithm =  LANG;

vector<vector<Vector2d>> testleft, testright;
int testlevel = 2;
CenterLine testcenterline;
CenterLine testcenterline2;
vector<CenterLine> testcenterlines;
vector<CenterLine> testcenterlines2;
vector<vector<Vector2d>> testleftregions, testrightregions;
vector<Vector2d> testpoints;
vector<Vector2d> testpoints2;
vector<double> testvalues;

PathResampling::PathResampling(const int timepoint):
    _timepoint(timepoint)
{
    beginning = myclock::now();
    numofUDspaces = 6;
    numofpaths = RepSubsetSize;
    numofTimeSteps = 73;
    sampledTimeSteps = 73; // [0, 72]
    numofRecLevels = 10;
    leftrightWindow = 12; // 24 / 2 = 12
    stormcharacteristicsinterpolator = NULL;
    stormcharacteristicsinterpolators.resize(sampledTimeSteps);
    for(unsigned int i = 0; i < stormcharacteristicsinterpolators.size(); i++){
        stormcharacteristicsinterpolators.at(i) = NULL;
    }
    _centerlineroot = NULL;
    // Temporarily commented
    //ReadOriginalPointsOD();
    InitPoissonPointsGenerators();
    if(!IsCenterLineComputed){
        SDCenterLines = GenerateStructuredPathsByPartitingODSpace();
        SDCenterLinesShadow = SDCenterLines;

        SimplifyCenterlines();
        AppliedSimplifyAlgorithm = BSPLINECURVE;
        for(auto & cl : SDCenterLines){
            cl = RemoveSelfLoop(cl);
        }
        for(auto & cl : SDCenterLines){
            cl.insert(cl.begin(), *cl.begin());
        }
        //for(auto & cl : SDCenterLines){
        // cl.push_back(cl.back());
        //}
        SimplifyCenterlines();
        for(auto & cl : SDCenterLines){
            cl.resize(sampledTimeSteps);
        }

        IsCenterLineComputed = true;
        SDCenterLinesShadow = SDCenterLines;
        SDCenterLinesShadow.resize(numofpaths);
        //ExtractCenterlineSubset(numofpaths);
        SDCenterLinesShadow = ExtractCenterlineSubset(numofpaths);
        //SDCenterLines = SDCenterLinesShadow;
        cout << "Number of centerlines: " << SDCenterLines.size() << endl;
    }

    // output mean tracks locations(every 12 hours) to file to
    // compute oil rigs for the experiment
    OutputMeanTrack2File();

    ComputeStormcharacteristicsInterpolations();

    IsShuffled = false;

    //string filename = "../ODFullset801.csv";
    //string filename = "../ODSubset_1000_200.csv";
    //string filename = "../ODSubset_size_1000_100.csv";
    //string filename = "../ODSubset_size_1000_100_prev.csv";
    //string filename = "../OLS-subset-100.csv";
    string filename = "../ODSubset_801_100_NDRS.csv";
    subsamplingdata = CDFSample::readFromFile2(filename.c_str());
}

PathResampling::PathResampling(const int _nrows, const int _ncols, const int _numofUDspaces,
                               const int _numofpaths): nrows(_nrows), ncols(_ncols), numofUDspaces(_numofUDspaces), numofpaths(_numofpaths){
    beginning = myclock::now();
    numofUDspaces = 6;
    numofpaths = 50;
    numofTimeSteps = 72;
    sampledTimeSteps = 72; // [0, 72]
    numofRecLevels = 4;
    leftrightWindow = 24;
    _centerlineroot = NULL;
    // Temporarily commented
    //ReadOriginalPointsOD();
    InitPoissonPointsGenerators();
    if(!IsCenterLineComputed){
        SDCenterLines = GenerateStructuredPathsByPartitingODSpace();
        SDCenterLinesShadow = SDCenterLines;
        SimplifyCenterlines();
        AppliedSimplifyAlgorithm = BSPLINECURVE;
        for(auto & cl : SDCenterLines){
            cl = RemoveSelfLoop(cl);
        }
        for(auto & cl : SDCenterLines){
            cl.insert(cl.begin(), *cl.begin());
        }
        for(auto & cl : SDCenterLines){
            cl.push_back(cl.back());
        }
        SimplifyCenterlines();
        for(auto & cl : SDCenterLines){
            cl.resize(sampledTimeSteps);
        }
        IsCenterLineComputed = true;
    }

}

PathResampling::~PathResampling(){
}

void PathResampling::InitPoissonPointsGenerators(){
    generators.reserve(numofUDspaces);
    for(int i = 0; i < numofUDspaces; i++){
        myclock::duration d = myclock::now() - beginning;
        generators.push_back(PoissonPointsGenerator(numofpaths, d.count()));
    }
    GeneratedPointsUD.resize(numofUDspaces);
}

std::vector<path*> PathResampling::Process(){
    ReadUDSpaces();
    ConstructInvmaps();
    ComputeUDMinDistances();
    // Generates first path by picking the center points at individual UD spaces
    GenerateFirstPath();
    GenerateRestPaths();
    DrawPathsSubset();
    projmaps[5].DrawUDGrid();
    projmaps[5].DrawODGrid();
    //DrawGeneratedLocations(5);
    return pathSubset;
}

vector<vector<Vector2d>> PathResampling::ReadFullEnsembleFromFiles(){
    string pointsfileprefix = OutputPrefix + "InternalData/PathSamplingData/FullPredictedLocations/OD-%d.txt";
    vector<vector<Vector2d>> fullensemble(numofTimeSteps, vector<Vector2d>());
    for(int i = 0; i < numofTimeSteps; i++){
        const char* cstr = pointsfileprefix.c_str();
        char buff[200];
        sprintf(buff, cstr, i);
        fullensemble[i] = CDFSample::readTestSamples(buff);
        // Translate to screen coordinates
        for(auto& p : fullensemble[i]){
            p = translateToScreen(p.x, p.y);
        }
        //cout << "Read " << fullensemble[i].size() << " points from " << buff << endl;
    }
    return fullensemble;
}

std::vector<std::vector<StormCharacteristicInterpolator::PredictionNode> > PathResampling::ReadFullPredictionsFromCSVFile()
{
    string predictionfilename = OutputPrefix + "InternalData/PathSamplingData/FullPredictions/Predictions-%d.csv";
    vector<vector<StormCharacteristicInterpolator::PredictionNode>> res;
    res.resize(numofTimeSteps);
    for(int i = 0; i < numofTimeSteps; i++){
        const char* cstr = predictionfilename.c_str();
        char buff[200];
        sprintf(buff, cstr, i);
        vector<Vector4d> data = CDFSample::readFromFile2(buff);
        for(unsigned int j = 0; j < data.size(); j++){
            Vector2d loc(data[j].x, data[j].y);
            double intensity = data[j].z;
            double size = data[j].w;
            loc = translateToScreen(loc.x,loc.y);
            StormCharacteristicInterpolator::PredictionNode topush(loc, size, intensity);
            //cout << ">>>" << intensity << " " << size << endl;
            //cout << "<<<" << topush._intensity << " " << topush._size << endl;
            res.at(i).push_back(topush);
        }
    }
    _beginningnode = res.at(0).at(0);
    return res;
}

void PathResampling::ReadOriginalPointsOD(){
    cout << "Reading original points from file ..." << endl;
    string pointsfileprefix = OutputPrefix + "InternalData/PathSamplingData/OD-%d.txt";
    OriginalPointsOD.clear();
    OriginalPointsOD.resize(numofUDspaces);
    for(int i = 0; i < numofUDspaces; i++){
        const char* cstr = pointsfileprefix.c_str();
        char buff[200];
        sprintf(buff, cstr, i+1);
        OriginalPointsOD.at(i) = CDFSample::readTestSamples(buff);
        cout << "Read " << OriginalPointsOD.at(i).size() << " points from " << buff << endl;
    }
    // Since the orignal points contains duplications, after remove duplications those sets at
    // different time instances have different sizes.
    // Here we resize the vectors to keep them in the same size, e.g. 900.
    for(auto& s : OriginalPointsOD){
        s.resize(900);
    }
    // The first points are identical, since we did not output them as file,
    // we add the first points here
    vector<Vector2d> fp(900, Vector2d(-84.4, 24.4));
    OriginalPointsOD.insert(OriginalPointsOD.begin(), fp);

    // These points are measured in lat and lon so we translate them to screen coords
    for(auto& s : OriginalPointsOD){
        for(auto& p : s){
            p = translateToScreen(p.x, p.y); //Katrina
        }
    }
}

void PathResampling::ReadUDSpaces(){
    UDSpaces.resize(numofUDspaces);
    for(int i = 1; i <= numofUDspaces; i++){
        char buff[200];
        string str = OutputPrefix + "InternalData/PathSamplingData/UDSpace-%d.txt";
        const char* cstr = str.c_str();
        sprintf(buff, cstr, i);
        UDSpace udspace;
        CDFSample::readNewSpace(buff, udspace.U, udspace.V, nrows, ncols);
        udspace.nrows = nrows;
        udspace.ncols = ncols;
        UDSpaces[i-1] = udspace;
        //cout << "Finish reading UD space: " << buff << endl;
    }
}

void PathResampling::ConstructInvmaps(){
    projmaps.resize(numofUDspaces);
    for(int i = 0; i < numofUDspaces; i++){
        // Needs to read OD grid from file
        char buff[200];
        string str = OutputPrefix + "InternalData/PathSamplingData/ODGrid-%d.txt";
        const char* cstr = str.c_str();
        sprintf(buff, cstr, i + 1);
        std::vector<Vector2d> ODGrid = CDFSample::readTestSamples(buff);
        // Needs to read UD grid from file
        str = OutputPrefix + "InternalData/PathSamplingData/UDGrid-%d.txt";
        cstr = str.c_str();
        sprintf(buff, cstr, i + 1);
        std::vector<Vector2d> UDGrid = CDFSample::readTestSamples(buff);
        // Find point in OD space
        CDFSample cdfsample;
        cdfsample.findTriangles(nrows, ncols);
        projmaps[i].ODGrid = ODGrid;
        projmaps[i].UDGrid = UDGrid;
        projmaps[i].cdfsample = cdfsample;
        projmaps[i].nrows = nrows;
        projmaps[i].ncols = ncols;
    }
}

void PathResampling::ComputeUDMinDistances(){
    for(int i = 0; i < numofUDspaces; i++){
        float area = projmaps[i].UDGridArea();
        //float f = sqrt(area * (1.0 / float(numofpaths)));
        float f = sqrt(area * (1.0f / static_cast<float>(numofpaths))) * 0.55f * sqrt(2.0f);
        minDistances.push_back(f);
        cout << " UD " << i << " " << f << endl;
        // Set min distances of individual poisson points generator
        generators[i].SetMinDis(f);
    }
}

// Generates first path by picking the center points at individual UD spaces
void PathResampling::GenerateFirstPath(){
    path* p = new path(0, 0, 0, 0, 0);
    p->posList.clear();
    //Vector2d s = translateToScreen(-80.08, 21.9); // issac
    Vector2d s = translateToScreen(-84.4, 24.4); //Katrina
    p->posList.push_back(new Vector2d(s.x, s.y));
    int count = 0;
    for(auto m : projmaps){
        Vector2d vud = m.CenterOfUDGrid();
        generators[count].PushFirstPoint(vud.x, vud.y);
        Vector2d v = m.CenterOfODGrid();
        p->posList.push_back(new Vector2d(v.x, v.y));
        GeneratedPointsUD[count].push_back(m.CenterOfUDGrid());
        count ++;
    }
    pathSubset.push_back(p);
}

// Generates resut paths by considering crossings 
void PathResampling::GenerateRestPaths(){
    int count = 0;
    while(pathSubset.size() < numofpaths){
        cout << "Path idx: " << count++ << endl;
        GenerateNewPath();
    }
    //WriteGeneratedPointsToFiles();
}

// Generates a new path
void PathResampling::GenerateNewPath(){
    path* p = new path(0, 0, 0, 0, 0);
    p->posList.clear();
    // Adds first location
    //Vector2d s = translateToScreen(-80.08, 21.9);
    Vector2d s = translateToScreen(-84.4, 24.4); // Katrina
    p->posList.push_back(new Vector2d(s.x, s.y));

    //  Generates locations at different points in time
    for(int i = 0; i < numofUDspaces; i++){
        bool inside = false;
        // Keeps generating points until find a point inside the UD space and has no intersections with
        // generated paths
        sPoint v;
        Vector2d v2d, toPush;
        Vector2d* preLoc = p->posList.back();
        int counter = 0;
        //// debug codes
        //v = generators[i].GeneratePoint(projmaps[i]);
        //v2d = Vector2d(v.x, v.y);
        //toPush = projmaps[i].InvMap(v2d);

        do{
            v = generators[i].GeneratePoint(projmaps[i]);
            v2d = Vector2d(v.x, v.y);
            toPush = projmaps[i].InvMap(v2d);
            counter ++;
        } while((HasIntersection(preLoc->x, preLoc->y, toPush.x, toPush.y, p) && counter < 100));
        //} while((HasIntersection(preLoc->x, preLoc->y, toPush.x, toPush.y, p) ||
        //  HasSharpTurn(p, toPush)) && counter < 100);
        //cout << "UD " << i << " " << v2d << endl;

        GeneratedPointsUD[i].push_back(v2d);
        p->posList.push_back(new Vector2d(toPush.x, toPush.y));
    }
    pathSubset.push_back(p);
}

void PathResampling::GeneratePoissonDiskPointsInUDSpaces(){
    for(int i = 0; i < numofUDspaces; i++){
        while(GeneratedPointsUD[i].size() < numofpaths){
            sPoint v = generators[i].GeneratePoint(projmaps[i]);
            Vector2d v2d(v.x, v.y);
            GeneratedPointsUD[i].push_back(v2d);
            Vector2d vOD = projmaps[i].InvMap(v2d);
        }
    }
}

void PathResampling::DrawSelectedPoints()
{
    bool uniformrandom = false;
    ColorGradient colorgradient;
    colorgradient.clearGradient();
    //colorgradient.color.push_back(Vector4d(253,212,158,   0.0));
    //colorgradient.color.push_back(Vector4d(252,141,89,    0.33));
    //colorgradient.color.push_back(Vector4d(215,48,31,     0.66));
    //colorgradient.color.push_back(Vector4d(127,0,0,       1.0));

    colorgradient.color.push_back(Vector4d(253,141,60,   0.0));
    colorgradient.color.push_back(Vector4d(252,78,42,    0.33));
    colorgradient.color.push_back(Vector4d(227,26,28,    0.66));
    colorgradient.color.push_back(Vector4d(177,0,38,     1.0));

    colorgradient.normalizeColors();
    float red(0.0f), green(0.0f), blue(0.0f);
    vector<double> intensities;
    for(const auto p : subsamplingdata){
        intensities.push_back(p.z);
    }
    vector<double> sizes;
    for(const auto p : subsamplingdata){
        sizes.push_back(p.w);
    }
    double maxint = *max_element(intensities.begin(), intensities.end());
    double minint = *min_element(intensities.begin(), intensities.end());
    //maxint = 39.3;
    //minint = 9.8;
    double delta = maxint - minint;
    if(!uniformrandom){
        for(auto& p : subsamplingdata){
            Vector2d q = translateToScreen(p.x,p.y);
            double intensity = (p.z - minint) / delta;
            double size = (p.w - minint) / delta;
            colorgradient.getColorAtValue(size, red, green, blue);
            glColor4f(red, green, blue, 1.0f);
            //glColor4f(0.7, 0, 0.1, 1);
            //glColor4f(0.5, 0.5, 0.5, 1);
            size = CDFSample::Stormsize2Display(Vector2d(p.x, p.y), size);
            CDFSample::drawSolidCircle(q.x, q.y, 6);
            //DrawCircle(q.x, q.y, size);
        }
    } else {
        if(!IsShuffled){
            random_shuffle(subsamplingdata.begin(), subsamplingdata.end());
            IsShuffled = true;
        }
        for(size_t j = 0; j < 100; j++){
            auto p = subsamplingdata.at(j);
            Vector2d q = translateToScreen(p.x,p.y);
            double intensity = (p.z - minint) / delta;
            double size = (p.w - minint) / delta;
            colorgradient.getColorAtValue(size, red, green, blue);
            glColor4f(red, green, blue, 1);
            size = CDFSample::Stormsize2Display(Vector2d(p.x, p.y), size);
            CDFSample::drawSolidCircle(q.x, q.y, 4);
            //DrawCircle(q.x, q.y, 10);
        }
    }
}

void PathResampling::DrawResampledPathsWithStormCharacteristics()
{

    //DrawSelectedPoints();
    //DrawSDCenterLines();
    DrawSDCenterLinesWithStormCharacteristic("intensity");
    ////DrawResampledPathsWithStormSizes();
    //DrawResampledPathsWithStormSizesSub();
}

void PathResampling::DrawSDCenterLinesWithStormCharacteristic(const string &type)
{
    cout << "Draw SD centerlines with " << type << endl;
    //ColorGradient colorgradient;
    //colorgradient.clearGradient();
    //colorgradient.color.push_back(Vector4d(103,0,13, 0.0f));
    //colorgradient.color.push_back(Vector4d(165,15,21, 0.25f));
    //colorgradient.color.push_back(Vector4d(203,24,29, 0.5f));
    //colorgradient.color.push_back(Vector4d(239,59,44, 0.75f));
    //colorgradient.color.push_back(Vector4d(251,106,74, 1.0f));
    //colorgradient.normalizeColors();
    float red(0.0f), green(0.0f), blue(0.0f);
    int counter = 0;
    vector<double> windspeed24;
    vector<double> categor24;
    vector<double> windspeed48;
    vector<double> categor48;
    for(const auto& cl : SDCenterLinesShadow){
        //const string str = OutputPrefix + "StatisticsVerification/interpolatedvalues/paths-intensity-" +to_string(counter) + ".txt";
        //vector<double> vals(cl.size());
        int precat(0);
        for(unsigned int i = 0; i < stormcharacteristicsinterpolators.size() - 1; i++){
            Vector2d p = cl.at(i);
            Vector2d q = cl.at(i+1);
            float f = i < 1 ? 1.0f : (stormcharacteristicsinterpolators.at(i))->SubRBFEval(p, type, true);
            //vals.at(i) = f;
            int category = StormCharacteristicInterpolator::StormCategory(f);
            if(i != 0){
                // if prepoint and curpoint have adjacent categories, correct computed category by extending category boundaries
                if(StormCharacteristicInterpolator::IsAjacent(precat, category)){
                    int newcategory = StormCharacteristicInterpolator::StormCategory(f, true);
                    // newly computed category must be the same with pre-category, otherwise meaning we jump two categories.
                    category = newcategory == precat ? newcategory : category;
                }

            }
            precat = category;
            float radius = 1.0f;
            if(i > 1){
                vector<double> radii = stormcharacteristicsinterpolators.at(i)->StormCharacteristicRadii(type);
                //if(i == 55){
                //    RBF tmp = stormcharacteristicsinterpolators.at(55)->SubRBF(type);
                //    tmp.RBFtest(radii);
                //}
            }
            //cout << f << " " << i << " " << category << " " <<  radius << endl;
            StormCharacteristicInterpolator::StormCategoryColorMapping2(category, &red, &green, &blue);
            glColor4f(red, green, blue, 1.0);
            glLineWidth(4);
            p = correctHurrianceLocation(p, DatafileName, SDCenterLinesShadow.at(0).at(0));
            q = correctHurrianceLocation(q, DatafileName, SDCenterLinesShadow.at(0).at(0));
            glBegin(GL_LINES);
            glVertex3f(p.x, p.y, 0);
            glVertex3f(q.x, q.y, 0);
            glEnd();
            if(i == 24){
                windspeed24.push_back(f);
                categor24.push_back(category);
            }
            if(i == 48){
                windspeed48.push_back(f);
                categor48.push_back(category);
            }
        }
        //WritePoints2CSVFile<Vector2d, double>(str, 2, &cl, &vals, &vals);
        counter ++ ;
    }
    for(size_t i = 0; i < SDCenterLinesShadow.size(); i++){
        cout << windspeed24.at(i) << " " << categor24.at(i) << " " << windspeed48.at(i) << " " << categor48.at(i) << endl;
    }
    // compute mean and median
    sort(windspeed24.begin(), windspeed24.end());
    sort(categor24.begin(), categor24.end());
    sort(windspeed48.begin(), windspeed48.end());
    sort(categor48.begin(), categor48.end());
    cout << "median intensity at 24: " << windspeed24.at(6) << " " << categor24.at(6) << endl;
    cout << "median intensity at 48: " << windspeed48.at(6) << " " << categor48.at(6) << endl;
    cout << "mean intensity at 24: " << accumulate(windspeed24.begin(), windspeed24.end(), 0) * 1.0 / windspeed24.size() << " " <<
            accumulate(categor24.begin(), categor24.end(), 0) / categor24.size() << endl;
    cout << "mean intensity at 48: " << accumulate(windspeed48.begin(), windspeed48.end(), 0) * 1.0 / windspeed48.size() << " " <<
            accumulate(categor48.begin(), categor48.end(), 0) / categor48.size() << endl;

}

void PathResampling::DrawResampledPathsWithStormSizes()
{
    //for(const auto& cl : SDCenterLinesShadow){
    //    for(unsigned int i = 0; i < stormcharacteristicsinterpolators.size(); i++){
    //        const Vector2d p = cl.at(i);
    //        const Vector2d q = translateToGeoCoor(p);
    //        if(i % 12 == 0){
    //            float r = i == 0 ? 0.0f : stormcharacteristicsinterpolators.at(i)->SubRBFEval(p, "size", true);
    //            r = CDFSample::Stormsize2Display(q, r);
    //            glColor4f(0.2, 0.2, 0.2, 0.5);
    //            DrawCircle(p.x, p.y, r);
    //        }
    //    }
    //}

    vector<int> indices;
    for(unsigned int i = 0; i < SDCenterLinesShadow.size(); i++){
        indices.push_back(i);
    }
    for(unsigned int i = 0; i < stormcharacteristicsinterpolators.size(); i++){
        int ncircles = numofpaths;
        switch(i / 12){
        case 0:
            ncircles = 1;
            break;
        case 1:
            ncircles = 1;
            break;
        case 2:
            ncircles = 3;
            break;
        case 3:
            ncircles = 5;
            break;
        case 4:
            ncircles = 7;
            break;
        case 5:
            ncircles = 9;
            break;
        case 6:
            ncircles = 9;
            break;
        default:
            ncircles = 15;
            break;
        }
        if(i % 12 != 0){
            continue;
        }
        if(!IsRandomShuffled){
            random_shuffle(indices.begin(), indices.end());
        }
        for(int j = 0; j < ncircles; j++){
            int id = indices.at(j);
            const auto& cl = SDCenterLinesShadow.at(id);
            Vector2d p = cl.at(i);
            const Vector2d q = translateToGeoCoor(p);
            float r = i == 0 ? 0.0f : stormcharacteristicsinterpolators.at(i)->SubRBFEval(p, "size", true);
            r = CDFSample::Stormsize2Display(q, r);
            p = correctHurrianceLocation(p, DatafileName, SDCenterLinesShadow.at(0).at(0));
            glColor4f(0.2, 0.2, 0.2, 0.2);
            DrawCircle(p.x, p.y, r);
        }

    }
    IsRandomShuffled = true;

}

void PathResampling::DrawResampledPathsWithStormSizesSub()
{
    cout << "Draw circles" << endl;
    vector<double> size24;
    vector<double> size48;
    for(unsigned int i = 0; i < stormcharacteristicsinterpolators.size(); i++){
        if(i % 12 != 0){
            continue;
        }
        cout << i << endl;
        vector<float> radii(SDCenterLinesShadow.size());
        vector<Vector2d> samples(SDCenterLinesShadow.size());
        for(unsigned int j = 0; j < SDCenterLinesShadow.size(); j++){
            const CenterLine cl = SDCenterLinesShadow.at(j);
            const Vector2d p = cl.at(i);
            const Vector2d q = translateToGeoCoor(p);
            float r = i == 0 ? _beginningnode._size : stormcharacteristicsinterpolators.at(i)->SubRBFEval(p, "size", true);
            if(i == 24){
                size24.push_back(r);
            }
            if(i == 48){
                size48.push_back(r);
            }
            r = CDFSample::Stormsize2Display(q, r);
            radii.at(j) = r;
            samples.at(j) = p;
        }
        vector<int> validlist = FilterClosePoints(samples, radii, 1.0f);
        for(unsigned int k = 0; k < validlist.size(); k++){
            if(validlist.at(k) != 0){
                continue;
            }
            //if(i == 0){
            //  glColor4f(0.2, 0.2, 0.2, 0.3);
            //} else if(i == 12){
            //  glColor4f(0.2, 0.2, 0.2, 0.26);
            //} else if(i == 24){
            //  glColor4f(0.2, 0.2, 0.2, 0.22);
            //} else if(i == 36)
            //  glColor4f(0.2, 0.2, 0.2, 0.18);
            //} else if(i == 48){
            //  glColor4f(0.2, 0.2, 0.2, 0.16);
            //} else if(i == 60){
            //  glColor4f(0.2, 0.2, 0.2, 0.12);
            //} else if(i == 72){
            //  glColor4f(0.2, 0.2, 0.2, 0.08);
            //}
            glColor4f(0.2, 0.2, 0.2, 0.2);
            Vector2d p = samples.at(k);
            const float r = radii.at(k);
            // Draw full circles at 0 hour,
            // otherwise draw dashed circles
            int interval = i == 0 ? -1 : 84 - i;
            interval = interval / 2;
            p = correctHurrianceLocation(p, DatafileName, SDCenterLinesShadow.at(0).at(0));
            DrawCircle(p.x, p.y, r, -1);
        }
    }
    // compute mean and median of storm size
    sort(size24.begin(), size24.end());
    sort(size48.begin(), size48.end());
    cout << size24.size() << "  " << size48.size() << endl;
    cout << "median size at 24: " << size24.at(7) << " " << size24.size() << endl;
    cout << "median size at 48: " << size48.at(7) << " " << size48.size() << endl;
    cout << "mean size at 24: " << accumulate(size24.begin(), size24.end(), 0) * 1.0 / size24.size() << endl;
    cout << "mean size at 48: " << accumulate(size48.begin(), size48.end(), 0) * 1.0 / size48.size() << endl;
}


void PathResampling::DrawPathsSubset(){
    float i = 1.0f;
    for(auto p : pathSubset){
        float f = i * (1.0f / float(pathSubset.size()));
        ColorGradient colorgradien;
        float red(0.0f), green(0.0f), blue(0.0f);
        colorgradien.getColorAtValue(f, red, green, blue);
        glColor4f(red, green, blue, 1);
        glLineWidth(0.5);
        glBegin(GL_LINE_STRIP);
        for(auto v : p->posList){
            glVertex3f(v->x, v->y, 0);
        }
        //glVertex3f(p->posList[3]->x, p->posList[3]->y, 0);
        //glVertex3f(p->posList[4]->x, p->posList[4]->y, 0);
        glEnd();
        //for(auto v : p->posList){
        // CDFSample::drawSolidCircle(v->x, v->y, 3);
        //}
        i += 1.0f;
    }

    //glColor4f(1, 0, 0, 1);
    //CDFSample::drawSolidCircle(500, 500, 10);
    //glColor4f(0, 1, 0, 1);
    //CDFSample::drawSolidCircle(500, 700, 10);
}

void PathResampling::WriteGeneratedPointsToFiles(){
    for(int i = 0; i < numofUDspaces; i++){
        char buff[200];
        string str = OutputPrefix + "InternalData/PathSamplingData/ReferencePoissonPoints-%d.txt";
        const char* cstr = str.c_str();
        sprintf(buff, cstr, i);
        CDFSample::write2file(GeneratedPointsUD[i], buff);
    }
}

bool PathResampling::HasIntersection(const float p0_x, const float p0_y, const float p1_x, const float p1_y,
                                     path* curPath){
    // Do not check segment v0,v1
    if(curPath->posList.size() <= 1){
        return false;
    }
    for(const auto p : pathSubset){
        // Check external intersection
        // Do not consider the first segments of paths because the first points are identical
        for(int i = 0; i < p->posList.size() - 1; i++){
            Vector2d* v0 = p->posList[i];
            Vector2d* v1 = p->posList[i+1];
            float i_x(0), i_y(0);
            // If has intersection, returns true
            if(GetLineIntersection(v0->x, v0->y, v1->x, v1->y, p0_x, p0_y, p1_x, p1_y, &i_x, &i_y)){
                return true;
            }
        }
    }

    // Check self intersection
    cout << curPath->posList.size() << endl;
    for(int i = 0; i < curPath->posList.size() - 2; i++){
        Vector2d* v0 = curPath->posList[i];
        Vector2d* v1 = curPath->posList[i+1];
        float i_x(0), i_y(0);
        if(GetLineIntersection(v0->x, v0->y, v1->x, v1->y, p0_x, p0_y, p1_x, p1_y, &i_x, &i_y)){
            return true;
        }
    }
    return false;
}

bool PathResampling::HasIntersection(path* curPath){
    int npro = omp_get_max_threads();
    omp_set_num_threads(npro);
    bool hasintersection = false;
#pragma omp parallel for
    for(int j = 0; j < curPath->posList.size() - 1; j++){
        Vector2d* u0 = curPath->posList[j];
        Vector2d* u1 = curPath->posList[j+1];
        for(const auto p : pathSubset){
            for(int i = 1; i < p->posList.size() - 1; i++){
                Vector2d* v0 = p->posList[i];
                Vector2d* v1 = p->posList[i+1];
                float i_x(0), i_y(0);
                if(GetLineIntersection(v0->x, v0->y, v1->x, v1->y, u0->x, u0->y, u1->x, u1->y, &i_x, &i_y)){
                    hasintersection = true;
                }
            }
        }
    }
    return hasintersection;
}

bool PathResampling::HasSharpTurn(path* curPath, Vector2d newLoc){
    if(curPath->posList.size() <= 2){
        return false;
    }
    Vector2d* v0 = curPath->posList.back();
    Vector2d* v1 = curPath->posList[curPath->posList.size() - 2];
    Vector2d e0 = *v1 - *v0;
    Vector2d e1 = newLoc - *v0;
    float f = (e0 * e1) / (e0.norm() * e1.norm());
    float degree = acos(f) * 180.0 / PI;
    degree = degree > 180.0f ? 360.0f - degree : degree;
    if(degree <= 90){
        return true;
    } else{
        return false;
    }
}

// Returns true if the lines intersect, otherwise 0. In addition, if the lines
// intersect the intersection point may be stored in the floats i_x and i_y.
bool PathResampling::GetLineIntersection(float p0_x, float p0_y, float p1_x, float p1_y,
                                         float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
{
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return true;
    }

    return false; // No collision
}

float PathResampling::Point2LineDistance(const Vector2d& p, const Vector2d& l0,
                                         const Vector2d& l1){
    Vector2d e0 = l1 - l0;
    Vector2d e1 = p - l0;
    Vector2d h = e1 - (e1 * e0) * (e0 / e0.norm());
    return h.norm();
}

float PathResampling::SignedPoint2LineDistance(const Vector2d& p, const Vector2d& l0,
                                               const Vector2d& l1){
    Vector2d e0 = l1 - l0;
    Vector2d e1 = p - l0;
    Vector3d u = e0 % e1;
    float dis = Point2LineDistance(p, l0, l1);
    dis = u.z > 0 ? dis : -dis;
    return dis;
}

std::vector<int> PathResampling::FilterClosePoints(const std::vector<Vector2d> &points, const vector<float> radii, const float scalefactor)
{
    vector<int> validlist(points.size(), 0);
    // This most left and most right should kept if they are
    // not too close to each other
    bool keepmostleftandright = true;
    Vector2d mostleft = points.at((points.size() - 1) * 0.5);
    Vector2d mostright = points.at(points.size() - 1);
    double l2r = (mostleft - mostright).norm();
    float maxr = max(radii.at((points.size() - 1) * 0.5), radii.at(points.size() - 1));
    keepmostleftandright = l2r < maxr * scalefactor ? false : true;
    for(unsigned int i = 0; i < points.size() - 1; i++){
        if((i == (points.size() - 1) * .5 || i == points.size() - 1) && keepmostleftandright){
            continue;
        }
        if(validlist.at(i) != 0){
            continue;
        }
        Vector2d p = points.at(i);

        // test most left
        if(i != (points.size() - 1) * 0.5)
        {
            double toleft = (mostleft - p).norm();
            maxr = max(radii.at((points.size() - 1) * 0.5), radii.at(i));
            if(toleft < maxr * scalefactor){
                validlist[i] += 1;
                continue;
            }
        }

        // test most right
        if(i != points.size() - 1){
            double toright = (mostright - p).norm();
            maxr = max(radii.at(points.size() - 1), radii.at(i));
            if(toright < maxr * scalefactor){
                validlist[i] += 1;
                continue;
            }
        }
        for(unsigned int j = i+1; j < points.size(); j++){
            if((j == (points.size() - 1) * .5 || j == points.size() - 1) && keepmostleftandright){
                continue;
            }
            Vector2d q = points.at(j);
            double f = (p - q).norm();
            float r = max(radii.at(i), radii.at(j));
            if(f < r * scalefactor){
                validlist[j] += 1;
                continue;
            }
        }
    }

    return validlist;
}

void PathResampling::TranslateCenterLines2Geo()
{
    for(auto& cl : SDCenterLinesShadow){
        for(auto& p : cl){
            p = translateToGeoCoor(p);
        }
    }
}

void PathResampling::TranslateCenterLines2Screen()
{
    for(auto& cl : SDCenterLinesShadow){
        for(auto& p : cl){
            p = translateToScreen(p.x,p.y);
        }
    }
}

void PathResampling::OutputMeanTrack2File()
{
    if(SDCenterLinesShadow.empty()){
        cerr << "Compute SD Centerlines first." << endl;
        exit(0);
    }
    vector<int> times;
    vector<Vector2d> locs;
    vector<double> errorRadii;
    CenterLine cl = SDCenterLinesShadow.at(0);
    for(unsigned int i = 0; i < cl.size(); i++){
        if(i % 12 != 0){
            continue;
        }
        times.push_back(i);
        Vector2d p = cl.at(i);
        p = translateToGeoCoor(p);
        locs.push_back(p);
        errorRadii.push_back(0.0);
    }
    string str = InputPrefix + DatafileName + "_Mean.txt";
    const char* cstr = str.c_str();
    CDFSample::write2file(times, locs, errorRadii, cstr);
}

void PathResampling::TransformSDCenterLines()
{
    for(auto& cl : SDCenterLinesShadow){
        for(auto& p : cl){
            if(DatafileName == "mcrall_al092008_091018"){
                p.y = p.y - 2;
            } else if (DatafileName == "mcrall_al092012_082612"){
                p.y = p.y - 2;
            }
        }
    }
}

void PathResampling::ComputeStormcharacteristicsInterpolations()
{
    vector<vector<StormCharacteristicInterpolator::PredictionNode>> fullpredictions = ReadFullPredictionsFromCSVFile();
    //stormcharacteristicsinterpolator = new StormCharacteristicInterpolator(fullpredictions.at(_timepoint), _timepoint);
    //for(const auto& cl : SDCenterLinesShadow){
    //    for(unsigned int i = 0; i < cl.size(); i++){
    //        const Vector2d p = cl.at(i);
    //        float f = stormcharacteristicsinterpolator->SubRBFEval(p, "intensity", true);
    //        cout << p << " " <<  f << endl;
    //    }
    //}

    //return;

    // start from 1
    for(unsigned int i = 1; i < stormcharacteristicsinterpolators.size(); i++){
        int hour = i;
        cout << "time:" << hour << endl;
        stormcharacteristicsinterpolators.at(hour) = new StormCharacteristicInterpolator(fullpredictions.at(hour), hour);
    }
}

void PathResampling::VerifyStatistics()
{
    BuildRBFFromFullEnsemble(_timepoint);
    // run OLS
    QString program("/bin/sh");
    string strargs = "/home/lel/Projects/HurVis/matlab-rbf/olsselect.sh";
    cout << strargs << endl;
    //QString args("/home/lel/Projects/HurVis/matlab-rbf/olsselect.sh" + " " + to_string(_timepoint));
    QString args(QString::fromStdString(strargs));
    QProcess process;
    process.start(program, QStringList() << args << QString::fromStdString(to_string(_timepoint)) << QString::fromStdString(DatafileName));
    if(!process.waitForFinished(30000)){
        cerr << "Matlab script was not executed correctly." << endl;
    }
    QString output(process.readAllStandardOutput());
    qDebug() << output;
    process.close();

    ComputeSDFieldsFromOLS();
    //ComputeSDFieldsFromResampling();
    //exit(0);
}

void PathResampling::ComputeSDFieldFromAllEnsembleSamples()
{
    vector<vector<StormCharacteristicInterpolator::PredictionNode>> tmp = ReadFullPredictionsFromCSVFile();
    vector<vector<StormCharacteristicInterpolator::PredictionNode>> predictions(1);
    for(const auto i : tmp){
        for (const auto j : i){
            predictions.at(0).push_back(j);
        }
    }
    cout << predictions.at(0).size() << endl;
}

// Compute SD fields build from full ensemble and a subset selected from OLS
void PathResampling::ComputeSDFieldsFromOLS()
{
    vector<Vector2d> fullensemble = stormcharacteristicsinterpolator->ValidKDTLocations();
    string subsetfile = OutputPrefix + "StatisticsVerification/OLS-selected-samples-" + to_string(_timepoint) + ".csv";
    BuildRBFFromSubset(fullensemble, subsetfile);
}

StormCharacteristicInterpolator* PathResampling::BuildRBFFromFullEnsemble(const int i)
{
    // read in full ensemble
    vector<vector<Vector2d>> fullensemble = ReadFullEnsembleFromFiles();
    // set output file name
    string str = OutputPrefix + "StatisticsVerification/" + to_string(_timepoint) + "-sdfieldfull" + ".png";
    QString* ofilename = new QString(QString::fromStdString(str));
    // build interpolator
    stormcharacteristicsinterpolator = new StormCharacteristicInterpolator(fullensemble.at(_timepoint), ofilename);
    string csvfile = OutputPrefix + "StatisticsVerification/" + "originallocations-sd-" + to_string(_timepoint) + ".csv";
    WritePoints2CSVFile<Vector2d, double>(csvfile, 2, &stormcharacteristicsinterpolator->Locations(), &stormcharacteristicsinterpolator->SDValues(), &stormcharacteristicsinterpolator->RBFRadii());
}

void PathResampling::BuildRBFsFromFullEnsemble()
{

}

void PathResampling::BuildRBFFromSubset(std::vector<Vector2d> &fullset, const string &subsetfilename)
{
    vector<string> datastring;
    ReadCSVFile(subsetfilename, ',', &datastring);
    vector<double> sdvals;
    vector<double> rbfradii;
    vector<Vector2d> points;
    for(auto& c : datastring){
        istringstream ss(c);
        vector<double> vals;
        string token;
        while(getline(ss, token, ',')){
            double f = stod(token);
            vals.push_back(f);
        }
        int index = int(vals[0]) - 1;
        points.push_back(fullset.at(index));
        //sdvals.push_back(vals[1]);
        //rbfradii.push_back(vals[2]);
        sdvals.push_back(stormcharacteristicsinterpolator->SDValues().at(index));
        rbfradii.push_back(stormcharacteristicsinterpolator->RBFRadii().at(index));
    }
    string str = OutputPrefix + "StatisticsVerification/" + to_string(_timepoint) + "-sdfiedsub" + ".png";
    QString* ofilename = new QString(QString::fromStdString(str));
    RBF rbf;
    rbf.compute(points,sdvals,rbfradii);
    //stormcharacteristicsinterpolator = new StormCharacteristicInterpolator(points, ofilename);
    stormcharacteristicsinterpolator->RBFRadii() = rbfradii;
    //stormcharacteristicsinterpolator->WriteScalarField2Image(rbf, true, *ofilename);


}

void PathResampling::ComputeSDFieldsFromResampling()
{
    BuildRBFFromSampledFullEnsemble();
    BuildRBFFromSampledSubset(true);
}

void PathResampling::BuildRBFFromSampledFullEnsemble()
{

    // Extract predicted locations from sampling results
    vector<Vector2d> sampledfullensemble;
    cout << SDCenterLines.size() << endl;
    for(const auto& cl : SDCenterLines){
        sampledfullensemble.push_back(cl.at(_timepoint));
    }
    cout << sampledfullensemble.size() << endl;
    string str = OutputPrefix + "StatisticsVerification/" +  to_string(_timepoint) + "-sdfieldsampledfull" + ".png";
    QString* ofilename = new QString(QString::fromStdString(str));
    stormcharacteristicsinterpolator = new StormCharacteristicInterpolator(sampledfullensemble, ofilename, &ReadFullEnsembleFromFiles().at(_timepoint));
    string csvfile = OutputPrefix + "StatisticsVerification/" + "sampledlocations-sd-" + to_string(_timepoint) + ".csv";
    WritePoints2CSVFile<Vector2d, double>(csvfile, 2, &stormcharacteristicsinterpolator->Locations(), &stormcharacteristicsinterpolator->SDValues(), &stormcharacteristicsinterpolator->RBFRadii());

}

void PathResampling::BuildRBFFromSampledSubset(const bool recomputeradii)
{
    vector<Vector2d> sampledsubset;
    for(const auto& cl : SDCenterLinesShadow){
        sampledsubset.push_back(cl.at(_timepoint));
    }
    string str = OutputPrefix + "StatisticsVerification/" + to_string(_timepoint) + "-sdfieldsampledsubset" + ".png";
    QString* ofilename = new QString(QString::fromStdString(str));
    vector<double> sdvals(sampledsubset.size());
    vector<double> rbfradii(sampledsubset.size());
    for(unsigned int i = 0; i < sampledsubset.size(); i++){
        sdvals[i] = stormcharacteristicsinterpolator->SDValues().at(i);
        rbfradii[i] = stormcharacteristicsinterpolator->RBFRadii().at(i);
        cout << i << " : " << sdvals[i] << endl;
    }
    RBF rbf;
    rbf.compute(sampledsubset, sdvals, rbfradii);
    if(!recomputeradii){
        stormcharacteristicsinterpolator->RBFRadii() = rbfradii;
        //stormcharacteristicsinterpolator->WriteScalarField2Image(rbf, true, *ofilename);
    } else {
        stormcharacteristicsinterpolator->SDValues() = sdvals;
        stormcharacteristicsinterpolator->SDRBF() = rbf;
        rbfradii = RecomputeRBFRadiiBasedOnRecursion();
        for(unsigned int i = 0; i < sampledsubset.size(); i++){
            float recursionlevel = log2(i);
            float factor = 0.5;
            factor = pow(0.5, recursionlevel);
            //rbfradii[i] = factor * (maxradius - rbfradii[i]);
            //rbfradii[i] = factor * rbfradii[i];
        }
        stormcharacteristicsinterpolator->RBFRadii() = rbfradii;
        //stormcharacteristicsinterpolator->WriteScalarField2Image(rbf, true, *ofilename);
    }
}

std::vector<double> PathResampling::RecomputeRBFRadiiBasedOnRecursion()
{
    vector<double> rbfradii;
    // BFS
    queue<CenterlineNode*> isVisited;
    isVisited.push(_centerlineroot);
    CenterlineNode* ptr;
    testpoints.clear();
    testvalues.clear();
    while(!isVisited.empty()){
        ptr = isVisited.front();
        isVisited.pop();
        vector<Vector2d> points;
        points.clear();
        ExtractLeftRightPointsOfCenterline(ptr,&points,SDCenterLines);
        vector<Vector2d> bbox;
        StormCharacteristicInterpolator::ComputeBBoxFromPointSet(points, &bbox);
        vector<Vector2d> validpoints;
        vector<double> densities;
        StormCharacteristicInterpolator::ComputeKDNNDensity(points, bbox, &validpoints, &densities);
        vector<double> rbfradiiatlevel;
        StormCharacteristicInterpolator::ComputeRBFRadiiUsingKNNDensity(bbox, validpoints, densities,&rbfradiiatlevel);
        rbfradii.push_back(rbfradiiatlevel[0]);
        testpoints = validpoints;
        testvalues = densities;
        if(ptr->left != NULL){
            isVisited.push(ptr->left);
        }
        if(ptr->right != NULL){
            isVisited.push(ptr->right);
        }
    }
    return rbfradii;
}

void PathResampling::ExtractLeftRightPointsOfCenterline(CenterlineNode* currentnode, std::vector<Vector2d> *points, const std::vector<CenterLine> &centerlines)
{
    if(currentnode == NULL){
        return;
    }
    int idx = currentnode->_id;
    CenterLine cl = centerlines.at(idx);
    Vector2d p = cl.at(_timepoint);
    points->push_back(p);
    ExtractLeftRightPointsOfCenterline(currentnode->left, points, centerlines);
    ExtractLeftRightPointsOfCenterline(currentnode->right, points, centerlines);
}

void PathResampling::DrawGeneratedLocations(const int UDId){
    glColor4f(0, 0, 1, 1);
    for(auto v : GeneratedPointsUD[UDId]){
        CDFSample::drawSolidCircle((v.x + 0.5) * 400, (v.y + 0.5) * 400, 6);
    }
}

void PathResampling::DrawODPoints(const std::vector<Vector2d>& points){
    for(auto v : points){
        CDFSample::drawSolidCircle(v.x, v.y, 3);
    }
}

void PathResampling::DrawPointsGroup(const std::vector<std::vector<Vector2d>>& group){
    for(const auto& p : group){
        DrawODPoints(p);
    }
}

void PathResampling::DrawODPointsWithSimplicialDepthValues(const vector<Vector2d>& points,
                                                           const vector<double>& sdvals){
    assert(points.size() == sdvals.size());
    ColorGradient colorgradien;
    float red(0.0f), green(0.0f), blue(0.0f);
    for(int i = 0; i < points.size(); i++){
        colorgradien.getColorAtValue(sdvals[i], red, green, blue);
        glColor4f(red, green, blue, 1.0);
        CDFSample::drawSolidCircle(points[i].x, points[i].y, 6);
    }
}

void PathResampling::DrawDebugInfo(){
    if(PathVisMode == 0){
        DrawResampledPathsWithStormCharacteristics();
    } else if(PathVisMode == 1){
        DrawSDCenterLines();
    }
    //glColor4f(0, 1, 0, 1);
    //DrawSDCenterLine(SDCenterLinesShadow.at(0));
    //Vector2d p(500, 500);
    //Vector2d q(-81.78, 24.5551);
    //float r = CDFSample::Stormsize2Display(q, 40);
    //p = translateToScreen(q.x,q.y);
    //glColor4f(1, 0, 0, 1);
    //DrawCircle(p.x, p.y, r);
    //glColor4f(1, 0, 0, 1);
    //DrawSDCenterLine(cl);
    //for(const auto& cl : SDCenterLinesShadow){
    //for(unsigned int i = 0; i < cl.size() - 1; i++){
    //    Vector2d p = cl.at(i);
    //    Vector2d q = cl.at(i+1);
    //    float red(0.0f), green(0.0f), blue(0.0f);
    //    StormCharacteristicInterpolator::StormCategoryColorMapping(i%7+1, &red, &green, &blue);
    //    glColor4f(red, green, blue, 1.0);
    //    glBegin(GL_LINES);
    //    glVertex3f(p.x, p.y, 0);
    //    glVertex3f(q.x, q.y, 0);
    //    glEnd();
    //}
    //}
    return;
    //cout << "Number of SD centerlines: " << SDCenterLines.size() << ";\t";
    //cout << "Forecast time: " << sampledTimeSteps - 1<< " hours" << ";\t";
    //cout << VARNAME(AppliedSimplifyAlgorithm) << ": " << AppliedSimplifyAlgorithm << endl;
    //glColor4f(0, 1, 1, 1);
    ////DrawBoundingBoxes();
    DrawOriginalPathsEnsemble();
    DrawSDCenterLines();
    //DrawOriginalPointsEnsemble(_timepoint);

    //glColor4f(1, 1, 0, 1);
    //CDFSample::drawSolidCircle(538.144,472.187,4);


    //glLineWidth(0.5);
    //glColor4f(0.3, 0.3 , 0.3, 1);
    //DrawSDCenterLine(testcenterline2);
    //glLineWidth(3);
    //glColor4f(255.0/255.0, 255.0/255.0, 191.0/255.0, 1);
    //DrawSDCenterLine(testcenterline);
    //glColor4f(1, 0 , 0, 1);
    //DrawODPoints(testpoints2);
    //for(int i = 0; i < testright.size(); i++){
    // //glColor4f(1, 0, 0, 0.01);
    // //DrawODPoints(testleft.at(i));
    // glColor4f(0, 0, 1, 0.01);
    // DrawODPoints(testright.at(i));
    //}
    //glColor4f(0.5, 0.5, 0.5, 1);
    //DrawSDCenterLine(testcenterlines2.at(60));
    //glColor4f(1.0, 1.0, 1.0, 1);
    //DrawSDCenterLine(testcenterlines.at(60));
    //for(const auto l : testcenterlines2){
    // glColor4f(1, 1, 1, 1);
    // DrawSDCenterLine(l);
    //}
    //for(const auto l : testcenterlines){
    // glColor4f(0.4, 0.4, 0.0, 1);
    // DrawSDCenterLine(l);
    //}

    //glColor4f(1, 0, 1, 1);
    //for(int i = 0; i < testleftregions.size(); i++){
    // DrawRegion(testleftregions[i]);
    // DrawRegion(testrightregions[i]);
    //}

    if(stormcharacteristicsinterpolator != NULL){
        //stormcharacteristicsinterpolator->DrawLocationWithScalarValues(stormcharacteristicsinterpolator->ValidKDTLocations(),stormcharacteristicsinterpolator->KDTDensity());
        stormcharacteristicsinterpolator->DrawLocationWithScalarValues(stormcharacteristicsinterpolator->Locations(),stormcharacteristicsinterpolator->SDValues());
    }

    //DrawSDCenterLinePointAtTimeStep(_timepoint);
    glColor4f(0, 0, 1, 1);
    DrawSDCenterLine(SDCenterLines[0]);
    //StormCharacteristicInterpolator::DrawLocationWithScalarValues(testpoints, testvalues);

    //stormcharacteristicsinterpolator->DrawScalarField(stormcharacteristicsinterpolator->SDRBF(), true);
    //stormcharacteristicsinterpolator->DrawReproducedScalarValues(stormcharacteristicsinterpolator->SDRBF());
}

void PathResampling::DrawSDCenterLines(){
    for(const auto& cl : SDCenterLines){
        glColor4f(0.3, 0.3, 0.3, 0.3);
        DrawSDCenterLine(cl);
    }

    //for(const auto& cl : SDCenterLinesShadow){
    // glColor4f(203.0/255.0, 24.0/255.0, 29.0/255.0, 0.7);
    // DrawSDCenterLine(cl);
    // //glColor4f(1, 1, 0, 1);
    // //CDFSample::drawSolidCircle(cl.at(_timepoint).x, cl.at(_timepoint).y, 3);
    //}
}

void PathResampling::DrawSDCenterLine(const CenterLine& centerline){
    glLineWidth(1.0);
    //glColor4f(1, 1, 1, 1);
    glBegin(GL_LINE_STRIP);
    for(const auto p : centerline){
        Vector2d q = correctHurrianceLocation(p, DatafileName, SDCenterLinesShadow.at(0).at(0));
        glVertex3f(q.x, q.y, 0);
    }
    glEnd();
    //for(const auto p : centerline){
    // glColor4f(1, 0, 0, 1);
    // CDFSample::drawSolidCircle(p.x, p.y, 1);
    //}
}

void PathResampling::DrawSDCenterLinePointAtTimeStep(const int timestep){
    for(const auto& cl : SDCenterLinesShadow){
        glColor4f(0, 0, 1, 1);
        CDFSample::drawSolidCircle(cl.at(timestep).x, cl.at(timestep).y, 3);
    }
}

void PathResampling::DrawOriginalPathsEnsemble(){
    vector<vector<Vector2d>> predictedLocations = ReadFullEnsembleFromFiles();
    glColor4f(1, 0, 0, 1.0);
    for(int i = 0; i < 1000; i++){
        glBegin(GL_LINE_STRIP);
        for(int j = 0; j < 72; j++){
            Vector2d p = predictedLocations[j][i];
            p = correctHurrianceLocation(p, DatafileName, p);
            glVertex3f(p.x, p.y, 0);
        }
        glEnd();
    }
}

void PathResampling::DrawOriginalPointsEnsemble(const int time)
{
    vector<vector<Vector2d>> predictedLocations = ReadFullEnsembleFromFiles();
    glColor4f(0, 1, 0, 1);
    for(int i = 0; i < 1000; i++){
        Vector2d p = predictedLocations[time].at(i);
        CDFSample::drawSolidCircle(p.x, p.y, 3);
    }
}

void PathResampling::DrawPartition(const Partition& partition){
    for(const auto r : partition){
        DrawRegion(r);
    }
}

void PathResampling::DrawRegion(const vector<Vector2d>& region){
    glBegin(GL_LINE_LOOP);
    glVertex3f(region[0].x, region[0].y, 0);
    glVertex3f(region[1].x, region[1].y, 0);
    glVertex3f(region[3].x, region[3].y, 0);
    glVertex3f(region[2].x, region[2].y, 0);
    glEnd();
}

void PathResampling::DrawTriangle(const Vector2d& p0, const Vector2d& p1, const Vector2d& p2){
    glBegin(GL_LINE_LOOP);
    glVertex3f(p0.x, p0.y, 0);
    glVertex3f(p1.x, p1.y, 0);
    glVertex3f(p2.x, p2.y, 0);
    glEnd();
}

void PathResampling::GenerateStructuredPaths(const std::vector<Vector4d*>& projPath,
                                             const std::vector<double>& errorRadii, std::vector<path*>* pathList){
    // Convert radii
    std::vector<double> convertedRadii;
    for(int i = 0; i < projPath.size(); i++){
        Vector2d newlocation = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRadii[i], 0);
        newlocation = translateToScreen(newlocation.x, newlocation.y);
        Vector2d curLocation = translateToScreen(projPath[i]->x, projPath[i]->y);
        double dist = (newlocation - curLocation).norm();
        convertedRadii.push_back(dist);
    }
    // Draws the circles
    double curDeg = 0;
    Vector2d locToDraw;
    glColor4f(1.0, 1.0, 1.0, 1);
    for(int i = 0; i < projPath.size() - 2; i++){
        for(curDeg = 0; curDeg < 360.0; curDeg++){
            glBegin(GL_POINTS);
            locToDraw = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRadii[i], curDeg);
            locToDraw = translateToScreen(locToDraw.x, locToDraw.y);
            glVertex3f(locToDraw.x, locToDraw.y, 0.0);
            glEnd();
        }
        Vector2d center = translateToScreen(projPath[i]->x, projPath[i]->y);
        CDFSample::drawSolidCircle(center.x, center.y, 3);
        //glBegin(GL_LINES);
        //glVertex3f(center.x, center.y, 0);
        //glVertex3f(center.x, center.y - 10, 0);
        //glEnd();
    }
    // Draw reference line
    for(int i = 0; i < projPath.size() - 2; i++){
        Vector2d nextLoc = translateToScreen(projPath[i+1]->x, projPath[i+1]->y);
        Vector2d curLoc  = translateToScreen(projPath[i]->x, projPath[i]->y);
        Vector2d RightLoc, LeftLoc;
        Vector2d n = nextLoc - curLoc;
        n = n / n.norm();
        Matrix2x2 rot(std::cos(0.5 * PI), std::sin(-0.5 * PI), std::sin(0.5 * PI), std::cos(0.5 * PI));
        n = n * rot;
        RightLoc = curLoc + errorRadii[i] * n;
        LeftLoc  = curLoc - errorRadii[i] * n;
        glColor4f(1, 1, 1, 1);
        //glBegin(GL_LINES);
        //locToDraw = translateToScreen(LeftLoc.x, LeftLoc.y);
        //glVertex3f(locToDraw.x, locToDraw.y, 0);
        //locToDraw = translateToScreen(RightLoc.x, RightLoc.y);
        //glVertex3f(locToDraw.x, locToDraw.y, 0);
        //glEnd();
        glLineWidth(0.1);
        glBegin(GL_LINES);
        glVertex3f(RightLoc.x, RightLoc.y, 0);
        glVertex3f(LeftLoc.x, LeftLoc.y, 0);
        glEnd();

    }

    pathList->clear();

    // Creates pseudorandom number generator
    myclock::duration d = myclock::now() - beginning;
    PRNG Generator(d.count());

    // Generates new paths
    while(pathSubset.size() < numofpaths){
        path* newpath = new path(0, 0, 0, 0, 0);
        newpath->posList.clear();
        newpath->latLonList.clear();

        // The first path is the center line
        if(pathSubset.size() == 0){
            for(const auto p : projPath){
                newpath->latLonList.push_back(new Vector2d(p->x, p->y));
                Vector2d newposition = translateToScreen(p->x, p->y);
                newpath->posList.push_back(new Vector2d(newposition.x, newposition.y));
            }
            newpath->latLonList.pop_back();
            newpath->posList.pop_back();
            newpath->posList.pop_back();
            pathSubset.push_back(newpath);
            continue;
        }

        // Randomly generates a degree theta and a distance f
        // theta is [0, 360]
        // f is [0, 1] -- this may be changed later
        float theta = Generator.RandomFloat() * 360.0f;
        float f = std::sqrt(Generator.RandomFloat() * 2.0f);

        // The first location is identical
        newpath->latLonList.push_back(new Vector2d(projPath[0]->x, projPath[0]->y));
        Vector2d firstpos = translateToScreen(projPath[0]->x, projPath[0]->y);
        newpath->posList.push_back(new Vector2d(firstpos.x, firstpos.y));

        // The rest locations are generated based on theta and f
        for(int i = 1; i < projPath.size() - 2; i++){
            // Computes base angle
            Vector2d nextLoc = translateToScreen(projPath[i+1]->x, projPath[i+1]->y);
            Vector2d prevLoc = translateToScreen(projPath[i-1]->x, projPath[i-1]->y);
            Vector2d curLoc  = translateToScreen(projPath[i]->x, projPath[i]->y);
            Vector2d e0 = nextLoc - curLoc;
            e0 = e0 / e0.norm();
            Vector2d e1(0, -1.0);
            //float alpha = (e0 * e1) / (e0.norm() * e1.norm());
            //alpha = std::acos(alpha);
            double alpha = std::atan2(e1.y, e1.x) - std::atan2(e0.y, e0.x);
            alpha *= -1.0f;
            Matrix2x2 rot(std::cos(alpha), std::sin(-alpha), std::sin(alpha), std::cos(alpha));
            e1 = rot * e1;
            rot = Matrix2x2(std::cos(theta), std::sin(-theta), std::sin(theta), std::cos(theta));
            e1 = rot * e1;
            //alpha = acos(alpha) * 180.0 / PI;
            //alpha = 0.5 * PI - alpha;
            //Vector2d newlocation = locateDestination_2(projPath[i]->x, projPath[i]->y, f * errorRadii[i],  alpha + theta);
            Vector2d newlocation = curLoc + f * convertedRadii[i] * e1;
            //newpath->latLonList.push_back(new Vector2d(newlocation.x, newlocation.y));
            newpath->posList.push_back(new Vector2d(newlocation.x, newlocation.y));
        }

        // Updates posList for display
        //for(auto p : newpath->latLonList){
        // Vector2d newposition = translateToScreen(p->x, p->y);
        // newpath->posList.push_back(new Vector2d(newposition.x, newposition.y));
        //}


        // Update path subset
        if(HasIntersection(newpath)){
            continue;
        } else{
            pathSubset.push_back(newpath);
        }
        //pathSubset.push_back(newpath);

    }

    DrawPathsSubset();
}

void PathResampling::PhysicallyBasedPathsGeneration(const std::vector<Vector4d*>& projPath, const std::vector<double>& errorRadii, std::vector<path*>* pathList){
    ReadUDSpaces();
    ConstructInvmaps();
    ComputeUDMinDistances();
    //GeneratePoissonDiskPointsInUDSpaces();
    //WriteGeneratedPointsToFiles();

    //GenerateFirstPath();
    //GenerateStructuredPathsByPDSamplingInCircles(projPath, errorRadii, pathList);
    //ProjectPredictedLocationsToUDSpaces(projPath);
    //DrawProjectedPointsInUDSpaces(0);

    ConstructPathsFromParticleSimulation(projPath);

    DrawPathsSubset();
    //int i = 0;
    //projmaps[i].DrawUDGrid();
    //projmaps[i].DrawODGrid();
    //DrawGeneratedLocations(i);
}


void PathResampling::GenerateStructuredPathsByPDSamplingInCircles(const std::vector<Vector4d*>& projPath, const std::vector<double>& errorRadii, std::vector<path*>* pathList){
    // Convert radii
    std::vector<double> convertedRadii;
    for(int i = 0; i < projPath.size(); i++){
        Vector2d newlocation = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRadii[i], 0);
        newlocation = translateToScreen(newlocation.x, newlocation.y);
        Vector2d curLocation = translateToScreen(projPath[i]->x, projPath[i]->y);
        double dist = (newlocation - curLocation).norm();
        convertedRadii.push_back(dist);
    }

    // Draws the circles
    double curDeg = 0;
    Vector2d locToDraw;
    glColor4f(1.0, 1.0, 1.0, 1);
    for(int i = 0; i < projPath.size() - 1; i++){
        for(curDeg = 0; curDeg < 360.0; curDeg++){
            glBegin(GL_POINTS);
            locToDraw = locateDestination_2(projPath[i]->x, projPath[i]->y, errorRadii[i], curDeg);
            locToDraw = translateToScreen(locToDraw.x, locToDraw.y);
            glVertex3f(locToDraw.x, locToDraw.y, 0.0);
            glEnd();
        }
        Vector2d center = translateToScreen(projPath[i]->x, projPath[i]->y);
        CDFSample::drawSolidCircle(center.x, center.y, 3);
        //glBegin(GL_LINES);
        //glVertex3f(center.x, center.y, 0);
        //glVertex3f(center.x, center.y - 10, 0);
        //glEnd();
    }
    // Draw reference line
    for(int i = 0; i < projPath.size() - 1; i++){
        Vector2d nextLoc = translateToScreen(projPath[i+1]->x, projPath[i+1]->y);
        Vector2d curLoc  = translateToScreen(projPath[i]->x, projPath[i]->y);
        Vector2d RightLoc, LeftLoc;
        Vector2d n = nextLoc - curLoc;
        n = n / n.norm();
        Matrix2x2 rot(std::cos(0.5 * PI), std::sin(-0.5 * PI), std::sin(0.5 * PI), std::cos(0.5 * PI));
        n = n * rot;
        RightLoc = curLoc + errorRadii[i] * n;
        LeftLoc  = curLoc - errorRadii[i] * n;
        glColor4f(1, 1, 1, 1);
        //glBegin(GL_LINES);
        //locToDraw = translateToScreen(LeftLoc.x, LeftLoc.y);
        //glVertex3f(locToDraw.x, locToDraw.y, 0);
        //locToDraw = translateToScreen(RightLoc.x, RightLoc.y);
        //glVertex3f(locToDraw.x, locToDraw.y, 0);
        //glEnd();
        glLineWidth(0.1);
        glBegin(GL_LINES);
        glVertex3f(RightLoc.x, RightLoc.y, 0);
        glVertex3f(LeftLoc.x, LeftLoc.y, 0);
        glEnd();
    }

    if(ReferencePoints.empty()){
        myclock::duration d = myclock::now() - beginning;
        cout << d.count() << endl;
        PoissonPointsGenerator ppg(numofpaths, d.count());
        std::vector<sPoint> tmp = ppg.GeneratePoissonPoints();
        cout << tmp.size() << endl;
        for(auto v : tmp){
            ReferencePoints.push_back(Vector2d(v.x, v.y));
        }
    } else {
        int counter = 0;
        while(pathSubset.size() < numofpaths){
            path* newpath = new path(0, 0, 0, 0, 0);
            newpath->posList.clear();
            newpath->latLonList.clear();

            // The first path is the center line
            if(pathSubset.size() == 0){
                for(const auto p : projPath){
                    newpath->latLonList.push_back(new Vector2d(p->x, p->y));
                    Vector2d newposition = translateToScreen(p->x, p->y);
                    newpath->posList.push_back(new Vector2d(newposition.x, newposition.y));
                }
                newpath->latLonList.pop_back();
                newpath->posList.pop_back();
                newpath->posList.pop_back();
                pathSubset.push_back(newpath);
                continue;
            }

            // The first location is identical
            newpath->latLonList.push_back(new Vector2d(projPath[0]->x, projPath[0]->y));
            Vector2d firstpos = translateToScreen(projPath[0]->x, projPath[0]->y);
            newpath->posList.push_back(new Vector2d(firstpos.x, firstpos.y));

            for(int i = 1; i < projPath.size() - 1; i++){
                Vector2d newlocation(projPath[i]->x, projPath[i]->y);
                newlocation = translateToScreen(projPath[i]->x, projPath[i]->y);
                Vector2d v = ReferencePoints[counter];
                Vector2d nextLoc = translateToScreen(projPath[i+1]->x, projPath[i+1]->y);
                Vector2d prevLoc = translateToScreen(projPath[i-1]->x, projPath[i-1]->y);
                Vector2d curLoc  = translateToScreen(projPath[i]->x, projPath[i]->y);
                Vector2d e0 = nextLoc - curLoc;
                e0 = e0 / e0.norm();
                Vector2d e1(0, -1.0);
                double alpha = std::atan2(e1.y, e1.x) - std::atan2(e0.y, e0.x);
                alpha *= -1.0f;
                Matrix2x2 rot(std::cos(alpha), std::sin(-alpha), std::sin(alpha), std::cos(alpha));
                v = v - Vector2d(0.5, 0.5);
                //v = v / v.norm() * (v.norm() * v.norm());
                v = rot * v;
                newlocation = newlocation + convertedRadii[i] * 2 * v;
                newpath->posList.push_back(new Vector2d(newlocation.x, newlocation.y));
            }
            counter++;
            pathSubset.push_back(newpath);
        }
        pathSubset.pop_back();
    }
}

void PathResampling::ProjectPredictedLocationsToUDSpaces(const std::vector<Vector4d*>& projPath){
    // predicted locations in UD spaces
    projectedPoints.clear();
    projectedPoints.resize(numofUDspaces);

    for(int i = 0; i < pathSubset.size(); i++){
        for(int j = 1; j < projPath.size() - 1; j++){
            // Ignore the first point
            Vector2d query(pathSubset[i]->posList[j]->x, pathSubset[i]->posList[j]->y);
            Vector2d projPoint = projmaps[j-1].ForwardMap(query, UDSpaces[j-1].U, UDSpaces[j-1].V);
            projectedPoints[j-1].push_back(projPoint);
        }
    }

    // Writes projected point in UD spaces to file
    int count = 0;
    for(const auto vec : projectedPoints){
        char buff[200];
        string str = OutputPrefix + "InternalData/PathSamplingData/ProjectedPointsInUD-%d.txt";
        const char* cstr = str.c_str();
        sprintf(buff, cstr, count++);
        CDFSample::write2file(vec, buff);
    }

}

void PathResampling::DrawProjectedPointsInUDSpaces(const int UDId){
    projmaps[UDId].DrawUDGrid();
    for(const auto p : projectedPoints[UDId]){
        glColor4f(0, 0, 1, 1);
        CDFSample::drawSolidCircle((p.x + 0.5) * 400, (p.y + 0.5) * 400, 3);
    }
}


void PathResampling::ReadAdjustedPointsFromFile(){
    AdjustedPoints.resize(4);
    for(int i = 0; i < 4; i++){
        char buff[200];
        string str = OutputPrefix + "InternalData/PathSamplingData/OutputParticles-%d.txt";
        const char* cstr = str.c_str();
        sprintf(buff, cstr, i);
        cout << buff << endl;
        AdjustedPoints[i] = CDFSample::readTestSamples(buff);
        cout << AdjustedPoints[i].size() << endl;
    }
}

void PathResampling::ReadPointsFromFiles(std::vector<std::vector<Vector2d>>* points,
                                         const int numofsets, const string& filenameprefix){
    points->clear();
    points->resize(numofsets);
    for(int i = 0; i < numofsets; i++){
        const char* cstr = filenameprefix.c_str();
        char buff[200];
        sprintf(buff, cstr, i);
        points->at(i) = CDFSample::readTestSamples(buff);
        cout << "Read " << points->at(i).size() << " points from " << buff << endl;
    }
}

void PathResampling::ConstructPathsFromParticleSimulation(const std::vector<Vector4d*>& projPath){
    ReadAdjustedPointsFromFile();
    for(int i = 0; i < numofpaths - 1; i++){
        path* newpath = new path(0, 0, 0, 0, 0);
        newpath->posList.clear();
        newpath->latLonList.clear();
        // The first location is identical
        newpath->latLonList.push_back(new Vector2d(projPath[0]->x, projPath[0]->y));
        Vector2d firstpos = translateToScreen(projPath[0]->x, projPath[0]->y);
        newpath->posList.push_back(new Vector2d(firstpos.x, firstpos.y));

        for(int j = 0; j < AdjustedPoints.size(); j++){
            Vector2d pUD = AdjustedPoints[j][i];
            Vector2d pOD = projmaps[j].InvMap(pUD);
            newpath->posList.push_back(new Vector2d(pOD.x, pOD.y));
        }

        pathSubset.push_back(newpath);
    }
}

void PathResampling::GenerateStructuredPathsByOptimizeCostMatrix(const std::vector<Vector4d*>& projPath,
                                                                 const std::vector<double>& errorRadii,
                                                                 std::vector<path*>* pathList){
    ReadUDSpaces();
    ConstructInvmaps();
    ComputeUDMinDistances();

    // Uncomment the following two lines if have not generated Poisson disk subsets
    //GeneratePoissonDiskPointsInUDSpaces();
    //WriteGeneratedPointsToFiles();

    // Read selected points from original ensemble
    //string pointsfileprefix = OutputPrefix + "InternalData/PathSamplingData/OD-selected-%d.txt";
    //ReadPointsFromFiles(&GeneratedPointsOD, numofUDspaces, pointsfileprefix);
    //for(auto& l : GeneratedPointsOD){
    // for(auto& v : l){
    //  v = translateToScreen(v.x, v.y);
    // }
    //}

    string pointsfileprefix = OutputPrefix + "InternalData/PathSamplingData/ReferencePoissonPoints-%d.txt";
    ReadPointsFromFiles(&GeneratedPointsUD, numofUDspaces, pointsfileprefix);
    // Projects generated points from UD to OD
    GeneratedPointsOD.resize(GeneratedPointsUD.size());
    for(int i = 0; i < GeneratedPointsUD.size(); i++){
        GeneratedPointsOD[i].resize(GeneratedPointsUD[i].size());
        for(int j = 0; j < GeneratedPointsUD[i].size(); j++){
            Vector2d pUD = GeneratedPointsUD[i][j];
            Vector2d pOD = projmaps[i].InvMap(pUD);
            GeneratedPointsOD[i][j] = pOD;
        }
    }

    // Test the results by using the orignal points ensembles
    GeneratedPointsOD.clear();
    GeneratedPointsOD = OriginalPointsOD;

    //GenerateFirstPath();
    //GenerateStructuredPathsByPDSamplingInCircles(projPath, errorRadii, pathList);
    //ProjectPredictedLocationsToUDSpaces(projPath);
    //DrawProjectedPointsInUDSpaces(0);

    //ConstructPathsFromParticleSimulation(projPath);
    ConstructPathsByOptimizeCostMatrix(projPath);

    DrawPathsSubset();
    //int i = 4;
    //projmaps[i].DrawUDGrid();
    //projmaps[i].DrawODGrid();
    //DrawGeneratedLocations(i);
    //DrawODPoints(GeneratedPointsOD[i]);
}

void PathResampling::ConstructPathsByOptimizeCostMatrix(const std::vector<Vector4d*>& projPath){
    pathSubset.clear();
    // Generate the first segments
    for(int i = 0; i < numofpaths; i++){
        path* newpath = new path(0, 0, 0, 0, 0);
        newpath->posList.clear();
        newpath->latLonList.clear();
        // The first location is identical
        newpath->latLonList.push_back(new Vector2d(projPath[0]->x, projPath[0]->y));
        Vector2d firstpos = translateToScreen(projPath[0]->x, projPath[0]->y);
        newpath->posList.push_back(new Vector2d(firstpos.x, firstpos.y));
        // The second location can be selected arbitrarily
        Vector2d secondpos = GeneratedPointsOD[0][i];
        newpath->posList.push_back(new Vector2d(secondpos.x, secondpos.y));
        pathSubset.push_back(newpath);
    }

    // Generate the rest segments
    for(int i = 0; i < numofUDspaces - 1; i++){ // for individual time step except the last one
        std::vector<Vector2d> prevPoints(numofpaths); // points in the previous time step
        std::vector<Vector2d> currPoints(numofpaths); // points in the current time step
        std::vector<Vector2d> nextPoints(numofpaths); // points in the next time step
        for(int j = 0; j < numofpaths; j++){
            prevPoints[j] = *pathSubset[j]->posList[pathSubset[j]->posList.size() - 2];
            currPoints[j] = *pathSubset[j]->posList[pathSubset[j]->posList.size() - 1];
            nextPoints[j] = GeneratedPointsOD[i + 1][j];
        }
        // Computes bearing changes matrix
        std::vector<std::vector<float>> bmatrix = ComputeBearingChangesMatrix(prevPoints, currPoints, nextPoints);
        // Computes speed changes matrix
        std::vector<std::vector<float>> smatrix = ComputeSpeedChangesMatrix(prevPoints, currPoints, nextPoints);
        // Computes length matrix
        std::vector<std::vector<float>> lmatrix = ComputePathLengthMatrix(prevPoints, currPoints, nextPoints);
        // Computes weighted distance matrix
        std::vector<std::vector<float>> wmatrix =
                ComputeWeightedDistanceMatrix(bmatrix, smatrix, lmatrix);
        //PrintMatrix<float>(bmatrix);
        //PrintMatrix<float>(smatrix);
        //PrintMatrix<float>(wmatrix);
        //PrintMatrix<float>(lmatrix);
        // Computes optimized assignment
        std::vector<int> assignment = ComputeAssignment(wmatrix);
        // Construct new segments of paths
        for(int j = 0; j < numofpaths; j++){
            const int npidx = assignment[j];
            const Vector2d np = nextPoints[npidx];
            pathSubset[j]->posList.push_back(new Vector2d(np.x, np.y));
        }
    }
}

vector<vector<float>> PathResampling::ComputeBearingChangesMatrix(const std::vector<Vector2d>& prevPoints,
                                                                  const std::vector<Vector2d>& currPoints,
                                                                  const std::vector<Vector2d>& nextPoints){
    std::vector<std::vector<float>> bmatrix(prevPoints.size(), std::vector<float>(prevPoints.size()));
    std::vector<Vector2d> prevDir(prevPoints.size());
    // Previous directions are already determined
    for(int i = 0; i < prevPoints.size(); i++){
        prevDir[i] = currPoints[i] - prevPoints[i];
        prevDir[i] = prevDir[i] / prevDir[i].norm();
    }
    // Considers all possible next directions
    float minval = std::numeric_limits<float>::max();
    float maxval = std::numeric_limits<float>::min();
    for(int i = 0; i < prevPoints.size(); i++){
        for(int j = 0; j < prevPoints.size(); j++){
            Vector2d nextDir = nextPoints[j] - currPoints[i];
            nextDir = nextDir / nextDir.norm();
            Vector2d e0 = prevDir[i];
            Vector2d e1 = nextDir;
            float angle = (e0 * e1) / (e0.norm() * e1.norm());
            angle = std::abs(std::acos(angle));
            //double angle = std::abs(std::atan2(nextDir.y, nextDir.x) - std::atan2(prevDir[i].y, prevDir[i].x));
            bmatrix[i][j] = angle;
            minval = minval > bmatrix[i][j] ? bmatrix[i][j] : minval;
            maxval = maxval < bmatrix[i][j] ? bmatrix[i][j] : maxval;
        }
    }
    // Normalize all values to [0, 1]
    for(int i = 0; i < currPoints.size(); i++){
        for(int j = 0; j < nextPoints.size(); j++){
            float val = bmatrix[i][j];
            val = (val - minval) / (maxval - minval);
            bmatrix[i][j] = val;
        }
    }
    return bmatrix;
}

vector<vector<float>> PathResampling::ComputeSpeedChangesMatrix( const std::vector<Vector2d>& prevPoints,
                                                                 const std::vector<Vector2d>& currPoints,
                                                                 const std::vector<Vector2d>& nextPoints){
    vector<vector<float>> smatrix(currPoints.size(), vector<float>(currPoints.size()));
    std::vector<float> prevSpeed(prevPoints.size());
    // Previous speed are already determined
    for(int i = 0; i < prevPoints.size(); i++){
        prevSpeed[i] = (currPoints[i] - prevPoints[i]).norm();
    }
    // Finds max and min for later normlization
    float minval = std::numeric_limits<float>::max();
    float maxval = std::numeric_limits<float>::min();
    for(int i = 0; i < currPoints.size(); i++){
        for(int j = 0; j < nextPoints.size(); j++){
            float dis = (nextPoints[j] - currPoints[i]).norm();
            smatrix[i][j] = std::abs(dis - prevSpeed[i]);
            minval = minval > smatrix[i][j] ? smatrix[i][j] : minval;
            maxval = maxval < smatrix[i][j] ? smatrix[i][j] : maxval;
        }
    }
    // Normalize all values to [0, 1]
    for(int i = 0; i < currPoints.size(); i++){
        for(int j = 0; j < nextPoints.size(); j++){
            float val = smatrix[i][j];
            val = (val - minval) / (maxval - minval);
            smatrix[i][j] = val;
        }
    }
    return smatrix;
}

vector<vector<float>> PathResampling::ComputePathLengthMatrix(
        const std::vector<Vector2d>& prevPoints,
        const std::vector<Vector2d>& currPoints,
        const std::vector<Vector2d>& nextPoints){
    vector<vector<float>> lmatrix(currPoints.size(), vector<float>(currPoints.size()));
    // Finds max and min for later normalization
    float minval = std::numeric_limits<float>::max();
    float maxval = std::numeric_limits<float>::min();
    for(int i = 0; i < currPoints.size(); i++){
        for(int j = 0; j < nextPoints.size(); j++){
            float dis = (nextPoints[j] - currPoints[i]).norm();
            lmatrix[i][j] = dis;
            minval = minval > dis ? dis : minval;
            maxval = maxval < dis ? dis : maxval;
        }
    }
    // Normalizes all values to [0, 1]
    for(int i = 0; i < currPoints.size(); i++){
        for(int j = 0; j < nextPoints.size(); j++){
            float val = lmatrix[i][j];
            val = (val / minval) / (maxval - minval);
            lmatrix[i][j] = val;
        }
    }
    return lmatrix;
}

// bmatrix: matrix stores weights of bearing
// smatrix: matrix stores weights of speed
// lmatrix: matrix stores weights of total length
vector<vector<float>> PathResampling::ComputeWeightedDistanceMatrix(
        const vector<vector<float>>& bmatrix,
        const vector<vector<float>>& smatrix,
        const vector<vector<float>>& lmatrix){
    vector<vector<float>> wmatrix(bmatrix.size(), vector<float>(bmatrix.size()));
    const float bweight = 0.4f;
    const float sweight = 0.0f;
    const float lweight = 0.6f;
    for(int i = 0; i < bmatrix.size(); i++){
        for(int j = 0; j < bmatrix.size(); j++){
            // uses squared scores
            wmatrix[i][j] = bweight * bmatrix[i][j] * bmatrix[i][j] +
                    sweight * smatrix[i][j] * smatrix[i][j] +
                    lweight * lmatrix[i][j] * lmatrix[i][j];
        }
    }
    return wmatrix;
}

vector<int> PathResampling::ComputeAssignment(const vector<vector<float>>& wmatrix){
    const int size = wmatrix.size();
    dlib::matrix<int> cost(size, size);
    for(int row = 0; row < size; row ++){
        for(int col = 0; col < size; col ++){
            //cost(row, col) = std::numeric_limits<int>::max() - wmatrix[row][col] * 10e5;
            cost(row, col) = (1.0 - wmatrix[row][col]) * 10e6;
        }
    }
    std::vector<long> assignment = dlib::max_cost_assignment(cost);
    cout << "The optimized solution is: " << endl;
    PrintVector<long>(assignment);
    std::vector<int> toReturn(assignment.size());
    for(int i = 0; i < assignment.size(); i++){
        toReturn[i] = assignment[i];
    }
    return toReturn;
}

vector<double> PathResampling::ComputeSimplicialDepthValues(const vector<Vector2d>& points){
    vector<double> res = PathCBD::fastSegmentSimplexDepth3(points);
    // Normalizes to [0, 1] using the ranks
    vector<int> sortedindices = PathCBD::sortSegmentIdx(res);
    for(int j = 0; j < sortedindices.size(); j++){
        res[sortedindices[j]] = 1.0 - j * 1.0 / sortedindices.size();
    }
    return res;
}

CenterLine PathResampling::FindSDCenterLine(const vector<vector<Vector2d>>& points,
                                            const vector<vector<double>>& sdvals){
    assert(points.size() == sdvals.size());
    int n = points.size();
    CenterLine res;
    for(int i = 0; i < n; i++){
        // If there is not point at a particular hour,
        // we skip it.
        if(points.at(i).empty()){
            continue;
        }
        // Needs to get the sorted indices of SD values
        vector<int> sortedindices = PathCBD::sortSegmentIdx(sdvals[i]);
        int centeridx = sortedindices[0];
        res.push_back(points[i][centeridx]);
    }
    //DrawSDCenterLine(res);
    return res;
}

vector<Vector2d> PathResampling::ComputePerpendicularDirsOfSegOfCenterLine(const CenterLine& centerline){
    vector<Vector2d> res;
    for(int i = 0; i < centerline.size() - 1; i++){
        Vector2d p0 = centerline[i];
        Vector2d p1 = centerline[i+1];
        Vector2d dir = p1 - p0;
        dir = dir / dir.norm();
        double degree = DegToRad(90);
        Matrix2x2 rm(cos(degree), sin(-degree), sin(degree), cos(degree));
        dir = dir * rm;
        res.push_back(dir);
        // Duplicates the last one
        if(i == centerline.size() - 2){
            res.push_back(dir);
        }
    }
    return res;
}

void PathResampling::ComputeLeftRightRegionsOfSegments(const CenterLine& centerline,
                                                       const vector<Vector2d>& pdirs,
                                                       vector<vector<Vector2d>>* leftregions,
                                                       vector<vector<Vector2d>>* rightregions){
    leftregions->clear();
    rightregions->clear();
    leftregions->resize(centerline.size() - 1);
    rightregions->resize(centerline.size() - 1);
    float longdistance = 1000.0;
    for(int i = 0; i < centerline.size() - 1; i++){
        // Computes left region
        Vector2d p0 = centerline[i];
        Vector2d p1 = centerline[i+1];
        Vector2d p2 = p0 + pdirs[i] * longdistance;
        Vector2d p3 = p1 + pdirs[i+1] * longdistance;
        leftregions->at(i).push_back(p0);
        leftregions->at(i).push_back(p1);
        leftregions->at(i).push_back(p2);
        leftregions->at(i).push_back(p3);
        // Computes right region
        Vector2d p4 = p0 - pdirs[i] * longdistance;
        Vector2d p5 = p1 - pdirs[i+1] * longdistance;
        rightregions->at(i).push_back(p0);
        rightregions->at(i).push_back(p1);
        rightregions->at(i).push_back(p4);
        rightregions->at(i).push_back(p5);
    }
}


bool PathResampling::IsInRegion(const vector<Vector2d>& region,
                                const Vector2d& p){
    // Divides each rectangle into two triangles and check the barycentric coordinates
    Vector2d v0 = region[0];
    Vector2d v1 = region[1];
    Vector2d v2 = region[2];
    Vector2d v3 = region[3];
    Vector2d q = p;
    Vector3d ws0 = CDFSample::BaryCentricCoor(q, v0, v1, v2);
    Vector3d ws1 = CDFSample::BaryCentricCoor(q, v1, v2, v3);
    if(CDFSample::isInside(ws0) || CDFSample::isInside(ws1)){
        return true;
    }
    return false;
}

bool PathResampling::IsInRegions(const vector<vector<Vector2d>>& regions,
                                 const Vector2d& p){
    bool isinregions = false;
    for(const auto region : regions){
        // each region is represented by a rectangle
        assert(region.size() == 4);
        bool flag = IsInRegion(region, p);
        isinregions = (isinregions || flag);
    }
    return isinregions;
}

bool PathResampling::IsInPartition(const Vector2d& p, const Partition& partition){
    return IsInRegions(partition, p);
}

CenterLine PathResampling::CreateDummyCenterline(const CenterLine& centerline){
    CenterLine res = centerline;
    Vector2d p = centerline[centerline.size() - 2];
    Vector2d q = centerline[centerline.size() - 1];
    Vector2d u = q - p;
    u = u / u.norm();
    Vector2d dummypoint = q + 10 * u;
    res.push_back(dummypoint);
    return res;
}

int PathResampling::LeftRightOfSegment(const Vector2d& v0, const Vector2d& v1, const Vector2d& p){
    int leftright = 0;
    Vector2d e0 = v0 - p;
    Vector3d e03d(e0.x, e0.y, 0);
    Vector2d e1 = v1 - p;
    Vector3d e13d(e1.x, e1.y, 0);
    Vector3d c = e03d % e13d;
    if(c.z < 0){
        return -1;
    } else if(c.z > 0){
        return 1;
    } else {
        return 0;
    }
}

int PathResampling::LeftRightOfCenterlineGlobal(const CenterLine& centerline, const Vector2d& p){
    int leftright = 0;
    for(int i = 1; i < centerline.size() - 1; i++){
        leftright += LeftRightOfCenterline(centerline, p, i);
    }
    if(leftright < 0){
        return -1;
    } else if(leftright > 0){
        return 1;
    }
}

int PathResampling::LeftRightOfCenterline(const CenterLine& centerline, const Vector2d& p, const int time){
    assert(numofTimeSteps > time);
    assert(time >= 1);
    Vector2d dir = centerline[time + 1] - centerline[time - 1];
    dir = dir / dir.norm();
    Vector2d q0 = centerline[time];
    Vector2d q1 = q0 + dir * 20;
    Vector2d pq0 = q0 - p;
    Vector2d pq1 = q1 - p;
    Vector3d u = pq0 % pq1;
    if(u.z < 0){
        return -1;
    } else {
        return 1;
    }
}

int PathResampling::LeftRightOfCenterline2(const CenterLine& centerline, const Vector2d& p, const int time,
                                           const int window){
    assert(centerline.size() > time + window + 1);
    assert(time - window - 1 >= 0);
    assert(!boundingboxes.empty());
    // Finds bounding box
    QRectF bbox = *boundingboxes.begin();
    // Find in and out directions
    Vector2d in = centerline.at(time - window + 1) - centerline.at(time - window -1);
    in = in / in.norm();
    Vector2d out = centerline.at(time + window + 1) - centerline.at(time + window - 1);
    out = out / out.norm();
    // Constructs two lines
    QLineF linein(
                QPointF(centerline.at(time - window).x,        centerline.at(time - window).y),
                QPointF(centerline.at(time - window).x + in.x, centerline.at(time - window).y + in.y));
    QLineF lineout(
                QPointF(centerline.at(time + window).x,         centerline.at(time + window).y),
                QPointF(centerline.at(time + window).x + out.x, centerline.at(time + window).y + out.y));
    // Computes angle between these two lines
    qreal angle = linein.angleTo(lineout);
    // Extends lines a certain amount to check intersection type
    linein.setLength(100);
    lineout.setLength(100);
    QPointF s;
    QLineF::IntersectType intersecttype = linein.intersect(lineout, &s);
    //if(theta <= 90 && intersect){
    //}


    return 1;
}

QPolygon PathResampling::ConstructLeftRightRegionsOfCenterline(const CenterLine& centerline){
    assert(centerline.size() >=2);
    QPolygonF left;
    //testpoints.clear();
    Vector2d indir = centerline[1] - centerline[0];
    indir = indir / indir.norm();
    //testcenterline.clear();
    //testcenterline.push_back(centerline[0]);
    //testcenterline.push_back(centerline[0] - 100 * indir);
    Vector2d outdir = centerline[centerline.size() - 1] - centerline[centerline.size() - 2];
    outdir = outdir / outdir.norm();
    QLineF in(
                QPointF(centerline.at(0).x, centerline.at(0).y),
                QPointF(centerline.at(1).x, centerline.at(1).y));
    QLineF out(
                QPointF(centerline.at(centerline.size() - 2).x, centerline.at(centerline.size() - 2).y),
                QPointF(centerline.at(centerline.size() - 1).x, centerline.at(centerline.size() - 1).y));
    //testpoints.push_back(Vector2d(out.p1().x(), out.p1().y()));
    // Find intersection of in line and bbox
    QRectF bbox = *boundingboxes.begin();
    QPolygonF bpolygon(bbox);
    unsigned int inindex = 0;
    unsigned int outindex = 0;
    QPointF infinal, outfinal;
    for(unsigned int i = 0; i < bpolygon.size(); i++){
        unsigned int j = (i + 1) % bpolygon.size();
        QLineF edge(bpolygon.at(i), bpolygon.at(j));
        QPointF ins, outs;
        in.intersect(edge, &ins);
        out.intersect(edge, &outs);
        //testpoints.push_back(Vector2d(ins.x(), ins.y()));
        //testpoints.push_back(Vector2d(outs.x(), outs.y()));
        ins.setX(ins.x() + indir.x * 0.05);
        ins.setY(ins.y() + indir.y * 0.05);
        //if(bbox.contains(ins)){
        if(bpolygon.containsPoint(ins, Qt::OddEvenFill)){
            Vector2d u(ins.x() - centerline[0].x, ins.y() - centerline[0].y);
            u = u / u.norm();
            float theta = indir * u;
            //cout << "in theta : " << theta << endl;
            if(theta == -1){
                //testpoints.push_back(Vector2d(ins.x(), ins.y()));
                inindex = j;
                infinal = ins;
            }
        }
        outs.setX(outs.x() - outdir.x * 0.05);
        outs.setY(outs.y() - outdir.y * 0.05);
        //if(bbox.contains(outs)){
        if(bpolygon.containsPoint(outs, Qt::OddEvenFill)){
            Vector2d u(outs.x() - centerline.at(centerline.size() - 1).x, outs.y() -
                       centerline.at(centerline.size() - 1).y);
            u = u / u.norm();
            float theta = outdir * u;
            //cout << "out theta : " << theta << endl;
            if(theta == 1 ){
                //testpoints.push_back(Vector2d(outs.x(), outs.y()));
                outindex = i;
                outfinal = outs;
            }
        }
    }
    //cout << "------" << endl;

    // Checks whether the out line intersects the centerline or not
    for(int i = 0; i < centerline.size() - 2; i++){
        Vector2d p = centerline.at(i);
        Vector2d q = centerline.at(i+1);
        QPointF qp(p.x, p.y);
        QPointF qq(q.x, q.y);
        QLineF seg(qp, qq);
        QPointF s;
        QLineF extendedout = out;
        extendedout.setLength(1000);
        //testcenterline.push_back(Vector2d(extendedout.p1().x(), extendedout.p1().y()));
        //testcenterline.push_back(Vector2d(extendedout.p2().x(), extendedout.p2().y()));
        QLineF::IntersectType stype = extendedout.intersect(seg, &s);
        if(stype == QLineF::UnboundedIntersection){
            continue;
        }
        Vector2d u(s.x() - out.p2().x(), s.y() - out.p2().y());
        u = u / u.norm();
        float theta = outdir * u;
        if(theta == 1 && bpolygon.containsPoint(s, Qt::OddEvenFill) &&
                LeftRightOfSegment(p, q, Vector2d(out.p1().x(), out.p1().y()) == -1)){
            for(int j = i+1; j < centerline.size(); j++){
                QPointF m(centerline.at(j).x,  centerline.at(j).y);
                left.push_back(m);
                testpoints.push_back(centerline.at(j));
            }
            left.push_back(s);
            testpoints.push_back(Vector2d(s.x(), s.y()));
            return left.toPolygon();
        }

    }

    // Constructs left polygon
    left.push_back(infinal);
    for(unsigned int i = inindex;;i++){
        i = i % bpolygon.size();
        QPointF p = bpolygon.at(i);
        left.push_back(p);
        if(i == outindex){
            break;
        }
    }
    left.push_back(outfinal);
    for(unsigned int i = centerline.size() - 1; i >= 1; i--){
        QPoint p(centerline[i].x, centerline[i].y);
        left.push_back(p);
    }


    for(unsigned int i = 0; i < left.size(); i++){
        QPointF p = left.at(i);
        //testpoints.push_back(Vector2d(p.x(), p.y()));
    }

    return left.toPolygon();

    //QLineF bboxbottom(bbox.bottomLeft(), bbox.bottomRight());
    //testpoints.push_back(Vector2d(bbox.bottomLeft().x(), bbox.bottomLeft().y()));
    //testpoints.push_back(Vector2d(bbox.bottomRight().x(), bbox.bottomRight().y()));
    //QPointF inbboxintersection;
    //in.intersect(bboxbottom, &inbboxintersection);
    //testpoints.push_back(Vector2d(inbboxintersection.x(), inbboxintersection.y()));
    //cout << bbox.contains(inbboxintersection) << endl;

}



QPolygon PathResampling::ConstructLeftRightRegionsOfCenterline(const CenterLine& centerline,
                                                               const int timewindow, const int currenttime){
    //testpoints.clear();
    //testcenterline.clear();
    assert(centerline.size() >=2);
    QPolygonF left;
    // (p0,p1) is the first segment
    int idxp = currenttime - timewindow;
    idxp = idxp < 0 ? 0 : idxp;
    Vector2d p0 = centerline[idxp];
    Vector2d p1 = centerline[idxp+1];
    // (q0, q1) is the last segment
    int idxq = currenttime + timewindow;
    idxq = idxq > centerline.size() - 1 ? centerline.size() - 1 : idxq;
    Vector2d q0 = centerline[idxq - 1];
    Vector2d q1 = centerline[idxq];
    // Computes in and out directions
    Vector2d indir = p1 - p0;
    indir = indir / indir.norm();
    Vector2d outdir = q1 - q0;
    outdir = outdir / outdir.norm();
    // Transfers in and out segments into QLineF formats
    QLineF in(QPointF(p0.x, p0.y), QPointF(p1.x, p1.y));
    QLineF out(QPointF(q0.x, q0.y), QPointF(q1.x, q1.y));
    //testpoints.push_back(Vector2d(out.p1().x(), out.p1().y()));
    // Find intersection of in line and bbox
    QRectF bbox = *boundingboxes.begin();
    QPolygonF bpolygon(bbox);
    unsigned int inindex = 0;
    unsigned int outindex = 0;
    QPointF infinal, outfinal;
    for(unsigned int i = 0; i < bpolygon.size(); i++){
        unsigned int j = (i + 1) % bpolygon.size();
        QLineF edge(bpolygon.at(i), bpolygon.at(j));
        // intersection of in(out) segment and an edge of bounding box
        QPointF ins, outs;
        in.intersect(edge, &ins);
        out.intersect(edge, &outs);
        //testpoints.push_back(Vector2d(ins.x(), ins.y()));
        //testpoints.push_back(Vector2d(outs.x(), outs.y()));
        // Adds a small offset to make sure the contains function works correctly
        ins.setX(ins.x() + indir.x * 0.5);
        ins.setY(ins.y() + indir.y * 0.5);
        if(bpolygon.containsPoint(ins, Qt::OddEvenFill)){
            // Make sure to find the intersection on the opposite direction of the in direction
            Vector2d u(ins.x() - p0.x, ins.y() - p0.y);
            u = u / u.norm();
            float theta = indir * u;
            //cout << "in theta : " << theta << endl;
            if(theta == -1){
                //testpoints.push_back(Vector2d(ins.x(), ins.y()));
                inindex = j;
                infinal = ins;
            }
        }
        outs.setX(outs.x() - outdir.x * 0.5);
        outs.setY(outs.y() - outdir.y * 0.5);
        if(bpolygon.containsPoint(outs, Qt::OddEvenFill)){
            // Make sure to find the intersection on the same direction of the out direction
            Vector2d u(outs.x() - q1.x, outs.y() - q1.y);
            u = u / u.norm();
            float theta = outdir * u;
            //cout << "out theta : " << theta << endl;
            if(theta == 1 ){
                //testpoints.push_back(Vector2d(outs.x(), outs.y()));
                outindex = i;
                outfinal = outs;
            }
        }
    }
    //cout << "------" << endl;

    // Checks whether the out line intersects the centerline or not
    for(int i = idxp; i < idxq - 1; i++){
        Vector2d p = centerline.at(i);
        Vector2d q = centerline.at(i+1);
        QPointF qp(p.x, p.y);
        QPointF qq(q.x, q.y);
        QLineF seg(qp, qq);
        QPointF s;
        QLineF extendedout = out;
        extendedout.setLength(1000);
        //testcenterline.push_back(Vector2d(extendedout.p1().x(), extendedout.p1().y()));
        //testcenterline.push_back(Vector2d(extendedout.p2().x(), extendedout.p2().y()));
        QLineF::IntersectType stype = extendedout.intersect(seg, &s);
        if(stype == QLineF::UnboundedIntersection){
            continue;
        }
        Vector2d u(s.x() - out.p2().x(), s.y() - out.p2().y());
        u = u / u.norm();
        float theta = outdir * u;
        if(theta == 1 && bpolygon.containsPoint(s, Qt::OddEvenFill) &&
                LeftRightOfSegment(p, q, Vector2d(out.p1().x(), out.p1().y()) == -1)){
            for(int j = i+1; j < idxq; j++){
                QPointF m(centerline.at(j).x,  centerline.at(j).y);
                left.push_back(m);
                testpoints.push_back(centerline.at(j));
            }
            left.push_back(s);
            testpoints.push_back(Vector2d(s.x(), s.y()));
            return left.toPolygon();
        }

    }


    // Constructs left polygon
    left.push_back(infinal);
    for(unsigned int i = inindex;;i++){
        i = i % bpolygon.size();
        QPointF p = bpolygon.at(i);
        left.push_back(p);
        if(i == outindex){
            break;
        }
    }
    left.push_back(outfinal);
    for(int i = idxq; i >= idxp; i--){
        QPoint p(centerline[i].x, centerline[i].y);
        left.push_back(p);
    }


    for(unsigned int i = 0; i < left.size(); i++){
        QPointF p = left.at(i);
        //testpoints.push_back(Vector2d(p.x(), p.y()));
    }

    return left.toPolygon();

    //QLineF bboxbottom(bbox.bottomLeft(), bbox.bottomRight());
    //testpoints.push_back(Vector2d(bbox.bottomLeft().x(), bbox.bottomLeft().y()));
    //testpoints.push_back(Vector2d(bbox.bottomRight().x(), bbox.bottomRight().y()));
    //QPointF inbboxintersection;
    //in.intersect(bboxbottom, &inbboxintersection);
    //testpoints.push_back(Vector2d(inbboxintersection.x(), inbboxintersection.y()));
    //cout << bbox.contains(inbboxintersection) << endl;

}

int PathResampling::LeftRightOfCenterline(const CenterLine& centerline, const Vector2d& p, const int time,
                                          const int window){
    assert(centerline.size() > time + window + 1);
    assert(time - window - 1 >= 0);
    int leftright = 0;
    for(int i = time - window - 1; i < time + window + 1; i++){
        Vector2d dir = centerline[i + 1] - centerline[i - 1];
        dir = dir / dir.norm();
        Vector2d q0 = centerline[i];
        Vector2d q1 = q0 + dir * 20;
        Vector2d pq0 = q0 - p;
        Vector2d pq1 = q1 - p;
        Vector3d u = pq0 % pq1;
        if(u.z < 0){
            leftright -= 1;
        } else {
            leftright += 1;
        }
    }
    if(leftright < 0){
        return -1;
    } else if(leftright > 0){
        return 1;
    }

}

int PathResampling::LeftRightOfCenterline(const CenterLine& centerline, const Vector2d& p){
    if(centerline.size() == 0){
        cerr << "This is no valid centerline." << endl;
        exit(0);
    }
    int leftright = 0;
    // Check left or right of individual segments
    for(int i = 0; i < centerline.size() - 1; i++){
        Vector2d v0 = centerline[i];
        Vector2d v1 = centerline[i+1];
        leftright += LeftRightOfSegment(v0, v1, p);
    }
    // If more left than right returns left
    if(leftright > 0){
        return -1;
    } else if (leftright < 0){
        return 1;
    }
}

CenterLine PathResampling::DividePointsIntoLeftRight(const vector<vector<Vector2d>>& points,
                                                     const CenterLine& centerline,
                                                     vector<vector<Vector2d>>* leftpoints, vector<vector<Vector2d>>* rightpoints){
    leftpoints->clear();
    rightpoints->clear();
    leftpoints->resize(points.size());
    rightpoints->resize(points.size());
    // Return if centerline is empty
    if(centerline.empty()){
        cerr << "There is no valid centerline." << endl;
        exit(0);
    }
    // Finds perpendicular directions of segments of the centerline
    // Here we need to create a dummy node to extent the center line to include all possible points
    //CenterLine dummycenterline = CreateDummyCenterline(centerline);
    // Simplifies centerline to remove noises
    //CenterLine dummycenterline = RadialDistanceSimplifier(centerline, 40.0f);
    //CenterLine dummycenterline = DouglasPeuckerSimplifier(centerline, 10);
    //CenterLine dummycenterline = ReumannWitkamSimplifier(centerline, 5);
    //CenterLine dummycenterline = PerpendicularDistanceSimplifier(centerline, 10, 2);
    //CenterLine dummycenterline =  OpheimSimplifier(centerline, 10.0f, 30.0f);
    CenterLine dummycenterline = LangSimplifier(centerline, 40.0f, 7);
    dummycenterline = RemoveSelfLoop(dummycenterline);
    testcenterlines2.push_back(centerline);
    // Fits centerline to b spline curve to obtain correct number of points
    //vector<Vector2d> intersections;
    //if(IsSelfIntersect(centerline, &intersections)){
    // testcenterlines.push_back(dummycenterline);
    // testcenterlines2.push_back(centerline);
    //}
    dummycenterline.insert(dummycenterline.begin(), *dummycenterline.begin());
    //dummycenterline.push_back(dummycenterline.back());
    dummycenterline = BSplineCurve(dummycenterline, numofTimeSteps);
    testcenterlines.push_back(dummycenterline);
    // Adds the first point to spline
    //dummycenterline.insert(dummycenterline.begin(), centerline[0]);
    //dummycenterline = CreateDummyCenterline(dummycenterline);
    //vector<Vector2d> pdirs = ComputePerpendicularDirsOfSegOfCenterLine(dummycenterline);
    //vector<vector<Vector2d>> leftregions, rightregions;
    //ComputeLeftRightRegionsOfSegments(dummycenterline, pdirs, &leftregions, &rightregions);
    //// Groups points
    //for(int i = 0; i < points.size(); i++){
    // if(points.at(i).empty()){
    //  continue;
    // }
    // for(const auto& p : points[i]){
    //  if(IsInRegions(leftregions, p)){
    //   leftpoints->at(i).push_back(p);
    //  } else if(IsInRegions(rightregions, p)){
    //   rightpoints->at(i).push_back(p);
    //  }
    // }
    //}

    //cout << centerline.size() << " " << dummycenterline.size() << " " << points.size() << endl;
    //for(int i = 1; i < points.size(); i++){
    // if(points.at(i).empty()){
    //  continue;
    // }
    // for(const auto& p : points[i]){
    //  //int leftright = (i - 1 - leftrightWindow >=0) && (i + 1 + leftrightWindow < dummycenterline.size()) ?
    //  // LeftRightOfCenterline(dummycenterline, p, i, leftrightWindow) :
    //  // LeftRightOfCenterline(dummycenterline, p, i);
    //  int leftright = LeftRightOfCenterline(dummycenterline, p, i);
    //  //int leftright = LeftRightOfCenterlineGlobal(dummycenterline, p);
    //  if(leftright <= 0){
    //   leftpoints->at(i).push_back(p);
    //  } else if(leftright > 0){
    //   rightpoints->at(i).push_back(p);
    //  }
    // }
    //}

    //for(int i = 0; i < points.size(); i++){
    // for(const auto& p : points[i]){
    //  int leftright = LeftRightOfSegment(dummycenterline[i], dummycenterline[i+1], p);
    //  if(leftright > 0){
    //   leftpoints->at(i).push_back(p);
    //  } else if (leftright < 0){
    //   rightpoints->at(i).push_back(p);
    //  }
    // }
    //}


    //QPolygon leftpolygon = ConstructLeftRightRegionsOfCenterline(dummycenterline, 70, 30);
    for(int i = 1; i < points.size(); i++){
        QPolygon leftpolygon = ConstructLeftRightRegionsOfCenterline(dummycenterline, i, leftrightWindow);
        for(const auto& p : points[i]){
            QPoint qp(p.x, p.y);
            if(leftpolygon.containsPoint(qp, Qt::WindingFill)){
                leftpoints->at(i).push_back(p);
            } else {
                rightpoints->at(i).push_back(p);
            }
        }
    }

    //for(int i = 1; i < points.size(); i++){
    // if(!leftpoints->at(i).empty() && rightpoints->at(i).empty()){
    //  size_t n = leftpoints->at(i).size();
    //  if(n > 10 && i > 20 && i < 50){
    //   //if(testcenterline.empty() == false){
    //   // continue;
    //   //}
    //   testcenterline = dummycenterline;
    //   testcenterline2 = centerline;
    //   testpoints2 = leftpoints->at(i);
    //  }
    // }
    //}


    return dummycenterline;
}

int PathResampling::RecursivelyComputeSimplicialDepthCenterlines(CenterlineNode **currentnode, std::queue<std::vector<std::vector<Vector2d>>> *isVisited, vector<CenterLine>* centerlines, vector<vector<double>>* simplicialDepthValues, const int level, const int nlevels, const vector<vector<Vector2d>>& points){
    // Return if we obtained enough centerlines
    //if(centerlines->size() == ncenterlines){
    // return;
    //}
    if(level >= nlevels && numofRecLevels <= 10){
        return level + 1;
    }
    if(level >= nlevels && numofRecLevels > 10){
        vector<vector<Vector2d>> tmp = points;
        int counter = 0;
        CenterLine outliner;
        while(counter != points.size() - 1){
            outliner.clear();
            counter = 0;
            for(int i = 0; i < tmp.size(); i++){
                if(tmp.at(i).empty()){
                    counter++;
                    continue;
                }
                outliner.push_back(tmp.at(i).back());
                tmp.at(i).pop_back();
            }
            centerlines->push_back(outliner);
        }

        return level + 1;
    }
    if(points.empty()){
        cerr << "The sets of points is empty." << endl;
        exit(0);
    }
    // if more than 60% of points sets are empty, return
    int nemptysets = 0;
    for(const auto& set : points){
        nemptysets = set.empty() ? nemptysets + 1 : nemptysets;
    }
    if(nemptysets >= 0.3 * points.size()){
        return level + 1;
    }
    // Computes simplicial depth values of points at individual time steps
    //cout << "compute simplicial depths ..." << endl;
    for(int i = 0; i < points.size(); i++){
        if(points.at(i).empty()){
            //cerr << "The set of points at time step " << i << " at level " << level << " is empty." << endl;
            continue;
            //exit(0);
        }
        simplicialDepthValues->at(i) = ComputeSimplicialDepthValues(points[i]);
    }
    // Finds a center line and stores it in the list
    //cout << "find SD centerline" << endl;
    CenterLine centerline = FindSDCenterLine(points, *simplicialDepthValues);
    centerline.insert(centerline.begin(), points.at(0).at(0));
    //centerlines->push_back(centerline);
    // Divides points into the left group and the right group based on the centerline
    //cout << "divide points into left and right" << endl;
    vector<vector<Vector2d>> left, right;
    CenterLine dummycenterline = DividePointsIntoLeftRight(points, centerline, &left, &right);
    //cout << "level " << level << "; loc: " << dummycenterline.at(_timepoint) << endl;
    centerlines->push_back(dummycenterline);
    *currentnode = new CenterlineNode(centerlines->size() - 1);
    // The left and right at time 0 is identical
    left.at(0) = points.at(0);
    right.at(0) = points.at(0);
    //if(level == numofRecLevels - 1){
    // testleft = left;
    // testright = right;
    // //testcenterline = dummycenterline;
    // //testcenterlines.push_back(dummycenterline);
    //}

    // Recursively compute SD centerlines of the left and right groups
    //isVisited->push(left);
    //isVisited->push(right);
    RecursivelyComputeSimplicialDepthCenterlines(&((*currentnode)->left), isVisited, centerlines, simplicialDepthValues, level + 1,  nlevels, left);
    RecursivelyComputeSimplicialDepthCenterlines(&((*currentnode)->right), isVisited, centerlines, simplicialDepthValues, level + 1,  nlevels, right);

    return level + 1;
}

std::vector<CenterLine> PathResampling::ExtractCenterlineSubset(const int subsetsize)
{
    vector<CenterLine> subset;
    queue<CenterlineNode*> isVisited;
    if(_centerlineroot == NULL){
        cerr << "center line root is NULL" << endl;
        exit(-1);
    }
    isVisited.push(_centerlineroot);
    while(!isVisited.empty()){
        CenterlineNode* currnode = isVisited.front();
        isVisited.pop();
        int idx = currnode->_id;
        //cout << "idx :" << idx << endl;
        subset.push_back(SDCenterLines.at(idx));
        if(currnode->left != NULL){
            isVisited.push(currnode->left);
        }
        if(currnode->right != NULL){
            isVisited.push(currnode->right);
        }
        if(subset.size() == subsetsize){
            return subset;
        }
    }
    return subset;
}

vector<pair<int, float>> PathResampling::SortCenterlines(
        const vector<CenterLine>& centerlines){
    // Finds the most center centerline
    CenterLine cl = centerlines[0];
    // Finds a reference time point, e.g. 3
    const int reftimepoint = 3;
    // Finds a reference segment represented by 2 2D points
    vector<Vector2d> seg;
    seg.push_back(cl[reftimepoint]);
    seg.push_back(cl[reftimepoint + 1]);
    vector<pair<int, float>> indexdistancepairs(centerlines.size());
    for(int i = 0; i < centerlines.size(); i++){
        indexdistancepairs[i].first = i;
        indexdistancepairs[i].second =
                SignedPoint2LineDistance(centerlines[i][reftimepoint],seg[0], seg[1]);
    }
    sort(indexdistancepairs.begin(), indexdistancepairs.end(),
         [&](const pair<int, float>& p0, const pair<int, float>& p1){
        return p0.second < p1.second;
    });
    return indexdistancepairs;
}

vector<Partition> PathResampling::ComputePartitions(
        const vector<CenterLine>& centerlines,
        const vector<pair<int, float>>& sortedindices){
    vector<Partition> partitions;
    // Loops through centerlines to find individual partitions
    for(int i = 0; i < sortedindices.size() - 1; i++){
        Partition partition;
        CenterLine l0 = centerlines[sortedindices[i].first];
        CenterLine l1 = centerlines[sortedindices[i+1].first];
        assert(l0.size() == l1.size());
        // Find subregions
        for(int j = 0; j < l0.size() - 1; j++){
            SubRegion subregion;
            subregion.push_back(l0[j]);
            subregion.push_back(l0[j+1]);
            subregion.push_back(l1[j]);
            subregion.push_back(l1[j+1]);
            partition.push_back(subregion);
            //DrawRegion(subregion);
        }
        partitions.push_back(partition);
    }
    return partitions;
}

vector<vector<vector<Vector2d>>> PathResampling::GroupPoints2Partitions(
        const vector<vector<Vector2d>>& points,
        const vector<Partition>& partitions){
    vector<vector<vector<Vector2d>>> groups(partitions.size());
    // Each partition has a group
    for(int i = 0; i < partitions.size(); i++){
        // Each group has points at all different time steps
        vector<vector<Vector2d>> group(points.size());
        for(int j = 0; j < points.size(); j++){
            // Test points time step by time step
            for(const auto& p : points[j]){
                if(IsInPartition(p, partitions[i])){
                    group[j].push_back(p);
                }
            }
        }
        groups[i] = group;
        //glColor4f(0, 0, 1, 1);
        //DrawPointsGroup(groups[i]);
    }
    return groups;
}

Vector2d PathResampling::Deboor(const int k, const int degree, const int i, const float u,
                                const float knots[], Vector2d* coeff){
    if(k == 0)
        return coeff[i];
    else{
        float t = (u - knots[i]) / (knots[i+degree+1-k]-knots[i]);
        if(knots[i+degree+1-k]-knots[i] < 10e-4)
            t = 0;
        return Deboor(k-1, degree, i-1, u, knots, coeff) * (1 - t)
                + Deboor(k-1, degree, i, u, knots, coeff) * t;
    }
}

CenterLine PathResampling::BSplineCurve(const std::vector<Vector2d>& controlPolygons,
                                        const int smoothiness){
    int N = controlPolygons.size();
    int n = N - 1; //index
    int D = 2; //degree, in this example , degree = 3, cubic
    int K = D;
    int KnotsNum = N+D+1;
    float knots[KnotsNum];

    Vector2d *coeff = new Vector2d[N];
    for(int i = 0; i < N; i++){
        coeff[i] = controlPolygons[i];
    }

    for(int i = 0 ; i < KnotsNum; i++){
        if(i <= K)
            knots[i] = 0.0;
        else if(i > K && i < N)
            knots[i] = i - D;
        else if(i >= N && i <= N + D)
            knots[i] = n - D + 1;
    }

    //cout << N << " " << KnotsNum << " " << D << endl;
    //for(int i = 0; i < KnotsNum; i++){
    // cout << knots[i] << " ";
    //}
    //cout << endl;
    //exit(0);

    int upper = N - D; //interval = [0, upper]
    int samplenum = smoothiness + 2;

    CenterLine res;
    for(int j = 1; j < samplenum-1; j++){
        float u = 1.0 * upper / samplenum * j;

        float interval = 0;
        for(int k = 0; k < KnotsNum; k++){
            if(knots[k] > u){
                interval = k - 1;
                break;
            }
            else if(knots[k] == u){
                interval = k;
                break;
            }
        }
        Vector2d point = Deboor(K, K, interval, u, knots, coeff);
        res.push_back(point);
    }
    return res;
}

CenterLine PathResampling::DouglasPeuckerSimplifier(const CenterLine& cl, const float tolerance){
    // Re-store centerline
    deque<double> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    double* result = new double[polyline.size()];
    for(int i = 0; i < polyline.size(); i++){
        result[i] = -1.0f;
    }
    // Simplifies the 2d polyline
    psimpl::simplify_douglas_peucker<2>(polyline.begin(), polyline.end(), tolerance, result);
    CenterLine res;
    for(int i = 0; i < polyline.size() - 1; i += 2){
        if(result[i] == -1.0f && result[i+1] == -1.0f){
            continue;
        }
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;

}

CenterLine PathResampling::DouglasPeuckerNSimplifier(const CenterLine& cl, const unsigned count){
    // Re-store centerline
    list<long> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    vector<double> result;
    // Simplifies the 2d polyline
    psimpl::simplify_douglas_peucker_n<2>(polyline.begin(), polyline.end(), count, back_inserter(result));
    CenterLine res;
    for(int i = 0; i < result.size() - 1; i += 2){
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;
}


CenterLine PathResampling::RadialDistanceSimplifier(const CenterLine& cl, const float tolerance){
    // Re-store centerline
    vector<double> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    vector<double> result;
    // Simplifies the 2d polyline
    psimpl::simplify_radial_distance<2>(polyline.begin(), polyline.end(), tolerance, back_inserter(result));
    CenterLine res;
    for(int i = 0; i < result.size() - 1; i += 2){
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;
}


CenterLine PathResampling::PerpendicularDistanceSimplifier(const CenterLine& cl, const float tolerance,
                                                           const unsigned repeat){
    // Re-store centerline
    deque<double> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    deque<double> result;
    psimpl::simplify_perpendicular_distance<2>(polyline.begin(), polyline.end(), tolerance, repeat,
                                               back_inserter(result));
    CenterLine res;
    for(int i = 0; i < result.size() - 1; i += 2){
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;
}

CenterLine PathResampling::OpheimSimplifier(const CenterLine& cl, const float minimum, const float maximum){
    // Re-store centerline
    vector<double> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    vector<double> result;
    // Simplifies the 2d polyline
    psimpl::simplify_opheim<2>(polyline.begin(), polyline.end(),
                               minimum, maximum, back_inserter(result));
    CenterLine res;
    for(int i = 0; i < result.size() - 1; i += 2){
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;
}

CenterLine PathResampling::LangSimplifier(const CenterLine& cl, const float tolerance,
                                          const unsigned lookahead){
    vector<float> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    vector<double> result;
    psimpl::simplify_lang<2>(polyline.begin(), polyline.end(), tolerance, lookahead, back_inserter(result));
    CenterLine res;
    for(int i = 0; i < result.size() - 1; i += 2){
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;
}


CenterLine PathResampling::ReumannWitkamSimplifier(const CenterLine& cl, const float tolerance){
    // Re-store centerline
    vector<float> polyline;
    for(const auto& p : cl){
        polyline.push_back(p.x);
        polyline.push_back(p.y);
    }
    vector<double> result;
    // Simplifies the 2d polyline
    psimpl::simplify_reumann_witkam<2>(polyline.begin(), polyline.end(), tolerance, back_inserter(result));
    CenterLine res;
    for(int i = 0; i < result.size() - 1; i += 2){
        res.push_back(Vector2d(result[i], result[i+1]));
    }
    return res;
}

// Simplifies a centerline
// typeofalgorithm:
//  0 = do not apply any simplifications
//  1 = Reumann Witkam
//  2 = Lang
//  3 = Opheim
//  4 = Perpendicular distance
//  5 = Radial distance
//  6 = Douglas-Peucker N
//  7 = Douglas-Peucker
//  8 = B spline curve
CenterLine PathResampling::SimplifyCenterline(const CenterLine& cl, const int typeofalgorithm){
    switch(typeofalgorithm){
    case 0:
        return cl;
    case 1:
        return ReumannWitkamSimplifier(cl, 10.0f);
    case 2:
        return LangSimplifier(cl, 30.0f, 20);
    case 3:
        return OpheimSimplifier(cl, 10.0f, 12.0f);
    case 4:
        return PerpendicularDistanceSimplifier(cl, 12.0f, 2);
    case 5:
        return RadialDistanceSimplifier(cl, 50.0f);
    case 6:
        return DouglasPeuckerNSimplifier(cl, 30);
    case 7:
        return DouglasPeuckerSimplifier(cl, 10.0f);
    case 8:
        return BSplineCurve(cl, numofTimeSteps);
    default:
        return cl;
    }
}

void PathResampling::SimplifyCenterlines(){
    for(auto& cl : SDCenterLines){
        cl = SimplifyCenterline(cl, AppliedSimplifyAlgorithm);
    }
}

vector<CenterLine> PathResampling::GenerateStructuredPathsByPartitingODSpace(){
    // Copy orignal points to avoid misoperation of the vector
    vector<vector<Vector2d>> predictedLocations = ReadFullEnsembleFromFiles();
    ComputeBoundingBoxes(predictedLocations);

    // Recursively computes a specified number of simplicial depth centerlines
    vector<CenterLine> centerlines;
    vector<vector<double>> simplicialdepthvalues(predictedLocations.size());
    //int level = -1;
    //int nlevels = pow(2, numofRecLevels);  // the maximum level we can reach
    int level = 0;
    int nlevels = numofRecLevels;
    centerlines.clear();
    queue<vector<vector<Vector2d>>> isVisited;
    _centerlineroot = new CenterlineNode(0);
    level = RecursivelyComputeSimplicialDepthCenterlines(&_centerlineroot, &isVisited, &centerlines, &simplicialdepthvalues, level, nlevels, predictedLocations);
    if(_centerlineroot == NULL){
        cerr << "center line root is NULL" << endl;
        exit(-1);
    }

    //isVisited.push(predictedLocations);
    //queue<CenterlineNode*> nodeQueue;
    //nodeQueue.push(_centerlineroot);
    //CenterlineNode* parent = NULL;
    //while(!isVisited.empty()){
    //    predictedLocations = isVisited.front();
    //    isVisited.pop();
    //    parent = nodeQueue.front();
    //    nodeQueue.pop();
    //    level = RecursivelyComputeSimplicialDepthCenterlines(parent, &isVisited, &centerlines, &simplicialdepthvalues, level, nlevels, predictedLocations);
    //}
    return centerlines;

    // Sorts centerlines topologically
    //vector<pair<int, float>> sortedindices = SortCenterlines(centerlines);

    //glBegin(GL_LINE_STRIP);
    //for(int i = 0; i < sortedindices.size(); i++){
    // int j = sortedindices[i].first;
    // glVertex3f(centerlines[j].at(3).x, centerlines[j].at(3).y, 0);
    //}
    //glEnd();


    // Computes partitions
    //vector<Partition> partitions = ComputePartitions(centerlines, sortedindices);
    // Draw partitions
    //for(const auto& p : partitions){
    // glColor4f(1, 0, 0, 1);
    // DrawPartition(p);
    //}

    // Read selected points from original ensemble
    //vector<vector<Vector2d>> selectedPoints;
    //string pointsfileprefix = OutputPrefix + "InternalData/PathSamplingData/OD-selected-%d.txt";
    //ReadPointsFromFiles(&selectedPoints, predictedLocations.size(), pointsfileprefix);
    //for(auto& l : selectedPoints){
    // for(auto& v : l){
    //  v = translateToScreen(v.x, v.y);
    // }
    //}
    //for(const auto& ps : selectedPoints){
    // glColor4f(1, 0, 1, 0.4);
    // DrawODPoints(ps);
    //}

    // Group selected points to individual partitions
    //PointsGroup groups = GroupPoints2Partitions(selectedPoints, partitions);
    //for(const auto& g : groups){
    // for(const auto& p : g){
    //  cout << p.size() << " ";
    //  glColor4f(1, 0, 1, 0.4);
    //  DrawODPoints(p);
    // }
    // cout << endl;
    //}
}

void PathResampling::ComputeBoundingBoxes(const vector<vector<Vector2d>>& points){
    boundingboxes.clear();
    float minx(numeric_limits<float>::max()), miny(numeric_limits<float>::max()),
            maxx(numeric_limits<float>::min()), maxy(numeric_limits<float>::min());
    for(unsigned int i = 1; i < points.size(); i++){
        for(const auto p : points[i]){
            minx = minx > p.x ? p.x : minx;
            miny = miny > p.y ? p.y : miny;
            maxx = maxx < p.x ? p.x : maxx;
            maxy = maxy < p.y ? p.y : maxy;
        }
    }
    // Adds offset to include points extractly on the bbox
    QRectF bbox(QPointF(minx - 20 , miny - 20), QPointF(maxx + 20, maxy + 20));
    boundingboxes.push_back(bbox);
}

Vector2d PathResampling::GetStartPos()
{
    if(SDCenterLinesShadow.empty()){
        return Vector2d(0, 0);
    } else {
        return SDCenterLinesShadow.at(0).at(0);
    }
}

void PathResampling::DrawBoundingBox(const int i){
    glBegin(GL_LINE_LOOP);
    Vector2d p = translateToScreen(boundingboxes[i].bottomLeft().x(), boundingboxes[i].bottomLeft().y());
    glVertex3f(p.x, p.y, 0);
    p = translateToScreen(boundingboxes[i].bottomRight().x(), boundingboxes[i].bottomRight().y());
    glVertex3f(p.x, p.y, 0);
    p = translateToScreen(boundingboxes[i].topRight().x(), boundingboxes[i].topRight().y());
    glVertex3f(p.x, p.y, 0);
    p = translateToScreen(boundingboxes[i].topLeft().x(), boundingboxes[i].topLeft().y());
    glVertex3f(p.x, p.y, 0);
    //glVertex3f(boundingboxes[i].bottomLeft().x(),  boundingboxes[i].bottomLeft().y(), 0);
    //glVertex3f(boundingboxes[i].bottomRight().x(), boundingboxes[i].bottomRight().y(), 0);
    //glVertex3f(boundingboxes[i].topRight().x(),    boundingboxes[i].topRight().y(), 0);
    //glVertex3f(boundingboxes[i].topLeft().x(),     boundingboxes[i].topLeft().y(), 0);
    glEnd();
}

void PathResampling::DrawCircle(float _x, float _y, float radius, const int interval){
    int angle;
    float x, y, angle_radians;
    float x1, y1;
    int counter = 0;
    float theta = 360.0f / (interval * 2.0f);
    for(angle = 0; angle <= 360; angle += 1.0f, counter++){
        angle_radians = angle*(double)M_PI/180.0;
        x = _x + radius*(double)cos(angle_radians);
        y = _y + radius*(double)sin(angle_radians);
        if(angle == 0){
            x1 = x;
            y1 = y;
        }

        if(interval != -1){
            if( (int(angle / theta) % 2) == 0){
                glLineWidth(3);
                glBegin(GL_LINES);
                glVertex3f(x,y,0.0);
                glVertex3f(x1,y1,0.0);
                glEnd();
            } else {
                glLineWidth(1);
                glBegin(GL_LINES);
                glVertex3f(x,y,0.0);
                glVertex3f(x1,y1,0.0);
                glEnd();
            }
        } else {
            glLineWidth(2);
            glBegin(GL_LINES);
            glVertex3f(x,y,0.0);
            glVertex3f(x1,y1,0.0);
            glEnd();
        }
        x1 = x;
        y1 = y;
    }
}

void PathResampling::DrawBoundingBoxes(){
    for(int i = 0; i < boundingboxes.size(); i++){
        DrawBoundingBox(i);
    }
}

bool PathResampling::IsSelfIntersect(const CenterLine& cl, vector<Vector2d>* intersections){
    intersections->clear();
    for(int i = 2; i < cl.size() - 1; i++){
        float p0_x = cl[i].x;
        float p0_y = cl[i].y;
        float p1_x = cl[i+1].x;
        float p1_y = cl[i+1].y;
        for(int j = 0; j < i - 1; j++){
            float p2_x = cl[j].x;
            float p2_y = cl[j].y;
            float p3_x = cl[j+1].x;
            float p3_y = cl[j+1].y;
            float i_x(0.0), i_y(0.0);
            if(GetLineIntersection(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, &i_x, &i_y)){
                intersections->push_back(Vector2d(i_x, i_y));
            }
        }
    }
    return intersections->empty() ? false : true;
}

bool PathResampling::IsSelfIntersect(const CenterLine& cl, Vector2d* intersections,
                                     int* id0, int* id1){
    for(int i = 2; i < cl.size() - 1; i++){
        float p0_x = cl[i].x;
        float p0_y = cl[i].y;
        float p1_x = cl[i+1].x;
        float p1_y = cl[i+1].y;
        for(int j = 0; j < i - 1; j++){
            float p2_x = cl[j].x;
            float p2_y = cl[j].y;
            float p3_x = cl[j+1].x;
            float p3_y = cl[j+1].y;
            float i_x(0.0), i_y(0.0);
            if(GetLineIntersection(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, &i_x, &i_y)){
                intersections->x = i_x;
                intersections->y = i_y;
                *id0 = j;
                *id1 = i+1;
                return true;
            }
        }
    }
    return false;
}


CenterLine PathResampling::RemoveSelfLoop(const CenterLine& cl){
    if(cl.size() <= 4){
        return cl;
    }
    CenterLine newcl = cl;
    int id0(0), id1(0);
    Vector2d p;
    // Removes self loop happens in short period of time
    while(IsSelfIntersect(newcl, &p, &id0, &id1) && (id1 - id0) <= 24){
        CenterLine tmp;
        for(int i = 0; i <= id0; i++){
            tmp.push_back(newcl[i]);
        }
        Vector2d avg = p;
        for(int i = id0 + 1; i < id1; i++){
            avg = avg + cl[i];
        }
        avg = avg * (1.0 / (id1 - id0));
        tmp.push_back(avg);
        //tmp.push_back(p);
        for(int i = id1; i < newcl.size(); i++){
            tmp.push_back(newcl[i]);
        }
        newcl = tmp;
    }
    return newcl;
}

std::vector<Vector2d> PathResampling::PointSubset(const int time)
{
    std::vector<Vector2d> res;
    for(const auto& cl : SDCenterLines){
        res.push_back(cl.at(time));
    }
    return res;
}



