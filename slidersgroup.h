#ifndef SLIDERSGROUP_H
#define SLIDERSGROUP_H

#include <QGroupBox>
#include <QSlider>

class SlidersGroup : public QGroupBox
{
    Q_OBJECT
public:
    explicit SlidersGroup(Qt::Orientation orientation, const QString &title, QWidget *parent = 0);

signals:
    void valueChanged(int value);

public slots:
    void setValue(const int value);
    void setMinimum(const int value);
    void setMaximum(const int value);

private:
    QSlider *slider;

};

#endif // SLIDERSGROUP_H
