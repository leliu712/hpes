#include "MathMorphology.h"
#include <algorithm>

MathMorphology::MathMorphology(float* _set, Matrix _element, int _rows, int _cols, int _size):
 element(_element),
 rows(_rows),
 cols(_cols),
 size(_size){
  set = new float[size];
  for(int i = 0; i < size; i++)
   set[i] = _set[i];
  //cout << element << endl;
  //cout << rows << " " << cols << endl;
  erows = element.nrows();
  ecols = element.ncols();
  //cout << erows << " " << ecols << endl;
}

MathMorphology::~MathMorphology(){
 delete[] set;
}

// Place the structuring element anywhere in the image
// Does it hit the set? Then the origin of the structuring element is part of the dilated set. 
void MathMorphology::dilation(){
 int lowb = erows / 2;
 int upb = erows - lowb - 1;
 std::vector<int> group;
 for(int i = 0; i < rows; i++){
  for(int j = 0; j < cols; j++){
   int flag = 0;  // overlap flag
   //constrcut an element group
   for(int _i = -lowb; _i <= upb; _i++){
    for(int _j = -lowb; _j <= upb; _j++){
     int newRow = i + _i;
     int newCol = j + _j;
     if(newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols){
      int idx = getIndex(newRow, newCol);
      if(set[idx] == 1) // find overlap
       flag = 1;
     }
    }
   }

   if(flag == 1){
    //for(int k = 0; k < cell.size(); k++){
    // group.push_back(cell[k]);
    //}
    group.push_back(getIndex(i, j));
   }

  }
 }

 clearSet();
 for(int i = 0; i < group.size(); i++){
  set[group[i]] = 1;
 }
}


// Place the structuring element anywhere in the image
// Is it fully contained by the set? Then the origin of the structuring element is part of the eroded set
void MathMorphology::erosion(){
 int lowb = erows / 2;
 int upb  = erows - lowb - 1;
 std::vector<int> group;
 for(int i = 0; i < rows; i++){
  for(int j = 0; j < cols; j++){
   int flag = 0;  // overlap flag
   //constrcut an element group
   for(int _i = -lowb; _i <= upb; _i++){
    for(int _j = -lowb; _j <= upb; _j++){
     int newRow = i + _i;
     int newCol = j + _j;
     if(newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols){
      int idx = getIndex(newRow, newCol);
      if(set[idx] == 1) // find overlap
       flag ++;
     }
    }
   }

   if(flag == erows*ecols){
    group.push_back(getIndex(i, j));
   }

  }
 }

 clearSet();
 for(int i = 0; i < group.size(); i++){
  set[group[i]] = 1;
 }
}

void MathMorphology::closing(){
 dilation();
 erosion();
}

void MathMorphology::opening(){
 erosion();
 dilation();
}

void MathMorphology::dilationImage(){
 int lowb = erows / 2;
 int upb = erows - lowb - 1;
 std::vector<int> group;
 std::vector<float> vals;
 for(int i = 0; i < rows; i++){
  for(int j = 0; j < cols; j++){
   int flag = 0;  // overlap flag
   float* list = new float[erows * ecols];
   for(int k = 0; k < erows * ecols; k++)
    list[k] = -10e6;
   //constrcut an element group
   int k = 0;
   for(int _i = -lowb; _i <= upb; _i++){
    for(int _j = -lowb; _j <= upb; _j++){
     int newRow = i + _i;
     int newCol = j + _j;
     if(newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols){
      int idx = getIndex(newRow, newCol);
      list[k] = set[idx];
     }
     k++;
    }
   }

   float max = *std::max_element(list, list + erows * ecols);
   group.push_back(getIndex(i, j));
   vals.push_back(max);

   delete[] list;

  }
 }

 clearSet();
 for(int i = 0; i < group.size(); i++){
  set[group[i]] = vals[i];
 }
}

void MathMorphology::erosionImage(){
 int lowb = erows / 2;
 int upb = erows - lowb - 1;
 std::vector<int> group;
 std::vector<float> vals;
 for(int i = 0; i < rows; i++){
  for(int j = 0; j < cols; j++){
   int flag = 0;  // overlap flag
   float* list = new float[erows * ecols];
   for(int k = 0; k < erows * ecols; k++)
    list[k] = -10e6;
   //constrcut an element group
   int k = 0;
   for(int _i = -lowb; _i <= upb; _i++){
    for(int _j = -lowb; _j <= upb; _j++){
     int newRow = i + _i;
     int newCol = j + _j;
     if(newRow >= 0 && newRow < rows && newCol >= 0 && newCol < cols){
      int idx = getIndex(newRow, newCol);
      list[k] = set[idx];
     }
     k++;
    }
   }

   float max = *std::min_element(list, list + erows * ecols);
   group.push_back(getIndex(i, j));
   vals.push_back(max);

   delete[] list;
  }
 }

 clearSet();
 for(int i = 0; i < group.size(); i++){
  set[group[i]] = vals[i];
 }
}

void MathMorphology::openingImage(){
 erosionImage();
 dilationImage();
}

void MathMorphology::closingImage(){
 dilationImage();
 erosionImage();
}
