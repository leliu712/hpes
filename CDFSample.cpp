#ifdef __APPLE__
# include <GLUT/glut.h>
#else
# include <GL/glut.h>
# include <GL/glu.h>
#endif

#include <string>
#include <fstream>
#include <algorithm>
#include <omp.h>
#include "CDFSample.h"
#include "ColorGradient.h"
#include "PoissonDiskSample.h"
#include "Icon.h"
#include "gauss.h"

#define HAVE_EXTR_POINTS

static int FirstTime = 0;

extern string InputPrefix;
extern string OutputPrefix;
extern string DatafileName;

CDFSample::CDFSample(){ 
 DisplayFlag = 1; 
}

void CDFSample::compute3(
  float minX, float maxX, float minY, float maxY,
  int width, int height, int nrows, int ncols, std::vector<double> density,
  std::vector<Vector2d> input, const int frameid,
  std::vector<double> inputDensity)
{

 cout << "here" << endl;
 //write2file(input, "input.txt");
 std::vector<Vector2d> vertices; 
 float dw = width / float(nrows-1);
 float dh = height / float(ncols-1);
 glPointSize(3);
 glColor4f(1, 0, 0, 1);
 glBegin(GL_POINTS);
 for(int i = 0; i < nrows; i++){
  float y = minY + dh * i;
  for(int j = 0; j < ncols; j++){
   float x = minX + dw * j;
   vertices.push_back(Vector2d(x, y));
   glVertex3f(x, y, 0.0);
  }
 }
 glEnd();

 std::vector< std::vector<float> > CDFX;
 std::vector< std::vector<float> > CDFY;
 // compute PX
 // PX is a vector has ncols elements
 std::vector<float> PX;
 for(int i = 0; i < ncols; i++)
 {
  float total(0);
  for(int j = 0; j < nrows; j++)
  {
   int idx = ncols * j + i;
   float f = density[idx];
   total += f;
  }
  PX.push_back(total);
 }
 float nonzeromin = *std::upper_bound(density.begin(), density.end(), 0);

 // scale densities by PX
 for(int i = 0; i < ncols; i++)
  for(int j = 0; j < nrows; j++)
  {
   int idx = ncols * j + i;
   density[idx] /= PX[i];
   density[idx] = density[idx] == 0 ? nonzeromin : density[idx];
  }
 for(int i = 0; i < density.size(); i++)
  cout << "cdf file " << i << " " << density[i] << endl;

 // assign u coordinates for all grid nodes
 for(int i = 0; i < nrows; i++)
 {
  std::vector<float> cdf;
  float total(0);
  for(int j = 0; j < ncols; j++)
  {
   float f = PX[j];
   total += f;
   cdf.push_back(total);
  }
  float localmax = *std::max_element(cdf.begin(), cdf.end());
  for(int j = 0; j < ncols; j++)
   cdf[j] /= localmax;
  CDFX.push_back(cdf);
 }

 // assign v coordinates
 for(int i = 0; i < ncols; i++)
 {
  std::vector<float> cdf;
  float total(0);
  for(int j = 0; j < nrows; j++)
  {
   int idx = ncols * j + i;
   float f = density[idx];
   total += f;
   cdf.push_back(total);
  }
  float localmax = *std::max_element(cdf.begin(), cdf.end());
  for(int j = 0; j < cdf.size(); j++)
   cdf[j] /= localmax;
  CDFY.push_back(cdf);
 }

 std::vector<float> tmp;
 for(int i = 0; i < CDFY.size(); i++)
  for(int j = 0; j < CDFY[i].size(); j++)
   tmp.push_back(CDFY[i][j]);
 float CDFYmax = *std::max_element(tmp.begin(), tmp.end());
 cout << CDFYmax << endl;
 for(int i = 0; i < CDFY.size(); i++)
 {
  cout << i << endl;
  for(int j = 0; j < CDFY[i].size(); j++)
   cout << CDFY[i][j] << endl;
  cout << endl;
 }
 cout << "here" << endl;
 exit(0);
 //readNewSpace("NewSpace.txt", CDFX, CDFY, nrows, ncols);
 write2file2(CDFX, CDFY, "testCenterCDFSpace.txt");
 exit(0);
 std::vector<float> scalefactors = scaleFactors(CDFX, CDFY, vertices, nrows, ncols);
 std::vector<int> sampleingrid = samplesinGrid(input, dw, dh, nrows, ncols);

 // poisson disk sampling
 PoissonDiskSample<std::vector<Vector2d>, float> poissondisksample;
 float spacearea = (maxX - minX) * (maxY - minY);
 poissondisksample.setInputDensities(inputDensity);
 poissondisksample.weightedSampleElim2(input, input, scalefactors, sampleingrid, spacearea);
 //poissondisksample.weightedSampleElim(input, input);
 //
 //// compute triangles
 //findTriangles(nrows, ncols);
 //
 //std::vector<float> radii;
 //std::vector<Vector2d> radii_2D;
 //for(int i = 0; i < newSpace.size(); i++)
 //{
 // Vector2d tmp = newSpace[i] + Vector2d(0.06, 0);
 // if(newSpace[i].x + 0.06 > 1)
 //  tmp = newSpace[i] - Vector2d(0.06, 0);
 // Vector2d coord_x;
 // //coord_x = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
 // coord_x = invmap(tmp, cdfgrid, vertices);
 // tmp = newSpace[i] + Vector2d(0, 0.06);
 // if(newSpace[i].y + 0.06 > 1)
 //  tmp = newSpace[i] - Vector2d(0, 0.06);
 // Vector2d coord_y;
 // //coord_y = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
 // coord_y = invmap(tmp, cdfgrid, vertices);
 // float disX = (coord_x - input[i]).norm();
 // float disY = (coord_y - input[i]).norm();
 // radii.push_back(std::max(disX, disY));
 // radii_2D.push_back(Vector2d(disX, disY));
 // Radii.push_back(2 * radii[i]);
 //}
 //
 ////write2file(radii, "Original-Radii.txt");
 ////write2file(radii_2D, "Original-Radii-2D.txt");
 //
 ////write2file(newSpace, "newspace.txt");
}

std::vector<Vector2d> CDFSample::SamplingByPDSampling(
  float minX, float maxX, float minY, float maxY,
  int width, int height, int nrows, int ncols, std::vector<double> density,
  std::vector<Vector2d> input, const int frameid,
  std::vector<double> sdvals,
  const float bandminx,
  const float bandmaxx,
  const float bandminy,
  const float bandmaxy
  )
{
 //std::vector<UDSpace> udspaces;
 //udspaces.resize(6);
 //char buff[200];
 //// Reads ud spaces at different points in time
 //for(int i = 1; i <= 6; i++){
 // string str = OutputPrefix + "InternalData/PathSamplingData/UDSpace-%d.txt";
 // const char* cstr = str.c_str();
 // sprintf(buff, cstr, i);
 // cout << "read ud space: " << buff << endl;
 // readNewSpace(buff, udspaces[i - 1].U, udspaces[i - 1].V, nrows, ncols);
 //}
 //PoissonGenerator::DefaultPRNG PRNG;
 //const auto Points = PoissonGenerator::GeneratePoissonPoints(100, PRNG);
 //
 //std::vector<Vector2d> samplingResults;
 //return samplingResults;
}



std::vector<Vector2d> CDFSample::SamplingByWSESelecting(
  float minX, float maxX, float minY, float maxY,
  int width, int height, int nrows, int ncols, std::vector<double> density,
  std::vector<Vector2d> input, const int frameid,
  std::vector<double> sdvals,
  const float bandminx,
  const float bandmaxx,
  const float bandminy,
  const float bandmaxy
  )
{
 char buff[200];
 std::vector<Vector2d> aaa;
 //drawInputwithIcon(input, frameid);
 //return aaa;
 Vector2d LLC(bandminx, bandminy), URC(bandmaxx, bandmaxy);
 LLC = translateToGeoCoor(LLC);
 URC = translateToGeoCoor(URC);
 float bregion = abs(LLC.x - URC.x) * abs(LLC.y - URC.y);
 float basedensity = input.size() / bregion;
 int subsize = 4 * bregion; // OLS 0.72
 //subsize = 30; // 42 61
 //glBegin(GL_LINE_LOOP);
 //glVertex3f(bandminx, bandminy, 0);
 //glVertex3f(bandmaxx, bandminy, 0);
 //glVertex3f(bandmaxx, bandmaxy, 0);
 //glVertex3f(bandminx, bandmaxy, 0);
 //glEnd();
 //std::vector<Vector2d> aaa;
 //return aaa;
 //cout << LLC << " " << URC << endl;
 //cout << subsize << endl;
 //exit(0);

 std::vector<Vector2d> vertices; 
 float dw = width / float(nrows-1);
 float dh = height / float(ncols-1);
 glPointSize(0.1);
 glColor4f(1, 0, 0, 1);
 glBegin(GL_POINTS);
 for(int i = 0; i < nrows; i++){
  float y = minY + dh * i;
  for(int j = 0; j < ncols; j++){
   float x = minX + dw * j;
   vertices.push_back(Vector2d(x, y));
   //glVertex3f(x, y, 0.0);
  }
 }
 glEnd();
 string str = OutputPrefix + "InternalData/PathSamplingData/ODGrid-%d.txt";
 const char* cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 write2file(vertices, buff);

 std::vector< std::vector<float> > CDFX;
 std::vector< std::vector<float> > CDFY;

 str = OutputPrefix + "InternalData/PathSamplingData/UDSpace-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 cout << "read warpped space: " << buff << endl;
 readNewSpace(buff, CDFX, CDFY, nrows, ncols);
 //str = OutputPrefix + "InternalData/PathSamplingData/UDSpaceForDisplay-%d.txt";
 //cstr = str.c_str();
 //sprintf(buff, cstr, frameid);
 //write2file2(CDFX, CDFY, buff);
 //exit(0);

 // output all samples in the OD space
 std::vector<Vector2d> OD;
 for(int i = 0; i < input.size(); i++)
 {
  Vector2d toPush = translateToGeoCoor(input[i]);
  OD.push_back(toPush);
 }
 str = OutputPrefix + "InternalData/PathSamplingData/OD-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 write2file(OD, buff);
 cout << OD.size() << " "<< sdvals.size() << endl;
 //exit(0);

 // transfer samples to the uv space
 std::vector<Vector2d> newSpace;
 for(int i = 0; i < input.size(); i++){
  Vector2d p = input[i];
  int row = int((p.y - minY) / dh);
  int col = int((p.x - minX) / dw);
  int* vid = new int[4];
  /* v2 v3
   * v0 v1 */
  vid[0] = ncols * row + col;
  vid[1] = ncols * row + (col + 1);
  vid[2] = ncols * (row + 1) + col;
  vid[3] = ncols * (row + 1) + (col + 1);
  Vector2d* v = new Vector2d[4];
  for(int j = 0; j < 4; j++)
   v[j] = vertices[vid[j]];
  float* fracs = new float[2];
  fracs[0] = (p.x - v[0].x) / (v[1].x - v[0].x);
  fracs[1] = (p.y - v[0].y) / (v[2].y - v[0].y);

  // eval long x axis
  std::vector<float>* cdfs = new std::vector<float>[2];
  cdfs[0] = CDFX[row];
  cdfs[1] = CDFX[row+1];
  float* cdfval = new float[4];
  cdfval[0] = cdfs[0][col];
  cdfval[1] = cdfs[0][col+1];
  cdfval[2] = cdfs[1][col];
  cdfval[3] = cdfs[1][col+1];
  float val0, val1, valx, valy;
  val0 = cdfval[0] * (1 - fracs[0]) + cdfval[1] * fracs[0];
  val1 = cdfval[2] * (1 - fracs[0]) + cdfval[3] * fracs[0];
  valx = val0 * (1 - fracs[1]) + val0 * fracs[1];

  //eval along y axis
  cdfs[0].clear();
  cdfs[1].clear();
  cdfs[0] = CDFY[col];
  cdfs[1] = CDFY[col+1];
  cdfval[0] = cdfs[0][row];
  cdfval[1] = cdfs[0][row+1];
  cdfval[2] = cdfs[1][row];
  cdfval[3] = cdfs[1][row+1];
  val0 = cdfval[0] * (1 - fracs[1]) + cdfval[1] * fracs[1];
  val1 = cdfval[2] * (1 - fracs[1]) + cdfval[3] * fracs[1];
  valy = val0 * (1 - fracs[0]) + val1 * fracs[0];

  newSpace.push_back((Vector2d(valx, valy)));
 }
 str = OutputPrefix + "InternalData/PathSamplingData/UD-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 write2file(newSpace, buff);
 //exit(0);

 // compute triangles
 findTriangles(nrows, ncols);
 //removeOutlier(newSpace, input, nrows, ncols, vertices, sdvals);


 // randomly generate samples to fill the boundaries
 std::vector<Vector2d> cdfgrid;
 for(int i = 0; i < nrows; i++){
  for(int j = 0; j < ncols; j++)
  {
   float x = CDFX[i][j];
   float y = CDFY[j][i];
   cdfgrid.push_back(Vector2d(x, y));
  }
 }
 str = OutputPrefix + "InternalData/PathSamplingData/UDGrid-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 write2file(cdfgrid, buff);
 std::vector<Vector2d> extrasamples;
 fillBoundaries(cdfgrid, nrows, ncols, CDFX, CDFY, newSpace,
   extrasamples, frameid);
 //std::vector<float> tmplist;
 //for(int i = 0; i < newSpace.size(); i++)
 // tmplist.push_back(newSpace[i].x);
 //std::cout << *std::max_element(tmplist.begin(), tmplist.end()) << std::endl;
 //exit(0);

 //write input file for matlab code for OLS selection
 //sprintf(buff, "OURData/OLS-input-%d.txt", frameid);
 //sprintf(buff, "OURData/OLS-input-%d.txt", frameid);
 str = OutputPrefix + "InternalData/PathSamplingData/fullSD-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 std::vector<float> tmp_sdvals;
 for(int i = 0; i < sdvals.size(); i++)
  tmp_sdvals.push_back(sdvals[i]);
 cout << tmp_sdvals.size() << " " << input.size() << endl;
 write2file(tmp_sdvals, buff);
 //exit(0);

 // poisson disk sampling
 PoissonDiskSample<std::vector<Vector2d>, float> poissondisksample;
 //poissondisksample.setInputDensities(density);
 std::vector<Vector2d> selectedSamples; 
 float minDis = 0;
 poissondisksample.setSubsetSize(10);
 selectedSamples = poissondisksample.weightedSampleElim(extrasamples, input, minDis, frameid);
 //sprintf(buff, "NHCData/PDS-refSamples-%d.txt", frameid);
 //sprintf(buff, "NewData/PDS-refSamples-%d.txt", frameid);
 //write2file(selectedSamples, buff);
 ////selectedSamples = readTestSamples("refSamples-4-100.txt");
 //selectedSamples = readTestSamples("NHCData/PDS-refSamples-4-100.txt");

 //read in OLS selected samples;
 //sprintf(buff, "OURData/OLS-refSamples-%d-100.txt", frameid);
 //sprintf(buff, "NHCData/OLS-refSamples-%d-100.txt", frameid);
 //sprintf(buff, "NewData/OLS-refSamples-%d-100.txt", frameid);
 //selectedSamples = readTestSamples(buff);

 std::vector<Vector2d> toWrite;
 std::vector<Vector2d> toRebuild;
 Icon icon(0, 0, 65, 65);
 str = OutputPrefix + "InternalData/PathSamplingData/NHC-PDS-idx-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 char* buff2 = new char[200];
 str = InputPrefix + DatafileName + "_Cate.txt";
 cstr = str.c_str();
 sprintf(buff2, cstr, frameid);
 //std::vector<Vector2d> cats = readCategories(buff, buff2, selectedSamples);
 //cout << cats.size() << " " << selectedSamples.size() << endl;
 //cout << "subsize:" << subsize << endl;
 //cout << "bregion:" << bregion << endl;
 str = OutputPrefix + "InternalData/PathSamplingData/UD-selected-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 write2file(selectedSamples, buff);
 for(int i = 0; i < selectedSamples.size(); i++)
 {
  Vector2d v = selectedSamples[i];
  Vector2d v_ = invmap(v, cdfgrid, vertices);
  toRebuild.push_back(v_);
  glPointSize(4);
  glBegin(GL_POINTS);
  glVertex3f(v_.x, v_.y, 0.0);
  glEnd();
  icon.setCenter(v_.x, v_.y);
  //int cat = cats[i];
  //cout << cat << v_ << endl;
  v_ = translateToGeoCoor(v_);
  toWrite.push_back(v_);
  //float r = iconSize(v_, cats[i].y);
  //icon.setSize(r);
  //icon.draw(cats[i].x, 1);
 }
 str = OutputPrefix + "InternalData/PathSamplingData/OD-selected-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 write2file(toWrite, buff);
 return toRebuild;
}



// new idea
// first x then y 
void CDFSample::compute(int width, int height, int nrows, int ncols, std::vector<double> density,
  std::vector<Vector2d> input, const int frameid)
{

 write2file(input, "input.txt");
 std::vector<Vector2d> vertices; 
 float dw = width / float(nrows-1);
 float dh = height / float(ncols-1);
 for(int i = 0; i < nrows; i++){
  float y = dh * i;
  for(int j = 0; j < ncols; j++){
   float x = dw * j;
   vertices.push_back(Vector2d(x, y));
  }
 }

 std::vector< std::vector<float> > CDFX;
 std::vector< std::vector<float> > CDFY;
 // compute CDFs across rows
 for(int i = 0; i < nrows; i++){
  float total(0);
  std::vector<float> cdf;
  for(int j = 0; j < ncols; j++){
   if(j == 0)
    cdf.push_back(0);
   else
   {
    int idx = ncols * i + j;
    float p0 = density[idx];
    float p1 = density[idx-1];
    float p = 0.5 * (p0 + p1);
    float val = p * dw;
    total += val; 
    cdf.push_back(total);
   }
  }
  CDFX.push_back(cdf);
 }

 CenterCDFSpace(CDFX);
 ScaleCDFs(CDFX, 1.0, ncols);

 //testcode//for(int i = 0; i < ncols; i++)
 //testcode//{
 //testcode// std::vector<float> cdf;
 //testcode// for(int j = 0; j < nrows; j++)
 //testcode// {
 //testcode//  float f = 0 + 0.868 / (ncols - 1) * j;
 //testcode//  cdf.push_back(f);
 //testcode// }
 //testcode// CDFY.push_back(cdf);
 //testcode//}

 //write2file(CDFX, CDFY, "testCenterCDFSpace.txt");

 // transfer samples to (u,y) space 
 std::vector<Vector2d> newSpace;
 for(int i = 0; i < input.size(); i++){
  Vector2d p = input[i];
  int row = int(p.y / dh);
  int col = int(p.x / dw);
  int* vid = new int[4];
  /* v2 v3
   * v0 v1 */
  vid[0] = ncols * row + col;
  vid[1] = ncols * row + (col + 1);
  vid[2] = ncols * (row + 1) + col;
  vid[3] = ncols * (row + 1) + (col + 1);
  Vector2d* v = new Vector2d[4];
  for(int j = 0; j < 4; j++)
   v[j] = vertices[vid[j]];
  float* fracs = new float[2];
  fracs[0] = (p.x - v[0].x) / (v[1].x - v[0].x);
  fracs[1] = (p.y - v[0].y) / (v[2].y - v[0].y);

  std::vector<float>* cdfs = new std::vector<float>[2];
  cdfs[0] = CDFX[row];
  cdfs[1] = CDFX[row+1];
  float* cdfval = new float[4];
  cdfval[0] = cdfs[0][col];
  cdfval[1] = cdfs[0][col+1];
  cdfval[2] = cdfs[1][col];
  cdfval[3] = cdfs[1][col+1];
  float val0, val1, valx, valy;
  val0 = cdfval[0] * (1 - fracs[0]) + cdfval[1] * fracs[0];
  val1 = cdfval[2] * (1 - fracs[0]) + cdfval[3] * fracs[0];
  valx = val0 * (1 - fracs[1]) + val0 * fracs[1];
  valy = input[i].y * 10e-4; // for rbf interpolation
  newSpace.push_back((Vector2d(valx, valy)));
 }
 write2file(newSpace, "samples-uy.txt");

 // re-compute knn density field
 std::vector<int> validlist;
 float distance_threshold = 0;
 std::vector<double> knndensity = KDTreeDensity<std::vector<Vector2d>>(newSpace, newSpace.size(),
   2, validlist, 0);
 std::vector<Vector2d> validpoints;
 std::vector<double> validdensity;
 for(int i = 0; i < validlist.size(); i++)
  if(validlist[i] != 0)
  {
   validpoints.push_back(newSpace[i]);
   validdensity.push_back(knndensity[i]);
  }
 RBF densityRBF;
 float rbfradius = 0.12;
 densityRBF.compute(validpoints, validdensity, rbfradius);
 writeDensityField(densityRBF);

 // re-build the cdf grid in [u,v] space
 // and compute density values at grid nodes
 vertices.clear();
 density.clear();
 dw = 1.0 / (ncols-1); //x range is [-0.5,0.5];
 dh = height * 10e-4 / (nrows-1); // y range is [0, 0.868];
 cout << dw << " " << dh << endl;
 for(int i = 0; i < nrows; i++)
  for(int j = 0; j < ncols; j++)
  {
   float y =    0 +  dh * i;
   float x = -0.5 + dw * j;
   vertices.push_back(Vector2d(x, y));
   float f = densityRBF.eval(Vector2d(x, y));
   density.push_back(f);
  }
 write2file(vertices, "uy-grid.txt");
 write2file(density, "uy-grid-density.txt");

 CDFY.clear();
 // compute CDFs across cols
 for(int j = 0; j < ncols; j++){
  float total(0);
  std::vector<float> cdf;
  for(int i = 0; i < nrows; i++){
   if(i == 0)
    cdf.push_back(0);
   else
   {
    int idx = nrows * j + i;
    float p0 = density[idx];
    float p1 = density[idx-1];
    float p = 0.5 * (p0 + p1);
    float val = p * dh;
    total += val;
    cdf.push_back(total);
   }
  }
  CDFY.push_back(cdf);
 }

 CenterCDFSpace(CDFY);
 ScaleCDFs(CDFY, 1.0, nrows);

 // compute scale factors from CDFY
 Vcoords(CDFY, 0, height);

 //testcode//CDFX.clear();
 //testcode//for(int i = 0; i < nrows; i++)
 //testcode//{
 //testcode// std::vector<float> cdf;
 //testcode// for(int j = 0; j < ncols; j++)
 //testcode// {
 //testcode//  float f = 0 + 1.0 / (ncols-1) * j;
 //testcode//  cdf.push_back(f);
 //testcode// }
 //testcode// CDFX.push_back(cdf);
 //testcode//}

 write2file(CDFX, CDFY, "testCenterCDFSpace.txt");
 exit(0);

 // transfer samples to [u,v] space
 for(int i = 0; i < newSpace.size(); i++){
  Vector2d p = newSpace[i];
  int row = int(p.y / dh);
  int col = int(p.x / dw);
  int* vid = new int[4];
  /* v2 v3
   * v0 v1 */
  vid[0] = ncols * row + col;
  vid[1] = ncols * row + (col + 1);
  vid[2] = ncols * (row + 1) + col;
  vid[3] = ncols * (row + 1) + (col + 1);
  cout << vid[0] << " " << vid[1] << " " << vid[2] << " " << vid[3] << endl;
  Vector2d* v = new Vector2d[4];
  for(int j = 0; j < 4; j++)
   v[j] = vertices[vid[j]];
  float* fracs = new float[2];
  fracs[0] = (p.x - v[0].x) / (v[1].x - v[0].x);
  fracs[1] = (p.y - v[0].y) / (v[2].y - v[0].y);

  std::vector<float>* cdfs = new std::vector<float>[2];
  float* cdfval = new float[4];
  float val0, val1, valy;
  cdfs[0] = CDFY[col];
  cdfs[1] = CDFY[col+1];
  cdfval[0] = cdfs[0][row];
  cdfval[1] = cdfs[0][row+1];
  cdfval[2] = cdfs[1][row];
  cdfval[3] = cdfs[1][row+1];
  val0 = cdfval[0] * (1 - fracs[1]) + cdfval[1] * fracs[1];
  val1 = cdfval[2] * (1 - fracs[1]) + cdfval[3] * fracs[1];
  valy = val0 * (1 - fracs[0]) + val1 * fracs[0];
  newSpace[i].y = valy;
 }
 write2file(newSpace, "uvspace.txt");
 exit(0);

 // transfer grid to CDF space
 std::vector<Vector2d> cdfgrid;
 for(int i = 0; i < nrows; i++){
  for(int j = 0; j < ncols; j++)
  {
   float x = CDFX[i][j];
   float y = CDFY[j][i];
   cdfgrid.push_back(Vector2d(x, y));
  }
 }


 // randomly generate samples to fill the boundaries
 std::vector<Vector2d> extrasamples;
 fillBoundaries(cdfgrid, nrows, ncols, CDFX, CDFY, newSpace,
   extrasamples, frameid);
 write2file(extrasamples, "newsamples.txt");
 // poisson disk sampling
 PoissonDiskSample<std::vector<Vector2d>, float> poissondisksample;
 poissondisksample.setInputDensities(density);
 //std::vector<Vector2d> testsamples = readTestSamples("testsamples.txt");
 //poissondisksample.weightedSampleElim(testsamples, input);
 //poissondisksample.weightedSampleElim(newSpace, input);
 //write2file(newSpace, "newspace.txt");
 //exit(0);
 //poissondisksample.weightedSampleElim(extrasamples, input, 0.0);

 // compute triangles
 findTriangles(nrows, ncols);

 std::vector<float> radii;
 std::vector<Vector2d> radii_2D;
 for(int i = 0; i < newSpace.size(); i++)
 {
  Vector2d tmp = newSpace[i] + Vector2d(0.06, 0);
  if(newSpace[i].x + 0.06 > 1)
   tmp = newSpace[i] - Vector2d(0.06, 0);
  Vector2d coord_x;
  //coord_x = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
  coord_x = invmap(tmp, cdfgrid, vertices);
  tmp = newSpace[i] + Vector2d(0, 0.06);
  if(newSpace[i].y + 0.06 > 1)
   tmp = newSpace[i] - Vector2d(0, 0.06);
  Vector2d coord_y;
  //coord_y = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
  coord_y = invmap(tmp, cdfgrid, vertices);
  float disX = (coord_x - input[i]).norm();
  float disY = (coord_y - input[i]).norm();
  radii.push_back(std::max(disX, disY));
  radii_2D.push_back(Vector2d(disX, disY));
  Radii.push_back(2 * radii[i]);
 }

 //write2file(radii, "Original-Radii.txt");
 //write2file(radii_2D, "Original-Radii-2D.txt");

 //write2file(newSpace, "newspace.txt");
}



// new idea
// create a center coord sys
//void CDFSample::compute(int width, int height, int nrows, int ncols, std::vector<double> density,
//  std::vector<Vector2d> input, const int frameid)
//{
//
// write2file(input, "input.txt");
// std::vector<Vector2d> vertices; 
// //input = duplicateSamples(input);
// float dw = width / float(nrows);
// float dh = height / float(ncols);
// for(int i = 0; i < nrows; i++){
//  float y = dh * i;
//  for(int j = 0; j < ncols; j++){
//   float x = dw * j;
//   vertices.push_back(Vector2d(x, y));
//  }
// }
//
// // compute CDFs across rows
// // trapezoid rule
// std::vector< std::vector<float> > testrows;
// std::vector< std::vector<float> > CDFX;
// for(int i = 0; i < nrows; i++){
//  float total(0);
//  std::vector<float> cdf;
//  std::vector<float> testrow;
//  for(int j = 0; j < ncols; j++){
//   if(j == 0)
//   {
//    cdf.push_back(0);
//    testrow.push_back(0);
//   }
//   else
//   {
//    int idx = ncols * i + j;
//    float p0 = density[idx];
//    float p1 = density[idx-1];
//    float p = 0.5 * (p0 + p1);
//    float val = p * dw;
//    total += val; 
//    cdf.push_back(total);
//    testrow.push_back(p);
//   }
//  }
//  CDFX.push_back(cdf);
//  testrows.push_back(testrow);
// }
//
// // compute CDFs across cols
// std::vector< std::vector<float> > CDFY;
// for(int j = 0; j < ncols; j++){
//  float total(0);
//  std::vector<float> cdf;
//  for(int i = 0; i < nrows; i++){
//   if(i == 0)
//    cdf.push_back(0);
//   else
//   {
//    int idx = nrows * j + i;
//    float p0 = density[idx];
//    float p1 = density[idx-1];
//    float p = 0.5 * (p0 + p1);
//    float val = p * dh;
//    total += val;
//    cdf.push_back(total);
//   }
//  }
//  CDFY.push_back(cdf);
// }
//
// //for(int i = 0; i < nrows; i++)
// //{
// // for(int j = 0; j < ncols; j++)
// //  printf("%1.6f\t", testrows[i][j]);
// // printf("%\n");
// //}
// //cout << endl;
// //for(int i = 0; i < nrows; i++)
// //{
// // for(int j = 0; j < ncols; j++)
// //  printf("%1.6f\t", CDFX[i][j]);
// // printf("%\n");
// //}
// 
// for(int i = 0; i < ncols; i++)
// {
//  for(int j = 0; j < nrows; j++)
//  {
//   int idx = j * ncols + i;
//   Vector2d tmp = vertices[idx];
//   //CDFY[i][j] = -0.5 + 1.0 / (nrows-1) * j;
//   CDFY[i][j] = tmp.y;
//  }
// }
//
// CenterCDFSpace(CDFX);
// //CenterCDFSpace(CDFY);
//
// // write CDFs to file
// write2file(CDFX, CDFY, "testCenterCDFSpace.txt");
// exit(0);
//
// // eval
// std::vector<Vector2d> newSpace;
// for(int i = 0; i < input.size(); i++){
//  Vector2d p = input[i];
//  int row = int(p.y / dh);
//  int col = int(p.x / dw);
//  int* vid = new int[4];
//  /* v2 v3
//   * v0 v1 */
//  vid[0] = ncols * row + col;
//  vid[1] = ncols * row + (col + 1);
//  vid[2] = ncols * (row + 1) + col;
//  vid[3] = ncols * (row + 1) + (col + 1);
//  Vector2d* v = new Vector2d[4];
//  for(int j = 0; j < 4; j++)
//   v[j] = vertices[vid[j]];
//  float* fracs = new float[2];
//  fracs[0] = (p.x - v[0].x) / (v[1].x - v[0].x);
//  fracs[1] = (p.y - v[0].y) / (v[2].y - v[0].y);
//
//  // eval long x axis
//  std::vector<float>* cdfs = new std::vector<float>[2];
//  cdfs[0] = CDFX[row];
//  cdfs[1] = CDFX[row+1];
//  float* cdfval = new float[4];
//  cdfval[0] = cdfs[0][col];
//  cdfval[1] = cdfs[0][col+1];
//  cdfval[2] = cdfs[1][col];
//  cdfval[3] = cdfs[1][col+1];
//  float val0, val1, valx, valy;
//  val0 = cdfval[0] * (1 - fracs[0]) + cdfval[1] * fracs[0];
//  val1 = cdfval[2] * (1 - fracs[0]) + cdfval[3] * fracs[0];
//  valx = val0 * (1 - fracs[1]) + val0 * fracs[1];
//
//  //eval along y axis
//  cdfs[0].clear();
//  cdfs[1].clear();
//  cdfs[0] = CDFY[col];
//  cdfs[1] = CDFY[col+1];
//  cdfval[0] = cdfs[0][row];
//  cdfval[1] = cdfs[0][row+1];
//  cdfval[2] = cdfs[1][row];
//  cdfval[3] = cdfs[1][row+1];
//  val0 = cdfval[0] * (1 - fracs[1]) + cdfval[1] * fracs[1];
//  val1 = cdfval[2] * (1 - fracs[1]) + cdfval[3] * fracs[1];
//  valy = val0 * (1 - fracs[0]) + val1 * fracs[0];
//
//  newSpace.push_back((Vector2d(valx, valy)));
// }
//
// //char* filename = new char[100];
// ////sprintf(filename, "CDFSPACE/newspace_%d.txt", frameid);
// //sprintf(filename, "CenterCDFSpaceSamples.txt", frameid);
// //write2file(newSpace, filename);
// //exit(0);
//
// // transfer grid to CDF space
// std::vector<Vector2d> cdfgrid;
// for(int i = 0; i < nrows; i++)
//  for(int j = 0; j < ncols; j++)
//  {
//   float x = CDFX[i][j];
//   float y = CDFY[j][i];
//   cdfgrid.push_back(Vector2d(x, y));
//  }
//
// // randomly generate samples to fill the boundaries
// std::vector<Vector2d> extrasamples;
// fillBoundaries(cdfgrid, nrows, ncols, CDFX, CDFY, newSpace,
//   extrasamples);
// write2file(extrasamples, "newsamples.txt");
// // poisson disk sampling
// PoissonDiskSample<std::vector<Vector2d>, float> poissondisksample;
// poissondisksample.setInputDensities(density);
// //std::vector<Vector2d> testsamples = readTestSamples("testsamples.txt");
// //poissondisksample.weightedSampleElim(testsamples, input);
// //poissondisksample.weightedSampleElim(newSpace, input);
// //write2file(newSpace, "newspace.txt");
// //exit(0);
// poissondisksample.weightedSampleElim(extrasamples, input);
//
// // compute triangles
// findTriangles(nrows, ncols);
//
// std::vector<float> radii;
// std::vector<Vector2d> radii_2D;
// for(int i = 0; i < newSpace.size(); i++)
// {
//  Vector2d tmp = newSpace[i] + Vector2d(0.06, 0);
//  if(newSpace[i].x + 0.06 > 1)
//   tmp = newSpace[i] - Vector2d(0.06, 0);
//  Vector2d coord_x;
//  //coord_x = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
//  coord_x = invmap(tmp, cdfgrid, vertices);
//  tmp = newSpace[i] + Vector2d(0, 0.06);
//  if(newSpace[i].y + 0.06 > 1)
//   tmp = newSpace[i] - Vector2d(0, 0.06);
//  Vector2d coord_y;
//  //coord_y = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
//  coord_y = invmap(tmp, cdfgrid, vertices);
//  float disX = (coord_x - input[i]).norm();
//  float disY = (coord_y - input[i]).norm();
//  radii.push_back(std::max(disX, disY));
//  radii_2D.push_back(Vector2d(disX, disY));
//  Radii.push_back(2 * radii[i]);
// }
//
// //write2file(radii, "Original-Radii.txt");
// //write2file(radii_2D, "Original-Radii-2D.txt");
//
// //write2file(newSpace, "newspace.txt");
//}
//

// old idea,
// nomalized by total area
//void CDFSample::compute(int width, int height, int nrows, int ncols, std::vector<double> density,
//  std::vector<Vector2d> input, const int frameid)
//{
// write2file(input, "input.txt");
// std::vector<Vector2d> vertices; 
// float dw = width / float(nrows);
// float dh = height / float(ncols);
// for(int i = 0; i < nrows; i++){
//  float y = dh * i;
//  for(int j = 0; j < ncols; j++){
//   float x = dw * j;
//   vertices.push_back(Vector2d(x, y));
//  }
// }
//
// // compute CDFs across rows
// std::vector< std::vector<float> > CDFX;
// for(int i = 0; i < nrows; i++){
//  float total(0);
//  std::vector<float> cdf;
//  for(int j = 0; j < ncols; j++){
//   int idx = ncols * i + j;
//   float p = density[idx];
//   float val = p * dw;
//   total += val; 
//   cdf.push_back(total);
//  }
//  if(total != 0){
//   for(int j = 0; j < ncols; j++){
//    cdf[j] = cdf[j] / total;
//   }
//  }
//  CDFX.push_back(cdf);
// }
//
// // compute CDFs across cols
// std::vector< std::vector<float> > CDFY;
// for(int j = 0; j < ncols; j++){
//  float total(0);
//  std::vector<float> cdf;
//  for(int i = 0; i < nrows; i++){
//   int idx = nrows * j + i;
//   float p = density[idx];
//   float val = p * dh;
//   total += val;
//   cdf.push_back(total);
//  }
//  if(total != 0){
//   for(int i = 0; i < nrows; i++){
//    cdf[i] = cdf[i] / total;
//   }
//  }
//  CDFY.push_back(cdf);
// }
//
// // write CDFs to file
// write2file(CDFX, CDFY, "cdfspace.txt");
// exit(0);
//
// // eval
// std::vector<Vector2d> newSpace;
// for(int i = 0; i < input.size(); i++){
//  Vector2d p = input[i];
//  int row = int(p.y / dh);
//  int col = int(p.x / dw);
//  int* vid = new int[4];
//  /* v2 v3
//   * v0 v1 */
//  vid[0] = ncols * row + col;
//  vid[1] = ncols * row + (col + 1);
//  vid[2] = ncols * (row + 1) + col;
//  vid[3] = ncols * (row + 1) + (col + 1);
//  Vector2d* v = new Vector2d[4];
//  for(int j = 0; j < 4; j++)
//   v[j] = vertices[vid[j]];
//  float* fracs = new float[2];
//  fracs[0] = (p.x - v[0].x) / (v[1].x - v[0].x);
//  fracs[1] = (p.y - v[0].y) / (v[2].y - v[0].y);
//
//  // eval long x axis
//  std::vector<float>* cdfs = new std::vector<float>[2];
//  cdfs[0] = CDFX[row];
//  cdfs[1] = CDFX[row+1];
//  float* cdfval = new float[4];
//  cdfval[0] = cdfs[0][col];
//  cdfval[1] = cdfs[0][col+1];
//  cdfval[2] = cdfs[1][col];
//  cdfval[3] = cdfs[1][col+1];
//  float val0, val1, valx, valy;
//  val0 = cdfval[0] * (1 - fracs[0]) + cdfval[1] * fracs[0];
//  val1 = cdfval[2] * (1 - fracs[0]) + cdfval[3] * fracs[0];
//  valx = val0 * (1 - fracs[1]) + val0 * fracs[1];
//
//  //eval along y axis
//  cdfs[0].clear();
//  cdfs[1].clear();
//  cdfs[0] = CDFY[col];
//  cdfs[1] = CDFY[col+1];
//  cdfval[0] = cdfs[0][row];
//  cdfval[1] = cdfs[0][row+1];
//  cdfval[2] = cdfs[1][row];
//  cdfval[3] = cdfs[1][row+1];
//  val0 = cdfval[0] * (1 - fracs[1]) + cdfval[1] * fracs[1];
//  val1 = cdfval[2] * (1 - fracs[1]) + cdfval[3] * fracs[1];
//  valy = val0 * (1 - fracs[0]) + val1 * fracs[0];
//
//  newSpace.push_back((Vector2d(valx, valy)));
// }
//
// // poisson disk sampling
// //PoissonDiskSample<std::vector<Vector2d>, float> poissondisksample;
// //poissondisksample.weightedSampleElim(newSpace, input);
// 
// // compute triangles
// std::vector<Vector2d> cdfgrid;
// for(int i = 0; i < nrows; i++)
//  for(int j = 0; j < ncols; j++)
//  {
//   float x = CDFX[i][j];
//   float y = CDFY[j][i];
//   cdfgrid.push_back(Vector2d(x, y));
//  }
// findTriangles(nrows, ncols);
//
// std::vector<float> radii;
// std::vector<Vector2d> radii_2D;
// for(int i = 0; i < newSpace.size(); i++)
// {
//  Vector2d tmp = newSpace[i] + Vector2d(0.06, 0);
//  if(newSpace[i].x + 0.06 > 1)
//   tmp = newSpace[i] - Vector2d(0.06, 0);
//  Vector2d coord_x;
//  //coord_x = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
//  coord_x = invmap(tmp, cdfgrid, vertices);
//  tmp = newSpace[i] + Vector2d(0, 0.06);
//  if(newSpace[i].y + 0.06 > 1)
//   tmp = newSpace[i] - Vector2d(0, 0.06);
//  Vector2d coord_y;
//  //coord_y = invmap(tmp, CDFX, CDFY, vertices, nrows, ncols);
//  coord_y = invmap(tmp, cdfgrid, vertices);
//  float disX = (coord_x - input[i]).norm();
//  float disY = (coord_y - input[i]).norm();
//  radii.push_back(std::max(disX, disY));
//  radii_2D.push_back(Vector2d(disX, disY));
//  Radii.push_back(2 * radii[i]);
// }
//
// //std::vector<float> shadow;
// //shadow = radii;
// //std::sort(shadow.begin(), shadow.end());
// //float extr = shadow[int(shadow.size() * 0.98)];
// //for(int i = 0; i < radii.size(); i++)
// //{
// // radii[i] = radii[i] > extr ? extr : radii[i];
// // radii_2D[i][0] = radii_2D[i][0] > extr ? extr : radii_2D[i][0];
// // radii_2D[i][1] = radii_2D[i][1] > extr ? extr : radii_2D[i][1];
// // Radii.push_back(2 * radii[i]);
// //}
// //write2file(radii, "Original-Radii.txt");
// //write2file(radii_2D, "Original-Radii-2D.txt");
//
//
// //std::vector<float> radii_x;
// //std::vector<float> radii_y;
// //std::vector<Vector2d> radii_2D;
// //radii_x = invmapbyRBF(input, newSpace, 0, 1);
// //radii_y = invmapbyRBF(input, newSpace, 1, 1);
// //for(int i = 0; i < input.size(); i++)
// //{
// // radii_2D.push_back(Vector2d(radii_x[i], radii_y[i]));
// //}
// //write2file(radii_2D, "Original-Radii-2D.txt");
//
//
// //char* filename = new char[100];
// //sprintf(filename, "CDFSPACE/newspace_%d.txt", frameid);
// //write2file(newSpace, filename);
// //write2file(newSpace, "newspace.txt");
//}

// given a locaion in the original space and its CDF value, 
// return the corresponding RBF kernel radius in the original space
//Vector2d CDFSample::evalCDFinv(
//  const float radius,
//  const Vector2d value,
//  const Vector2d p,
//  const float dw,
//  const float dh,
//  std::vector< std::vector<float> > CDFX,
//  std::vector< std::vector<float> > CDFY,
//  std::vector<Vector2d> vertices,
//  const int nrows, const int ncols, const int flag)
//{
// int row = int(p.y / dh);
// int col = int(p.x / dw);
// int vid[4];
// std::vector<float> cdf0[2];
// if(flag == 0) // go for X direction
// {
//  cdf0[0] = CDFX[row];
//  cdf0[1] = CDFX[row+1];
// }
// else
// {
//  cdf0[0] = CDFY[col];
//  cdf0[1] = CDFY[col+1];
// }
//
// std::vector<float>::iterator it[4];
// float vals[4];
// float valsx[4];
// float valsy[4];
// float fracs[3];
// int i(0);
// for(it[1] = cdf0[0].begin(); it[1] != cdf0[0].end(); it[1]++)
// {
//  if(value[flag] + radius <= 1.0)
//  {
//   if(*it[1] > value[flag] + radius)
//    break;
//   i++; 
//  }
//  else
//  {
//   if(*it[1] > value[flag] - radius)
//    break;
//   i++;
//  }
// }
//
// it[0] = it[1]-1;
// vals[0] = *it[0]; vals[1] = *it[1];
// fracs[0] = (value[flag] + radius - vals[0]) / (vals[1] - vals[0]);
// vals[0] = cdf0[1][i-1]; vals[1] = cdf0[1][i];
// fracs[1] = (value[flag] + radius - vals[0]) / (vals[1] - vals[0]);
//
//
// std::vector<float> cdf1[2];
// if(flag == 0)
// {
//  cdf1[0] = CDFY[i-1];
//  vals[2] = cdf1[0][row];
//  vals[3] = cdf1[0][row+1];
// }
// else
// {
//  cdf1[0] = CDFX[i-1]; 
//  vals[2] = cdf1[0][col];
//  vals[3] = cdf1[0][col+1];
// }
// fracs[3] = (value[(flag+1)%2] - vals[2]) / (vals[3] - vals[2]);
//
// vid[0] = ncols * row + i - 1;
// vid[1] = ncols * row + i;
// vid[2] = ncols * (row + 1) + i - 1;
// vid[3] = ncols * (row + 1) + i;
// Vector2d vs[4];
// vs[0] = vertices[vid[0]];
// vs[1] = vertices[vid[1]];
// vs[2] = vertices[vid[2]];
// vs[3] = vertices[vid[3]];
// Vector2d toReturn;
// Vector2d tmp0, tmp1;
// tmp0 = (vs[0] * (1 - fracs[0]) + vs[1] * fracs[0]);
// tmp1 = (vs[2] * (1 - fracs[1]) + vs[3] * fracs[1]);
// toReturn = tmp0 * (1 - fracs[2]) + tmp1 * fracs[2];
// return(toReturn);
//}

void CDFSample::write2file(std::vector<Vector2d> data, const char* filename){
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i< size; i++){
  myfile << data[i].x << " " << data[i].y << endl;
 }
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

// Given a point in the CDF space,
// return its coords in the original space.
Vector2d CDFSample::invmap(const Vector2d query,
  const std::vector< std::vector<float> > CDFX,
  const std::vector< std::vector<float> > CDFY,
  const std::vector<Vector2d> vertices, 
  const int nrows, const int ncols)
{
 float Fx(query.x);
 float Fy(query.y);
 int row_idx(0);
 int col_idx(0);
 //search through rows
 float minDis = 10e6;
 for(int i = 0; i < CDFX.size(); i++)
 {
  std::vector<float> cdfx = CDFX[i];
  std::vector<float>::iterator itx = std::upper_bound(cdfx.begin(), cdfx.end(), Fx) - 1; 
  int col = itx - cdfx.begin();

  //search through column at row 
  std::vector<float> cdfy = CDFY[col];
  std::vector<float>::iterator ity = std::upper_bound(cdfy.begin(), cdfy.end(), Fy) - 1; 
  int row = ity - cdfy.begin();
  Vector2d node(cdfx[col], cdfy[row]);
  Vector2d tmp = node - query;
  float dis = tmp.norm();
  //if(tmp.x <= 0 && tmp.y <= 0 && dis <= minDis)
  cout << row << " " << i <<  endl;
  if(row == i && dis <= minDis && tmp.x <= 0 && tmp.y <= 0)
  {
   cout << "in side here" << endl;
   row_idx = i;
   col_idx = col;
   minDis = dis;
  }
 }

 /*
  * v0_v1
  * | / |
  * v3_v2
  */
 Vector3d barycentric(0, 0, 0);
 int flag;
 Vector2d Fidx[4];
 Fidx[0] = Vector2d(row_idx, col_idx);
 Fidx[1] = Fidx[0] + Vector2d(0, 1);
 Fidx[2] = Fidx[0] + Vector2d(1, 1);
 Fidx[3] = Fidx[0] + Vector2d(1, 0);

 Vector2d Fv[4];
 Vector2d v[4];

 if(!checkCell(Fidx, CDFX, CDFY, query, barycentric, flag))
 {
  std::vector<Vector2d> shift;
  shift.push_back((Vector2d(0, 1)));
  shift.push_back((Vector2d(0, -1)));
  shift.push_back((Vector2d(1, 0)));
  shift.push_back((Vector2d(-1, 0)));

  for(int i = 0; i < 4; i++)
  {
   for(int j = 0; j < 4; j++)
    Fidx[j] = Fidx[j] + shift[i];
   if(checkCell(Fidx, CDFX, CDFY, query, barycentric, flag))
   {
    break;
   }
  }
 }
 else
 {
 }

 //glColor4f(0, 0, 1, 1);
 //glBegin(GL_LINE_LOOP);
 //for(int i = 0; i < 4; i++)
 //{
 // int vidx = Fidx[i].x * ncols + Fidx[i].y;
 // v[i] = vertices[vidx];
 // Fv[i].x = CDFX[Fidx[i].x][Fidx[i].y];
 // Fv[i].y = CDFY[Fidx[i].y][Fidx[i].x];
 // glVertex3f(Fv[i].x * 1000, Fv[i].y * 800, 0);
 //}
 //glEnd();

 //if(!isInside(barycentric))
 //{
 // glBegin(GL_LINE_LOOP);
 // for(int i = 0; i < 4; i++)
 // {
 //  glVertex3f(Fv[i].x * 1000, Fv[i].y * 800, 0);
 // }
 // glEnd();
 // glColor4f(1, 0, 0, 1);
 // for(int i = 0; i < 4; i++)
 // {
 //  glBegin(GL_LINES);
 //  glVertex3f(query.x* 1000, query.y * 800, 0);
 //  glVertex3f(Fv[i].x* 1000, Fv[i].y * 800, 0);
 //  glEnd();
 // }
 //}
 Vector2d toReturn;
 if(flag == 0)
  toReturn = v[0] * barycentric.x + v[3] * barycentric.y + v[1] * barycentric.z;
 else
  toReturn = v[3] * barycentric.x + v[2] * barycentric.y + v[1] * barycentric.z;
 return(toReturn);
}

std::vector<double> CDFSample::readFromFile(const char* filename)
{
 ifstream file(filename);
 if(!file.is_open())
 {
  cerr << "Bad file: " << filename << endl;
  exit(-1);
 }
 std::vector<double> data;
 while(!file.eof())
 {
  double toPush;
  file >> toPush;
  data.push_back(toPush);
 }
 data.pop_back();
 return data;
}

std::vector<Vector4d> CDFSample::readFromFile2(const char* filename)
{
 ifstream file(filename);
 if(!file.is_open())
 {
  cerr << "Bad file: " << filename << endl;
  exit(-1);
 }
 std::vector<Vector4d> data;
 int cnt = 0;
 while(!file.eof())
 {
  Vector4d toPush;
  file >> toPush.x >> toPush.y >> toPush.z >> toPush.w;
  data.push_back(toPush);
  cout << cnt++ << endl;
  cout << toPush.x << " " << toPush.y << " " << toPush.z << " " << toPush.w << endl;
 }
 data.pop_back();
 return data;
}



void CDFSample::write2file(std::vector<Vector2d> data, std::vector<float> vals,
  const char* filename)
{
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i < size; i++)
  myfile << data[i].x << " " << data[i].y << " " << vals[i] << endl;
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

void CDFSample::write2file(std::vector<Vector2d> data, std::vector<double> vals,
  const char* filename)
{
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i < size; i++)
  myfile << data[i].x << " " << data[i].y << " " << vals[i] << endl;
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

void CDFSample::write2file(std::vector<double> data, const char* filename)
{
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i < size; i++)
  myfile << data[i] << endl;
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

void CDFSample::write2file(std::vector<int> index, std::vector<Vector2d> data, std::vector<double> errorRadii, const char *filename)
{
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i < size; i++)
  myfile << index[i] << " " << data[i].x << " " << data[i].y << " " << errorRadii[i] <<  endl;
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

void CDFSample::write2file(std::vector<int> data, const char* filename)
{
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i < size; i++)
  myfile << data[i] << endl;
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

void CDFSample::write2file(std::vector<float> data, const char* filename)
{
 ofstream myfile(filename, fstream::out);
 int size = data.size();
 for(int i = 0; i < size; i++)
  myfile << data[i] << endl;
 myfile.close();
 cout << "finish writing " << size << " entries to " << filename << endl;
}

void CDFSample::write2file(
  std::vector< std::vector<float> > CDFX,
  std::vector< std::vector<float> > CDFY,
  const char* filename)
{
 ofstream myfile(filename, fstream::out);
 for(int row = 0; row < CDFX.size(); row++)
 {
  std::vector<float> cdfx = CDFX[row];
  for(int col = 0; col < CDFY.size(); col++)
  {
   std::vector<float> cdfy = CDFY[col];
   float x = cdfx[col];
   float y = cdfy[row];
   myfile << row << " " << col << " " << x << " " << y << endl;
  }
 }
 myfile.close();
 cout << "Finish writing " << CDFX.size() * CDFY.size() << " entries to " << filename << endl;
 exit(0);
}

Vector3d CDFSample::BaryCentricCoor(Vector3d x, Vector3d p0, Vector3d p1, Vector3d p2)
{
 Vector3d e01 = p1 - p0;
 Vector3d e12 = p2 - p1;
 Vector3d e20 = p0 - p2;
 Vector3d n = e01 % e12;
 n = n / n.norm();
 float A =  0.5 * (e01 % e12) * n;
 float Au = 0.5 * (e12 % (x - p1)) * n;
 float Av = 0.5 * (e20 % (x - p0)) * n;
 float u = Au / A;
 float v = Av / A;
 float w = 1 - u - v;
 //cout << u << " " << v << " " << w << endl;
 return Vector3d(u, v, w);
}

bool CDFSample::checkCell(Vector2d* Fidx,
  const std::vector< std::vector<float> > CDFX,
  const std::vector< std::vector<float> > CDFY,
  const Vector2d query,
  Vector3d& barycentric,
  int& flag)
{
 Vector2d Fv[4];
 for(int i = 0; i < 4; i++)
 {
  int row = Fidx[i].x;
  int col = Fidx[i].y;
  cout << row << " " << col << endl;
  Fv[i].x = CDFX[row][col];
  Fv[i].y = CDFY[col][row];
 }
 Vector3d Ftri[3];
 {
  Ftri[0] = Fv[0]; Ftri[1] = Fv[3]; Ftri[2] = Fv[1];
  barycentric = BaryCentricCoor(Vector3d(query.x, query.y, 0), Ftri[0], Ftri[1], Ftri[2]);
 }
 if(isInside(barycentric))
 {
  flag = 0; //upper
  return true; 
 }
 else
 {
  Ftri[0] = Fv[3]; Ftri[1] = Fv[2]; Ftri[2] = Fv[1];
  barycentric = BaryCentricCoor(Vector3d(query.x, query.y, 0), Ftri[0], Ftri[1], Ftri[2]);
  if(isInside(barycentric))
  {
   flag = 1; //lower
   return true;
  }
 }
 return false;
}


std::vector<float> CDFSample::invmapbyRBF(const std::vector<Vector2d> input,
  const std::vector<Vector2d> newspace,
  const int dim, const float lambda)
{
 std::vector<float> radii;
 int N = input.size();
 float density = N; // CDF space is a 1X1 space 
 float width = 1.0;
 float radius = lambda * (width / sqrt(density)); 

 std::vector<double> values;
 for(int i = 0; i < newspace.size(); i++)
  values.push_back(newspace[i][dim]);

 RBF rbf;
 rbf.compute(input, values, radius);

 for(int i = 0; i < N; i++)
 {
  float f = rbf.eval(input[i]);
  radii.push_back(f);
 }

 return radii;
}

void CDFSample::RBFinterp(const std::vector<Vector2d> input,
  const std::vector<double> val)
{
 //ifstream myfile("subset_idx.txt");
 ifstream myfile("PDSFiles/PDSFile.txt");
 string line;
 std::vector<int> indices;
 if(myfile.is_open())
 {
  while(getline(myfile, line))
  {
   indices.push_back(stoi(line));
  }
  myfile.close();
 }

 std::vector<Vector2d> subset;
 std::vector<double> subsetVal;
 for(int i = 0; i < indices.size(); i++)
 {
  subset.push_back(input[i]);
  subsetVal.push_back(val[i]);
  //glColor4f(1, 0, 0, 1);
  //glPointSize(5);
  //glBegin(GL_POINTS);
  //glVertex3f(input[i].x, input[i].y, 0);
  //glEnd();
 }
 //return;


 RBF rbf;
 rbf.compute(subset, subsetVal, Radii);
 ColorGradient colorgradient;
 std::vector<float> interp;
 ofstream outfile("PDSFiles/PDS-sub-interp.txt");
 for(int i = 0; i < 1000; i++)
 {
  for(int j = 0; j < 1000; j++)
  {
   float f = rbf.eval(Vector2d(j, i), Radii);
   float red, green, blue;
   colorgradient.getColorAtValue(f, red, green, blue);
   glColor4f(red, green, blue, 1);
   glBegin(GL_POINTS);
   glVertex3f(j, i, 0);
   glEnd();
   interp.push_back(f);
   outfile << j << " " << i << " " << f << endl;
  }
 }
 outfile.close();
 cout << *std::max_element(interp.begin(), interp.end());
 cout << "write PDS interpolation results to file." << endl;
 exit(0);
}

Vector2d CDFSample::invmap(Vector2d query,
  const std::vector<Vector2d> CDFgrid,
  const std::vector<Vector2d> vertices)
{
 Vector2d toReturn(0, 0);
 for(int i = 0; i < tnodes.size(); i++)
 {
  Vector2d* v = new Vector2d[3];
  int* idx = new int[3];
  idx[0] = tnodes[i].v[0];
  idx[1] = tnodes[i].v[1];
  idx[2] = tnodes[i].v[2];
  Vector2d* p = new Vector2d[3];
  p[0] = CDFgrid[idx[0]];
  p[1] = CDFgrid[idx[1]];
  p[2] = CDFgrid[idx[2]];
  Vector3d barycentric = BaryCentricCoor(query, p[0], p[1], p[2]);
  Vector2d* P = new Vector2d[3];
  P[0] = vertices[idx[0]];
  P[1] = vertices[idx[1]];
  P[2] = vertices[idx[2]];
  //glLineWidth(0.5);
  //glColor4f(0, 1, 0, 0.5);
  //glBegin(GL_LINE_LOOP);
  //glVertex3f((p[0][0] + 0.5) * 800, (p[0][1] + 0.5) * 800, 0);
  //glVertex3f((p[1][0] + 0.5) * 800, (p[1][1] + 0.5) * 800, 0);
  //glVertex3f((p[2][0] + 0.5) * 800, (p[2][1] + 0.5) * 800, 0);
  //glEnd();
  if(isInside(barycentric))
  {
   toReturn = P[0] * barycentric[0] + P[1] * barycentric[1] + P[2] * barycentric[2];
   return toReturn;
  }
 }
 //glColor4f(1, 0, 0, 1);
 //glPointSize(4);
 //glBegin(GL_POINTS);
 //glVertex3f((query.x + 0.5) * 800, (query.y + 0.5) * 800, 0);
 //glEnd();
 cerr << "should not get here" << endl;
 exit(0);
 return toReturn;
}

Vector2d CDFSample::invmap(Vector2d query,
  const std::vector<Vector2d> CDFgrid,
  const std::vector<Vector2d> vertices,
  int& flag)
{
 Vector2d toReturn(0, 0);
 flag = 0;
 bool found = false;
 int npro = omp_get_max_threads();
 omp_set_num_threads(npro);
#pragma omp parallel for
 for(int i = 0; i < tnodes.size(); i++)
 {
  Vector2d* v = new Vector2d[3];
  int* idx = new int[3];
  idx[0] = tnodes[i].v[0];
  idx[1] = tnodes[i].v[1];
  idx[2] = tnodes[i].v[2];
  Vector2d* p = new Vector2d[3];
  p[0] = CDFgrid[idx[0]];
  p[1] = CDFgrid[idx[1]];
  p[2] = CDFgrid[idx[2]];
  Vector3d barycentric = BaryCentricCoor(query, p[0], p[1], p[2]);
  Vector2d* P = new Vector2d[3];
  P[0] = vertices[idx[0]];
  P[1] = vertices[idx[1]];
  P[2] = vertices[idx[2]];
  //glLineWidth(0.5);
  //glColor4f(0, 1, 0, 0.5);
  //glBegin(GL_LINE_LOOP);
  //glVertex3f((p[0][0] + 0.5) * 800, (p[0][1] + 0.5) * 800, 0);
  //glVertex3f((p[1][0] + 0.5) * 800, (p[1][1] + 0.5) * 800, 0);
  //glVertex3f((p[2][0] + 0.5) * 800, (p[2][1] + 0.5) * 800, 0);
  //glEnd();
  if(isInside(barycentric))
  {
   toReturn = P[0] * barycentric[0] + P[1] * barycentric[1] + P[2] * barycentric[2];
   found = true;
   //break;
   //return toReturn;
  }
 }
 if(found){
  return toReturn;
 } else{
  flag = -1;
  return toReturn;
 }
 //glColor4f(1, 0, 0, 1);
 //glPointSize(4);
 //glBegin(GL_POINTS);
 //glVertex3f((query.x + 0.5) * 800, (query.y + 0.5) * 800, 0);
 //glEnd();
 flag = -1;
 return toReturn;
}



std::vector<Vector2d> CDFSample::duplicateSamples(const std::vector<Vector2d> input)
{
 std::vector<Vector2d> toReturn(input);
 // find left and right bounding axes
 float leftright[2], topbottom[2];
 std::vector<float> tmp;
 for(int i = 0; i < input.size(); i++)
  tmp.push_back(input[i].x);
 leftright[0] = *std::min_element(tmp.begin(), tmp.end());
 leftright[1] = *std::max_element(tmp.begin(), tmp.end());
 // find top and bottom
 tmp.clear();
 for(int i = 0; i < input.size(); i++)
  tmp.push_back(input[i].y);
 topbottom[0] = *std::min_element(tmp.begin(), tmp.end());
 topbottom[1] = *std::max_element(tmp.begin(), tmp.end());

 // duplicate sample on x direction 
 for(int j = 0; j < 2; j++)
  for(int i = 0; i < input.size(); i++)
  {
   float dis = input[i].x - leftright[j];
   Vector2d newsample(leftright[j] - dis, input[i].y);
   toReturn.push_back(newsample);
  }

 // duplicate sample on y direction
 for(int j = 0; j < 2; j++)
  for(int i = 0; i < input.size(); i++)
  {
   float dis = input[i].y - topbottom[j];
   Vector2d newsample(input[i].x, topbottom[j] - dis);
   toReturn.push_back(newsample);
  }

 return toReturn;
}

void CDFSample::fillBoundaries(
  const std::vector<Vector2d> cdfgrid,
  const int nrows,
  const int ncols,
  const std::vector< std::vector<float> > CDFX,
  const std::vector< std::vector<float> > CDFY,
  const std::vector<Vector2d> input,
  std::vector<Vector2d>& extrasamples,
  const int frameid)
{
 char* buff = new char[200];
 string str = OutputPrefix + "InternalData/PathSamplingData/NHC-extra-samples-%d.txt";
 const char* cstr = str.c_str();
 sprintf(buff, cstr, frameid);
 //sprintf(buff, "NHC-extra-samples-%d.txt", frameid);
 std::vector<Vector2d> generatedsamples;
#ifdef HAVE_EXTR_POINTS
 generatedsamples = readTestSamples(buff); 
#else
 std::vector<Vector2d> segments = extractEdges(cdfgrid, nrows, ncols, CDFX, CDFY, input, frameid);
 //exit(0);
 //std::vector<Vector2d> segments = readTestSamples("concavehull.txt");
 for(int i = 0; i < segments.size() - 1; i++)
 {
  Vector2d p0 = segments[i];
  Vector2d p1 = segments[i+1];
  //glColor4f(0, 0, 1, 1);
  //glBegin(GL_LINES);
  //glVertex3f((p0.x+0.7)*500, (p0.y+0.7)*400, 0);
  //glVertex3f((p1.x+0.7)*500, (p1.y+0.7)*400, 0);
  //glEnd();
 }
 std::vector<Vector2d> newsegments;
 std::vector<Vector2d> directions;
 std::vector<Vector2d> vertices;
 std::vector<Vector2d> newvertices;
 newvertices.clear();
 segments.push_back(segments[0]);
 for(int i = 0; i < segments.size() - 1; i++)
 {
  Vector2d p0 = segments[i];
  Vector2d p1 = segments[i + 1];
  Vector2d v = Vector2d(p1[0], p1[1]) - Vector2d(p0[0], p0[1]);
  Vector2d dir = rotate(270 * PI / 180.0, v); 
  directions.push_back(dir);
  float rmax = PoissonDiskSample<std::vector<Vector2d>, float>::Rmax(1.0, 100); 
  dir = dir * rmax;
  Vector2d toPush = p0 + dir;
  glColor4f(0, 0, 1, 1);
  //glBegin(GL_LINES);
  //glVertex3f((    p0.x + 0.7)* 500, (    p0.y + 0.7) * 400, 0);
  //glVertex3f((toPush.x + 0.7)* 500, (toPush.y + 0.7) * 400, 0);
  //glEnd();
  newsegments.push_back(toPush);
  //toPush = p1 + dir;
  //newsegments.push_back(toPush);
 }
 newsegments.push_back(newsegments[0]);
 // generate random points
 float lambda = 1.0;
 float density = input.size() / 1.0 * lambda;  //reference density, 1.0 is the area of the space 
 for(int i = 0; i < segments.size() - 1; i++)
 {
  //construct two triangles
  Vector2d e0 = newsegments[i]    - segments[i];
  Vector2d e1 = newsegments[i+1]  - segments[i];
  Vector3d tmp = e0 % e1;
  float area = 0.5 * tmp.norm();
  glColor4f(1, 0, 0, 1);
  //glBegin(GL_LINE_LOOP);
  //glVertex3f((newsegments[i].x   +0.7)*500, (newsegments[i].y   +0.7)*400, 0.0);
  //glVertex3f((   segments[i].x   +0.7)*500, (   segments[i].y   +0.7)*400, 0.0);
  //glVertex3f((   segments[i+1].x +0.7)*500, (   segments[i+1].y +0.7)*400, 0.0);
  //glEnd();

  e0 = newsegments[i]   - segments[i+1];
  e1 = newsegments[i+1] - segments[i+1];
  tmp = e0 % e1;
  area += 0.5 * tmp.norm();

  //glColor4f(0, 1, 0, 1);
  //glBegin(GL_LINES);
  //glVertex3f(     (segments[i].x + 0.7) * 500,      (segments[i].y + 0.7) * 400, 0);
  //glVertex3f(   (segments[i+1].x + 0.7) * 500,    (segments[i+1].y + 0.7) * 400, 0);
  //glVertex3f(  (newsegments[i].x + 0.7) * 500,   (newsegments[i].y + 0.7) * 400, 0);
  //glVertex3f((newsegments[i+1].x + 0.7) * 500, (newsegments[i+1].y + 0.7) * 400, 0);
  //glEnd();

  int targetsamples = std::ceil(density * area);
  //cout << area << endl;
  //cout << targetsamples << endl;
  //cout << area << " --- " << targetsamples << endl;
  int j = 0;
  while(j < targetsamples)
  {
   j++;
   //cout << "---" << endl;
   //cout << segments[i] << " "<< segments[i+1] << 
   // newsegments[i] << " "<< newsegments[i+1] << endl;
   Vector2d newsample = generateRandomPoint(
     segments[i],    segments[i+1], 
     newsegments[i], newsegments[i+1]);
   generatedsamples.push_back(newsample);
  }
 }
 write2file(generatedsamples, buff);
#endif

 for(int i = 0; i < input.size(); i++)
  extrasamples.push_back(input[i]);
 for(int i = 0; i < generatedsamples.size(); i++)
  extrasamples.push_back(generatedsamples[i]);
}

void CDFSample::readNewSpace(const char* filename,
  std::vector< std::vector<float> >& CDFX,
  std::vector< std::vector<float> >& CDFY,
  const int nrows, const int ncols)
{
 ifstream file(filename);
 if(!file.is_open())
 {
  cerr << "Bad file: " << filename << endl;
  exit(-1);
 }
 std::vector<Vector2d> data;
 while(!file.eof())
 {
  Vector2d toPush;
  file >> toPush.x >> toPush.y;
  data.push_back(toPush);
 }
 data.pop_back();

 CDFX.clear();
 CDFY.clear();
 for(int i = 0; i < nrows; i++)
 {
  std::vector<float> cdfx;
  cdfx.clear();
  for(int j = 0; j < ncols; j++)
  {
   int idx = i * ncols + j;
   float x = data[idx].x;
   cdfx.push_back(x);
  }
  CDFX.push_back(cdfx);
 }

 for(int j = 0; j < ncols; j++)
 {
  std::vector<float> cdfy;
  cdfy.clear();
  for(int i = 0; i < nrows; i++)
  {
   int idx = i * ncols + j;
   float y = data[idx].y;
   cdfy.push_back(y);
  }
  CDFY.push_back(cdfy);
 }
}

void CDFSample::removeOutlier(std::vector<Vector2d>& samples, std::vector<Vector2d>& input,
  const int nrows, const int ncols,
  const std::vector<Vector2d> vertices,
  std::vector<double>& sdvals)
{
 // read in warped spaces
 std::vector< std::vector<Vector2d> > warpedspaces;
 for(int i = 0; i < 6; i++)
 {
  char buff[100];
  //sprintf(buff, "../springmesh/OURData/NewSpace-%d.txt", i+1);
  sprintf(buff, "../springmesh/NHCData/NewSpace-%d.txt", i+1);
  cout << "read warpped space for filtering: " << buff << endl;
  std::vector< std::vector<float> > U;
  std::vector< std::vector<float> > V;
  U.clear();
  V.clear();
  readNewSpace(buff, U, V, nrows, ncols);
  std::vector<Vector2d> space;
  for(int j = 0; j < nrows; j++)
  {
   for(int k = 0; k < ncols; k++)
   {
    float x = U[j][k];
    float y = V[k][j];
    space.push_back(Vector2d(x, y));
   }
  }
  warpedspaces.push_back(space);
 }

 std::vector<Vector2d> nsamples;
 std::vector<Vector2d> ninput;
 std::vector<double> nsdvals;

 for(int i = 0; i < samples.size(); i++)
 {
  Vector2d v = samples[i];
  int counter = 0;
  for(int j = 0; j < 6; j++)
  {
   std::vector<Vector2d> space = warpedspaces[j];
   int flag = 0;
   invmap(v, space, vertices, flag);
   counter += flag;
  }
  if(counter == 0)
  {
   nsamples.push_back(v);
   ninput.push_back(input[i]);
   nsdvals.push_back(sdvals[i]);
  }
 }

 samples.clear();
 samples = nsamples;
 input.clear(); 
 input = ninput;
 sdvals.clear();
 sdvals = nsdvals;
 cout << "filter input samples..." << endl;
}

void CDFSample::rebuildSDField(const std::vector<Vector2d> selectedSamples,
  RBF densityRBF,
  RBF SDRBF,
  const int timeframe,
  const float dw,
  const float factor,
  const std::vector<double> rbfradii)
{
 std::vector<double> selectedSDVal;
 std::vector<float> densities;
 std::vector<double> rbfradii2;
 rbfradii2.clear();
 float localmin = 10e6;
 ColorGradient colorgradient;
 for(int i = 0; i < selectedSamples.size(); i++)
 {
  float f = densityRBF.eval(selectedSamples[i]);
  if(f > 0 && f < localmin)
   localmin = f;
  densities.push_back(f);
  //glPointSize(3.0);
  //glBegin(GL_POINTS);
  //glVertex3f(selectedSamples[i].x, selectedSamples[i].y, 0.0);
  //glEnd();
 }
 for(int i = 0; i < selectedSamples.size(); i++)
 {
  float f = densities[i];
  if(f == 0)
   f = localmin;
  float r = dw / sqrt(f) * factor;
  rbfradii2.push_back(r);
 }
 for(int i = 0; i < selectedSamples.size(); i++)
 {
  float f = SDRBF.eval(selectedSamples[i], rbfradii);
  selectedSDVal.push_back(f);
  float red, green, blue;
  colorgradient.getColorAtValue(f, red, green, blue);
  glColor4f(red, green, blue, 1);
  glPointSize(5.0);
  glBegin(GL_POINTS);
  glVertex3f(selectedSamples[i].x, selectedSamples[i].y, 0.0);
  glEnd();
 }
 string str = OutputPrefix + "InternalData/subSD-%d.txt";
 const char* cstr = str.c_str();
 char* buff = new char[200];
 sprintf(buff, cstr, timeframe);
 write2file(selectedSDVal, buff);
 exit(0);
 cout << *std::max_element(selectedSDVal.begin(), selectedSDVal.end()) << endl;
 std::vector<double> tmp = selectedSDVal;
 std::sort(tmp.begin(), tmp.end());
 std::reverse(tmp.begin(), tmp.end());
 float b0, b1, b2;
 b0 = tmp[tmp.size() * 0.33];
 b1 = tmp[tmp.size() * 0.66];
 b2 = tmp[tmp.size() * 0.99];
 RBF rbf;
 rbf.compute(selectedSamples, selectedSDVal, rbfradii2);
 std::vector<Vector2d> locs;
 std::vector<float> colors;
 std::vector<Vector2d> pixels;
 for(int i = 0; i < 1024; i++)
 {
  for(int j = 0; j < 868; j++)
  {
   Vector2d c(i, j);
   float f = rbf.eval(c, rbfradii2);
   pixels.push_back(c);
   //float red, green, blue;
   //colorgradient.getColorAtValue(f, red, green, blue);
   //glColor4f(red, green, blue, 1);
   //glBegin(GL_POINTS);
   //glVertex3f(i, j, 0.0);
   //glEnd();
   c = translateToGeoCoor(c);
   locs.push_back(c);
   colors.push_back(f);
  }
 }
 char* buf = new char[100];
 sprintf(buf, "NewData/SD/OLS-NHC-rebuild-SD-%d.txt", timeframe);
 //sprintf(buf, "NewData/SD/PDS-NHC-rebuild-SD-%d.txt", timeframe);
 //sprintf(buf, "NHCData/SD/OLS-NHC-rebuild-SD-%d.txt", timeframe);
 //sprintf(buf, "NHCData/SD/PDS-NHC-rebuild-SD-%d.txt", timeframe);
 write2file(locs, colors, buf);
 //sprintf(buf, "NHCData/SD/OLS-NHC-rebuild-SD2-%d.txt", timeframe);
 sprintf(buf, "NewData/SD/OLS-NHC-rebuild-SD2-%d.txt", timeframe);
 //sprintf(buf, "NewData/SD/PDS-NHC-rebuild-SD2-%d.txt", timeframe);
 write2file(pixels, colors, buf);
 cout << "rebuilding SD field " << timeframe << endl;
 exit(0);
}

void CDFSample::rebuildIntensityField(const std::vector<Vector2d> selectedSamples,
  RBF densityRBF,
  std::vector<double> hurInt,
  const int timeframe,
  const float dw,
  const float factor)
{
 std::vector<float> densities;
 std::vector<double> rbfradii;
 rbfradii.clear();
 ColorGradient colorgradient;
 float red, green, blue;
 float localmax = *std::max_element(hurInt.begin(), hurInt.end());
 float localmin = *std::min_element(hurInt.begin(), hurInt.end());
 Icon icon(0, 0, 65, 65);
 for(int i = 0; i < selectedSamples.size(); i++)
 {
  float f = densityRBF.eval(selectedSamples[i]);
  densities.push_back(f);
  int cat = determineCat(hurInt[i]);
  icon.setCenter(selectedSamples[i].x, selectedSamples[i].y);
  //icon.draw(cat);

  float frac = (hurInt[i] - localmin) / (localmax - localmin);
  colorgradient.getColorAtValue(frac, red, green, blue);
  glColor4f(red, green, blue, 1);
  //drawSolidCircle(selectedSamples[i].x, selectedSamples[i].y, 10);
 }
 //return;

 for(int i = 0; i < selectedSamples.size(); i++)
 {
  float f = densities[i];
  float r = dw / sqrt(f) * factor;
  rbfradii.push_back(r);
  cout << hurInt[i] << endl;
 }
 RBF rbf;
 rbf.compute(selectedSamples, hurInt, rbfradii);
 char* buff = new char[100];
 sprintf(buff, "NewData/demo-selected-%d.txt", timeframe);
 std::vector<Vector2d> PDSsub = readTestSamples(buff);
 for(int i = 0; i < PDSsub.size(); i++)
 {
  Vector2d c = translateToScreen(PDSsub[i].x, PDSsub[i].y);
  float f = rbf.eval(c, rbfradii);
  int cat = determineCat(f);
  float frac = (f - localmin) / (localmax - localmin);
  colorgradient.getColorAtValue(frac, red, green, blue);
  glColor4f(red, green, blue, 1);
  //drawSolidCircle(c.x, c.y, 10);
  icon.setCenter(c.x, c.y);
  icon.draw(cat, 1);
 }
 return;
 std::vector<Vector2d> locs;
 std::vector<float> colors;
 std::vector<Vector2d> pixels;
 for(int i = 0; i < 1024; i++)
 {
  for(int j = 0; j < 868; j++)
  {
   Vector2d c(i, j);
   float f = rbf.eval(c, rbfradii);
   pixels.push_back(c);
   //float red, green, blue;
   float frac = (f - localmin) / (localmax - localmin);
   colorgradient.getColorAtValue(frac, red, green, blue);
   glColor4f(red, green, blue, 1);
   glBegin(GL_POINTS);
   glVertex3f(i, j, 0.0);
   glEnd();
   c = translateToGeoCoor(c);
   locs.push_back(c);
   colors.push_back(f);
  }
 }
 std::cout << *std::max_element(colors.begin(), colors.end()) << " " << *std::min_element(colors.begin(), colors.end()) << endl;
 return;
 char* buf = new char[100];
 sprintf(buf, "NewData/IntField/OLS-NHC-rebuild-IntField-%d.txt", timeframe);
 write2file(locs, colors, buf);
 sprintf(buf, "NewData/IntField/OLS-NHC-rebuild-IntField2-%d.txt", timeframe);
 write2file(pixels, colors, buf);
 cout << "rebuilding Intensity field " << timeframe << endl;
 exit(0);
}



std::vector<Vector2d> CDFSample::readCategories(const char* idxfile, const char* catfile,
  std::vector<Vector2d>& subset)
{
 ifstream file(catfile);
 if(!file.is_open())
 {
  cerr << "Bad file: " << catfile << endl;
  exit(-1);
 }
 std::vector<Vector2d> cats;
 while(!file.eof())
 {
  int cat;
  float size;
  file >> cat >> size;
  if(cat == 0)
   exit(0);
  cats.push_back(Vector2d(cat, size));
 }
 cats.pop_back();



 ifstream file2(idxfile);
 if(!file2.is_open())
 {
  cerr << "Bad file: " << idxfile << endl;
  exit(-1);
 }
 std::vector<int> idx;
 subset.clear();
 while(!file2.eof())
 {
  int toPush;
  float x, y;
  file2 >> toPush >> x >> y;
  idx.push_back(toPush - 1);
  Vector2d sub(x, y);
  subset.push_back(sub);
 }
 idx.pop_back();
 subset.pop_back();
 string str = OutputPrefix + "InternalData/sub-idx-4.txt";
 const char* cstr = str.c_str();
 write2file(idx, cstr);

 std::vector<Vector2d> data;
 for(int i = 0; i < idx.size(); i++)
 {
  int j = idx[i];
  Vector2d topush = cats[j];
  data.push_back(topush);
 }

 return data;
}

float CDFSample::Stormsize2Display(Vector2d lonlat, float nmi)
{
    //convert nmi to km
    //http://www.metric-conversions.org/length/nautical-miles-to-kilometers.htm
    float km = nmi / 0.53996;
    Vector2d newlatlon = locateDestination_2(lonlat.x, lonlat.y, km, 0);
    Vector2d v0 = translateToScreen(lonlat.x, lonlat.y);
    Vector2d v1 = translateToScreen(newlatlon.x, newlatlon.y);
    float d = (v1 - v0).norm();
    return d;
}

void CDFSample::drawInputwithIcon(const std::vector<Vector2d> input,
                                  const int frameid)
{
    char* buff = new char[100];
    sprintf(buff, "NHCData/NHC-cats-%d.txt", frameid);
    ifstream file(buff);
    if(!file.is_open())
    {
        cerr << "Bad file: " << buff << endl;
        exit(-1);
    }
    std::vector<Vector2d> cats;
    while(!file.eof())
    {
        int cat;
        float size;
        file >> cat >> size;
        cats.push_back(Vector2d(cat, size));
    }
    cats.pop_back();

 Icon icon(0, 0, 65, 65);
 for(int i = 0; i < input.size(); i++)
 {
  Vector2d c = input[i]; 
  icon.setCenter(c.x, c.y);
  icon.draw(cats[i].x, 1);
 }
 return;
}

void CDFSample::drawSolidCircle(float _x, float _y, float radius){
 int angle;
 float x, y, angle_radians;
 float x1, y1;
 glBegin(GL_POLYGON);
 for(angle = 0; angle <= 360; angle += 1){
  angle_radians = angle*(double)M_PI/180.0;
  x = _x + radius*(double)cos(angle_radians);
  y = _y + radius*(double)sin(angle_radians);
  if(angle == 0){
   x1 = x; 
   y1 = y;
  }
  glVertex3f(x,y,0.0); 
  glVertex3f(x1,y1,0.0); 
  x1 = x; 
  y1 = y;
 }
 glEnd();
}

void CDFSample::readFromFile(const char* filename, std::vector<Vector2d>& v0, std::vector<int>& v1)
{
 ifstream file(filename);
 if(!file.is_open())
 {
  cerr << "Bad file: " << filename << endl;
  exit(-1);
 }
 v0.clear(); v1.clear();
 while(!file.eof())
 {
  Vector2d v;
  int f;
  file >> f >> v.x >> v.y;
  v0.push_back(v);
  v1.push_back(f);
 }
 v0.pop_back();
 v1.pop_back();
}

void CDFSample::dynamicDraw(const int timeframe,
  std::vector<PrePoint>& subpoints,
  std::vector<PrePoint>& fullpoints,
  std::vector<std::vector<PrePoint>>& pointsband,
  std::vector<std::vector<PrePoint>>& pointsbandbackup,
  std::vector<double> bandsVals,
  float& UDDmin, std::vector<float>& ODDmin)
{
 std::vector<Vector2d> fullOD;
 std::vector<Vector2d> fullUD;
 std::vector<Vector2d> subOD;
 std::vector<Vector2d> subUD;
 std::vector<double> fullSD;
 std::vector<double> subSD;
 std::vector<double> subIdx;
 std::vector<std::vector<int>> bandsIdx;
 bandsIdx.resize(3);
 std::vector<double> cats;
 std::vector<double> RAvg;
 std::vector<Vector4d> Radii;
 //float fadeConstant = 0.00125 * 0.03;
 //float initOpc = 0.018;
 float fadeConstant =  0.01;
 float initOpc = 1;

 char* buff = new char[200];
 string str = OutputPrefix + "InternalData/OD-%d.txt";
 const char* cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 fullOD = readTestSamples(buff);
 str = OutputPrefix + "InternalData/UD-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 fullUD = readTestSamples(buff);
 str = OutputPrefix + "InternalData/fullSD-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 fullSD = readFromFile(buff);
 str = OutputPrefix + "InternalData/OD-selected-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 subOD = readTestSamples(buff);
 str = OutputPrefix + "InternalData/UD-selected-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 subUD = readTestSamples(buff);
 str = OutputPrefix + "InternalData/sub-idx-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 subIdx = readFromFile(buff);
 //readFromFile(buff, subUD, subIdx);
 str = OutputPrefix + "InternalData/subSD-%d.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 subSD = readFromFile(buff);
 str = InputPrefix + DatafileName + "_Cate.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 cats = readFromFile(buff);
 str = InputPrefix + DatafileName + "_50R4.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 RAvg = readFromFile(buff);
 str = InputPrefix + DatafileName + "_50R4_All.txt";
 cstr = str.c_str();
 sprintf(buff, cstr, timeframe);
 Radii = readFromFile2(buff);
 //
 //cout << fullOD.size() << " " << fullUD.size() << " " << fullSD.size() << endl;
 //cout << subOD.size() << " " << subUD.size() << " " << subSD.size() << " " << subIdx.size() << endl;

 if(fullpoints.size() == 0)
 {
  pointsband.resize(3);
  pointsbandbackup.resize(3);
  for(int i = 0; i < 3; i++)
  {
   pointsband[i].clear();
   pointsbandbackup[i].clear();
  }
  for(int i = 0; i < fullOD.size(); i++)
  {
   PrePoint toPush(fullOD[i], initOpc, 1);
   toPush.UDPos() = fullUD[i];
   toPush.SD() = fullSD[i];
   toPush.Idx() = i;
   fullpoints.push_back(toPush);
   if(fullSD[i] >= bandsVals[0])
    pointsband[0].push_back(toPush);
   else if(fullSD[i] >= bandsVals[1] && fullSD[i] < bandsVals[0])
    pointsband[1].push_back(toPush);
   else if(fullSD[i] < bandsVals[1])
    pointsband[2].push_back(toPush);
  }
 }

 if(subpoints.size() == 0)
 {
  UDDmin = 10e6;
  ODDmin.resize(3, 10e6);
  std::vector< std::vector<PrePoint> > subpointsband;
  subpointsband.resize(3);
  srand(std::time(0));
  std::vector<float> lifetime;
  for(int i = 0; i < subOD.size(); i++) lifetime.push_back(i);
  std::random_shuffle(lifetime.begin(), lifetime.end());
  for(int i = 0; i < subOD.size(); i++)
  {
   float delta = 1.0 / subOD.size();
   lifetime[i] = lifetime[i] * delta;
  }
  for(int i = 0; i < subOD.size(); i++)
  {
   //float f = gauss(0.5, 0.2, 50);
   //if(f < 0) f = 0;
   //else if(f > 1) f = 1;
   float f = (rand() % subOD.size() + 1) * (1.0 / (subOD.size() + 1));
   PrePoint toPush(subOD[i], lifetime[i] * initOpc, 1);
   toPush.UDPos() = subUD[i];
   toPush.Idx() = subIdx[i];
   toPush.SD() = subSD[i];
   subpoints.push_back(toPush);
   if(subSD[i] >= bandsVals[0])
    subpointsband[0].push_back(toPush);
   else if(subSD[i] >= bandsVals[1] && subSD[i] < bandsVals[0])
    subpointsband[1].push_back(toPush);
   else if(subSD[i] < bandsVals[1])
    subpointsband[2].push_back(toPush);

   //ColorGradient colorgradient;
   //float red, green, blue;
   //colorgradient.getColorAtValue(subSD[i], red, green, blue);
   //glColor4f(red, green, blue, subSD[i]);
   //Vector2d v = translateToScreen(subOD[i].x, subOD[i].y);
   //drawSolidCircle(v.x, v.y, 10);
  }

  for(int i = 0; i < subpointsband.size(); i++)
  {
   for(int j = 0; j < subpointsband[i].size() - 1; j++)
   {
    for(int k = j+1; k < subpointsband[i].size(); k++)
    {
     Vector2d v0 = subpointsband[i][j].Loc();
     Vector2d v1 = subpointsband[i][k].Loc();
     float dis = (v0 - v1).norm();
     ODDmin[i] = std::min(UDDmin, dis);
    }
   }
  }
  //ODDmin[2] = ODDmin[1];

  for(int i = 0; i < subpoints.size() - 1; i++)
  {
   for(int j = i+1; j < subpoints.size(); j++)
   {
    Vector2d v0 = subpoints[i].UDPos();
    Vector2d v1 = subpoints[j].UDPos();
    float dis = (v0 - v1).norm();
    UDDmin = std::min(UDDmin, dis);
   }
  }
 } 

 Icon icon(0, 0, 30, 30);
 for(int i = 0; i < subpoints.size(); i++)
 {
  Vector2d v = subpoints[i].Loc();
  int idx = subpoints[i].Idx();
  int cat = cats[idx];
  int radavg = RAvg[idx];
  float rad = Stormsize2Display(v, radavg);
  Vector4d radii = Radii[idx];
  std::vector<Vector2d> corners = findFourCorners(radii, v);
  for(int j = 0; j < corners.size(); j++) corners[j] = translateToScreen(corners[j].x, corners[j].y);
  v = translateToScreen(v.x, v.y);
  icon.setCenter(v.x, v.y);
  icon.draw(cat, subpoints[i].Opc());
  if(cat < 1 || cat > 7)
  {
   cout << cats.size() << " " << idx << endl;
   cout << cat << endl;
   exit(0);
  }
  //glColor4f(1, 0, 0, subpoints[i].Opc());
  //drawSolidCircle(v.x, v.y, rad);
  //drawSolidPolygon(corners);
  subpoints[i].Opc() = subpoints[i].Opc() - fadeConstant;
 }

 //cout << pointsband[0].size() << " " << pointsband[1].size() << " " << pointsband[2].size() << endl;
 PrePoint toAdd;
 std::vector<PrePoint>::iterator it;

 //random shuffle pointsband's indices
 std::srand(std::time(0));
 std::vector< std::vector<int> > nums;
 nums.resize(pointsband.size());
 for(int i = 0; i < pointsband.size(); i++)
 {
  int size = pointsband[i].size();
  for(int j = 0; j < size; j++)
   nums[i].push_back(j);
  std::random_shuffle(nums[i].begin(), nums[i].end());
 }

 for(it = subpoints.begin(); it != subpoints.end();)
 {
  if((*it).Opc() <= 0.0)
  {
   //remove
   float f = (*it).SD();
   Vector2d removed = (*it).Loc();
   it = subpoints.erase(it);

   //find a new point
   int interval(0);
   if(f >= bandsVals[0]) interval = 0;
   else if(f >= bandsVals[1] && f < bandsVals[0]) interval = 1;
   else if(f < bandsVals[1]) interval = 2;
   int size = pointsband[interval].size();
   bool keeplooking = true;
   int idx = -1;
   int counter = -1;
   float constant = 0.2;

   while(keeplooking)
   {
    if(counter >= size - 1)
    {
     counter = -1;
     constant *= 0.95;
     //cout << "counter :" << counter << endl;
    }
    counter ++;
    //std::srand(std::time(0));
    counter = rand() % size;
    idx = nums[interval][counter];
    //cout << nums[interval].size() << " " << counter << " " << idx << endl;
    toAdd = pointsband[interval][idx];
    bool state0 = true;
    for(int i = 0; i < subpoints.size(); i++)
    {
     Vector2d v0 = toAdd.UDPos();
     Vector2d v1 = subpoints[i].UDPos();
     float dis = (v0 - v1).norm();
     if(dis < constant * UDDmin)
      state0 = false;
    }

    bool state1 = true;
    for(int i = 0; i < subpoints.size(); i++)
    {
     Vector2d v0 = toAdd.Loc();
     Vector2d v1 = subpoints[i].Loc();
     float dis = (v0 - v1).norm();
     if(dis < ODDmin[0] * constant)
      state1 = false;
    }

    bool state2 = true;
    Vector2d v0 = toAdd.Loc();
    Vector2d v1 = removed;
    float dis = (v0 - v1).norm();
    if(dis < 10e-3)
     state2 = false;
    keeplooking = !(state0 && state1 && state2);
   }
   subpoints.push_back(toAdd);
   //pointsband[interval].erase(pointsband[interval].begin() + idx);
   //pointsbandbackup[interval].push_back(toAdd);
   //if(pointsband[interval].size() == 0)
   //{
   // pointsband[interval] = pointsbandbackup[interval];
   // pointsbandbackup.clear();
   //}
  }
  else
   it ++;
 }
 //if(subpoints.size() < 42)
 // subpoints.push_back(toAdd);
 //for(int i = 0; i < pointsband[2].size(); i++)
 //{
 // Vector2d v = pointsband[2][i].Loc();
 // v = translateToScreen(v.x, v.y);
 // icon.setCenter(v.x, v.y);
 // icon.draw(1, 1);
 //}
}

Vector2d CDFSample::forwardmap(const Vector2d& query, 
  const std::vector<Vector2d>& UDGrid, const std::vector<Vector2d>& ODGrid,
  const int nrows, const int ncols,
  const std::vector<std::vector<float>>& U, const std::vector<std::vector<float>>& V){
 float width = ODGrid.back().x - ODGrid.front().x; 
 float height = ODGrid.back().y - ODGrid.front().y;
 float minX = ODGrid.front().x;
 float minY = ODGrid.front().y;
 float dw = width  / float(nrows-1);
 float dh = height / float(ncols-1);
 int row = int((query.y - minY) / dh);
 int col = int((query.x - minX) / dw);
 int* vid = new int[4];
 /* v2 v3
  * v0 v1 */
 vid[0] = ncols * row + col;
 vid[1] = ncols * row + (col + 1);
 vid[2] = ncols * (row + 1) + col;
 vid[3] = ncols * (row + 1) + (col + 1);
 Vector2d* v = new Vector2d[4];
 for(int j = 0; j < 4; j++)
  v[j] = ODGrid[vid[j]];
 float* fracs = new float[2];
 fracs[0] = (query.x - v[0].x) / (v[1].x - v[0].x);
 fracs[1] = (query.y - v[0].y) / (v[2].y - v[0].y);

 // eval along x axis
 std::vector<float>* cdfs = new std::vector<float>[2];
 cdfs[0] = U[row];
 cdfs[1] = U[row+1];
 float* cdfval = new float[4];
 cdfval[0] = cdfs[0][col];
 cdfval[1] = cdfs[0][col+1];
 cdfval[2] = cdfs[1][col];
 cdfval[3] = cdfs[1][col+1];
 float val0, val1, valx, valy;
 val0 = cdfval[0] * (1 - fracs[0]) + cdfval[1] * fracs[0];
 val1 = cdfval[2] * (1 - fracs[0]) + cdfval[3] * fracs[0];
 valx = val0 * (1 - fracs[1]) + val0 * fracs[1];

 //eval along y axis
 cdfs[0].clear();
 cdfs[1].clear();
 cdfs[0] = V[col];
 cdfs[1] = V[col+1];
 cdfval[0] = cdfs[0][row];
 cdfval[1] = cdfs[0][row+1];
 cdfval[2] = cdfs[1][row];
 cdfval[3] = cdfs[1][row+1];
 val0 = cdfval[0] * (1 - fracs[1]) + cdfval[1] * fracs[1];
 val1 = cdfval[2] * (1 - fracs[1]) + cdfval[3] * fracs[1];
 valy = val0 * (1 - fracs[0]) + val1 * fracs[0];

 return Vector2d(valx, valy);
}

